package com.wenzhoujie;

import android.os.Bundle;
import android.view.View;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.lingou.www.R;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.fragment.UserShareByUidFragment;
import com.wenzhoujie.fragment.UserShareTopByUidFragment;
import com.wenzhoujie.fragment.UserShareTopByUidFragment.UserShareTopByUidFragmentListener;
import com.wenzhoujie.model.UHome_userModel;
import com.wenzhoujie.model.act.UActModel;
import com.wenzhoujie.utils.IocUtil;

public class UserCenterActivity extends BaseActivity
{

	public static final String EXTRA_UID_ID = "extra_uid";

	private UserShareTopByUidFragment mFragTop = null;
	private UserShareByUidFragment mFragBot = null;

	private String uid;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		setmTitleType(TitleType.TITLE_SIMPLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_user_center);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		getIntentData();
		addFragments();
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
				// TODO Auto-generated method stub

			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("");
	}

	private void getIntentData()
	{
		uid = getIntent().getStringExtra(EXTRA_UID_ID);
	}

	private void addFragments()
	{
		if (!AppHelper.isEmptyString(uid))
		{
			mFragTop = new UserShareTopByUidFragment();
			mFragTop.setUid(uid);
			mFragTop.setmListener(new UserShareTopByUidFragmentListener()
			{
				@Override
				public void onSuccess(UActModel model)
				{
					if (model != null)
					{
						UHome_userModel p = model.getHome_user();
						if (p != null)
						{
							mTitleSimple.setTitleTop(p.getUser_name());
						}
					}

				}
			});
			replaceFragment(mFragTop, R.id.act_user_center_fl_top);

			mFragBot = new UserShareByUidFragment();
			mFragBot.setUid(uid);
			replaceFragment(mFragBot, R.id.act_user_center_fl_bot);
		}
	}

}
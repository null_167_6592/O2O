package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.huewu.pla.lib.MultiColumnListView;
import com.huewu.pla.lib.internal.PLA_AdapterView;
import com.huewu.pla.lib.internal.PLA_AdapterView.OnItemClickListener;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.adapter.MyPhotosAdapter;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.customview.SDMultiColumnListView;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.MyPhotosActivityItemModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.MyPhotosActivityModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

public class MyPhotosAndLovesActivity extends BaseActivity
{
	/** uid */
	public static final String EXTRA_UID = "extra_uid";
	/** act */
	public static final String EXTRA_ACT = "extra_act";
	/** title */
	public static final String EXTRA_TITLE = "extra_title";

	@ViewInject(id = R.id.act_my_photos_smclv_images)
	private SDMultiColumnListView mSmclvImages = null;

	private List<MyPhotosActivityItemModel> mListModel = new ArrayList<MyPhotosActivityItemModel>();
	private String uid = null;
	private String act = null;
	private String strTitle = null;

	private MyPhotosAdapter mAdapter = null;

	/** 当前页数 */
	private int mCurPage = 1;
	/** 总页数 */
	private int mTotalPage = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_my_photos);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initIntentData();
		initTitle();
		bindDefaultData();
		initSDMultiColumnListView();

	}

	private void bindDefaultData()
	{
		mAdapter = new MyPhotosAdapter(mListModel, MyPhotosAndLovesActivity.this);
		mSmclvImages.getRefreshableView().setAdapter(mAdapter);

		mSmclvImages.getRefreshableView().setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(PLA_AdapterView<?> parent, View view, int position, long id)
			{
				ArrayList<String> share2_list = new ArrayList<String>();
				for (int i = 0; i < mListModel.size(); i++)
				{
					share2_list.add(String.valueOf(mListModel.get(i).getShare_id()));
				}
				Intent intent = new Intent();
				intent.putExtra(ShareDetailActivity.EXTRA_SHARE_ID, mListModel.get(Integer.parseInt(String.valueOf(id))).getShare_id());
				intent.putExtra(ShareDetailActivity.EXTRA_PAGE_ID, Integer.parseInt(String.valueOf(id)));
				intent.putStringArrayListExtra(ShareDetailActivity.EXTRA_SHARE_LIST, share2_list);
				intent.setClass(MyPhotosAndLovesActivity.this, ShareDetailActivity.class);
				startActivity(intent);
			}
		});
	}

	private void initSDMultiColumnListView()
	{
		mSmclvImages.setMode(Mode.BOTH);
		mSmclvImages.setOnRefreshListener(new OnRefreshListener2<MultiColumnListView>()
		{

			@Override
			public void onPullDownToRefresh(PullToRefreshBase<MultiColumnListView> refreshView)
			{
				refreshData();
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<MultiColumnListView> refreshView)
			{
				loadMore();
			}

		});
		mSmclvImages.setRefreshing();
	}

	protected void refreshData()
	{
		mCurPage = 1;
		requestMyPhotos(false);
	}

	protected void loadMore()
	{
		if (++mCurPage > mTotalPage && mTotalPage != -1)
		{
			mSmclvImages.onRefreshComplete();
			SDToast.showToast("没有更多数据了");
		} else
		{
			requestMyPhotos(true);
		}
	}

	private void initIntentData()
	{
		Intent intent = getIntent();
		if (intent.hasExtra(EXTRA_UID))
		{
			uid = intent.getStringExtra(EXTRA_UID);
		}
		if (intent.hasExtra(EXTRA_ACT))
		{
			act = intent.getStringExtra(EXTRA_ACT);
		}
		if (intent.hasExtra(EXTRA_TITLE))
		{
			strTitle = intent.getStringExtra(EXTRA_TITLE);
		}

	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});

		if (strTitle != null)
		{
			mTitleSimple.setTitleTop(strTitle);
		}
	}

	protected void requestMyPhotos(final boolean isLoadMore)
	{

		RequestModel model = new RequestModel();
		model.put("act", act);
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (user != null)
		{
			model.put("email", user.getUser_name());
			model.put("pwd", user.getUser_pwd());
		}
		model.put("uid", uid);
		model.put("page", mCurPage);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{
				AppHelper.showLoadingDialog("请稍候...");
			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				MyPhotosActivityModel model = JsonUtil.json2Object(responseInfo.result, MyPhotosActivityModel.class);
				if (!SDInterfaceUtil.isActModelNull(model))
				{
					if (model.getPage() != null)
					{
						mCurPage = model.getPage().getPage();
						mTotalPage = model.getPage().getPage_total();
					}
					if (model.getItem() != null && model.getItem().size() > 0)
					{
						if (!isLoadMore)
						{
							mListModel.clear();
						}
						mListModel.addAll(model.getItem());
					} else
					{
						mListModel.clear();
					}
					mAdapter.updateListViewData(mListModel);
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				AppHelper.hideLoadingDialog();
				mSmclvImages.onRefreshComplete();
			}
		};

		InterfaceServer.getInstance().requestInterface(model, handler);

	}

}
package com.wenzhoujie.config;

import com.wenzhoujie.common.ConfigManager;
import com.wenzhoujie.constant.Constant.SearchTypeNormal;

public class O2oConfig
{

	/** 保存搜索界面最后一次选中的搜索类型 */
	public static final String LAST_SEARCH_TYPE = "last_search_type";

	public static int getLastSearchType()
	{
		return ConfigManager.getConfig().getInt(LAST_SEARCH_TYPE, SearchTypeNormal.TUAN);
	}

	public static void setLastSearchType(int value)
	{
		ConfigManager.getConfig().setInt(LAST_SEARCH_TYPE, value);
	}

	/** 分享界面最后一次选中的标签，最新还是最热 */
	public static final String LAST_SHARE_INDEX = "last_share_index";

	public static int getLastshareIndex()
	{
		return ConfigManager.getConfig().getInt(LAST_SHARE_INDEX, 0);
	}

	public static void setLastshareIndex(int value)
	{
		ConfigManager.getConfig().setInt(LAST_SHARE_INDEX, value);
	}
	
	
	public static final String REGION_CONF_VERSION = "region_conf_version";
	
	public static int getRegionConfVersion()
	{
		return ConfigManager.getConfig().getInt(REGION_CONF_VERSION, 0);
	}
	
	public static void setRegionConfVersion(int value)
	{
		ConfigManager.getConfig().setInt(REGION_CONF_VERSION, value);
	}
	
	//
	public static final String SESSION_ID = "session_id";

	public static String getSessionId()
	{
		return ConfigManager.getConfig().getString(SESSION_ID, "");
	}

	public static void setSessionId(String value)
	{
		ConfigManager.getConfig().setString(SESSION_ID, value);
	}
	
	//
	public static final String REF_ID = "ref_id";

	public static String getRefId()
	{
		return ConfigManager.getConfig().getString(REF_ID, "");
	}

	public static void setRefId(String value)
	{
		ConfigManager.getConfig().setString(REF_ID, value);
	}

}

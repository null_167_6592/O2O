package com.wenzhoujie.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.wenzhoujie.library.dialog.SDDialogBase;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.LoginNewActivity;
import com.wenzhoujie.MerchantLocationActivity;
import com.wenzhoujie.OrderDetailActivity;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.Auto_orderActModel;
import com.wenzhoujie.model.act.MerchantitemActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDFormatUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 门店详情头部信息fragment
 * 
 * @author js02
 * 
 */
public class MerchantDetailInfoFragment extends BaseFragment implements OnClickListener
{

	private static final int REQUEST_CODE_REQUEST_LOGIN = 1;

	@ViewInject(id = R.id.frag_merchant_detail_info_iv_image)
	private ImageView mIvImage = null;

	@ViewInject(id = R.id.frag_merchant_detail_info_tv_name)
	private TextView mTvName = null;

	@ViewInject(id = R.id.frag_merchant_detail_info_rb_star)
	private RatingBar mRbStar = null;

	@ViewInject(id = R.id.frag_merchant_detail_info_tv_star_number)
	private TextView mTvStarNumber = null;

	@ViewInject(id = R.id.frag_merchant_detail_info_tv_phone_number)
	private TextView mTvPhoneNumber = null;

	@ViewInject(id = R.id.frag_merchant_detail_info_tv_call_phone)
	private TextView mTvCallPhone = null;

	@ViewInject(id = R.id.frag_merchant_detail_info_tv_address)
	private TextView mTvAddress = null;

	@ViewInject(id = R.id.frag_merchant_detail_info_tv_check_location)
	private TextView mTvCheckLocation = null;

	@ViewInject(id = R.id.frag_merchant_detail_info_ll_pay_right_now)
	private LinearLayout mLlPayRightNow = null;

	@ViewInject(id = R.id.frag_merchant_detail_info_tv_pay_right_now)
	private TextView mTvPayRightNow = null;

	private MerchantitemActModel mMerchantitemActModel = null;

	public void setmMerchantitemActModel(MerchantitemActModel mMerchantitemActModel)
	{
		this.mMerchantitemActModel = mMerchantitemActModel;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_merchant_detail_info, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		initViewState();
		bindData();
		registeClick();
	}

	private void initViewState()
	{
		if (mMerchantitemActModel != null)
		{
			int autoOrder = SDTypeParseUtil.getIntFromString(mMerchantitemActModel.getIs_auto_order(), -1);
			switch (autoOrder)
			{
			case 1:
				LocalUserModel user = App.getApplication().getmLocalUser();
				if (user == null)
				{
					mTvPayRightNow.setText("登陆后付款");
				} else
				{
					mTvPayRightNow.setText("立即付款");
				}
				mLlPayRightNow.setVisibility(View.VISIBLE);
				mTvPayRightNow.setOnClickListener(this);
				break;
			default:
				mLlPayRightNow.setVisibility(View.GONE);
				mTvPayRightNow.setOnClickListener(null);
				break;
			}
		}
	}

	private void bindData()
	{
		if (mMerchantitemActModel != null)
		{
			SDViewBinder.setImageView(mIvImage, mMerchantitemActModel.getLogo()); // 门店图片
			SDViewBinder.setTextView(mTvName, mMerchantitemActModel.getName()); // 门店名字
			SDViewBinder.setRatingBar(mRbStar, Float.valueOf(mMerchantitemActModel.getAvg_point())); // 门店星级数
			SDViewBinder.setTextView(mTvStarNumber, SDFormatUtil.formatNumberString(mMerchantitemActModel.getAvg_point(), 1)); // 星级数评分
			SDViewBinder.setTextView(mTvPhoneNumber, mMerchantitemActModel.getTel()); // 门店电话
			SDViewBinder.setTextView(mTvAddress, mMerchantitemActModel.getAddress()); // 门店地址

		}

	}

	private void registeClick()
	{
		mTvCallPhone.setOnClickListener(this);
		mTvCheckLocation.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.frag_merchant_detail_info_tv_call_phone:
			if (mMerchantitemActModel != null)
			{
				Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mMerchantitemActModel.getTel()));
				startActivity(intent);
			}
			break;
		case R.id.frag_merchant_detail_info_tv_check_location:
			if (mMerchantitemActModel != null)
			{
				if (!TextUtils.isEmpty(mMerchantitemActModel.getAddress()))
				{
					Intent intent = new Intent(App.getApplication(), MerchantLocationActivity.class);
					intent.putExtra(MerchantLocationActivity.EXTRA_MODEL_MERCHANTITEMACTMODEL, mMerchantitemActModel);
					startActivity(intent);
				} else
				{
					SDToast.showToast("未找到商家地址");
				}
			}
			break;
		case R.id.frag_merchant_detail_info_tv_pay_right_now:
			LocalUserModel user = App.getApplication().getmLocalUser();
			if (user == null)
			{
				startActivityForResult(new Intent(App.getApplication(), LoginNewActivity.class), REQUEST_CODE_REQUEST_LOGIN);
			} else
			{
				showConfirmOrderDialog();
			}

			break;
		default:
			break;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		switch (requestCode)
		{
		case REQUEST_CODE_REQUEST_LOGIN:
			if (resultCode == LoginNewActivity.RESULT_CODE_LOGIN_SUCCESS)
			{
				initViewState();
			}
			break;

		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * 显示下单窗口
	 */
	private void showConfirmOrderDialog()
	{
		if (mMerchantitemActModel != null)
		{
			View view = LayoutInflater.from(App.getApplication()).inflate(R.layout.dialog_merchant_submit_order, null);
			final Dialog dialog = new SDDialogBase().setDialogView(view);
			dialog.show();
			TextView tvName = (TextView) view.findViewById(R.id.dialog_merchant_submit_order_tv_name);
			TextView tvBrief = (TextView) view.findViewById(R.id.dialog_merchant_submit_order_tv_brief);
			final EditText etMoney = (EditText) view.findViewById(R.id.dialog_merchant_submit_order_et_money);
			TextView tvConfirm = (TextView) view.findViewById(R.id.dialog_merchant_submit_order_tv_confirm);
			TextView tvCancel = (TextView) view.findViewById(R.id.dialog_merchant_submit_order_tv_cancel);
			final TextView tvTip = (TextView) view.findViewById(R.id.dialog_merchant_submit_order_tv_tip);

			SDViewBinder.setTextView(tvName, mMerchantitemActModel.getName());
			SDViewBinder.setTextView(tvBrief, mMerchantitemActModel.getBrief());
			etMoney.addTextChangedListener(new TextWatcher()
			{

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count)
				{
					// TODO Auto-generated method stub

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after)
				{
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s)
				{
					String money = s.toString();
					if (!TextUtils.isEmpty(money))
					{
						tvTip.setText("");
					}
				}
			});
			tvConfirm.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					String money = etMoney.getText().toString();
					if (TextUtils.isEmpty(money))
					{
						tvTip.setText("请输入金额");
						return;
					}
					requestAutoOrder(money, dialog);
				}
			});
			tvCancel.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					dialog.dismiss();
				}
			});
		}
	}

	/**
	 * 请求自主下单接口
	 */
	protected void requestAutoOrder(String money, final Dialog dialog)
	{
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (user != null)
		{
			RequestModel model = new RequestModel();
			model.put("act", "auto_order");
			model.put("location_id", mMerchantitemActModel.getId());
			model.put("money", money);
			model.put("email", user.getUser_name());
			model.put("pwd", user.getUser_pwd());
			RequestCallBack<String> handler = new RequestCallBack<String>()
			{

				@Override
				public void onStart()
				{
					AppHelper.showLoadingDialog("请稍候...");
				}

				@Override
				public void onSuccess(ResponseInfo<String> responseInfo)
				{
					Auto_orderActModel actModel = JsonUtil.json2Object(responseInfo.result, Auto_orderActModel.class);
					if (!SDInterfaceUtil.isActModelNull(actModel))
					{
						if (actModel.getResponse_code() == 1)
						{
							int status = SDTypeParseUtil.getIntFromString(actModel.getStatus(), 0);
							if (status == 1)
							{
								AppHelper.hideLoadingDialog();
								dialog.dismiss();
								Intent intent = new Intent(App.getApplication(), OrderDetailActivity.class);
								intent.putExtra(OrderDetailActivity.EXTRA_ORDER_ID, String.valueOf(actModel.getOrder_id()));
								startActivity(intent);
							}
						}
					}
				}

				@Override
				public void onFailure(HttpException error, String msg)
				{

				}

				@Override
				public void onFinish()
				{
					AppHelper.hideLoadingDialog();
				}
			};
			InterfaceServer.getInstance().requestInterface(model, handler);
		}
	}

}
package com.wenzhoujie.fragment;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wenzhoujie.library.customview.FlowLayout;
import com.wenzhoujie.library.customview.SDTabItemCorner;
import com.wenzhoujie.library.customview.SDViewNavigatorManager;
import com.wenzhoujie.library.customview.SDViewNavigatorManager.SDViewNavigatorManagerListener;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.DetailWebViewActivity;
import com.wenzhoujie.WebViewActivity;
import com.wenzhoujie.model.GoodsAttrsAttrModel;
import com.wenzhoujie.model.GoodsAttrsModel;
import com.wenzhoujie.model.act.GoodsdescActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 团购详情，商品套餐属性fragment
 * 
 * @author js02
 * 
 */
public class TuanDetailAttrsFragment extends BaseFragment implements OnClickListener
{
	@ViewInject(id = R.id.frag_tuan_detail_four_ll_arrts_packages)
	private LinearLayout mLlArrtsPackages = null;

	@ViewInject(id = R.id.frag_tuan_detail_four_ll_attrs)
	private LinearLayout mLlAttrs = null;

	@ViewInject(id = R.id.frag_tuan_detail_four_ll_more_detail)
	private LinearLayout mLlMoreDetail = null;

	@ViewInject(id = R.id.frag_tuan_detail_four_flow_attr1_container)
	private FlowLayout mFlowAttr1 = null;

	@ViewInject(id = R.id.frag_tuan_detail_four_flow_attr2_container)
	private FlowLayout mFlowAttr2 = null;

	@ViewInject(id = R.id.frag_tuan_detail_four_tv_attr1_name)
	private TextView mTvAttr1Name = null;

	@ViewInject(id = R.id.frag_tuan_detail_four_tv_attr2_name)
	private TextView mTvAttr2Name = null;

	@ViewInject(id = R.id.frag_tuan_detail_four_ll_attrs1)
	private LinearLayout mLlAttrs1 = null;

	@ViewInject(id = R.id.frag_tuan_detail_four_ll_attrs2)
	private LinearLayout mLlAttrs2 = null;

	private GoodsdescActModel mGoodsModel = null;

	private int mSelectAttr1Index = 0;
	private int mSelectAttr2Index = 0;

	private SDViewNavigatorManager mViewManagerAttr1 = null;
	private SDViewNavigatorManager mViewManagerAttr2 = null;

	private TextView mTvCurrentPrice = null;

	public void setmGoodsModel(GoodsdescActModel mGoodsModel)
	{
		this.mGoodsModel = mGoodsModel;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_tuan_detail_attrs, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		registeClick();
		bindDataByGoodsModel(mGoodsModel);
	}

	protected void updateScore_limitNum_price()
	{
		if (mGoodsModel != null && mGoodsModel.getAttr() != null)
		{
			GoodsAttrsModel attrsModel = mGoodsModel.getAttr();
			String price = attrsModel.getAttr_1_2Price_format(mSelectAttr1Index, mSelectAttr2Index);

			if (mTvCurrentPrice == null)
			{
				mTvCurrentPrice = (TextView) getActivity().findViewById(R.id.frag_tuan_detail_first_tv_current_price);
			}

			if (mTvCurrentPrice != null && !TextUtils.isEmpty(price))
			{
				SDViewBinder.setTextView(mTvCurrentPrice, price, "￥未找到");
			}
		}

	}

	private void bindDataByGoodsModel(GoodsdescActModel model)
	{
		if (model != null)
		{
			int hasAttr = SDTypeParseUtil.getIntFromString(model.getHas_attr(), -1);
			if (hasAttr > 0 && model.getAttr() != null) // 有属性
			{
				mLlArrtsPackages.setVisibility(View.VISIBLE);
				int hasAttr1 = SDTypeParseUtil.getIntFromString(model.getAttr().getHas_attr_1(), -1);
				if (hasAttr1 > 0 && model.getAttr().getAttr_1() != null && model.getAttr().getAttr_1().size() > 0) // 有属性1
				{
					mLlAttrs1.setVisibility(View.VISIBLE);
					model.getAttr().setSelected_attr_1(null);
					if (TextUtils.isEmpty(model.getAttr().getAttr_title_1()))
					{
						mTvAttr1Name.setText("未找到");
					} else
					{
						mTvAttr1Name.setText(model.getAttr().getAttr_title_1() + "：");
					}
					if (mViewManagerAttr1 == null)
					{
						mViewManagerAttr1 = new SDViewNavigatorManager();
						mViewManagerAttr1.setmListener(new Attr1SDViewNavigatorManagerListener());
					}

					SDTabItemCorner[] items = new SDTabItemCorner[model.getAttr().getAttr_1().size()];

					List<GoodsAttrsAttrModel> listAttr1Model = model.getAttr().getAttr_1();
					GoodsAttrsAttrModel modelAttr1 = null;
					for (int i = 0; i < listAttr1Model.size(); i++)
					{
						modelAttr1 = listAttr1Model.get(i);
						SDTabItemCorner tab = getAttrTabByGoodsAttrsAttrModel(modelAttr1);
						if (tab != null)
						{
							items[i] = tab;
							mFlowAttr1.addView(tab);
						}
					}
					mViewManagerAttr1.setItemsArr(items);
//					mViewManagerAttr1.setSelectIndex(0, items[0], true);
				}

				int hasAttr2 = SDTypeParseUtil.getIntFromString(model.getAttr().getHas_attr_2(), -1);
				if (hasAttr2 > 0 && model.getAttr().getAttr_2() != null && model.getAttr().getAttr_2().size() > 0) // 有属性2
				{
					mLlAttrs2.setVisibility(View.VISIBLE);
					model.getAttr().setSelected_attr_2(null);
					if (TextUtils.isEmpty(model.getAttr().getAttr_title_2()))
					{
						mTvAttr2Name.setText("未找到");
					} else
					{
						mTvAttr2Name.setText(model.getAttr().getAttr_title_2() + "：");
					}
					if (mViewManagerAttr2 == null)
					{
						mViewManagerAttr2 = new SDViewNavigatorManager();
						mViewManagerAttr2.setmListener(new Attr2SDViewNavigatorManagerListener());
					}

					SDTabItemCorner[] items = new SDTabItemCorner[model.getAttr().getAttr_2().size()];

					List<GoodsAttrsAttrModel> listAttr2Model = model.getAttr().getAttr_2();
					GoodsAttrsAttrModel modelAttr2 = null;
					for (int i = 0; i < listAttr2Model.size(); i++)
					{
						modelAttr2 = listAttr2Model.get(i);
						SDTabItemCorner tab = getAttrTabByGoodsAttrsAttrModel(modelAttr2);
						if (tab != null)
						{
							items[i] = tab;
							mFlowAttr2.addView(tab);
						}
					}
					mViewManagerAttr2.setItemsArr(items);
//					mViewManagerAttr2.setSelectIndex(0, items[0], true);
				}

			} else
			{
				mLlArrtsPackages.setVisibility(View.GONE);
			}
		}
	}

	class Attr1SDViewNavigatorManagerListener implements SDViewNavigatorManagerListener
	{
		@Override
		public void onItemClick(View v, int index)
		{
			mSelectAttr1Index = index;
			if (mGoodsModel != null && mGoodsModel.getAttr() != null)
			{
				mGoodsModel.getAttr().setSelected_attr_1(String.valueOf(mSelectAttr1Index));
			}
			updateScore_limitNum_price();
		}
	}

	class Attr2SDViewNavigatorManagerListener implements SDViewNavigatorManagerListener
	{
		@Override
		public void onItemClick(View v, int index)
		{
			mSelectAttr2Index = index;
			if (mGoodsModel != null && mGoodsModel.getAttr() != null)
			{
				mGoodsModel.getAttr().setSelected_attr_2(String.valueOf(mSelectAttr2Index));
			}
			updateScore_limitNum_price();
		}
	}

	private SDTabItemCorner getAttrTabByGoodsAttrsAttrModel(GoodsAttrsAttrModel model)
	{
		if (model != null && model.getAttr_name() != null)
		{
			SDTabItemCorner tab = new SDTabItemCorner(getActivity());
			tab.getmAttr().setmBackgroundDrawableNormalResId(R.drawable.bg_tab_single_normal);
			tab.getmAttr().setmBackgroundDrawableSelectedResId(R.drawable.bg_tab_single_press);
			tab.getmAttr().setmTextColorNormalResId(R.color.main_color);
			tab.getmAttr().setmTextColorSelectedResId(android.R.color.white);
			tab.setTabName(model.getAttr_name());
			return tab;
		} else
		{
			return null;
		}
	}

	private void registeClick()
	{
		mLlMoreDetail.setOnClickListener(this);

	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.frag_tuan_detail_four_ll_more_detail:
			clickMoreDetail();
			break;

		default:
			break;
		}
	}

	private void clickMoreDetail()
	{
		if (mGoodsModel != null && mGoodsModel.getGoods_desc() != null)
		{
			Intent intent = new Intent(getActivity(), DetailWebViewActivity.class);
			intent.putExtra(WebViewActivity.EXTRA_TITLE, "商品详情");
			intent.putExtra(WebViewActivity.EXTRA_HTML_CONTENT, mGoodsModel.getGoods_desc());
			startActivity(intent);
		}
	}
}
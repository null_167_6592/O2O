package com.wenzhoujie.fragment;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.lingou.www.R;
import com.wenzhoujie.CommentListActivity;
import com.wenzhoujie.adapter.CommentListAdapter;
import com.wenzhoujie.app.App;
import com.wenzhoujie.model.CommentListActivityItemModel;
import com.wenzhoujie.model.act.YouhuiitemActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 优惠详情页（评价fragment）
 * 
 * @author js02
 * 
 */
public class YouhuiDetailCommentFragment extends BaseFragment implements OnClickListener
{
	@ViewInject(id = R.id.frag_youhui_detail_comment_ll_all)
	private LinearLayout mLlAll = null;

	@ViewInject(id = R.id.frag_youhui_detail_comment_ll_comment_content)
	private LinearLayout mLlCommentContent = null;

	@ViewInject(id = R.id.frag_youhui_detail_comment_ll_more_comment)
	private LinearLayout mLlMoreComment = null;

	private List<CommentListActivityItemModel> mListModel = null;

	private YouhuiitemActModel mYouhuiitemActModel = null;

	public void setmYouhuiitemActModel(YouhuiitemActModel mYouhuiitemActModel)
	{
		this.mYouhuiitemActModel = mYouhuiitemActModel;
		if (mYouhuiitemActModel != null)
		{
			this.mListModel = mYouhuiitemActModel.getComment_list();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_youhui_detail_comment, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		bindData();
		registeClick();
	}

	private void registeClick()
	{
		mLlMoreComment.setOnClickListener(this);
	}

	private void bindData()
	{
		if (mListModel != null && mListModel.size() > 0)
		{
			mLlAll.setVisibility(View.VISIBLE);
			mLlCommentContent.removeAllViews();
			CommentListAdapter adapter = new CommentListAdapter(mListModel, getActivity());
			for (int i = 0; i < mListModel.size(); i++)
			{
				mLlCommentContent.addView(adapter.getView(i, null, null));
			}
		} else
		{
			mLlAll.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.frag_youhui_detail_comment_ll_more_comment:
			clickMoreComment();
			break;

		default:
			break;
		}

	}

	private void clickMoreComment()
	{
		if (mYouhuiitemActModel != null && !TextUtils.isEmpty(mYouhuiitemActModel.getId()))
		{
			Intent intent = new Intent();
			intent.putExtra(CommentListActivity.EXTRA_YH_ID, mYouhuiitemActModel.getId());
			intent.setClass(App.getApplication(), CommentListActivity.class);
			startActivity(intent);
		}

	}

}
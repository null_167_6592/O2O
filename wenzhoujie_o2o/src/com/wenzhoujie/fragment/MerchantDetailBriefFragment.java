package com.wenzhoujie.fragment;

import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lingou.www.R;
import com.wenzhoujie.model.act.MerchantitemActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 门店详情，商家描述
 * 
 * @author js02
 * 
 */
public class MerchantDetailBriefFragment extends BaseFragment
{

	@ViewInject(id = R.id.frag_merchant_detail_brief_ll_all)
	private LinearLayout mLlAll = null;

	@ViewInject(id = R.id.frag_merchant_detail_brief_tv_brief)
	private TextView mTvBrief = null;

	private MerchantitemActModel mMerchantitemActModel = null;

	public void setmMerchantitemActModel(MerchantitemActModel mMerchantitemActModel)
	{
		this.mMerchantitemActModel = mMerchantitemActModel;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_merchant_detail_brief, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		bindData();
	}

	private void bindData()
	{
		if (mMerchantitemActModel != null)
		{
			String brief = mMerchantitemActModel.getBrief();
			if (TextUtils.isEmpty(brief))
			{
				mLlAll.setVisibility(View.GONE);
			} else
			{
				mLlAll.setVisibility(View.VISIBLE);
				mTvBrief.setText(Html.fromHtml(brief));
			}
		}
	}

}
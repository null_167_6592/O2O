package com.wenzhoujie.fragment;

import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;

import com.wenzhoujie.library.title.SDTitleSimple;
import com.wenzhoujie.library.title.SDTitleTwoRightButton;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.sunday.eventbus.SDBaseEvent;
import com.sunday.eventbus.SDEventManager;
import com.sunday.eventbus.SDEventObserver;
import com.wenzhoujie.BaseActivity;
import com.wenzhoujie.common.TitleManager;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.i.IBaseActivity;

public class BaseFragment extends Fragment implements SDEventObserver, IBaseActivity
{

	protected SDTitleSimple mTitleSimple;
	protected SDTitleTwoRightButton mTitleTwoRightBtns;
	private TitleType mTitleType = TitleType.TITLE_NONE;

	public TitleType getmTitleType()
	{
		return mTitleType;
	}

	public View setmTitleType(TitleType mTitleType, View view)
	{
		this.mTitleType = mTitleType;
		View returnView = null;
		switch (mTitleType)
		{
		case TITLE_NONE:
			returnView = view;
			break;
		case TITLE_SIMPLE:
			mTitleSimple = TitleManager.newSDTitleSimple();
			returnView = SDViewUtil.wrapperTitle(view, mTitleSimple);
			break;
		case TITLE_TWO_RIGHT_BUTTON:
			mTitleTwoRightBtns = TitleManager.newSDTitleTwoRightButton();
			returnView = SDViewUtil.wrapperTitle(view, mTitleTwoRightBtns);
			break;
		default:
			returnView = view;
			break;
		}
		return returnView;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		baseInit();
	}

	@Override
	public void baseInit()
	{
		SDEventManager.register(this);
	}

	@Override
	public void onDestroy()
	{
		SDEventManager.unregister(this);
		super.onDestroy();
	}

	@Override
	public void onPause()
	{
		super.onPause();
	}

	@Override
	public void onResume()
	{
		super.onResume();
	}

	@Override
	public void addFragment(Fragment fragment, int containerId)
	{
		getChildFragmentManager().beginTransaction().add(containerId, fragment).commitAllowingStateLoss();
	}

	@Override
	public void replaceFragment(Fragment fragment, int containerId)
	{
		getChildFragmentManager().beginTransaction().replace(containerId, fragment).commitAllowingStateLoss();
	}

	@Override
	public void showFragment(Fragment fragment)
	{
		getChildFragmentManager().beginTransaction().show(fragment).commitAllowingStateLoss();
	}

	@Override
	public void hideFragment(Fragment fragment)
	{
		getChildFragmentManager().beginTransaction().hide(fragment).commitAllowingStateLoss();
	}

	public BaseActivity getBaseActivity()
	{
		Activity activity = getActivity();
		if (activity != null && activity instanceof BaseActivity)
		{
			return (BaseActivity) activity;
		}
		return null;
	}

	@Override
	public void onEvent(SDBaseEvent event)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onEventMainThread(SDBaseEvent event)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onEventBackgroundThread(SDBaseEvent event)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onEventAsync(SDBaseEvent event)
	{
		// TODO Auto-generated method stub

	}
	
	public void hideFragmentView()
	{
		SDViewUtil.hide(getView());
	}

	public void showFragmentView()
	{
		SDViewUtil.show(getView());
	}

	public void invisibleFragmentView()
	{
		SDViewUtil.invisible(getView());
	}

	public boolean toggleFragmentView(List<?> list)
	{
		if (list != null && !list.isEmpty())
		{
			showFragmentView();
			return true;
		} else
		{
			hideFragmentView();
			return false;
		}
	}

	public boolean toggleFragmentView(Object obj)
	{
		if (obj != null)
		{
			showFragmentView();
			return true;
		} else
		{
			hideFragmentView();
			return false;
		}
	}

	public boolean toggleFragmentView(String content)
	{
		if (!TextUtils.isEmpty(content))
		{
			showFragmentView();
			return true;
		} else
		{
			hideFragmentView();
			return false;
		}
	}

	public boolean toggleFragmentView(int show)
	{
		if (show == 1)
		{
			showFragmentView();
			return true;
		} else
		{
			hideFragmentView();
			return false;
		}
	}

	public boolean toggleFragmentView(boolean show)
	{
		if (show)
		{
			showFragmentView();
			return true;
		} else
		{
			hideFragmentView();
			return false;
		}
	}

}

package com.wenzhoujie.fragment;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.wenzhoujie.library.utils.SDViewUtil;
import com.lingou.www.R;
import com.wenzhoujie.ECshopListActivity;
import com.wenzhoujie.MainActivity;
import com.wenzhoujie.MerchantListActivity;
import com.wenzhoujie.TuanListActivity;
import com.wenzhoujie.WebViewActivity;
import com.wenzhoujie.YouHuiListActivity;
import com.wenzhoujie.adapter.HomeRecommendSupplierAdapter;
import com.wenzhoujie.app.App;
import com.wenzhoujie.constant.ApkConstant;
import com.wenzhoujie.customview.SDGridViewInScroll;
import com.wenzhoujie.model.IndexActSupplier_listModel;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.act.IndexActNewModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 首页推荐商家
 * 
 * @author js02
 * 
 */
public class HomeRecommendSupplierFragment extends BaseFragment implements OnClickListener
{

	@ViewInject(id = R.id.frag_home_recommend_supplier_ll_all)
	private LinearLayout mLlAll = null;
	//下面的图片列表
	@ViewInject(id = R.id.frag_home_recommend_supplier_ll_suppliers)
	private LinearLayout mLlSuppliers = null;
	@ViewInject(id = R.id.frag_home_recommend_supplier_ll_suppliers2)
	private LinearLayout mLlSuppliers2 = null;
	
	//更多的按钮
	@ViewInject(id = R.id.frag_home_recommend_supplier_ll_all_suppliers)
	private TextView mTvAllSuppliers = null;
	
	@ViewInject(id = R.id.meirijingxuanbtn)
	private ImageButton meiRiJXBTN = null;
	@ViewInject(id = R.id.xinpintuijiebtn)
	private ImageButton xinPinTJBTN = null;
	@ViewInject(id = R.id.jucantupianbtn)
	private ImageButton juCanBTN = null;
	@ViewInject(id = R.id.aiguangjiebtn)
	private ImageButton aiGJBTN = null;
	

	private List<IndexActSupplier_listModel> mListModel = new ArrayList<IndexActSupplier_listModel>();

	private IndexActNewModel mIndexModel = null;

	public void setmIndexModel(IndexActNewModel indexModel)
	{
		this.mIndexModel = indexModel;
		this.mListModel = mIndexModel.getSupplier_list();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_home_recommend_supplier, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		bindData();
		registeClick();
	}

	private void bindData()
	{
		if (mListModel != null && mListModel.size() > 0)
		{	//System.out.println("mListModel.size()"+mListModel);
			mLlAll.setVisibility(View.VISIBLE);
			mLlSuppliers.removeAllViews();
			mLlSuppliers2.removeAllViews();
			HomeRecommendSupplierAdapter adapter = new HomeRecommendSupplierAdapter(mListModel, getActivity());
			int listSize = mListModel.size();
			DisplayMetrics dm =getResources().getDisplayMetrics();  
	        int w_screen = dm.widthPixels; 
			int hei = w_screen/3;
			//LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, (float)0.5); //WRAP_CONTENT
			LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(0, hei, (float)0.5);
			LinearLayout.LayoutParams paramLi = new LinearLayout.LayoutParams(1, LayoutParams.MATCH_PARENT);
			LinearLayout.LayoutParams paramDiv = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			for (int i = 0; i < 2; i++)
			{
				View view = adapter.getView(i, null, null);
				mLlSuppliers.addView(view, param);
				
				if (i != 1)
				{ //这个是分割线
					LinearLayout Ll = new LinearLayout(App.getApplication());
					Ll.setPadding(0, SDViewUtil.dp2px(10), 0, SDViewUtil.dp2px(15));
					TextView tvVerDiv = new TextView(App.getApplication());
					tvVerDiv.setBackgroundColor(getResources().getColor(R.color.stroke));
					Ll.addView(tvVerDiv, paramDiv);
					mLlSuppliers.addView(Ll, paramLi);
					
				}
				
			}
			if(listSize>2)
			for (int i = 2; i < 4; i++)
			{
				
				View view2 = adapter.getView(i, null, null);
				mLlSuppliers2.addView(view2, param);
				
				if (i != 3)
				{ //这个是分割线
					
					LinearLayout Ll1 = new LinearLayout(App.getApplication());
					Ll1.setPadding(0, SDViewUtil.dp2px(10), 0, SDViewUtil.dp2px(15));
					TextView tvVerDiv2 = new TextView(App.getApplication());
					tvVerDiv2.setBackgroundColor(getResources().getColor(R.color.stroke));
					Ll1.addView(tvVerDiv2, paramDiv);
					mLlSuppliers2.addView(Ll1, paramLi);
				}
				
			}
		} else
		{
			mLlAll.setVisibility(View.GONE);
		}
	}

	private void registeClick()
	{
		//mTvAllSuppliers.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
		mTvAllSuppliers.setOnClickListener(this);
		meiRiJXBTN.setOnClickListener(this);
		xinPinTJBTN.setOnClickListener(this);
		juCanBTN.setOnClickListener(this);
		aiGJBTN.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		
		case R.id.frag_home_recommend_supplier_ll_all_suppliers:
			clickAllSuppliers();
			break;
		case R.id.meirijingxuanbtn:
			clickMeiRiJX();
			break;
		case R.id.xinpintuijiebtn:
			clickXinPinTJ();
			break;
		case R.id.jucantupianbtn:
			clickJuCan();
			break;
		case R.id.aiguangjiebtn:
			clickAiGuangJie();
			break;
		default:
			break;
		}
	}

	private void clickAllSuppliers()
	{
		/*Activity activity = getBaseActivity();
		if (activity != null && activity instanceof MainActivity)
		{
			MainActivity mainActivity = (MainActivity) activity;
			mainActivity.setSelectIndex(1, null, true);
		}*/
		
		Intent intent = new Intent();
		intent.setClass(App.getApplication(), MerchantListActivity.class);
		intent.putExtra(MerchantListFragment.EXTRA_TITLE_TXT, "更多推荐");
		intent.putExtra(MerchantListFragment.EXTRA_FROM_TXT, "tuijian");
		
		startActivity(intent);
	}
	
	private void clickMeiRiJX()
	{
		
		Intent intent = new Intent();
		intent.setClass(App.getApplication(), TuanListActivity.class);
		startActivity(intent);
	}
	
	private void clickXinPinTJ()
	{
		/*Activity activity = getBaseActivity();
		if (activity != null && activity instanceof MainActivity)
		{
			MainActivity mainActivity = (MainActivity) activity;
			mainActivity.setSelectIndex(1, null, true);
		}*/
		Intent intent = new Intent();
		intent.setClass(App.getApplication(), ECshopListActivity.class);
		intent.putExtra(ECshopListFragment.EXTRA_TITLE_TXT, "商品列表");
		intent.putExtra(ECshopListFragment.EXTRA_CATE_ID, "109");
		intent.putExtra(ECshopListFragment.EXTRA_CATE_NAME, "新品尝鲜");
		
		startActivity(intent);
	}
	
	private void clickJuCan()
	{
		/*Activity activity = getBaseActivity();
		if (activity != null && activity instanceof MainActivity)
		{
			MainActivity mainActivity = (MainActivity) activity;
			mainActivity.setSelectIndex(1, null, true);
		}*/
		/*LocalUserModel userModel = App.getApplication().getmLocalUser();
		
		String uname = "";
		String upass = "";
		if(userModel!=null)
		{
			uname =  userModel.getUser_name();
			upass = userModel.getUser_pwd();
		}
		
		String JCUrl = ApkConstant.SERVER_URL+"/dc/index.php?user="+uname+"&pass="+md5(upass);
		Intent intent = new Intent(App.getApplication(), WebViewActivity.class);
		intent.putExtra(WebViewActivity.EXTRA_URL, JCUrl);
		startActivity(intent);*/
		Intent intent = new Intent();
		intent.setClass(App.getApplication(), MerchantListActivity.class);
		intent.putExtra(MerchantListFragment.EXTRA_TITLE_TXT, "商家列表");
		intent.putExtra(MerchantListFragment.EXTRA_CATE_ID, "4");
		intent.putExtra(MerchantListFragment.EXTRA_CATE_TYPE_ID, "61");
		intent.putExtra(MerchantListFragment.EXTRA_CATE_NAME, "母婴商店");
		
		startActivity(intent);
	}
	public static String md5(String string) {
	    byte[] hash;
	    try {
	        hash = MessageDigest.getInstance("MD5").digest(string.getBytes("UTF-8"));
	    } catch (NoSuchAlgorithmException e) {
	        throw new RuntimeException("MD5 should be supported?", e);
	    } catch (UnsupportedEncodingException e) {
	        throw new RuntimeException("UTF-8 should be supported?", e);
	    }

	    StringBuilder hex = new StringBuilder(hash.length * 2);
	    for (byte b : hash) {
	        if ((b & 0xFF) < 0x10) hex.append("0");
	        hex.append(Integer.toHexString(b & 0xFF));
	    }
	    return hex.toString();
	}
	private void clickAiGuangJie()
	{
		/*Activity activity = getBaseActivity();
		if (activity != null && activity instanceof MainActivity)
		{
			MainActivity mainActivity = (MainActivity) activity;
			mainActivity.setSelectIndex(1, null, true);
		}*/
		Intent intent = new Intent();
		intent.setClass(App.getApplication(), MerchantListActivity.class);
		intent.putExtra(MerchantListFragment.EXTRA_TITLE_TXT, "商家列表");
		intent.putExtra(MerchantListFragment.EXTRA_CATE_ID, "3");
		intent.putExtra(MerchantListFragment.EXTRA_CATE_TYPE_ID, "102");
		intent.putExtra(MerchantListFragment.EXTRA_CATE_NAME, "酒水配送");
		
		startActivity(intent);
	}

}
package com.wenzhoujie.fragment;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.lingou.www.R;
import com.sunday.eventbus.SDBaseEvent;
import com.wenzhoujie.TuanListActivity;
import com.wenzhoujie.adapter.HomeRecommendDealsAdapter;
import com.wenzhoujie.event.EnumEventTag;
import com.wenzhoujie.model.IndexActDeal_listModel;
import com.wenzhoujie.model.act.IndexActNewModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 首页推荐团购
 * 
 * @author js02
 * 
 */
public class HomeRecommendDealsFragment extends BaseFragment implements OnClickListener
{

	@ViewInject(id = R.id.frag_home_recommend_deals_ll_all)
	private LinearLayout mLlAll = null;

	@ViewInject(id = R.id.frag_home_recommend_deals_ll_deals)
	private LinearLayout mLlDeals = null;

	@ViewInject(id = R.id.frag_home_recommend_deals_ll_all_deals)
	private LinearLayout mLlAllDeals = null;

	private IndexActNewModel mIndexModel = null;

	private List<IndexActDeal_listModel> mListModel = new ArrayList<IndexActDeal_listModel>();

	public void setmIndexModel(IndexActNewModel indexModel)
	{
		this.mIndexModel = indexModel;
		this.mListModel = mIndexModel.getDeal_list();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_home_recommend_deals, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		bindData();
		registeClick();
	}

	private void bindData()
	{
		if (mListModel != null && mListModel.size() > 0)
		{
			mLlAll.setVisibility(View.VISIBLE);
			mLlDeals.removeAllViews();
			HomeRecommendDealsAdapter adapter = new HomeRecommendDealsAdapter(mListModel, getActivity());
			int listSize = mListModel.size();
			for (int i = 0; i < listSize; i++)
			{
				View view = adapter.getView(i, null, null);
				mLlDeals.addView(view);
			}
		} else
		{
			mLlAll.setVisibility(View.GONE);
		}
	}

	private void registeClick()
	{
		mLlAllDeals.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.frag_home_recommend_deals_ll_all_deals:
			clickAllDeals();
			break;
		default:
			break;
		}
	}

	private void clickAllDeals()
	{
		startActivity(new Intent(getActivity(), TuanListActivity.class));
	}

	@Override
	public void onEventMainThread(SDBaseEvent event)
	{
		// TODO Auto-generated method stub
		super.onEventMainThread(event);
		switch (EnumEventTag.valueOf(event.getEventTagInt()))
		{
		case LOCATION_SUCCESS:
			evnetLocationSuccess();
			break;

		default:
			break;
		}
	}

	private void evnetLocationSuccess()
	{
		if (mListModel != null)
		{
			for (IndexActDeal_listModel model : mListModel)
			{
				model.calculateDistance();
			}
			bindData();
		}

	}

}
package com.wenzhoujie.fragment;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.lingou.www.R;
import com.sunday.eventbus.SDBaseEvent;
import com.wenzhoujie.adapter.OrderFeeItemAdapter;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.common.CommonInterface;
import com.wenzhoujie.event.EnumEventTag;
import com.wenzhoujie.model.FeeinfoModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 订单详情页(费用详情fragment)
 * 
 * @author js02
 * 
 */
public class OrderDetailFeeFragment extends BaseFragment
{

	@ViewInject(id = R.id.frag_order_detail_fee_ll_all)
	private LinearLayout mLlAll = null;

	@ViewInject(id = R.id.frag_order_detail_fee_ll_fees)
	private LinearLayout mLlFees = null;

	private List<FeeinfoModel> mListModel = null;

	public void setListFeeinfoModel(List<FeeinfoModel> listModel)
	{
		this.mListModel = listModel;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_order_detail_fee, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		bindData(mListModel);
		CommonInterface.refreshLocalUser();
	}

	private void bindData(List<FeeinfoModel> listModel)
	{
		if (listModel != null && listModel.size() > 0)
		{
			mLlAll.setVisibility(View.VISIBLE);
			mLlFees.removeAllViews();
			OrderFeeItemAdapter adapter = new OrderFeeItemAdapter(listModel, getActivity());
			for (int i = 0; i < listModel.size(); i++)
			{
				View itemView = adapter.getView(i, null, null);
				mLlFees.addView(itemView);
			}
		} else
		{
			mLlAll.setVisibility(View.GONE);
		}

	}

	@Override
	public void onEventMainThread(SDBaseEvent event)
	{
		// TODO Auto-generated method stub
		super.onEventMainThread(event);
		switch (EnumEventTag.valueOf(event.getEventTagInt()))
		{
		case REFRESH_USER_MONEY_SUCCESS:
			if (AppHelper.getLocalUser() != null && mListModel != null && mListModel.size() > 0)
			{
				List<FeeinfoModel> listModel = new ArrayList<FeeinfoModel>();
				for (int i = 0; i < mListModel.size(); i++)
				{
					listModel.add(mListModel.get(i));
				}
				String userMoneyFormat = AppHelper.getLocalUser().getUser_money_format();
				if (!AppHelper.isEmptyString(userMoneyFormat))
				{
					FeeinfoModel userMoneyModel = new FeeinfoModel();
					userMoneyModel.setItem("账户余额");
					userMoneyModel.setValue(AppHelper.getLocalUser().getUser_money_format());
					listModel.add(userMoneyModel);
				}
				bindData(listModel);
			}
			break;

		default:
			break;
		}
	}

}
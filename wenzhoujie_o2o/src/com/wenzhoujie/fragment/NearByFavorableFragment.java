package com.wenzhoujie.fragment;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.adapter.FavorableListAdapter;
import com.wenzhoujie.app.App;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.FavorableListActItemModel;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.SearchConditionModel;
import com.wenzhoujie.model.act.FavorableListActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

public class NearByFavorableFragment extends BaseFragment implements OnClickListener
{

	@ViewInject(id = R.id.frag_nearby_discount_lv)
	private PullToRefreshListView mIpLvVip = null;

	@ViewInject(id = R.id.frag_nearby_discount_iv_empty)
	private ImageView mIvEmpty = null;

	private List<FavorableListActItemModel> mlistModel = new ArrayList<FavorableListActItemModel>();
	private FavorableListAdapter mAdapter = null;

	private SearchConditionModel mSearcher = new SearchConditionModel();

	private int mCurPage = 1;
	private int mTotalPage = -1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_nearby_discount, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{

		bindDefaultData();
		initPullListView();

	}

	private void bindDefaultData()
	{
		mAdapter = new FavorableListAdapter(mlistModel, getActivity());
		mIpLvVip.setAdapter(mAdapter);
	}

	protected void requestNearByVip(final boolean isLoadMore)
	{

		RequestModel model = new RequestModel();
		model.put("act", "nearbyyouhui");
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (user != null)
		{
			model.put("city_id", mSearcher.getCity_id());
			model.put("email", user.getUser_name());
			model.put("pwd", user.getUser_pwd());
		}
		model.put("latitude_top", mSearcher.getM_latitude() + 0.2);
		model.put("latitude_bottom", mSearcher.getM_latitude() - 0.2);
		model.put("longitude_left", mSearcher.getM_longitude() - 0.2);
		model.put("longitude_right", mSearcher.getM_longitude() + 0.2);
		model.put("m_latitude", mSearcher.getM_latitude());
		model.put("m_longitude", mSearcher.getM_longitude());
		model.put("cate_id", mSearcher.getCate_id());
		model.put("page", mCurPage);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{

			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				FavorableListActModel model = JsonUtil.json2Object(responseInfo.result, FavorableListActModel.class);
				if (!SDInterfaceUtil.isActModelNull(model))
				{
					switch (model.getResponse_code())
					{
					case 0:

						break;
					case 1:
						if (model.getPage() != null)
						{
							mCurPage = model.getPage().getPage();
							mTotalPage = model.getPage().getPage_total();
						}
						if (!isLoadMore)
						{
							mlistModel.clear();
						}
						if (model.getItem() != null)
						{
							mlistModel.addAll(model.getItem());
						} else
						{
							SDToast.showToast("没有更多数据了");
						}
						mAdapter.updateListViewData(mlistModel);
						break;
					default:
						break;
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				mIpLvVip.onRefreshComplete();
				SDViewUtil.toggleEmptyMsgByList(mlistModel, mIvEmpty);
			}
		};

		InterfaceServer.getInstance().requestInterface(model, handler);

	}

	private void initPullListView()
	{
		mIpLvVip.setMode(Mode.BOTH);
		mIpLvVip.setOnRefreshListener(new OnRefreshListener2<ListView>()
		{

			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				mCurPage = 1;
				requestNearByVip(false);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				if (++mCurPage > mTotalPage && mTotalPage != -1)
				{
					SDToast.showToast("没有更多数据了");
					mIpLvVip.onRefreshComplete();
				} else
				{
					requestNearByVip(true);
				}
			}
		});
		mIpLvVip.setRefreshing();
	}

	@Override
	public void onClick(View arg0)
	{
		// TODO Auto-generated method stub

	}

}
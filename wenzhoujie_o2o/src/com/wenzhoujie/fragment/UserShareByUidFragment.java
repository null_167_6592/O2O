package com.wenzhoujie.fragment;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.adapter.ShareContentAdapter;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.UItemModel;
import com.wenzhoujie.model.act.UActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 根据传进来的uid查看该用户的分享内容
 * 
 * @author js02
 * 
 */
public class UserShareByUidFragment extends BaseFragment
{

	@ViewInject(id = R.id.frag_user_share_by_uid_ptrlv_content)
	private PullToRefreshListView mPtrlvContent = null;

	private ShareContentAdapter mAdapter;
	private List<UItemModel> mListModel = new ArrayList<UItemModel>();

	private int mPage;
	private int mPageTotal;

	private String uid;

	public String getUid()
	{
		return uid;
	}

	public void setUid(String uid)
	{
		this.uid = uid;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_user_share_by_uid, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{

		bindDefaultData();
		initPullListView();
	}

	private void bindDefaultData()
	{
		mAdapter = new ShareContentAdapter(mListModel, getActivity());
		mPtrlvContent.setAdapter(mAdapter);

	}

	private void initPullListView()
	{
		mPtrlvContent.setMode(Mode.BOTH);
		mPtrlvContent.setOnRefreshListener(new OnRefreshListener2<ListView>()
		{

			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				refresh();

			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				loadMore();

			}
		});
		mPtrlvContent.setRefreshing();
	}

	protected void loadMore()
	{
		if (++mPage > mPageTotal && mPageTotal > 0)
		{
			SDToast.showToast("没有更多数据了");
			mPtrlvContent.onRefreshComplete();
		} else
		{
			requestFollow(true);
		}
	}

	protected void refresh()
	{
		mPage = 1;
		requestFollow(false);
	}

	private void requestFollow(final boolean isLoadMore)
	{
		if (AppHelper.isEmptyString(uid))
		{
			SDToast.showToast("要查看的用户id为空");
			mPtrlvContent.onRefreshComplete();
			return;
		}

		if (AppHelper.getLocalUser() == null)
		{
			SDToast.showToast("未登录");
			return;
		}

		RequestModel model = new RequestModel();
		model.put("act", "u");
		model.put("uid", uid);
		model.put("email", AppHelper.getLocalUser().getUser_name());
		model.put("pwd", AppHelper.getLocalUser().getUser_pwd());
		model.put("page", mPage);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{

			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				UActModel model = JsonUtil.json2Object(responseInfo.result, UActModel.class);
				if (!SDInterfaceUtil.isActModelNull(model))
				{
					switch (model.getResponse_code())
					{
					case 0:

						break;
					case 1:

						if (model.getPage() != null)
						{
							mPage = model.getPage().getPage();
							mPageTotal = model.getPage().getPage_total();
						}
						SDViewUtil.updateAdapterByList(mListModel, model.getItem(), mAdapter, isLoadMore);
						break;
					default:
						break;
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				AppHelper.hideLoadingDialog();
				mPtrlvContent.onRefreshComplete();
			}
		};

		InterfaceServer.getInstance().requestInterface(model, handler);

	}

}
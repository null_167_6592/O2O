package com.wenzhoujie.fragment;

import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.lingou.www.R;
import com.wenzhoujie.adapter.OrderDetailGroupCouponsAdapter;
import com.wenzhoujie.model.OrderDetailCoupon_listModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

public class OrderDetailGroupCouponsFragment extends BaseFragment
{

	@ViewInject(id = R.id.frag_order_detail_group_coupons_ll_all)
	private LinearLayout mLlAll = null;

	@ViewInject(id = R.id.frag_order_detail_group_coupons_ll_group_coupons)
	private LinearLayout mLlGroupCoupons = null;

	private List<OrderDetailCoupon_listModel> mListModel = null;

	public void setListOrderDetailCoupon_listModel(List<OrderDetailCoupon_listModel> listModel)
	{
		this.mListModel = listModel;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_order_detail_group_coupons, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		bindData();
	}

	private void bindData()
	{
		if (mListModel != null && mListModel.size() > 0)
		{
			mLlAll.setVisibility(View.VISIBLE);
			mLlGroupCoupons.removeAllViews();
			OrderDetailGroupCouponsAdapter adapter = new OrderDetailGroupCouponsAdapter(mListModel, getActivity());
			for (int i = 0; i < mListModel.size(); i++)
			{
				View itemView = adapter.getView(i, null, null);
				mLlGroupCoupons.addView(itemView);
			}
		} else
		{
			mLlAll.setVisibility(View.GONE);
		}
	}

}
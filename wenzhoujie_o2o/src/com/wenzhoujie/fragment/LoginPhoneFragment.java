package com.wenzhoujie.fragment;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wenzhoujie.AgreementActivity;
import com.wenzhoujie.LoginNewActivity;
import com.wenzhoujie.RegisterActivity;
import com.wenzhoujie.library.customview.ClearEditText;
import com.wenzhoujie.library.customview.SDSendValidateButton;
import com.wenzhoujie.library.customview.SDSendValidateButton.SDSendValidateButtonListener;
import com.wenzhoujie.library.utils.SDToast;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.BaseActModel;
import com.wenzhoujie.model.act.PhoneShortcutLoginActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;
import com.wenzhoujie.utils.ViewInject;

public class LoginPhoneFragment extends BaseFragment implements OnClickListener
{

	public static final String EXTRA_PHONE_NUMBER = "extra_phone_number";

	@ViewInject(id = R.id.act_phone_shortcut_login_ll_regStep)
	private LinearLayout mLlRegstep = null;

	@ViewInject(id = R.id.act_phone_shortcut_login_et_mobile)
	private ClearEditText mEtMobile = null;

	@ViewInject(id = R.id.act_phone_shortcut_login_et_code)
	private ClearEditText mEtCode = null;

	@ViewInject(id = R.id.act_phone_shortcut_login_btn_sand_validate)
	private SDSendValidateButton mBtnSendValidateCode = null;

	@ViewInject(id = R.id.act_phone_shortcut_login_btn_register)
	private Button mBtnRegister = null;
	
	@ViewInject(id=R.id.wenzj_xy)
	private TextView tv_wenzj_xy;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_phone_login, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		getIntentData();
		registeClick();
		initSDSendValidateButton();
	}

	private void getIntentData()
	{
		String mobile = getActivity().getIntent().getStringExtra(EXTRA_PHONE_NUMBER);
		if (!TextUtils.isEmpty(mobile))
		{
			mEtMobile.setText(mobile);
		}
	}

	/**
	 * 初始化发送验证码按钮
	 */
	private void initSDSendValidateButton()
	{
		mBtnSendValidateCode.setmBackgroundEnableResId(R.drawable.layer_main_color_normal);
		mBtnSendValidateCode.setmBackgroundDisableResId(R.drawable.layer_main_color_press);
		mBtnSendValidateCode.setmListener(new SDSendValidateButtonListener()
		{
			@Override
			public void onTick()
			{
			}

			@Override
			public void onClickSendValidateButton()
			{
				String strMobile = mEtMobile.getText().toString();
				if (TextUtils.isEmpty(strMobile))
				{
					SDToast.showToast("请输入手机号码");
					mEtMobile.requestFocus();
					return;
				}
				if (strMobile.length() != 11)
				{
					SDToast.showToast("请输入11位的手机号码");
					mEtMobile.requestFocus();
					return;
				}
				requestValidateCode(strMobile);
			}
		});
		tv_wenzj_xy.getPaint().setFlags(Paint. UNDERLINE_TEXT_FLAG ); //下划线
		tv_wenzj_xy.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getActivity(), AgreementActivity.class);
				startActivity(intent);
			}
		});
	}

	private void registeClick()
	{
		mBtnRegister.setOnClickListener(this);

	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.act_phone_shortcut_login_btn_register:
			clickRegister();
			break;

		default:
			break;
		}
	}

	/**
	 * 快捷登录
	 */
	private void clickRegister()
	{

		if (TextUtils.isEmpty(mEtMobile.getText().toString().trim()))
		{
			SDToast.showToast("请输入手机号码!");
			return;
		}

		if (mEtMobile.getText().toString().trim().length() != 11)
		{
			SDToast.showToast("请输入11位的手机号码");
			return;
		}

		if (TextUtils.isEmpty(mEtCode.getText().toString().trim()))
		{
			SDToast.showToast("请输入验证码!");
			return;
		}

		requestShortcutLogin(mEtMobile.getText().toString(), mEtCode.getText().toString());
	}

	/**
	 * 
	 * 验证码接口
	 * 
	 */
	private void requestValidateCode(String phoneNewNum)
	{
		RequestModel model = new RequestModel();
		model.put("act", "register_verify_phone");
		model.put("mobile", phoneNewNum);
		model.put("is_login", 1);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{
				AppHelper.showLoadingDialog("请稍候...");
			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				BaseActModel actModel = JsonUtil.json2Object(responseInfo.result, BaseActModel.class);
				if (!SDInterfaceUtil.isActModelNull(actModel))
				{
					int status = SDTypeParseUtil.getIntFromString(actModel.getStatus(), 0);
					if (status == 1)
					{
						mBtnSendValidateCode.startTickWork();
					}
				}
				

			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				AppHelper.hideLoadingDialog();
			}
		};
		InterfaceServer.getInstance().requestInterface(model, handler);

	}

	/**
	 * 快捷登录接口
	 */
	private void requestShortcutLogin(String phoneNewNum, String code)
	{

		RequestModel model = new RequestModel(false);
		model.put("act", "register_verify_code");
		model.put("mobile", phoneNewNum);
		model.put("code", code);
		model.put("is_register", 1);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{
				AppHelper.showLoadingDialog("请稍候...");
			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				PhoneShortcutLoginActModel actModel = JsonUtil.json2Object(responseInfo.result, PhoneShortcutLoginActModel.class);
				if (!SDInterfaceUtil.isActModelNull(actModel))
				{
					int status = SDTypeParseUtil.getIntFromString(actModel.getStatus(), 0);
					if (status != 0)
					{
						dealLoginNormalSuccess(actModel);
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				AppHelper.hideLoadingDialog();
			}
		};
		InterfaceServer.getInstance().requestInterface(model, handler);

	}

	protected void dealLoginNormalSuccess(PhoneShortcutLoginActModel actModel)
	{
		LocalUserModel.dealLoginSuccess(actModel.getUid(), actModel.getUser_email(), actModel.getUser_name(), actModel.getUser_pwd(), actModel.getUser_avatar(), null,
				actModel.getUser_money(), actModel.getUser_money_format(), actModel.getUser_score(),actModel.getNick_name());
		getActivity().finish();
	}

	@Override
	public void onDestroy()
	{
		if (mBtnSendValidateCode != null)
		{
			mBtnSendValidateCode.stopTickWork();
		}
		super.onDestroy();
	}

}
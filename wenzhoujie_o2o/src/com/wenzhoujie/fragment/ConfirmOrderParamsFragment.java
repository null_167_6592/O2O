package com.wenzhoujie.fragment;

import android.view.View.OnClickListener;

import com.wenzhoujie.model.act.Calc_cartActModel;

public class ConfirmOrderParamsFragment extends OrderDetailParamsFragment implements OnClickListener
{

	private Calc_cartActModel mCalc_cartActModel = null;

	public void setCalc_cartActModel(Calc_cartActModel calc_cartActModel)
	{
		this.mCalc_cartActModel = calc_cartActModel;
		if (mCalc_cartActModel != null)
		{
			this.mOrderParmModel = mCalc_cartActModel.getOrder_parm();
		}
	}

	@Override
	protected void bindData()
	{
		if (mCalc_cartActModel != null)
		{
			setViewsVisibility();
			bindDeliveryAddress(mCalc_cartActModel.getDelivery());
			bindDeliveryMode();
			bindDeliveryTime();
			bindMobileNumber(mCalc_cartActModel.getSend_mobile());
		}
	}

	@Override
	protected String getDefaultDeliveryModeId()
	{
		if (mOrderParmModel != null)
		{
			return mOrderParmModel.getDelivery_id();
		} else
		{
			return null;
		}
	}

}
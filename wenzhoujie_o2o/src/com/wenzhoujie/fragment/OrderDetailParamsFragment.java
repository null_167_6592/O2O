package com.wenzhoujie.fragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.DeliveryAddressActivty;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.library.adapter.SDSimpleTextAdapter;
import com.wenzhoujie.library.dialog.SDDialogCustom;
import com.wenzhoujie.library.dialog.SDDialogCustom.SDDialogCustomListener;
import com.wenzhoujie.library.dialog.SDDialogMenu;
import com.wenzhoujie.library.dialog.SDDialogMenu.SDDialogMenuListener;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.wenzhoujie.model.DeliveryModel;
import com.wenzhoujie.model.Delivery_listModel;
import com.wenzhoujie.model.Delivery_time_listModel;
import com.wenzhoujie.model.Order_parmModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.Check_ecvActModel;
import com.wenzhoujie.model.act.My_order_detailActModel;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;
import com.wenzhoujie.utils.ViewInject;
import com.wenzhoujie.wheelview.TimePopupWindow;
import com.wenzhoujie.wheelview.TimePopupWindow.OnTimeSelectListener;
import com.wenzhoujie.wheelview.TimePopupWindow.Type;

/**
 * 订单详情页（参数设置fragment，如留言，手机号，等。。。）
 * 
 * @author js02
 * 
 */
public class OrderDetailParamsFragment extends BaseFragment implements
		OnClickListener {

	public static final int REQUEST_CODE_SELECT_DELIVERY_ADDRESS = 1;

	@ViewInject(id = R.id.frag_order_detail_params_ll_all)
	private LinearLayout mLlAll = null;

	@ViewInject(id = R.id.frag_order_detail_params_ll_has_delivery_address)
	private LinearLayout mLlHasDeliveryAddress = null;

	@ViewInject(id = R.id.frag_order_detail_params_tv_delivery_address)
	private TextView mTvDeliveryAddress = null;

	@ViewInject(id = R.id.frag_order_detail_params_ll_has_delivery_mode)
	private LinearLayout mLlHasDeliveryMode = null;

	@ViewInject(id = R.id.frag_order_detail_params_tv_delivery_mode)
	private TextView mTvDeliveryMode = null;

	@ViewInject(id = R.id.frag_order_detail_params_ll_has_delivery_time)
	private LinearLayout mLlHasDeliveryTime = null;

	@ViewInject(id = R.id.frag_order_detail_params_tv_delivery_time)
	private TextView mTvDeliveryTime = null;

	@ViewInject(id = R.id.frag_order_detail_params_ll_has_invoice)
	private LinearLayout mLlHasInvoice = null;

	@ViewInject(id = R.id.frag_order_detail_params_tv_invoice)
	private TextView mTvInvoice = null;

	@ViewInject(id = R.id.frag_order_detail_params_ll_has_daijin)
	private LinearLayout mLlHasDaijin = null;

	@ViewInject(id = R.id.frag_order_detail_params_tv_daijin)
	private TextView mTvDaijin = null;

	@ViewInject(id = R.id.frag_order_detail_params_ll_has_leave_message)
	private LinearLayout mLlHasLeaveMessage = null;

	@ViewInject(id = R.id.frag_order_detail_params_tv_leave_message)
	private TextView mTvLeaveMessage = null;

	@ViewInject(id = R.id.frag_order_detail_params_ll_has_mobile)
	private LinearLayout mLlHasMobile = null;

	@ViewInject(id = R.id.frag_order_detail_params_et_mobile)
	private EditText mEtMobile = null;

	protected Order_parmModel mOrderParmModel = null;

	private My_order_detailActModel mOrderDetailModel = null;

	private OrderDetailParamsFragmentListener mListener = null;
	// 时间选择器
	private TimePopupWindow pwTime;

	public void setMy_order_detailActModel(My_order_detailActModel model) {
		this.mOrderDetailModel = model;
		if (mOrderDetailModel != null) {
			this.mOrderParmModel = mOrderDetailModel.getOrder_parm();
		}
	}

	public void setListener(OrderDetailParamsFragmentListener listener) {
		this.mListener = listener;
	}

	// booleans
	/** 是否有配送地址 */
	private boolean mHasDeliveryAddress = false;
	/** 是否有配送方式 */
	private boolean mHasDeliveryMode = false;
	/** 是否有配送日期 */
	private boolean mHasDeliveryTime = false;
	/** 是否有发票信息 */
	private boolean mHasInvoice = false;
	/** 是否有代金券 */
	private boolean mHasDaijin = false;
	/** 是否有留言信息 */
	private boolean mHasLeaveMessage = false;
	/** 是否有手机号码 */
	private boolean mHasMobile = false;

	// ----------提交参数
	/** 配送方式id */
	private String delivery_id = null;
	/** 配送时间id */
	private String deliver_time = null;
	/** 发票抬头 */
	private String tax_title = null;
	/** 优惠券序号 */
	private String ecv_sn = null;
	/** 优惠券密码 */
	private String ecv_pwd = null;
	/** 留言信息 */
	private String content = null;

	private DeliveryModel deliveryModel = null;

	public boolean getHasDeliveryAddress() {
		return mHasDeliveryAddress;
	}

	public boolean getHasDeliveryMode() {
		return mHasDeliveryMode;
	}

	public boolean getHasDeliveryTime() {
		return mHasDeliveryTime;
	}

	public boolean getHasInvoice() {
		return mHasInvoice;
	}

	public boolean getHasDaijin() {
		return mHasDaijin;
	}

	public boolean getHasLeaveMessage() {
		return mHasLeaveMessage;
	}

	public boolean getHasMobile() {
		return mHasMobile;
	}

	public DeliveryModel getDeliveryModel() {
		return deliveryModel;
	}

	public String getDelivery_id() {
		return delivery_id;
	}

	public String getDeliver_time() {
		return deliver_time;
	}

	public String getTax_title() {
		return tax_title;
	}

	public String getEcv_sn() {
		return ecv_sn;
	}

	public String getEcv_pwd() {
		return ecv_pwd;
	}

	public String getContent() {
		return content;
	}

	public String getMobile() {
		return mEtMobile.getText().toString();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.frag_order_detail_params,
				container, false);
		// IocUtil.initInjectedView(OrderDetailParamsFragment.this, view);
		findViews(view);
		init();
		return view;
	}

	private void findViews(View view) {
		mLlAll = (LinearLayout) view
				.findViewById(R.id.frag_order_detail_params_ll_all);
		mLlHasDeliveryAddress = (LinearLayout) view
				.findViewById(R.id.frag_order_detail_params_ll_has_delivery_address);
		mTvDeliveryAddress = (TextView) view
				.findViewById(R.id.frag_order_detail_params_tv_delivery_address);
		mLlHasDeliveryMode = (LinearLayout) view
				.findViewById(R.id.frag_order_detail_params_ll_has_delivery_mode);
		mTvDeliveryMode = (TextView) view
				.findViewById(R.id.frag_order_detail_params_tv_delivery_mode);
		mLlHasDeliveryTime = (LinearLayout) view
				.findViewById(R.id.frag_order_detail_params_ll_has_delivery_time);
		mTvDeliveryTime = (TextView) view
				.findViewById(R.id.frag_order_detail_params_tv_delivery_time);
		mLlHasInvoice = (LinearLayout) view
				.findViewById(R.id.frag_order_detail_params_ll_has_invoice);
		mTvInvoice = (TextView) view
				.findViewById(R.id.frag_order_detail_params_tv_invoice);

		mLlHasDaijin = (LinearLayout) view
				.findViewById(R.id.frag_order_detail_params_ll_has_daijin);
		mTvDaijin = (TextView) view
				.findViewById(R.id.frag_order_detail_params_tv_daijin);

		mLlHasLeaveMessage = (LinearLayout) view
				.findViewById(R.id.frag_order_detail_params_ll_has_leave_message);
		mTvLeaveMessage = (TextView) view
				.findViewById(R.id.frag_order_detail_params_tv_leave_message);

		mLlHasMobile = (LinearLayout) view
				.findViewById(R.id.frag_order_detail_params_ll_has_mobile);
		mEtMobile = (EditText) view
				.findViewById(R.id.frag_order_detail_params_et_mobile);

	}

	private void init() {
		registeClick();
		bindData();
	}

	/**
	 * 获得默认配送方式id
	 * 
	 * @return
	 */
	protected String getDefaultDeliveryModeId() {
		if (mOrderDetailModel != null) {
			return mOrderDetailModel.getDelivery_id();
		} else {
			return null;
		}
	}

	/**
	 * 获得默认配送时间id
	 * 
	 * @return
	 */
	protected String getDefaultDeliveryTimeId() {
		if (mOrderDetailModel != null) {
			return mOrderDetailModel.getDeliver_time_id();
		} else {
			return null;
		}
	}

	protected void bindData() {
		if (mOrderDetailModel != null) {
			setViewsVisibility();
			setCanEditMessage(mOrderDetailModel);
			bindDeliveryAddress(mOrderDetailModel.getDeliveryAddr());
			bindDeliveryMode();
			bindDeliveryTime();
			bindTaxTitle(mOrderDetailModel.getTax_title());
			bindDaijin(mOrderDetailModel.getEvc_sn(),
					mOrderDetailModel.getEvc_pwd());
			bindLeaveMessage(mOrderDetailModel.getContent());
			bindMobileNumber(mOrderDetailModel.getSend_mobile());
		}

	}

	/**
	 * 绑定手机号码
	 * 
	 * @param mobile
	 */
	protected void bindMobileNumber(String mobile) {
		if (mHasMobile) {
			if (AppHelper.isEmptyString(mobile)) {
				mobile = "";
			}
			mEtMobile.setText(mobile);
		}
	}

	/**
	 * 绑定留言信息
	 * 
	 * @param content
	 */
	protected void bindLeaveMessage(String content) {
		if (mHasLeaveMessage) {
			SDViewBinder.setTextView(mTvLeaveMessage, content, "");
			this.content = content;
		}
	}

	/**
	 * 绑定发票抬头
	 * 
	 * @param tax
	 */
	protected void bindTaxTitle(String tax) {
		if (mHasInvoice) {
			SDViewBinder.setTextView(mTvInvoice, tax, "");
			this.tax_title = tax;
		}
	}

	/**
	 * 绑定代金券
	 * 
	 * @param evcSn
	 */
	protected void bindDaijin(String evcSn, String ecvPwd) {
		if (mHasDaijin) {
			SDViewBinder.setTextView(mTvInvoice, evcSn, "");
			this.ecv_sn = evcSn;
			this.ecv_pwd = ecvPwd;
		}
	}

	/**
	 * 绑定配送时间
	 */
	protected void bindDeliveryTime() {
		if (mHasDeliveryTime) {
			if (mOrderParmModel != null) {
				List<Delivery_time_listModel> listDelivery_time_listModels = mOrderParmModel
						.getDelivery_time_list();
				String selectDeliveryTimeId = getDefaultDeliveryTimeId();
				if (!AppHelper.isEmptyString(selectDeliveryTimeId)) {
					if (listDelivery_time_listModels != null
							&& listDelivery_time_listModels.size() > 0) // 绑定配送时间
					{
						for (Delivery_time_listModel delivery_time_listModel : listDelivery_time_listModels) {
							if (selectDeliveryTimeId
									.equals(delivery_time_listModel.getId())) {
								deliver_time = delivery_time_listModel
										.getName();
								SDViewBinder.setTextView(mTvDeliveryTime,
										delivery_time_listModel.getName());
								break;
							}
						}
					}
				}
			}
		}
	}

	/**
	 * 绑定配送方式
	 * 
	 * @param order_parmModel
	 */
	protected void bindDeliveryMode() {
		if (mHasDeliveryMode) {
			if (mOrderParmModel != null) {
				List<Delivery_listModel> listDelivery_listModels = mOrderParmModel
						.getDelivery_list();
				String selectDeliveryModeId = getDefaultDeliveryModeId();
				if (!AppHelper.isEmptyString(selectDeliveryModeId)) {
					if (listDelivery_listModels != null
							&& listDelivery_listModels.size() > 0) // 配送方式
					{
						for (Delivery_listModel delivery_listModel : listDelivery_listModels) {
							if (selectDeliveryModeId.equals(delivery_listModel
									.getId())) {
								delivery_id = delivery_listModel.getId();
								SDViewBinder.setTextView(mTvDeliveryMode,
										delivery_listModel.getName());
								break;
							}
						}
					}
				}
			}
		}
	}

	/**
	 * 绑定配送地址
	 * 
	 * @param deliveryAddr
	 */
	protected void bindDeliveryAddress(DeliveryModel model) {
		if (mHasDeliveryAddress) {
			if (model != null) {
				StringBuilder sb = new StringBuilder();
				if (model.getConsignee() != null) // 收件人
				{
					sb.append(model.getConsignee());
					sb.append("\n");
				}
				if (model.getPhone() != null) // 手机号码
				{
					sb.append(model.getPhone());
					sb.append("\n");
				}
				if (model.getDelivery() != null) // 地址
				{
					sb.append(model.getDelivery());
					sb.append("\n");
				}
				if (model.getDelivery_detail() != null) // 详细地址
				{
					sb.append(model.getDelivery_detail());
				}
				mTvDeliveryAddress.setText(sb.toString());
				deliveryModel = model;
			}
		}
	}

	/**
	 * 根据My_order_detailActModel设置各项信息是否可以被编辑
	 * 
	 * @param model
	 */
	protected void setCanEditMessage(My_order_detailActModel model) {
		if (model != null) {
			if (model.getHas_edit_delivery() == 1) // 地址可以被编辑
			{
				mLlHasDeliveryAddress.setOnClickListener(this);
			} else {
				mLlHasDeliveryAddress.setOnClickListener(null);
			}

			mLlHasDeliveryMode.setOnClickListener(null);

			if (model.getHas_edit_delivery_time() == 1) // 配送时间可以被编辑
			{
				mLlHasDeliveryTime.setOnClickListener(this);
			} else {
				mLlHasDeliveryTime.setOnClickListener(null);
			}

			if (model.getHas_edit_invoice() == 1) // 发票可以被编辑
			{
				mLlHasInvoice.setOnClickListener(this);
			} else {
				mLlHasInvoice.setOnClickListener(null);
			}

			if (model.getHas_edit_ecv() == 1) // 代金券可以被编辑
			{
				mLlHasDaijin.setOnClickListener(this);
			} else {
				mLlHasDaijin.setOnClickListener(null);
			}

			if (model.getHas_edit_message() == 1) // 订单留言可以被编辑
			{
				mLlHasLeaveMessage.setOnClickListener(this);
			} else {
				mLlHasLeaveMessage.setOnClickListener(null);
			}

			if (model.getHas_edit_moblie() == 1) // 手机号可以被编辑
			{
				mEtMobile.setEnabled(true);
			} else {
				mEtMobile.setEnabled(false);
			}
		}

	}

	/**
	 * 是否显示时间选择
	 * 
	 * @return
	 */
	private boolean isMerchantDelivery() {
		boolean result = true;
		if (mLlHasDeliveryMode.getVisibility() == View.VISIBLE
				&& mTvDeliveryMode.getText().toString().equals("到店消费")) {
			result = false;

		} else {
			result = true;
		}
		return result;
	}

	/**
	 * 根据Order_parmModel显示隐藏view
	 * 
	 * @param model
	 */
	protected void setViewsVisibility() {
		if (mOrderParmModel != null) {
			mHasDeliveryAddress = SDViewBinder.setViewsVisibility(
					mLlHasDeliveryAddress, mOrderParmModel.getHas_delivery()); // 设置配送地址是否可见
			mHasDeliveryMode = SDViewBinder.setViewsVisibility(
					mLlHasDeliveryMode, mOrderParmModel.getHas_delivery()); // 设置配送方式是否可见
			mHasDeliveryTime = SDViewBinder.setViewsVisibility(
					mLlHasDeliveryTime, isMerchantDelivery()); // 设置配送日期是否可见
			mHasInvoice = SDViewBinder.setViewsVisibility(mLlHasInvoice,
					mOrderParmModel.getHas_invoice()); // 设置发票是否可见
			mHasDaijin = SDViewBinder.setViewsVisibility(mLlHasDaijin,
					mOrderParmModel.getHas_ecv()); // 设置优惠券是否可见
			mHasLeaveMessage = SDViewBinder.setViewsVisibility(
					mLlHasLeaveMessage, mOrderParmModel.getHas_message()); // 设置留言框是否可见
			mHasMobile = SDViewBinder.setViewsVisibility(mLlHasMobile,
					mOrderParmModel.getHas_moblie()); // 设置手机号码是否可见
		}
	}

	private void registeClick() {
		mLlHasDeliveryAddress.setOnClickListener(this);
		mLlHasDeliveryMode.setOnClickListener(this);
		mLlHasDeliveryTime.setOnClickListener(this);
		mLlHasInvoice.setOnClickListener(this);
		mLlHasDaijin.setOnClickListener(this);
		mLlHasLeaveMessage.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.frag_order_detail_params_ll_has_delivery_address:
			clickDeliveryAddress();
			break;
		case R.id.frag_order_detail_params_ll_has_delivery_mode:
			clickDeliveryMode();
			break;
		case R.id.frag_order_detail_params_ll_has_delivery_time:
			// clickDeliveryTime();
			clickDeliverTimes();
			break;
		case R.id.frag_order_detail_params_ll_has_invoice:
			clickInvoice();
			break;
		case R.id.frag_order_detail_params_ll_has_daijin:
			clickCoupons();
			break;
		case R.id.frag_order_detail_params_ll_has_leave_message:
			clickLeaveMessage();
			break;
		default:
			break;
		}
	}

	public static String getTime(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return dateFormat.format(date.getTime());
	}

	/**
	 * 设置配送时间
	 */
	private void clickDeliverTimes() {
		// 时间选择器
		pwTime = new TimePopupWindow(getActivity(), Type.HOURS_MINS);
		// pwTime.setTime(new Date());
		// 时间选择后回调
		pwTime.setOnTimeSelectListener(new OnTimeSelectListener() {
			@Override
			public void onTimeSelect(Date date) {
				Calendar calendar = Calendar.getInstance();
				long currentTime = System.currentTimeMillis();
				calendar.setTimeInMillis(currentTime);
				// 配送时间延迟30分钟
				calendar.add(Calendar.MINUTE, 30);
				long selectTime = date.getTime();
				if (selectTime < calendar.getTimeInMillis()) {
					Toast.makeText(getActivity(), "配送时间不能小于当前时间", Toast.LENGTH_SHORT).show();

				} else {
					mTvDeliveryTime.setText(getTime(date) + "  左右送达");
					deliver_time = getTime(date);
				}
			}
		});
		pwTime.showAtLocation(mTvDeliveryTime, Gravity.CENTER, 0, 0, new Date());
	}

	/**
	 * 点击收货地址
	 */
	private void clickDeliveryAddress() {
		Intent intent = new Intent(App.getApplication(),
				DeliveryAddressActivty.class);
		intent.putExtra(DeliveryAddressActivty.EXTRA_DELIVERY_MODE, "0");
		startActivityForResult(intent, REQUEST_CODE_SELECT_DELIVERY_ADDRESS);
	}

	/**
	 * 点击配送模式
	 */
	private void clickDeliveryMode() {
		if (mOrderParmModel != null) {
			List<Delivery_listModel> listDelivery_listModels = mOrderParmModel
					.getDelivery_list();
			if (listDelivery_listModels != null
					&& listDelivery_listModels.size() > 0) {
				showDeliveryListDialog(listDelivery_listModels);
			}
		}
	}

	/**
	 * 点击配送日期
	 */
	private void clickDeliveryTime() {
		if (mOrderParmModel != null) {
			List<Delivery_time_listModel> listDeliveryTimeModel = mOrderParmModel
					.getDelivery_time_list();
			if (listDeliveryTimeModel != null
					&& listDeliveryTimeModel.size() > 0) {
				showSelectDeliveryTimeDialog(listDeliveryTimeModel);
			}
		}
	}

	/**
	 * 点击发票抬头
	 */
	private void clickInvoice() {
		View view = LayoutInflater.from(App.getApplication()).inflate(
				R.layout.dialog_input_invoice, null);
		final EditText etContent = (EditText) view
				.findViewById(R.id.dialog_input_invoice_et_content);
		new SDDialogCustom().setTextTitle("请输入发票抬头").setCustomView(view)
				.setmListener(new SDDialogCustomListener() {

					@Override
					public void onDismiss(DialogInterface iDialog,
							SDDialogCustom dialog) {
					}

					@Override
					public void onClickConfirm(View v, SDDialogCustom dialog) {
						tax_title = etContent.getText().toString();
						mTvInvoice.setText(tax_title);
					}

					@Override
					public void onClickCancel(View v, SDDialogCustom dialog) {
					}
				}).show();
	}

	/**
	 * 点击优惠券
	 */
	private void clickCoupons() {
		View view = LayoutInflater.from(App.getApplication()).inflate(
				R.layout.dialog_input_coupons, null);
		final EditText etSn = (EditText) view
				.findViewById(R.id.dialog_input_coupons_et_coupons_sn);
		final EditText etPwd = (EditText) view
				.findViewById(R.id.dialog_input_coupons_et_coupons_pwd);
		new SDDialogCustom().setTextTitle("请输入").setCustomView(view)
				.setmListener(new SDDialogCustomListener() {

					@Override
					public void onDismiss(DialogInterface iDialog,
							SDDialogCustom dialog) {
					}

					@Override
					public void onClickConfirm(View v, SDDialogCustom dialog) {
						ecv_sn = etSn.getText().toString();
						ecv_pwd = etPwd.getText().toString();
						if (!TextUtils.isEmpty(ecv_sn)
								&& !TextUtils.isEmpty(ecv_pwd)) {
							// TODO 检查优惠券是否有效
							requestCheckCoupons();
						}
					}

					@Override
					public void onClickCancel(View v, SDDialogCustom dialog) {
					}
				}).show();
	}

	/**
	 * 点击留言
	 */
	private void clickLeaveMessage() {
		View view = LayoutInflater.from(App.getApplication()).inflate(
				R.layout.dialog_input_invoice, null);
		final EditText etContent = (EditText) view
				.findViewById(R.id.dialog_input_invoice_et_content);
		etContent.setText(content);
		new SDDialogCustom().setTextTitle("请输入留言").setCustomView(view)
				.setmListener(new SDDialogCustomListener() {

					@Override
					public void onDismiss(DialogInterface iDialog,
							SDDialogCustom dialog) {
					}

					@Override
					public void onClickConfirm(View v, SDDialogCustom dialog) {
						content = etContent.getText().toString();
						mTvLeaveMessage.setText(content);
					}

					@Override
					public void onClickCancel(View v, SDDialogCustom dialog) {
					}
				}).show();
	}

	/**
	 * 显示选择配送日期窗口
	 * 
	 * @param listDeliveryTimeModel
	 */
	private void showSelectDeliveryTimeDialog(
			List<Delivery_time_listModel> listDeliveryTimeModel) {
		final SDSimpleTextAdapter adapter = new SDSimpleTextAdapter(
				listDeliveryTimeModel, getActivity());
		new SDDialogMenu().setAdapter(adapter)
				.setmListener(new SDDialogMenuListener() {

					@Override
					public void onItemClick(View v, int index,
							SDDialogMenu dialog) {
						Delivery_time_listModel model = (Delivery_time_listModel) adapter
								.getItem(index);
						if (model != null) {
							SDViewBinder.setTextView(mTvDeliveryTime,
									model.getName());
							deliver_time = model.getName();
						}
						dialog.dismiss();
					}

					@Override
					public void onDismiss(DialogInterface iDialog,
							SDDialogMenu dialog) {
					}

					@Override
					public void onCancelClick(View v, SDDialogMenu dialog) {
					}
				}).show();
	}

	/**
	 * 显示选择配送方式窗口
	 * 
	 * @param listDelivery_listModels
	 */
	private void showDeliveryListDialog(
			List<Delivery_listModel> listDelivery_listModels) {
		final SDSimpleTextAdapter adapter = new SDSimpleTextAdapter(
				listDelivery_listModels, getActivity());
		new SDDialogMenu().setAdapter(adapter)
				.setmListener(new SDDialogMenuListener() {

					@Override
					public void onItemClick(View v, int index,
							SDDialogMenu dialog) {
						Delivery_listModel model = (Delivery_listModel) adapter
								.getItem(index);
						if (model != null) {
							delivery_id = model.getId();
							mTvDeliveryMode.setText(model.getName());
							mHasDeliveryTime = SDViewBinder.setViewsVisibility(
									mLlHasDeliveryTime, isMerchantDelivery());
							if (dialog != null) {
								dialog.dismiss();
							}
							// TODO 请求计算接口
							if (mListener != null) {
								mListener.onCalculate();
							}
						}
						dialog.dismiss();
					}

					@Override
					public void onDismiss(DialogInterface iDialog,
							SDDialogMenu dialog) {
					}

					@Override
					public void onCancelClick(View v, SDDialogMenu dialog) {
					}
				}).show();
	}

	/**
	 * 请求优惠券是否有效接口
	 */
	protected void requestCheckCoupons() {
		if (AppHelper.getLocalUser() != null) {
			RequestModel model = new RequestModel();
			model.put("email", AppHelper.getLocalUser().getUser_name());
			model.put("pwd", AppHelper.getLocalUser().getUser_pwd());
			model.put("ecv_sn", ecv_sn);
			model.put("ecv_pwd", ecv_pwd);
			RequestCallBack<String> handler = new RequestCallBack<String>() {

				@Override
				public void onStart() {
					AppHelper.showLoadingDialog("请稍候");
				}

				@Override
				public void onSuccess(ResponseInfo<String> responseInfo) {
					Check_ecvActModel actModel = JsonUtil.json2Object(
							responseInfo.result, Check_ecvActModel.class);
					if (!SDInterfaceUtil.isActModelNull(actModel)) {
						if (actModel.getResponse_code() == 1) {
							int couponsState = SDTypeParseUtil
									.getIntFromString(
											actModel.getCheck_ecv_state(), 0);
							if (couponsState == 0) // 无效的优惠券
							{
								ecv_sn = null;
								ecv_pwd = null;
							} else {
								SDViewBinder.setTextView(mTvDaijin, ecv_sn);
							}
						}
					}
				}

				@Override
				public void onFailure(HttpException error, String msg) {

				}

				@Override
				public void onFinish() {
					AppHelper.hideLoadingDialog();
				}
			};
			InterfaceServer.getInstance().requestInterface(model, handler);
		} else {
			// TODO 跳到登录界面
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case REQUEST_CODE_SELECT_DELIVERY_ADDRESS:
			if (resultCode == DeliveryAddressActivty.RESULT_CODE_SELECT_DELIVERY_MODEL_SUCCESS) {
				if (data != null) {
					bindDeliveryAddress((DeliveryModel) (data
							.getSerializableExtra(DeliveryAddressActivty.EXTRA_RESULT_DELIVERY_MODEL)));
				}
			}
			break;

		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	public interface OrderDetailParamsFragmentListener {
		public void onCalculate();
	}

}
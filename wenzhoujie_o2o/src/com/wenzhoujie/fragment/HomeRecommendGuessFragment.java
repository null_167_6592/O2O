package com.wenzhoujie.fragment;

import java.util.ArrayList;
import java.util.List;

import com.lingou.www.R;
import com.sunday.eventbus.SDBaseEvent;
import com.wenzhoujie.MainActivity;
import com.wenzhoujie.MerchantListActivity;
import com.wenzhoujie.TuanListActivity;
import com.wenzhoujie.adapter.HomeRecommendDealsAdapter;
import com.wenzhoujie.adapter.HomeRecommendGuessAdapter;
import com.wenzhoujie.event.EnumEventTag;
import com.wenzhoujie.model.IndexActDeal_listModel;
import com.wenzhoujie.model.act.GuessGoodsActModel;
import com.wenzhoujie.model.act.GuessGoodsListActModel;
import com.wenzhoujie.model.act.IndexActNewModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

public class HomeRecommendGuessFragment extends BaseFragment implements OnClickListener {

	@ViewInject(id = R.id.frag_home_recommend_guess_ll_all)
	private LinearLayout mLlAll = null;

	@ViewInject(id = R.id.frag_home_recommend_guess_ll_guess)
	private LinearLayout mLlDeals = null;

	@ViewInject(id = R.id.frag_home_recommend_guess_ll_all_guess)
	private LinearLayout mLlAllDeals = null;

	private IndexActNewModel mIndexModel = null;

	private List<GuessGoodsActModel> mListModel = new ArrayList<GuessGoodsActModel>();

	public void setmIndexModel(IndexActNewModel actModel)
	{
		this.mIndexModel = actModel;
		this.mListModel = mIndexModel.getGuesses_list();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_home_recommend_guess, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		bindData();
		registeClick();
	}

	private void bindData()
	{
		if (mListModel != null && mListModel.size() > 0)
		{
			mLlAll.setVisibility(View.VISIBLE);
			mLlDeals.removeAllViews();
			HomeRecommendGuessAdapter adapter = new HomeRecommendGuessAdapter(mListModel, getActivity());
			int listSize = mListModel.size();
			for (int i = 0; i < listSize; i++)
			{
				View view = adapter.getView(i, null, null);
				mLlDeals.addView(view);
			}
		} else
		{
			mLlAll.setVisibility(View.GONE);
		}
	}

	private void registeClick()
	{
		mLlAllDeals.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.frag_home_recommend_guess_ll_all_guess:
			clickAllGuess();
			break;
		default:
			break;
		}
	}

	private void clickAllGuess()
	{
		startActivity(new Intent(getActivity(), MerchantListActivity.class));
		/*Activity activity = getBaseActivity();
		if (activity != null && activity instanceof MainActivity)
		{
		MainActivity mainActivity = (MainActivity) activity;
		mainActivity.setSelectIndex(1, null, true);
		}*/
	}

	@Override
	public void onEventMainThread(SDBaseEvent event)
	{
		// TODO Auto-generated method stub
		super.onEventMainThread(event);
		switch (EnumEventTag.valueOf(event.getEventTagInt()))
		{
		case LOCATION_SUCCESS:
			evnetLocationSuccess();
			break;

		default:
			break;
		}
	}

	private void evnetLocationSuccess()
	{
		if (mListModel != null)
			bindData();
	}

}

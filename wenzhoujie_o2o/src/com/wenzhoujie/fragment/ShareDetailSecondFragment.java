package com.wenzhoujie.fragment;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.wenzhoujie.library.utils.SDViewBinder;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.lingou.www.R;
import com.wenzhoujie.AlbumActivity;
import com.wenzhoujie.app.App;
import com.wenzhoujie.model.ShareItemCollectModel;
import com.wenzhoujie.model.ShareItemCommentModel;
import com.wenzhoujie.model.ShareItemImageModel;
import com.wenzhoujie.model.ShareItemModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

public class ShareDetailSecondFragment extends BaseFragment
{
	@ViewInject(id = R.id.frag_share_detail_second_ll_LinearAll)
	private LinearLayout mLlLinearAll = null;

	private ShareItemModel mShareItemModel = null;
	private List<ShareItemImageModel> mListShareItemImageModel = new ArrayList<ShareItemImageModel>();
	private List<ShareItemCollectModel> mListShareItemCollectModel = new ArrayList<ShareItemCollectModel>();

	private LinearLayout commentListAll = null;

	private ArrayList<String> mListImageUrl = new ArrayList<String>();

	public void setmShareItemModel(ShareItemModel mShareItemModel)
	{
		this.mShareItemModel = mShareItemModel;
		if (mShareItemModel != null)
		{
			this.mListShareItemCollectModel = mShareItemModel.getCollects();
			this.mListShareItemImageModel = mShareItemModel.getImgs();
			fillImageUrls(mListShareItemImageModel);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_share_detail_second, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		mLlLinearAll.removeAllViews();
		fillContent();
	}

	/**
	 * 填充内容
	 */
	private void fillContent()
	{

		if (mShareItemModel != null)
		{
			LinearLayout llImagesLine = null;
			for (int i = 0; i < mListShareItemImageModel.size(); i++)
			{
				if (i % 3 == 0)
				{
					llImagesLine = new LinearLayout(App.getApplication());
					llImagesLine.setOrientation(LinearLayout.HORIZONTAL);
					LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
					llImagesLine.setLayoutParams(lp);
					mLlLinearAll.addView(llImagesLine);
				}

				ImageView ivImage = new ImageView(App.getApplication());
				ivImage.setLayoutParams(new LinearLayout.LayoutParams(SDViewUtil.getScreenWidth() / 3 - 5, LayoutParams.WRAP_CONTENT));
				ivImage.setScaleType(ScaleType.CENTER_INSIDE);
				ivImage.setPadding(5, 5, 0, 0);
				ivImage.setTag(i);
				ivImage.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						Intent intent = new Intent();
						intent.setClass(App.getApplication(), AlbumActivity.class);
						intent.putStringArrayListExtra(AlbumActivity.EXTRA_LIST_IMAGES, mListImageUrl);
						Object tag = v.getTag();
						if (tag != null)
						{
							try
							{
								int index = (Integer) tag;
								intent.putExtra(AlbumActivity.EXTRA_IMAGES_INDEX, index);
							} catch (Exception e)
							{
							}
						}
						startActivity(intent);
					}
				});
				SDViewBinder.setImageViewByImagesScale(ivImage, mListShareItemImageModel.get(i).getImg());
				llImagesLine.addView(ivImage);

				LinearLayout LinearOne = new LinearLayout(App.getApplication());
				LinearOne.setOrientation(LinearLayout.VERTICAL);

				// 商品 团购 代金券 " 购买 " 按钮
				if (mListShareItemImageModel.get(i).getType().equals("sharegoods") || mListShareItemImageModel.get(i).getType().equals("sharetuan")
						|| mListShareItemImageModel.get(i).getType().equals("sharebyouhui"))
				{
					int img_height = mListShareItemImageModel.get(i).getImg_height();
					int img_width = mListShareItemImageModel.get(i).getImg_width();
					LinearOne.setPadding(400 - 178, ((img_height * 400) / img_width) - 51, 0, 0);
					TextView Price = new TextView(App.getApplication());
					Price.setLayoutParams(new FrameLayout.LayoutParams(180, 50));
					Price.setBackgroundResource(R.drawable.ico_buy);
					Price.setText(" " + mListShareItemImageModel.get(i).getPrice_format());
					Price.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
					Price.setTextSize(18);
					Price.setTextColor(Color.WHITE);

					LinearOne.addView(Price);
					Price.setTag(mListShareItemImageModel.get(i));
					Price.setOnClickListener(new OnClickListener()
					{
						@Override
						public void onClick(View v)
						{
							/*
							 * DetailsImgs img = (DetailsImgs) v.getTag();
							 * Intent intent = new Intent();
							 * intent.setClass(ShareListDetails
							 * .this,GoodsDetailActivity.class );
							 * intent.putExtra("goods_id", img.getData_id()+"");
							 * startActivity(intent);
							 */
						}
					});
					// 活动 优惠 "去看看 " 按钮
				} else if (mListShareItemImageModel.get(i).getType().equals("shareevent") || mListShareItemImageModel.get(i).getType().equals("sharefyouhui"))
				{
					int img_height = mListShareItemImageModel.get(i).getImg_height();
					int img_width = mListShareItemImageModel.get(i).getImg_width();
					LinearOne.setPadding(400 - 120, ((img_height * 400) / img_width) - 49, 0, 0);
					TextView go_to_see = new TextView(App.getApplication());
					go_to_see.setLayoutParams(new FrameLayout.LayoutParams(120, 50));
					go_to_see.setBackgroundResource(R.drawable.ico_go_see);
					go_to_see.setText("去看看");
					go_to_see.setGravity(Gravity.CENTER);
					go_to_see.setTextSize(15);
					go_to_see.setTextColor(Color.BLACK);

					LinearOne.addView(go_to_see);
					go_to_see.setTag(mListShareItemImageModel.get(i));

					if (mListShareItemImageModel.get(i).getType().equals("sharefyouhui"))
					{
						go_to_see.setOnClickListener(new OnClickListener()
						{
							@Override
							public void onClick(View v)
							{
								// DetailsImgs img = (DetailsImgs) v.getTag();
								// Intent intent = new Intent();
								// intent.setClass(ShareListDetails.this,
								// YouhuiDetailActivity.class);
								// intent.putExtra("yh_id", img.getData_id() +
								// "");
								// startActivity(intent);
							}
						});
					} else
					{
						go_to_see.setOnClickListener(new OnClickListener()
						{
							@Override
							public void onClick(View v)
							{
								// DetailsImgs img = (DetailsImgs) v.getTag();
								// Intent intent = new Intent();
								// intent.setClass(ShareListDetails.this,
								// EventsDetailActivity.class);
								// intent.putExtra("event_id",
								// img.getData_id());
								// startActivity(intent);
							}
						});
					}
				}
				mLlLinearAll.addView(LinearOne);

				if ((!"".equals(mListShareItemImageModel.get(i).getData_name())) && mListShareItemImageModel.get(i).getData_name() != null)
				{
					// 设置图片下文字描述
					TextView MiaoShu = new TextView(App.getApplication());
					MiaoShu.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
					MiaoShu.setBackgroundColor(Color.GRAY);
					MiaoShu.setText(mListShareItemImageModel.get(i).getData_name());
					MiaoShu.setTextColor(Color.WHITE);
					MiaoShu.setGravity(Gravity.CENTER);
					mLlLinearAll.addView(MiaoShu);
				}
			}

			// 分享Text设置
			if (mListShareItemCollectModel != null && mListShareItemCollectModel.size() > 0)
			{
				TextView fenxiang = new TextView(App.getApplication());
				fenxiang.setText("她们喜欢这个分享...");
				fenxiang.setPadding(10, 10, 10, 10);
				fenxiang.setTextColor(Color.BLUE);
				mLlLinearAll.addView(fenxiang);

				// 分享头像设置
				LinearLayout fenxiangL = new LinearLayout(App.getApplication());
				fenxiangL.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, 80));
				fenxiangL.setOrientation(LinearLayout.HORIZONTAL);
				for (int j = 0; j < mListShareItemCollectModel.size(); j++)
				{
					ImageView collectsImage = new ImageView(App.getApplication());
					collectsImage.setLayoutParams(new LinearLayout.LayoutParams(100, 70));
					collectsImage.setPadding(10, 10, 10, 10);
					SDViewBinder.setImageView(collectsImage, mListShareItemCollectModel.get(j).getUser_avatar());
					fenxiangL.addView(collectsImage);
				}
				mLlLinearAll.addView(fenxiangL);
			}

			// 评论Text

			loadcommentListLinear(mShareItemModel.getComments());
		}

	}

	private void fillImageUrls(List<ShareItemImageModel> listImages)
	{
		if (listImages != null && listImages.size() > 0)
		{
			for (ShareItemImageModel imageModel : listImages)
			{
				String url = imageModel.getImg();
				mListImageUrl.add(url);
			}
		}
	}

	private void loadcommentListLinear(ShareItemCommentModel shareListDetailsItemCommentsModel)
	{

		if (shareListDetailsItemCommentsModel.getList().size() > 0)
		{
			TextView pinglun = new TextView(App.getApplication());
			pinglun.setText("评论...");
			pinglun.setPadding(10, 10, 10, 10);
			pinglun.setTextColor(Color.BLUE);
			mLlLinearAll.addView(pinglun);

		}
	}

}
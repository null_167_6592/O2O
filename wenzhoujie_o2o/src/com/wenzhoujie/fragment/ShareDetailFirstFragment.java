package com.wenzhoujie.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.LoginNewActivity;
import com.wenzhoujie.UserCenterActivity;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.ShareItemModel;
import com.wenzhoujie.model.act.ShareActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.SDMyImageGetterUtil;
import com.wenzhoujie.utils.ViewInject;

public class ShareDetailFirstFragment extends BaseFragment implements OnClickListener
{
	@ViewInject(id = R.id.frag_share_detail_first_iv_userImage)
	private ImageView mIvShareuserimage = null;

	@ViewInject(id = R.id.frag_share_detail_first_tv_userName)
	private TextView mTvUsername = null;

	@ViewInject(id = R.id.frag_share_detail_first_iv_guanZhu)
	private ImageView mIvGuanzhu = null;

	@ViewInject(id = R.id.frag_share_detail_first_tv_userSource)
	private TextView mTvUsersource = null;

	@ViewInject(id = R.id.frag_share_detail_first_tv_time)
	private TextView mTvTime = null;

	@ViewInject(id = R.id.frag_share_detail_first_tv_content)
	private TextView mTvContent = null;

	private ShareItemModel mShareItemModel = null;

	private String share_id = null;

	private String uid = null;

	public void setShareListDetailsModel(ShareItemModel shareItemModel)
	{
		this.mShareItemModel = shareItemModel;
		if (shareItemModel != null)
		{
			this.share_id = shareItemModel.getShare_id();
			this.uid = shareItemModel.getUid();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_share_detail_first, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		registeClick();
		fillContent();
	}

	private void bindIsCollect(int isCollect)
	{
		if (isCollect == 0)
		{
			mIvGuanzhu.setImageResource(R.drawable.guanzhu_uncollect);
		} else if (isCollect == 1)
		{
			mIvGuanzhu.setImageResource(R.drawable.guanzhu_collect);
		}
	}

	/**
	 * 填充内容
	 */
	private void fillContent()
	{
		if (mShareItemModel != null)
		{
			// 设置是否关注了

			setIsFollow();

			// 加载用户头像
			SDViewBinder.setImageView(mIvShareuserimage, mShareItemModel.getUser_avatar());

			// 用户名
			mTvUsername.setText(mShareItemModel.getUser_name());

			// 来源和时间
			mTvUsersource.setText(mShareItemModel.getSource());
			mTvTime.setText(mShareItemModel.getTime());

			// 标题名称(含表情替换)

			if (mShareItemModel.getParse_expres().size() != 0)
			{
				String b = mShareItemModel.getContent();
				for (int i1 = 0; i1 < mShareItemModel.getParse_expres().size(); i1++)
				{
					String c = "\\[\\w*" + mShareItemModel.getParse_expres().get(i1).getKey() + "\\]";// expresList.get(i).getKey().toString();
					// \[[衰]\]
					String g = "<img src='" + mShareItemModel.getParse_expres().get(i1).getValue() + "'/>";
					String d = b.replaceAll(c, g);
					b = d;

					Spanned text2 = Html.fromHtml(b, new SDMyImageGetterUtil(getActivity(), mTvContent), null);
					mTvContent.setText(text2);
				}
			} else if (mShareItemModel.getParse_expres().size() == 0)
			{
				String textText = mShareItemModel.getContent();
				mTvContent.setText(textText);
			}
		}

	}

	private void setIsFollow()
	{
		if (mShareItemModel != null)
		{
			// 设置是否关注了
			Integer isFollow = mShareItemModel.getIs_follow_user();
			if (isFollow != null)
			{
				if (isFollow == 0)
				{
					mIvGuanzhu.setImageResource(R.drawable.guanzhu_uncollect);
				} else if (isFollow == 1)
				{
					mIvGuanzhu.setImageResource(R.drawable.guanzhu_collect);
				} else
				{
					mIvGuanzhu.setImageResource(R.drawable.guanzhu_uncollect);
				}
			} else
			{
				mIvGuanzhu.setImageResource(R.drawable.guanzhu_uncollect);
			}
		}
	}

	private void registeClick()
	{
		mIvGuanzhu.setOnClickListener(this);
		mIvShareuserimage.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{

		case R.id.frag_share_detail_first_iv_userImage:
			clickUserImage();
			break;

		case R.id.frag_share_detail_first_iv_guanZhu:
			clickGuanzhu();
			break;

		default:
			break;
		}
	}

	private void clickUserImage()
	{
		if (mShareItemModel != null && !AppHelper.isEmptyString(mShareItemModel.getUid()))
		{
			Intent intent = new Intent();
			intent.setClass(getActivity(), UserCenterActivity.class);
			intent.putExtra(UserCenterActivity.EXTRA_UID_ID, mShareItemModel.getUid());
			startActivity(intent);
		}
	}

	/**
	 * 关注
	 */
	private void clickGuanzhu()
	{
		requestGuanzhu();
	}

	/**
	 * 关注接口
	 */
	private void requestGuanzhu()
	{
		if (AppHelper.isEmptyString(share_id))
		{
			SDToast.showToast("分享id为空");
			return;
		}

		if (!AppHelper.isLogin())
		{
			startActivity(new Intent(getActivity(), LoginNewActivity.class));
			return;
		}

		if (AppHelper.getLocalUser().getUser_id().equals(uid))
		{
			SDToast.showToast("不能关注自己");
			return;
		}

		RequestModel model = new RequestModel();
		model.put("act", "share");
		model.put("act_2", "follow");
		model.put("email", AppHelper.getLocalUser().getUser_name());
		model.put("pwd", AppHelper.getLocalUser().getUser_pwd());
		model.put("share_id", share_id);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{
				AppHelper.showLoadingDialog("请稍候...");
			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				ShareActModel actModel = JsonUtil.json2Object(responseInfo.result, ShareActModel.class);
				if (!SDInterfaceUtil.isActModelNull(actModel))
				{
					if (actModel.getResponse_code() == 1)
					{
						ShareItemModel shareItem = actModel.getItem();
						if (shareItem != null)
						{
							int isCollect = shareItem.getIs_follow_user();
							bindIsCollect(isCollect);
						} else
						{
							SDToast.showToast("未知原因关注失败");
						}
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				AppHelper.hideLoadingDialog();
			}
		};
		InterfaceServer.getInstance().requestInterface(model, handler);
	}

}
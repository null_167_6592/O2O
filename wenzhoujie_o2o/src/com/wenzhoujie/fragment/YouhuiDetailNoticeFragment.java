package com.wenzhoujie.fragment;

import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lingou.www.R;
import com.wenzhoujie.model.act.YouhuiitemActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 优惠详情 （优惠券须知fragment）
 * 
 * @author js02
 * 
 */
public class YouhuiDetailNoticeFragment extends BaseFragment
{

	@ViewInject(id = R.id.ll_all)
	private LinearLayout mLlAll = null;

	@ViewInject(id = R.id.tv_youhui_notice)
	private TextView mTvMoreSuppliers = null;

	private YouhuiitemActModel mYouhuiitemActModel = null;

	public void setmYouhuiitemActModel(YouhuiitemActModel mYouhuiitemActModel)
	{
		this.mYouhuiitemActModel = mYouhuiitemActModel;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_youhui_detail_notice, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		bindData();
	}

	private void bindData()
	{

		if (mYouhuiitemActModel != null)
		{
			String notice = mYouhuiitemActModel.getUse_notice();
			if (!TextUtils.isEmpty(notice))
			{
				mLlAll.setVisibility(View.VISIBLE);
				mTvMoreSuppliers.setText(Html.fromHtml(notice));
			} else
			{
				mLlAll.setVisibility(View.GONE);
			}
		} else
		{
			mLlAll.setVisibility(View.GONE);
		}
	}

}
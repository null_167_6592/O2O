package com.wenzhoujie.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.lingou.www.R;
import com.wenzhoujie.model.act.YouhuiitemActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 优惠详情 （详情fragment）
 * 
 * @author js02
 * 
 */
public class YouhuiDetailDetailFragment extends BaseFragment
{

	@ViewInject(id = R.id.ll_all)
	private LinearLayout mLlAll = null;

	private WebViewFragment mFragWeb = null;

	private YouhuiitemActModel mYouhuiitemActModel = null;

	public void setmYouhuiitemActModel(YouhuiitemActModel mYouhuiitemActModel)
	{
		this.mYouhuiitemActModel = mYouhuiitemActModel;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_youhui_detail_detail, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		bindData();
	}

	private void bindData()
	{
		if (mYouhuiitemActModel != null)
		{
			String detail = mYouhuiitemActModel.getContent();
			if (!TextUtils.isEmpty(detail))
			{
				mFragWeb = new WebViewFragment();
				mFragWeb.setHtmlContent(detail);
				mFragWeb.setScaleToShowAll(false);
				mFragWeb.setSupportZoom(true);
				mFragWeb.setSupportZoomControls(true);
				replaceFragment(mFragWeb, R.id.frag_youhui_detail_detail_fl_webview);
			} else
			{
				mLlAll.setVisibility(View.GONE);
			}
		} else
		{
			mLlAll.setVisibility(View.GONE);
		}
	}
}
package com.wenzhoujie.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.lingou.www.R;
import com.wenzhoujie.AppWebViewActivity;
import com.wenzhoujie.fragment.WebViewFragment.WebViewFragmentListener;
import com.wenzhoujie.utils.IocUtil;

/**
 * 首页专题fragment
 * 
 * @author Administrator
 * 
 */
public class HomeZtFragment extends HomeBaseFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.frag_home_zt, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	protected void init() {
		bindData();
	}

	private void bindData() {
		if (mIndexModel == null) {
			return;
		}

		String ztHtml = mIndexModel.getZt_html();
		if (TextUtils.isEmpty(ztHtml)) {
			return;
		}

		WebViewFragment frag = new WebViewFragment();
		frag.setHtmlContent(ztHtml);
		frag.setmListener(new WebViewFragmentListener() {

			@Override
			public void onInitFinish(WebView webView) {

			}

			@Override
			public boolean onLoadUrl(WebView webView, String url) {
				if (!TextUtils.isEmpty(url)) {
					if ("about:blank".equals(url)) {

					} else {
						Intent intent = new Intent(getActivity(),
								AppWebViewActivity.class);
						intent.putExtra(AppWebViewActivity.EXTRA_URL, url);
						startActivity(intent);
					}
				}
				return true;
			}
		});

		replaceFragment(frag, R.id.frag_home_zt_fl_content);
	}

}

package com.wenzhoujie.fragment;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.lingou.www.R;
import com.wenzhoujie.adapter.HomeRecommendGoodsAdapter;
import com.wenzhoujie.customview.SDMoreView;
import com.wenzhoujie.customview.SDMoreViewBaseAdapter;
import com.wenzhoujie.model.IndexActDeal_listModel;
import com.wenzhoujie.model.MerchantdetailClassifyModel;
import com.wenzhoujie.model.act.MerchantitemActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 商家详细页商家其他商品
 * 
 * @author js02
 * 
 */
public class MerchantDetailGoodsFragment extends BaseFragment implements OnClickListener
{

	@ViewInject(id = R.id.frag_merchant_detail_goods_ll_all)
	private LinearLayout mLlAll = null;

	@ViewInject(id = R.id.frag_merchant_detail_goods_smv_more)
	private SDMoreView mSmvMore = null;

	private List<IndexActDeal_listModel> mListModel = new ArrayList<IndexActDeal_listModel>();

	private MerchantitemActModel mMerchantitemActModel = null;
	
	private HomeRecommendGoodsAdapter adapter;

	public void setmMerchantitemActModel(MerchantitemActModel merchantitemActModel)
	{
		this.mMerchantitemActModel = merchantitemActModel;
		this.mListModel = mMerchantitemActModel.getGoods_list();
	}
	
	public void setmMerchantitemActModelNotify(List<IndexActDeal_listModel> listModelto)
	{
		this.mListModel.clear();
		this.mListModel .addAll(listModelto);
		adapter = new HomeRecommendGoodsAdapter(mListModel, getActivity());
		mSmvMore.setAdapter(new SDMoreViewBaseAdapter()
		{

			@Override
			public View getView(int pos)
			{
				return adapter.getView(pos, null, null);
			}

			@Override
			public int getMaxShowCount()
			{
				return 3;
			}

			@Override
			public int getListCount()
			{
				return mListModel.size();
			}

			@Override
			public String getClickMoreString()
			{
				return "查看更多";
			}

			@Override
			public String getClickBackString()
			{
				return "点击收回";
			}

			@Override
			public boolean clickMore(View v)
			{
				return false;
			}
		});
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_merchant_detail_goods, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		bindData();
	}

	private void bindData()
	{
		if (mListModel != null && mListModel.size() > 0)
		{
			mLlAll.setVisibility(View.VISIBLE);
			adapter = new HomeRecommendGoodsAdapter(mListModel, getActivity());
			mSmvMore.setAdapter(new SDMoreViewBaseAdapter()
			{

				@Override
				public View getView(int pos)
				{
					return adapter.getView(pos, null, null);
				}

				@Override
				public int getMaxShowCount()
				{
					return 3;
				}

				@Override
				public int getListCount()
				{
					return mListModel.size();
				}

				@Override
				public String getClickMoreString()
				{
					return "查看更多";
				}

				@Override
				public String getClickBackString()
				{
					return "点击收回";
				}

				@Override
				public boolean clickMore(View v)
				{
					return false;
				}
			});
		} else
		{
			mLlAll.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		default:
			break;
		}
	}

}
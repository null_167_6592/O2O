package com.wenzhoujie.fragment;

import android.text.TextUtils;

import com.wenzhoujie.config.O2oConfig;
import com.wenzhoujie.constant.ApkConstant;
import com.wenzhoujie.library.utils.SDBase64;
import com.wenzhoujie.library.utils.UrlLinkBuilder;

public class AppWebViewFragment extends WebViewFragment
{

	@Override
	public void setUrl(String url)
	{
		this.mStrUrl = wrapperUrl(url);
	}

	private String wrapperUrl(String url)
	{
		String resultUrl = putSessionId(url);
		resultUrl = putPageType(resultUrl);
		resultUrl = putRefId(resultUrl);
		return resultUrl;
	}

	private String putPageType(String url)
	{
		if (TextUtils.isEmpty(url))
		{
			url = new UrlLinkBuilder(url).add("page_type", "app").build();
		}
		return url;
	}

	private String putRefId(String url)
	{
		String refId = O2oConfig.getRefId();
		if (!TextUtils.isEmpty(url) && !TextUtils.isEmpty(refId))
		{
			url = new UrlLinkBuilder(url).add("r", SDBase64.encode(refId)).build();
		}
		return url;
	}

	private String putSessionId(String url)
	{
		// 如果是域名的url，添加session
		if (url != null && url.contains(ApkConstant.SERVER_API_URL_MID))
		{
			String sessionId = O2oConfig.getSessionId();
			if (sessionId != null)
			{
				url = new UrlLinkBuilder(url).add("sess_id", sessionId).build();
			}
		}
		return url;
	}
	
}

package com.wenzhoujie.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.model.act.My_order_detailActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 订单详情页面(订单信息fragment)
 * 
 * @author js02
 * 
 */
public class OrderDetailInfoFragment extends BaseFragment
{

	@ViewInject(id = R.id.frag_order_detail_info_ll_all)
	private LinearLayout mLlAll = null;

	@ViewInject(id = R.id.frag_order_detail_info_tv_order_code)
	private TextView mTvOrderCode = null;

	@ViewInject(id = R.id.frag_order_detail_info_tv_order_price)
	private TextView mTvOrderPrice = null;

	@ViewInject(id = R.id.frag_order_detail_info_tv_order_status)
	private TextView mTvOrderStatus = null;

	@ViewInject(id = R.id.frag_order_detail_info_tv_create_time_format)
	private TextView mTvCreateTimeFormat = null;

	@ViewInject(id = R.id.frag_order_detail_info_tv_goods_number)
	private TextView mTvGoodsNumber = null;

	private My_order_detailActModel mModel = null;

	public void setMy_order_detailActModel(My_order_detailActModel model)
	{
		this.mModel = model;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.frag_order_detail_info, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		bindData();
	}

	private void bindData()
	{
		if (mModel != null)
		{
			mLlAll.setVisibility(View.VISIBLE);

			SDViewBinder.setTextView(mTvOrderCode, mModel.getSn(), "未找到"); // 订单编号
			SDViewBinder.setTextView(mTvOrderPrice, mModel.getTotal_money_format(), "未找到"); // 订单金额
			mTvOrderStatus.setText(mModel.getStatus_format());// 订单状态

			SDViewBinder.setTextView(mTvCreateTimeFormat, mModel.getCreate_time_format()); // 下单时间
			mTvGoodsNumber.setText(String.valueOf(SDTypeParseUtil.getIntFromString(mModel.getNum(), 0))); // 商品数量

		} else
		{
			mLlAll.setVisibility(View.GONE);
		}

	}

}
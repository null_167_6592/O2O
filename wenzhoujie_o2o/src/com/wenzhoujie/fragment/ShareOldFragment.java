package com.wenzhoujie.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lingou.www.R;
import com.wenzhoujie.BaseActivity;
import com.wenzhoujie.MineActivity;
import com.wenzhoujie.TakephotosActivity;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 分享fragment
 * 
 * @author js02
 * 
 */
public class ShareOldFragment extends BaseFragment implements OnClickListener
{

	@ViewInject(id = R.id.act_mine_information_rl_top)
	private RelativeLayout mRlTop = null;

	@ViewInject(id = R.id.act_mine_information_fl_top)
	private FrameLayout mFlTop = null;

	@ViewInject(id = R.id.act_mine_information_tv_title)
	private TextView mTvTitle = null;

	@ViewInject(id = R.id.act_mine_information_rl_commentmys_back)
	private RelativeLayout mRlCommentmysBack = null;

	@ViewInject(id = R.id.act_mine_information_btn_takephotos)
	private ImageView mBtnTakephotos = null;

	@ViewInject(id = R.id.act_mine_information_rb_radio_button1)
	private RadioButton mRbRadioButton1 = null;

	@ViewInject(id = R.id.act_mine_information_rb_radio_button3)
	private RadioButton mRbRadioButton3 = null;

	@ViewInject(id = R.id.act_mine_information_tv_information_more)
	private TextView mTvInformationMore = null;

	@ViewInject(id = R.id.act_mine_information_fl_container)
	private FrameLayout mFlContainer = null;

	private MyFollowFragment mFragFollow;
	private UserShareByUidFragment mFragMyPublish;

	private int mark = 1;
	private String tag = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.act_mine_information, container, false);
		IocUtil.initInjectedView(this, view);
		init();
		return view;
	}

	private void init()
	{
		registeClick();

		if ("personcenter".equals(tag))
		{
			clickRB3();
			tag = null;
		} else
		{
			mFragFollow = new MyFollowFragment();
			replaceFragment(mFragFollow, R.id.act_mine_information_fl_container);
		}

	}

	public void setTag(String tag)
	{
		this.tag = tag;
	}

	private void registeClick()
	{
		mBtnTakephotos.setOnClickListener(this);
		mRbRadioButton1.setOnClickListener(this);
		mRbRadioButton3.setOnClickListener(this);
		mTvInformationMore.setOnClickListener(this);
		mRlCommentmysBack.setOnClickListener(this);

	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.act_mine_information_btn_takephotos:
			clickTakePhotos();
			break;

		case R.id.act_mine_information_rl_commentmys_back:
			clickBack();
			break;

		case R.id.act_mine_information_rb_radio_button1:
			clickRB1();
			break;

		case R.id.act_mine_information_rb_radio_button3:
			clickRB3();
			break;

		case R.id.act_mine_information_tv_information_more:
			cliacMore();
			break;

		default:
			break;
		}
	}

	private void clickTakePhotos()
	{
		startActivity(new Intent(getActivity(), TakephotosActivity.class));
	}

	private void clickBack()
	{
		mRlTop.setVisibility(View.VISIBLE);
		mFlTop.setVisibility(View.GONE);
		BaseActivity baseActivity = getBaseActivity();
		if (mark == 1)
		{
			mRbRadioButton1.setBackgroundResource(R.drawable.ico_btn_left_select);
			mRbRadioButton1.setTextColor(Color.parseColor("#FF6900"));
			mRbRadioButton3.setBackgroundResource(R.drawable.ico_btn_right);
			mRbRadioButton3.setTextColor(Color.parseColor("#FFFFFF"));
			mFragFollow = new MyFollowFragment();
			if (baseActivity != null)
			{
				baseActivity.replaceFragment(mFragFollow, R.id.act_mine_information_fl_container);
			}
		} else
		{
			mRbRadioButton1.setBackgroundResource(R.drawable.ico_btn_left);
			mRbRadioButton1.setTextColor(Color.parseColor("#FFFFFF"));
			mRbRadioButton3.setBackgroundResource(R.drawable.ico_btn_right_select);
			mRbRadioButton3.setTextColor(Color.parseColor("#FF6900"));
			mFragMyPublish = new UserShareByUidFragment();
			if (baseActivity != null)
			{
				baseActivity.replaceFragment(mFragMyPublish, R.id.act_mine_information_fl_container);
			}
		}
	}

	/**
	 * 更多
	 */
	private void cliacMore()
	{
		startActivity(new Intent(getActivity(), MineActivity.class));
	}

	/**
	 * 我发表的
	 */
	private void clickRB3()
	{
		mark = 3;
		mRbRadioButton1.setBackgroundResource(R.drawable.ico_btn_left);
		mRbRadioButton1.setTextColor(Color.parseColor("#FFFFFF"));
		mRbRadioButton3.setBackgroundResource(R.drawable.ico_btn_right_select);
		mRbRadioButton3.setTextColor(Color.parseColor("#FF6900"));
		mFragMyPublish = new UserShareByUidFragment();
		BaseActivity baseActivity = getBaseActivity();
		if (baseActivity != null)
		{
			baseActivity.replaceFragment(mFragMyPublish, R.id.act_mine_information_fl_container);
		}
	}

	/**
	 * 关注的人
	 */
	private void clickRB1()
	{
		mark = 1;
		mRbRadioButton1.setBackgroundResource(R.drawable.ico_btn_left_select);
		mRbRadioButton1.setTextColor(Color.parseColor("#FF6900"));
		mRbRadioButton3.setBackgroundResource(R.drawable.ico_btn_right);
		mRbRadioButton3.setTextColor(Color.parseColor("#FFFFFF"));
		mFragFollow = new MyFollowFragment();
		BaseActivity baseActivity = getBaseActivity();
		if (baseActivity != null)
		{
			baseActivity.replaceFragment(mFragFollow, R.id.act_mine_information_fl_container);
		}
	}

}
package com.wenzhoujie.dao;

import com.wenzhoujie.model.LocalUserModel;

public class LocalUserModelDao
{

	public static boolean saveModel(LocalUserModel model)
	{
		return JsonDbModelDaoX.getInstance().insertOrUpdateJsonDbModel(model, true);

	}

	public static LocalUserModel getModel()
	{
		return JsonDbModelDaoX.getInstance().queryJsonDbModel(LocalUserModel.class, true);
	}

	public static void deleteAllModel()
	{
		JsonDbModelDaoX.getInstance().deleteJsonDbModel(LocalUserModel.class);
	}

}

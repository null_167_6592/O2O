package com.wenzhoujie.dao;

import com.wenzhoujie.model.act.InitActModel;

public class InitActModelDao
{
	public static boolean insertOrUpdateModel(InitActModel model)
	{
		return JsonDbModelDaoX.getInstance().insertOrUpdateJsonDbModel(model);
	}

	public static InitActModel queryModel()
	{
		return JsonDbModelDaoX.getInstance().queryJsonDbModel(InitActModel.class);
	}

}

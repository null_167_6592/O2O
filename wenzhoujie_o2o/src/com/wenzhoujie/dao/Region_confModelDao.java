package com.wenzhoujie.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.text.TextUtils;

import com.j256.ormlite.dao.Dao;
import com.lidroid.xutils.db.sqlite.Selector;
import com.lidroid.xutils.exception.DbException;
import com.wenzhoujie.common.DbManagerX;
import com.wenzhoujie.dao.DbHelper;
import com.wenzhoujie.model.Region_confModel;

public class Region_confModelDao
{

	private static Region_confModelDao mInstance = null;
	private Dao<Region_confModel, Integer> mDao = null;

	private Region_confModelDao()
	{
		mDao = DbHelper.getInstance().getDao(Region_confModel.class);
	}

	public static Region_confModelDao getInstance()
	{
		if (mInstance == null)
		{
			syncInit();
		}
		return mInstance;
	}
	
	private static synchronized void syncInit()
	{
		if (mInstance == null)
		{
			mInstance = new Region_confModelDao();
		}
	}

	public boolean executeSql(String sql)
	{
		if (!TextUtils.isEmpty(sql))
		{
			try
			{
				mDao.executeRawNoArgs(sql);
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return false;
	}

	public boolean deleteAllData()
	{
		return executeSql("delete from region_conf");
	}

	public List<Region_confModel> getCountryList()
	{
		return getListByPid("0");
	}

	public List<Region_confModel> getListByPid(String pid)
	{
		if (!TextUtils.isEmpty(pid))
		{
			try
			{
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("pid", pid);
				return mDao.queryForFieldValuesArgs(map);
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	public List<Region_confModel> getListById(String id)
	{
		if (!TextUtils.isEmpty(id))
		{
			try
			{
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("id", id);
				return mDao.queryForFieldValuesArgs(map);
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	public List<Region_confModel> getListByParentModel(Region_confModel parent)
	{
		return getListByPid(parent.getId());
	}

}

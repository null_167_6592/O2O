package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.adapter.MyYouhuiListAdapter;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.MyyouhuilistActItemModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.MyyouhuilistActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

public class MyYouhuiListActivity extends BaseActivity
{

	@ViewInject(id = R.id.act_my_youhui_list_ptrlv_youhui)
	private PullToRefreshListView mPtrlvYouhui = null;

	@ViewInject(id = R.id.act_my_youhui_list_iv_empty)
	private ImageView mIvEmpty = null;

	private MyYouhuiListAdapter mAdapter = null;
	private List<MyyouhuilistActItemModel> mListModel = new ArrayList<MyyouhuilistActItemModel>();

	private int mPage = 1;
	private int mPageTotal = 0;

	private String mStrStatus = "0";

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_my_youhui_list);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		bindDefaultData();
		initPullToRefreshListView();
	}

	private void initPullToRefreshListView()
	{
		mPtrlvYouhui.setMode(Mode.BOTH);
		mPtrlvYouhui.setOnRefreshListener(new OnRefreshListener2<ListView>()
		{
			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				refreshData();
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				loadMoreData();
			}
		});
		mPtrlvYouhui.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position, long id)
			{
				MyyouhuilistActItemModel model = mAdapter.getItem((int) id);
				if (model != null && !TextUtils.isEmpty(model.getId()))
				{
					Intent intent = new Intent(getApplicationContext(), YouHuiDetailActivity.class);
					intent.putExtra(YouHuiDetailActivity.EXTRA_YOUHUI_ID, model.getId());
					startActivity(intent);
				}
			}
		});

		mPtrlvYouhui.setRefreshing();
	}

	protected void refreshData()
	{
		mPage = 1;
		requestYouhuiList(false);

	}

	protected void loadMoreData()
	{
		mPage++;
		if (mPage > mPageTotal && mPageTotal != 0)
		{
			SDToast.showToast("没有更多数据了");
			mPtrlvYouhui.onRefreshComplete();
		} else
		{
			requestYouhuiList(true);
		}

	}

	private void requestYouhuiList(final boolean isLoadMore)
	{
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (user != null)
		{
			RequestModel model = new RequestModel();
			model.put("act", "myyouhuilist");
			model.put("email", user.getUser_name());
			model.put("pwd", user.getUser_pwd());
			// model.put("city_id", fanweApp.getCurCityId());
			model.put("status_type_id", mStrStatus);
			model.put("page", mPage);
			RequestCallBack<String> handler = new RequestCallBack<String>()
			{

				@Override
				public void onStart()
				{
					AppHelper.showLoadingDialog("请稍候...");
				}

				@Override
				public void onSuccess(ResponseInfo<String> responseInfo)
				{
					MyyouhuilistActModel actModel = JsonUtil.json2Object(responseInfo.result,
							MyyouhuilistActModel.class);
					if (!SDInterfaceUtil.isActModelNull(actModel))
					{
						if (actModel.getResponse_code() == 1)
						{
							if (actModel.getPage() != null)
							{
								mPage = actModel.getPage().getPage();
								mPageTotal = actModel.getPage().getPage_total();
							}
							SDViewUtil.updateAdapterByList(mListModel, actModel.getItem(), mAdapter, isLoadMore);
						}
					}
				}

				@Override
				public void onFailure(HttpException error, String msg)
				{

				}

				@Override
				public void onFinish()
				{
					AppHelper.hideLoadingDialog();
					mPtrlvYouhui.onRefreshComplete();
					SDViewUtil.toggleEmptyMsgByList(mListModel, mIvEmpty);
				}
			};
			InterfaceServer.getInstance().requestInterface(model, handler);
		}
	}

	private void bindDefaultData()
	{
		mAdapter = new MyYouhuiListAdapter(mListModel, this);
		mPtrlvYouhui.setAdapter(mAdapter);
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("我的优惠券");
	}

}
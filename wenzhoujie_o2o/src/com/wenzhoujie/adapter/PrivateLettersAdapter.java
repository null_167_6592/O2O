package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.PrivatelettersItemListActivity;
import com.wenzhoujie.model.PrivateLettersActivityMsg_listModel;
import com.wenzhoujie.utils.SDMyImageGetterUtil;
import com.wenzhoujie.utils.ViewHolder;

public class PrivateLettersAdapter extends SDBaseAdapter<PrivateLettersActivityMsg_listModel>
{

	public PrivateLettersAdapter(List<PrivateLettersActivityMsg_listModel> listModel, Activity activity)
	{
		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_private_letters, null);
		}
		ImageView ivUserAvatar = ViewHolder.get(convertView, R.id.item_private_letters_iv_useravatar);
		TextView tvTitle = ViewHolder.get(convertView, R.id.item_private_letters_tv_msg_titles);
		TextView tvCounts = ViewHolder.get(convertView, R.id.item_private_letters_tv_msg_counts);
		TextView tvContent = ViewHolder.get(convertView, R.id.item_private_letters_tv_msg_content);
		TextView tvTime = ViewHolder.get(convertView, R.id.item_private_letters_tv_msg_time);

		final PrivateLettersActivityMsg_listModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setImageView(ivUserAvatar, model.getUser_avatar());
			String text_a = model.getTuser_name();
			String ty = model.getUser_name() + "对<font color=red>" + text_a + "</font>说:";
			String tm = text_a.replace(text_a, ty);
			Spanned text = Html.fromHtml(tm);
			SDViewBinder.setTextView(tvTitle, text);
			SDViewBinder.setTextView(tvCounts, model.getMsg_count());

			String b = model.getContent();
			if (model.getParse_expres() != null)
			{
				if (model.getParse_expres().size() != 0)
				{
					for (int m = 0; m < model.getParse_expres().size(); m++)
					{

						String c = "\\[\\w*" + model.getParse_expres().get(m).getKey() + "\\]";
						String g = "<img src='" + model.getParse_expres().get(m).getValue() + "'/>";
						String d = b.replace(c, g);

						b = d;

						Spanned Stext = Html.fromHtml(b, new SDMyImageGetterUtil(mActivity, tvContent), null);
						SDViewBinder.setTextView(tvContent, Stext);
					}
				}
			} else
			{
				SDViewBinder.setTextView(tvContent, b);
			}

			SDViewBinder.setTextView(tvTime, model.getTime());

			convertView.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0)
				{
					Intent intent = new Intent();
					intent.setClass(mActivity, PrivatelettersItemListActivity.class);
					intent.putExtra(PrivatelettersItemListActivity.EXTRA_MID, model.getMlid());
					mActivity.startActivity(intent);
				}
			});
		}
		return convertView;
	}

}

package com.wenzhoujie.adapter;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.AddMerchantCommentActivity;
import com.wenzhoujie.ConfirmOrderActivity;
import com.wenzhoujie.LoginNewActivity;
import com.wenzhoujie.TuanDetailActivity;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.fragment.TuanDetailSecondFragment;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.wenzhoujie.model.CartGoodsModel;
import com.wenzhoujie.model.IndexActDeal_listModel;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.MerchantShopItem;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.GoodsdescActModel;
import com.wenzhoujie.utils.SDActivityUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewHolder;

public class SuperMarketCateGoodOneAdapter extends SDBaseAdapter<MerchantShopItem> {

	public SuperMarketCateGoodOneAdapter(List<MerchantShopItem> listModel, Activity activity)
	{
		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_smitem_list, null);
		}

		ImageView ivImage = ViewHolder.get(convertView, R.id.item_smitem_deals_iv_image);
		TextView tvName = ViewHolder.get(convertView, R.id.item_smitem_deals_tv_name);
		TextView tvPrice = ViewHolder.get(convertView, R.id.item_smitem_deals_tv_current_price);
		TextView tvOrgPrice = ViewHolder.get(convertView, R.id.item_smitem_deals_tv_original_price);
		tvOrgPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
		ImageView tvAddBuyBtn=ViewHolder.get(convertView, R.id.item_smitem_addtocart);

		final MerchantShopItem model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setImageView(ivImage, model.getImg());
			SDViewBinder.setTextView(tvName, model.getName());
			SDViewBinder.setTextView(tvPrice, "￥"+stringTo2double(String.valueOf(model.getCurrent_price())));
			SDViewBinder.setTextView(tvOrgPrice, "￥"+stringTo2double(String.valueOf(model.getOrigin_price())));

			ivImage.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					Intent intent = new Intent(App.getApplication(), TuanDetailActivity.class);
					intent.putExtra(TuanDetailActivity.EXTRA_GOODS_ID, String.valueOf(model.getId()));
					intent.putExtra(TuanDetailActivity.EXTRA_GOODS_TYPE, 0);
					mActivity.startActivity(intent);
				}
			});

			tvAddBuyBtn.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					//					Intent intent = new Intent(App.getApplication(), TuanDetailActivity.class);
					//					intent.putExtra(TuanDetailActivity.EXTRA_GOODS_ID, model.getId());
					//					intent.putExtra(TuanDetailActivity.EXTRA_GOODS_TYPE, 0);
					//					mActivity.startActivity(intent);
					//添加到購物車
					requestGoodsDetail(Integer.toString(model.getId()));
				}
			});
		}

		return convertView;
	}
	GoodsdescActModel mGoodsModel = null;
	private void requestGoodsDetail(String mStrGoodsId)
	{
		if (mStrGoodsId != null)
		{
			final LocalUserModel user = App.getApplication().getmLocalUser();
			if (user != null){
				RequestModel model = new RequestModel();
				model.put("act", "goodsdesc");
				model.put("id", mStrGoodsId);
				model.put("email", user.getUser_name());
				model.put("pwd", user.getUser_pwd());
				//model.put("m_latitude", mSearcher.getM_latitude());
				//model.put("m_longitude", mSearcher.getM_longitude());
				RequestCallBack<String> handler = new RequestCallBack<String>()
						{

					@Override
					public void onStart()
					{
						AppHelper.showLoadingDialog("请稍候...");
					}

					@Override
					public void onSuccess(ResponseInfo<String> responseInfo)
					{
						GoodsdescActModel actModel = new Gson().fromJson(responseInfo.result, GoodsdescActModel.class);
						if (!SDInterfaceUtil.isActModelNull(actModel))
						{
							mGoodsModel = actModel;
							handleBuyGoods(user);
						}
					}

					@Override
					public void onFailure(HttpException error, String msg)
					{

					}

					@Override
					public void onFinish()
					{
						AppHelper.hideLoadingDialog();
					}
						};
						InterfaceServer.getInstance().requestInterface(model, handler);
			}else{
				SDActivityUtil.startActivity(mActivity, LoginNewActivity.class);
			}
		}
	}

	private void handleBuyGoods(final LocalUserModel user) {
		if (user == null) {
			SDActivityUtil.startActivity(mActivity, LoginNewActivity.class);
		} else {
			if (mGoodsModel != null) {
				CartGoodsModel cartGoodsModel = new CartGoodsModel();
				cartGoodsModel.setGoods(mGoodsModel, true);
				if (cartGoodsModel.isHas_cart()) // 商品可以加入购物车
				{
					App.getApplication().getListCartGoodsModel().add(cartGoodsModel);
					addShopCart(user, cartGoodsModel);
				} else {
					Intent intent = new Intent(mActivity, ConfirmOrderActivity.class);
					List<CartGoodsModel> listModel = new ArrayList<CartGoodsModel>();
					listModel.add(cartGoodsModel);
					intent.putExtra(ConfirmOrderActivity.EXTRA_LIST_CART_GOODS_MODEL, (Serializable) listModel);
					mActivity.startActivity(intent);
				}
			}
		}
	}

	// 添加购物车
	private void addShopCart(LocalUserModel user, CartGoodsModel cartGoodsModel) {
		RequestModel model = new RequestModel();
		model.put("act", "cart");
		model.put("a", "add");
		model.put("deal_id", cartGoodsModel.getGoods_id());
		model.put("num", cartGoodsModel.getNum());
		//String [] s={};
		if (!cartGoodsModel.getAttr_id_a().equals("")) {
			model.put("attr", cartGoodsModel.getAttr_id_a());
		} if (!cartGoodsModel.getAttr_id_b().equals("")) {
			model.put("attr", cartGoodsModel.getAttr_id_b());
		}
		model.put("email", user.getUser_name());
		model.put("pwd", user.getUser_pwd());
		RequestCallBack<String> handler = new RequestCallBack<String>() {

			@Override
			public void onStart() {

			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo) {
				//JSONObject json = JSON.parseObject(responseInfo.result);
				// Toast.makeText(getActivity(), json+"", 1).show();
				SDToast.showToast("加入购物车成功!");
			}

			@Override
			public void onFailure(HttpException error, String msg) {

			}

			@Override
			public void onFinish() {

			}
		};
		InterfaceServer.getInstance().requestInterface(model, handler);

	}
	public String stringTo2double(String num) {
		Double total_price_double = Double.parseDouble(num);
		if(total_price_double>0){
			DecimalFormat df = new DecimalFormat("###.00");
			return df.format(total_price_double);
		}else
		{
			return "0.00";
		}
	}
}

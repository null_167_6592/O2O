package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDResourcesUtil;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.customview.SD2LvCategoryView.SD2LvCategoryViewAdapterInterface;
import com.wenzhoujie.customview.smSD2LvCategoryView.smSD2LvCategoryViewAdapterInterface;
import com.wenzhoujie.model.Bcate_listBcate_typeModel;
import com.wenzhoujie.model.smSmallCates;
import com.wenzhoujie.utils.ViewHolder;

public class smCategoryCateRightAdapter extends SDBaseAdapter<smSmallCates> implements smSD2LvCategoryViewAdapterInterface
{

	public smCategoryCateRightAdapter(List<smSmallCates> listRight, Activity activity)
	{
		super(listRight, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_category_right, null);
		}

		TextView tvTitle = ViewHolder.get(convertView, R.id.item_category_right_tv_title);

		smSmallCates model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setTextView(tvTitle, model.getName());
			if (model.isSelect())
			{
				convertView.setBackgroundResource(R.drawable.choose_item_right);
			} else
			{
				convertView.setBackgroundColor(SDResourcesUtil.getColor(R.color.bg_gray_categoryview_item_select));
			}
		}

		return convertView;
	}

	@Override
	public void setPositionSelectState(int position, boolean select, boolean notify)
	{
		getItem(position).setSelect(select);
		if (notify)
		{
			notifyDataSetChanged();
		}

	}

	@Override
	public String getTitleNameFromPosition(int position)
	{
		return getItem(position).getName();
	}

	@Override
	public BaseAdapter getAdapter()
	{
		return this;
	}

	@Override
	public Object getSelectModelFromPosition(int position)
	{
		return getItem(position);
	}

	@Override
	public int getTitleIndex()
	{
		return 0;
	}

	@Override
	public Object getRightListModelFromPosition_left(int position)
	{
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void updateRightListModel_right(Object rightListModel)
	{
		List<smSmallCates> listRight = (List<smSmallCates>) rightListModel;
		updateListViewData(listRight);
	}

	@Override
	public void setPositionSelectState_left(int positionLeft, int positionRight, boolean select)
	{

	}

	@Override
	public int getItemCount()
	{
		return getCount();
	}

}

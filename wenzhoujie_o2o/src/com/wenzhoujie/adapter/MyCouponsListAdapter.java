package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.model.CouponlistActItemModel;
import com.wenzhoujie.utils.ViewHolder;

public class MyCouponsListAdapter extends SDBaseAdapter<CouponlistActItemModel>
{

	public MyCouponsListAdapter(List<CouponlistActItemModel> listModel, Activity activity)
	{
		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_lv_my_coupons, null);
		}

		ImageView ivImage = ViewHolder.get(convertView, R.id.item_lv_my_coupons_iv_image);
		TextView tvTitle = ViewHolder.get(convertView, R.id.item_lv_my_coupons_tv_title);
		TextView tvSn = ViewHolder.get(convertView, R.id.item_lv_my_coupons_tv_sn);
		TextView tvOverdue = ViewHolder.get(convertView, R.id.item_lv_my_coupons_tv_overdue_date);

		CouponlistActItemModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setImageView(ivImage, model.getDealIcon());
			SDViewBinder.setTextView(tvTitle, model.getDealName());
			SDViewBinder.setTextView(tvSn, model.getSn_format_string());
			SDViewBinder.setTextView(tvOverdue, model.getDealName());
			SDViewBinder.setTextView(tvOverdue, model.getOverdue_time_format());
		}

		return convertView;
	}

}

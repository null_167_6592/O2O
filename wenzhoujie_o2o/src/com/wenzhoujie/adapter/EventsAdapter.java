package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.EventsDetailActivity;
import com.wenzhoujie.model.EventsListActItemModel;
import com.wenzhoujie.utils.ViewHolder;

public class EventsAdapter extends SDBaseAdapter<EventsListActItemModel>
{

	private Activity activity;

	public EventsAdapter(List<EventsListActItemModel> listModel, Activity activity)
	{
		super(listModel, activity);
		this.activity = activity;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_event_list, null);
		}
		ImageView ivDoNotNeedOrder = ViewHolder.get(convertView, R.id.item_event_list_iv_do_not_need_order);
		ImageView ivImage = ViewHolder.get(convertView, R.id.item_event_list_iv_image);
		ImageView ivIsNew = ViewHolder.get(convertView, R.id.item_event_list_iv_is_new);
		TextView tvAddress = ViewHolder.get(convertView, R.id.item_event_list_tv_address);
		TextView tvBrief = ViewHolder.get(convertView, R.id.item_event_list_tv_brief);
		TextView tvDistance = ViewHolder.get(convertView, R.id.item_event_list_tv_distance);
		TextView tvTime = ViewHolder.get(convertView, R.id.item_event_list_tv_event_time);
		TextView tvName = ViewHolder.get(convertView, R.id.item_event_list_tv_name);

		final EventsListActItemModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setImageView(ivImage, model.getIcon());
			SDViewBinder.setTextView(tvName, model.getName());
			SDViewBinder.setTextView(tvBrief, model.getBrief());
			SDViewBinder.setTextView(tvTime, model.getEvent_begin_time());
			SDViewBinder.setTextView(tvAddress, model.getAddress());
			SDViewBinder.setTextView(tvDistance, model.getDistance_format());

			convertView.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0)
				{
					Intent itemintent = new Intent();
					itemintent.putExtra(EventsDetailActivity.EXTRA_EVENTS_ID, model.getId());
					itemintent.setClass(activity, EventsDetailActivity.class);
					activity.startActivity(itemintent);
				}
			});

		}
		return convertView;
	}

}

package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDResourcesUtil;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.customview.SD2LvCategoryView.SD2LvCategoryViewAdapterInterface;
import com.wenzhoujie.model.Bcate_listBcate_typeModel;
import com.wenzhoujie.utils.ViewHolder;

public class CategoryCateRightAdapter extends SDBaseAdapter<Bcate_listBcate_typeModel> implements SD2LvCategoryViewAdapterInterface
{

	public CategoryCateRightAdapter(List<Bcate_listBcate_typeModel> listModel, Activity activity)
	{
		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_category_right, null);
		}

		TextView tvTitle = ViewHolder.get(convertView, R.id.item_category_right_tv_title);

		Bcate_listBcate_typeModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setTextView(tvTitle, model.getName());
			if (model.isSelect())
			{
				convertView.setBackgroundResource(R.drawable.choose_item_right);
			} else
			{
				convertView.setBackgroundColor(SDResourcesUtil.getColor(R.color.bg_gray_categoryview_item_select));
			}
		}

		return convertView;
	}

	@Override
	public void setPositionSelectState(int position, boolean select, boolean notify)
	{
		getItem(position).setSelect(select);
		if (notify)
		{
			notifyDataSetChanged();
		}

	}

	@Override
	public String getTitleNameFromPosition(int position)
	{
		return getItem(position).getName();
	}

	@Override
	public BaseAdapter getAdapter()
	{
		return this;
	}

	@Override
	public Object getSelectModelFromPosition(int position)
	{
		return getItem(position);
	}

	@Override
	public int getTitleIndex()
	{
		return 0;
	}

	@Override
	public Object getRightListModelFromPosition_left(int position)
	{
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void updateRightListModel_right(Object rightListModel)
	{
		List<Bcate_listBcate_typeModel> listRight = (List<Bcate_listBcate_typeModel>) rightListModel;
		updateListViewData(listRight);
	}

	@Override
	public void setPositionSelectState_left(int positionLeft, int positionRight, boolean select)
	{

	}

	@Override
	public int getItemCount()
	{
		return getCount();
	}

}

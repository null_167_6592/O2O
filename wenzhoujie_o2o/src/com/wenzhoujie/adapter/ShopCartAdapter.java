package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.model.CartGoodsModel;
import com.wenzhoujie.model.GoodsAttrsModel;
import com.wenzhoujie.model.act.GoodsdescActModel;
import com.wenzhoujie.utils.SDTypeParseUtil;
import com.wenzhoujie.utils.ViewHolder;

public class ShopCartAdapter extends SDBaseAdapter<CartGoodsModel>
{

	public ShopCartAdapter(List<CartGoodsModel> listModel, Activity activity)
	{
		super(listModel, activity);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		// if (convertView == null)
		// {
		convertView = mInflater.inflate(R.layout.item_shop_cart, null);
		// }

		TextView tvAddNumber = ViewHolder.get(convertView, R.id.item_shop_cart_tv_add_number);
		TextView tvMinusNumber = ViewHolder.get(convertView, R.id.item_shop_cart_tv_minus_number);
		EditText etNumber = ViewHolder.get(convertView, R.id.item_shop_cart_et_edit_number);
		TextView ivDeleteItem = ViewHolder.get(convertView, R.id.item_shop_cart_tv_delete);
		ImageView ivGoodsImage = ViewHolder.get(convertView, R.id.item_shop_cart_iv_goods_image);
		LinearLayout llAttrs = ViewHolder.get(convertView, R.id.item_shop_cart_ll_goods_attr);
		TextView tvAttrATitle = ViewHolder.get(convertView, R.id.item_shop_cart_tv_attr_a_title);
		TextView tvAttrAValue = ViewHolder.get(convertView, R.id.item_shop_cart_tv_attr_a_value);
		TextView tvAttrBTitle = ViewHolder.get(convertView, R.id.item_shop_cart_tv_attr_b_title);
		TextView tvAttrBValue = ViewHolder.get(convertView, R.id.item_shop_cart_tv_attr_b_value);
		TextView tvGoodsName = ViewHolder.get(convertView, R.id.item_shop_cart_tv_goods_name);
		TextView tvSinglePrice = ViewHolder.get(convertView, R.id.item_shop_cart_tv_single_price);
		TextView tvTotalPrice = ViewHolder.get(convertView, R.id.item_shop_cart_tv_total_price);

		CartGoodsModel cartModel = getItem(position);
		GoodsdescActModel goodsModel = cartModel.getGoods();
		if (cartModel != null && goodsModel != null)
		{
			SDViewBinder.setImageView(ivGoodsImage, goodsModel.getImage()); // 设置商品图片
			SDViewBinder.setTextView(tvGoodsName, goodsModel.getTitle()); // 设置商品名称
			if (SDViewBinder.setViewsVisibility(llAttrs, cartModel.isHas_attr())) // 有属性
			{
				GoodsAttrsModel attrsModel = goodsModel.getAttr();
				if (attrsModel != null)
				{
					if (cartModel.isHas_attr_1()) // 有属性1
					{
						SDViewBinder.setTextView(tvAttrATitle, cartModel.getAttr_title_a());
						SDViewBinder.setTextView(tvAttrAValue, cartModel.getAttr_value_a());
					}
					if (cartModel.isHas_attr_2()) // 有属性2
					{
						SDViewBinder.setTextView(tvAttrBTitle, cartModel.getAttr_title_b());
						SDViewBinder.setTextView(tvAttrBValue, cartModel.getAttr_value_b());
					}

				}
			}

			etNumber.setText(cartModel.getNum());
			SDViewBinder.setTextView(tvSinglePrice, cartModel.getPrice_format_string()); // 商品单价
			SDViewBinder.setTextView(tvTotalPrice, cartModel.getTotal_format_string()); // 小计价格

			ivDeleteItem.setOnClickListener(new ViewClickListener(position, convertView, cartModel));// 删除按钮监听

			tvAddNumber.setOnClickListener(new ViewClickListener(position, convertView, cartModel)); // 增加按钮

			tvMinusNumber.setOnClickListener(new ViewClickListener(position, convertView, cartModel)); // 减少按钮

			etNumber.addTextChangedListener(new GoodsNumberTextWatcher(position, convertView, cartModel));

		}

		return convertView;
	}

	class ViewClickListener implements OnClickListener
	{

		private int nPosition = -1;
		private View nConvertView = null;
		private CartGoodsModel nCartModel = null;

		public ViewClickListener(int nPosition, View convertView, CartGoodsModel nCartModel)
		{
			super();
			this.nPosition = nPosition;
			this.nConvertView = convertView;
			this.nCartModel = nCartModel;
		}

		@Override
		public void onClick(View v)
		{
			if (nCartModel != null)
			{
				switch (v.getId())
				{
				case R.id.item_shop_cart_tv_delete: // 删除按钮
					mListModel.remove(nCartModel);
					notifyDataSetChanged();
					break;
				case R.id.item_shop_cart_tv_add_number: // 增加按钮
					int limitNum = SDTypeParseUtil.getIntFromString(nCartModel.getLimit_num(), -1);
					if (limitNum > 0)
					{
						int numAdd = nCartModel.addNumber();
						if (numAdd > 0)
						{
							if (numAdd >= limitNum)
							{
								SDToast.showToast("已经达到最大购买数量");
							}
							updateViewsData(nCartModel, nConvertView, true);
						}
					} else
					{

					}
					break;
				case R.id.item_shop_cart_tv_minus_number: // 减少按钮
					int numMinus = nCartModel.minusNumber();
					if (numMinus > 0)
					{
						if (numMinus == 1)
						{
							SDToast.showToast("购买数量不能小于1");
						}
						updateViewsData(nCartModel, nConvertView, true);
					}
					break;

				default:
					break;
				}
			}

		}
	}

	private void updateViewsData(CartGoodsModel cartModel, View convertView, boolean updateEditText)
	{

		TextView tvTotalPrice = ViewHolder.get(convertView, R.id.item_shop_cart_tv_total_price);
		tvTotalPrice.setText(cartModel.getTotal_format_string());
		if (updateEditText)
		{
			EditText etNumber = ViewHolder.get(convertView, R.id.item_shop_cart_et_edit_number);
			etNumber.setText(cartModel.getNum());
		}
	}

	class GoodsNumberTextWatcher implements TextWatcher
	{
		private int nPosition = -1;
		private View nConvertView = null;
		private CartGoodsModel nCartModel = null;

		public GoodsNumberTextWatcher(int nPosition, View convertView, CartGoodsModel nCartModel)
		{
			super();
			this.nPosition = nPosition;
			this.nConvertView = convertView;
			this.nCartModel = nCartModel;
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void afterTextChanged(Editable s)
		{
			if (nCartModel != null)
			{
				int num = SDTypeParseUtil.getIntFromString(s.toString(), 0);
				int limitNum = SDTypeParseUtil.getIntFromString(nCartModel.getLimit_num(), 0);
				if (limitNum > 0)
				{
					if (num <= 0)
					{
						num = 1;
						SDToast.showToast("购买数量不能小于1");
						nCartModel.setNum(String.valueOf(num));
						updateViewsData(nCartModel, nConvertView, true);
					} else if (num > limitNum)
					{
						num = limitNum;
						SDToast.showToast("已经达到最大购买数量");
						nCartModel.setNum(String.valueOf(num));
						updateViewsData(nCartModel, nConvertView, true);
					} else if (num < limitNum)
					{
						nCartModel.setNum(String.valueOf(num));
						updateViewsData(nCartModel, nConvertView, false);
					} else if (num == limitNum)
					{
						SDToast.showToast("已经达到最大购买数量");
						nCartModel.setNum(String.valueOf(num));
						updateViewsData(nCartModel, nConvertView, false);
					}
				}
			}
		}

	}

}

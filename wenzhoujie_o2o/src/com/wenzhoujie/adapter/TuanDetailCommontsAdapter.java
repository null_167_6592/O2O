package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.model.GoodsCommentModel;
import com.wenzhoujie.utils.SDTypeParseUtil;
import com.wenzhoujie.utils.ViewHolder;

public class TuanDetailCommontsAdapter extends SDBaseAdapter<GoodsCommentModel>
{

	public TuanDetailCommontsAdapter(List<GoodsCommentModel> listModel, Activity activity)
	{
		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_lv_tuan_detail_comment, null);
		}
		TextView tvUserName = ViewHolder.get(convertView, R.id.item_lv_tuan_detail_comment_tv_user_name);
		TextView tvTimeTextView = ViewHolder.get(convertView, R.id.item_lv_tuan_detail_comment_tv_time);
		TextView tvContentTextView = ViewHolder.get(convertView, R.id.item_lv_tuan_detail_comment_tv_comment_content);
		RatingBar rbStar = ViewHolder.get(convertView, R.id.item_lv_tuan_detail_comment_rb_comment_star);
		View viewDiv = ViewHolder.get(convertView, R.id.item_lv_tuan_detail_comment_view_div);

		if (position == 0)
		{
			viewDiv.setVisibility(View.GONE);
		}

		GoodsCommentModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setTextView(tvUserName, model.getUser_name());
			SDViewBinder.setTextView(tvTimeTextView, model.getCreate_time_format());
			SDViewBinder.setTextView(tvContentTextView, model.getContent());
			SDViewBinder.setRatingBar(rbStar, SDTypeParseUtil.getFloatFromString(model.getPoint(), 0));
		}

		return convertView;
	}

}

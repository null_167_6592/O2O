package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.MerchantDetailNewActivity;
import com.wenzhoujie.SuperMarketDetailActivity;
import com.wenzhoujie.app.App;
import com.wenzhoujie.model.MerchantlistActItemModel;
import com.wenzhoujie.utils.ViewHolder;

public class MerchantListAdapter extends SDBaseAdapter<MerchantlistActItemModel>
{

	public MerchantListAdapter(List<MerchantlistActItemModel> listModel, Activity activity)
	{
		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_lv_merchant_list, null);
		}
		ImageView ivMerchant = ViewHolder.get(convertView, R.id.item_lv_merchant_list_iv_image_merchant); // 图片
		TextView tvDistance = ViewHolder.get(convertView, R.id.item_lv_merchant_list_tv_merchant_distance); // 距离
		RatingBar rbRatingStar = ViewHolder.get(convertView, R.id.item_lv_merchant_list_rb_rating_star); // 星级
		TextView tvMerchantTitle = ViewHolder.get(convertView, R.id.item_lv_merchant_list_tv_merchant_title); // 标题
		TextView tvMerchantAddress = ViewHolder.get(convertView, R.id.item_lv_merchant_list_tv_merchant_address); // 地址
		TextView tv_user_score = ViewHolder.get(convertView, R.id.item_lv_merchant_list_tv_user_score);
		final MerchantlistActItemModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setImageView(ivMerchant, model.getLogo());
			SDViewBinder.setTextView(tvMerchantTitle, model.getName());
			SDViewBinder.setRatingBar(rbRatingStar, model.getAvg_point_fromat_float());
			SDViewBinder.setTextView(tv_user_score, model.getComment_count());
			SDViewBinder.setTextView(tvMerchantAddress, model.getMobile_brief());
			SDViewBinder.setTextView(tvDistance, model.getDistance_format_string());

			convertView.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if(model.getShop_type() == 5)
					{
						
						Intent itemintent = new Intent();
						itemintent.putExtra(SuperMarketDetailActivity.EXTRA_MERCHANT_ID, model.getId());
						itemintent.putExtra(SuperMarketDetailActivity.EXTRA_MERCHANT_TITLE, model.getName());
						itemintent.setClass(App.getApplication(), SuperMarketDetailActivity.class);
						mActivity.startActivity(itemintent);
						
					}
					else
					{
					Intent itemintent = new Intent();
					itemintent.putExtra(MerchantDetailNewActivity.EXTRA_MERCHANT_ID, model.getId());
					itemintent.setClass(App.getApplication(), MerchantDetailNewActivity.class);
					mActivity.startActivity(itemintent);
					}
				}
			});
		}
		return convertView;
	}

}

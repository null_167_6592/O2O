package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.sunday.eventbus.SDEventManager;
import com.wenzhoujie.RefundGoodsActivity;
import com.wenzhoujie.RefundTuanActivity;
import com.wenzhoujie.RefuseDeliveryActivity;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.event.EnumEventTag;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.dialog.SDDialogConfirm;
import com.wenzhoujie.library.dialog.SDDialogCustom;
import com.wenzhoujie.library.dialog.SDDialogCustom.SDDialogCustomListener;
import com.wenzhoujie.library.utils.SDResourcesUtil;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.wenzhoujie.model.DealOrderItemModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.BaseActModel;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;
import com.wenzhoujie.utils.ViewHolder;

public class OrderGoodsListAdapter extends SDBaseAdapter<DealOrderItemModel> {

	private static final int COLOR_ENABLE = R.color.main_color;
	private static final int COLOR_DISABLE = R.color.gray;

	private boolean mShowActions = true;

	public OrderGoodsListAdapter(List<DealOrderItemModel> listModel,
			boolean showActions, Activity activity) {
		super(listModel, activity);
		this.mShowActions = showActions;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_order_goods_list,
					null);
		}

		View viewDiv = ViewHolder.get(convertView,
				R.id.item_order_goods_list_view_div); // 商品图片
		ImageView ivImage = ViewHolder.get(convertView,
				R.id.item_order_goods_list_iv_goods_image); // 商品图片
		TextView tvTitle = ViewHolder.get(convertView,
				R.id.item_order_goods_list_tv_goods_title); // 商品标题
		TextView tvNumber = ViewHolder.get(convertView,
				R.id.item_order_goods_list_tv_number); // 商品数量
		TextView tvAttr = ViewHolder.get(convertView,
				R.id.item_order_goods_list_tv_attr); // 商品属性
		TextView tvPriceSingle = ViewHolder.get(convertView,
				R.id.item_order_goods_list_tv_price_single); // 商品单价
		TextView tvPriceTotal = ViewHolder.get(convertView,
				R.id.item_order_goods_list_tv_price_total); // 商品总价
		TextView tv_refund = ViewHolder.get(convertView, R.id.tv_refund); // 我要退款
		TextView tv_delivery = ViewHolder.get(convertView, R.id.tv_delivery); // 物流状态
		LinearLayout ll_tv_delivery_receive = ViewHolder.get(convertView,
				R.id.ll_tv_delivery_receive);
		TextView tv_confirmation_receipt = ViewHolder.get(convertView,
				R.id.tv_confirmation_receipt); // 确认收货
		TextView tv_did_not_receive = ViewHolder.get(convertView,
				R.id.tv_did_not_receive); //没收到货

		SDViewUtil.hide(tv_refund);
		SDViewUtil.hide(ll_tv_delivery_receive);

		SDViewUtil.hide(tv_delivery);
		tv_refund.setOnClickListener(null);

		tv_refund.setTextColor(SDResourcesUtil.getColor(COLOR_DISABLE));

		if (position == 0) {
			viewDiv.setVisibility(View.GONE);
		}

		final DealOrderItemModel model = getItem(position);
		if (model != null) {
			if (!TextUtils.isEmpty(model.getAttr_content())) {
				tvTitle.setMaxLines(1);
			}

			SDViewBinder.setTextView(tvTitle, model.getName());
			SDViewBinder
					.setTextViewsVisibility(tvAttr, model.getAttr_content());

			if (mShowActions) {
				SDViewBinder.setImageView(ivImage, model.getImage());
				SDViewBinder.setTextView(tvNumber, model.getNumr());
				SDViewBinder.setTextView(tvPriceSingle, model.getPrice_format());
				SDViewBinder.setTextView(tvPriceTotal,
						model.getTotal_money_format());
				
				// 退款状态
				switch (model.getRefundState()) {
				case 0:
					SDViewUtil.show(tv_refund);
					tv_refund.setTextColor(SDResourcesUtil
							.getColor(COLOR_ENABLE));
					tv_refund.setText("我要退款");
					tv_refund.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// 跳到退款界面
							if (model.isShop()) {
								Intent intent = new Intent(mActivity,
										RefundGoodsActivity.class);
								intent.putExtra(RefundGoodsActivity.EXTRA_ID,
										model.getId());
								mActivity.startActivity(intent);
							} else {
								Intent intent = new Intent(mActivity,
										RefundTuanActivity.class);
								intent.putExtra(RefundGoodsActivity.EXTRA_ID,
										model.getId());
								mActivity.startActivity(intent);
							}
						}
					});
					break;
				case 1:
					SDViewUtil.show(tv_refund);
					tv_refund.setText("退款审核中");
					break;
				case 2:
					SDViewUtil.show(tv_refund);
					tv_refund.setText("已退款");
					break;
				case 3:
					SDViewUtil.show(tv_refund);
					tv_refund.setText("退款被拒");
					break;

				default:
					break;
				}

				// 物流状态
				switch (model.getDeliveryState()) {
				case 0:
					SDViewUtil.show(tv_delivery);
					tv_delivery.setText("未发货");
					break;
				case 1:
					SDViewUtil.show(ll_tv_delivery_receive);
					tv_confirmation_receipt.setOnClickListener(new OnClickListener()
					{

						@Override
						public void onClick(View v)
						{
							clickConfirmationReceipt(model);
						}
					});
					tv_did_not_receive.setOnClickListener(new OnClickListener()
					{

						@Override
						public void onClick(View v)
						{
							// TODO 没收到货
							Intent intent = new Intent(mActivity, RefuseDeliveryActivity.class);
							intent.putExtra(RefundGoodsActivity.EXTRA_ID, model.getId());
							mActivity.startActivity(intent);
						}
					});
					break;
				case 2:
					SDViewUtil.show(tv_delivery);
					tv_delivery.setText("已收货");
					break;
				case 3:
					SDViewUtil.show(tv_delivery);
					tv_delivery.setText("维权中");
					break;

				default:
					break;
				}
			}else{
				SDViewBinder.setImageView(ivImage, model.getDeal_icon());
				SDViewBinder.setTextView(tvNumber, model.getNumber()+"");
				SDViewBinder.setTextView(tvPriceSingle, "￥ "+model.getUnit_price());
				SDViewBinder.setTextView(tvPriceTotal,"￥ "+model.getTotal_price());
			}
		}
		return convertView;
	}

	/**
	 * 点击确认收货
	 * 
	 * @param model
	 */
	protected void clickConfirmationReceipt(final DealOrderItemModel model) {
		if (model == null) {
			return;
		}

		SDDialogConfirm dialog = new SDDialogConfirm();
		dialog.setTextContent("确认收货?");
		dialog.setmListener(new SDDialogCustomListener() {

			@Override
			public void onClickConfirm(View v, SDDialogCustom dialog) {
				requestConfirmationReceipt(model);
			}

			@Override
			public void onClickCancel(View v, SDDialogCustom dialog) {

			}

			@Override
			public void onDismiss(DialogInterface iDialog, SDDialogCustom dialog) {

			}
		});
		dialog.show();
	}

	/**
	 * 确认收货
	 * 
	 * @param model
	 */
	protected void requestConfirmationReceipt(final DealOrderItemModel model) {
		if (model == null) {
			return;
		}

		RequestModel requestModel = new RequestModel(false);
		requestModel.put("act", "order_verify_delivery");
		requestModel.putUser();
		requestModel.put("item_id", model.getId());
		RequestCallBack<String> handler = new RequestCallBack<String>() {

			@Override
			public void onStart() {
				AppHelper.showLoadingDialog("请稍候...");
			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo) {
				BaseActModel actModel = JsonUtil.json2Object(
						responseInfo.result, BaseActModel.class);
				if (!SDInterfaceUtil.isActModelNull(actModel)) {
					int status = SDTypeParseUtil.getIntFromString(
							actModel.getStatus(), 0);
					if (status == 1) {
						// 刷新列表
						SDEventManager.post(EnumEventTag.REFRESH_ORDER_LIST
								.ordinal());
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg) {

			}

			@Override
			public void onFinish() {
				AppHelper.hideLoadingDialog();
			}
		};
		InterfaceServer.getInstance().requestInterface(requestModel, handler);
	}

}

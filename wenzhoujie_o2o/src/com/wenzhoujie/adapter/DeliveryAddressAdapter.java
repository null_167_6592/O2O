package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.model.DeliveryModel;
import com.wenzhoujie.utils.ViewHolder;

public class DeliveryAddressAdapter extends SDBaseAdapter<DeliveryModel>
{
	/** 1:管理模式，0:选择模式 */
	private int mMode = 1;

	public DeliveryAddressAdapter(List<DeliveryModel> listModel, Activity activity)
	{
		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_lv_delivery_address, null);
		}

		TextView tvName = ViewHolder.get(convertView, R.id.item_lv_delivery_address_tv_name);
		TextView tvPhone = ViewHolder.get(convertView, R.id.item_lv_delivery_address_tv_mobile);
		TextView tvDeliveryAddress = ViewHolder.get(convertView, R.id.item_lv_delivery_address_tv_address);
		ImageView ivArrowRight = ViewHolder.get(convertView, R.id.item_lv_delivery_address_iv_arrow_right);

		if (mMode == 1)
		{
			ivArrowRight.setVisibility(View.VISIBLE);

		} else
		{
			ivArrowRight.setVisibility(View.GONE);
		}

		final DeliveryModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setTextView(tvName, model.getConsignee()); // 名字
			SDViewBinder.setTextView(tvPhone, model.getPhone()); // 电话
			SDViewBinder.setTextView(tvDeliveryAddress, model.getDelivery_long()); // 地址

		}

		return convertView;
	}

	public void updateMode(int mode)
	{
		mMode = mode;
		notifyDataSetChanged();
	}

}

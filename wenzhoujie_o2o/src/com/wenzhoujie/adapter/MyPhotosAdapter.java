package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.customview.ScaleImageView;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.model.MyPhotosActivityItemModel;
import com.wenzhoujie.utils.ViewHolder;

public class MyPhotosAdapter extends SDBaseAdapter<MyPhotosActivityItemModel>
{

	public MyPhotosAdapter(List<MyPhotosActivityItemModel> listModel, Activity activity)
	{
		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_my_photos, null);
		}

		ScaleImageView ivImage = ViewHolder.get(convertView, R.id.item_my_photos_iv_image);
		MyPhotosActivityItemModel model = getItem(position);
		if (model != null)
		{
			ivImage.setImageWidth(model.getWidthFormat());
			ivImage.setImageHeight(model.getHeightFormat());
			SDViewBinder.setImageView(ivImage, model.getImg());
		}
		return convertView;
	}

}

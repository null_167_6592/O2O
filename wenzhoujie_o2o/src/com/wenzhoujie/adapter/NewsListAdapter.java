package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.model.Article_listListModel;
import com.wenzhoujie.utils.ViewHolder;

public class NewsListAdapter extends SDBaseAdapter<Article_listListModel>
{

	public NewsListAdapter(List<Article_listListModel> listModel, Activity activity)
	{
		super(listModel, activity);

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_news_list, null);
		}
		TextView tvNews = ViewHolder.get(convertView, R.id.item_news_list_tv_title);

		final Article_listListModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setTextView(tvNews, model.getTitle());
		}
		return convertView;
	}
}

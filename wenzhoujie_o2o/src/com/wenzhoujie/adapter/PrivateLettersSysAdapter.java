package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.model.PrivateLettersActivitySys_MsgsModel;
import com.wenzhoujie.utils.ViewHolder;

public class PrivateLettersSysAdapter extends SDBaseAdapter<PrivateLettersActivitySys_MsgsModel>
{

	private Activity activity;

	public PrivateLettersSysAdapter(List<PrivateLettersActivitySys_MsgsModel> listModel, Activity activity)
	{
		super(listModel, activity);
		this.activity = activity;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_systems_message, null);
		}

		TextView tvMessage = ViewHolder.get(convertView, R.id.item_systems_messagetv_sys_message);
		TextView tvTime = ViewHolder.get(convertView, R.id.item_systems_messagetv_time);

		final PrivateLettersActivitySys_MsgsModel model = getItem(position);
		if (model != null)
		{
			String text_a = model.getTitle();
			String ty = "系统消息：<font color=blue>" + text_a + "</font>";
			String tm = text_a.replaceAll(text_a, ty);
			Spanned text = Html.fromHtml(tm);
			SDViewBinder.setTextView(tvMessage, text);
			SDViewBinder.setTextView(tvTime, model.getTime());

			tvMessage.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					// Intent intent = new Intent();
					// intent.setClass(activity, System_Msgs_Details.class);
					// intent.putExtra("mid",model.getMid());
					// activity.startActivity(intent);

				}
			});
		}
		return convertView;
	}

}

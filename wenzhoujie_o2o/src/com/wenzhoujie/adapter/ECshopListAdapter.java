package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.TuanDetailActivity;
import com.wenzhoujie.model.ECshoplistActItemModel;
import com.wenzhoujie.utils.ViewHolder;

public class ECshopListAdapter extends SDBaseAdapter<ECshoplistActItemModel>
{

	private Activity activity;

	public ECshopListAdapter(List<ECshoplistActItemModel> listModel, Activity activity)
	{
		super(listModel, activity);
		this.activity = activity;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_home_recommend_goods, null);
		}
		View viewDiv = ViewHolder.get(convertView, R.id.item_home_recommend_goods_view_div);
		ImageView ivImage = ViewHolder.get(convertView, R.id.item_home_recommend_goods_iv_image);
		ImageView ivDoNotNeedOrder = ViewHolder.get(convertView, R.id.item_home_recommend_goods_iv_do_not_need_order);
		ImageView ivIsNew = ViewHolder.get(convertView, R.id.item_home_recommend_goods_iv_is_new);
		TextView tvName = ViewHolder.get(convertView, R.id.item_home_recommend_goods_tv_name);
		TextView tvBrief = ViewHolder.get(convertView, R.id.item_home_recommend_goods_tv_brief);
		TextView tvCurrentPrice = ViewHolder.get(convertView, R.id.item_home_recommend_goods_tv_current_price);
		TextView tvOriginalPrice = ViewHolder.get(convertView, R.id.item_home_recommend_goods_tv_original_price);
		TextView tvBuyCount = ViewHolder.get(convertView, R.id.item_home_recommend_goods_tv_buy_count);
		viewDiv.setVisibility(View.GONE);

		final ECshoplistActItemModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setImageView(ivImage, model.getImage());
			SDViewBinder.setTextView(tvName, model.getSub_name());
			SDViewBinder.setTextView(tvBrief, model.getTitle());
			SDViewBinder.setTextView(tvCurrentPrice, model.getCur_price_format());
			tvOriginalPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG); // 设置中划线效果
			SDViewBinder.setTextView(tvOriginalPrice, model.getOri_price_format());
			SDViewBinder.setTextView(tvBuyCount, model.getBuy_count());

			convertView.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0)
				{
					Intent itemintent = new Intent();
					itemintent.putExtra(TuanDetailActivity.EXTRA_GOODS_ID, model.getGoods_id());
					itemintent.putExtra(TuanDetailActivity.EXTRA_GOODS_TYPE, 0);
					itemintent.setClass(activity, TuanDetailActivity.class);
					activity.startActivity(itemintent);

				}
			});
		}
		return convertView;
	}

}

package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.ShareDetailActivity;
import com.wenzhoujie.UserCenterActivity;
import com.wenzhoujie.model.UItemExpressModel;
import com.wenzhoujie.model.UItemImageModel;
import com.wenzhoujie.model.UItemModel;
import com.wenzhoujie.utils.SDMyImageGetterUtil;
import com.wenzhoujie.utils.ViewHolder;

public class ShareContentAdapter extends SDBaseAdapter<UItemModel>
{

	private Activity activity;

	public ShareContentAdapter(List<UItemModel> listModel, Activity activity)
	{
		super(listModel, activity);
		this.activity = activity;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_share_content, null);
		}

		LinearLayout llOriginal = ViewHolder.get(convertView, R.id.item_share_content_ll_original_fenxiang);
		ImageView ivUserAvatar = ViewHolder.get(convertView, R.id.item_share_content_iv_user_avatar);
		TextView tvUserName = ViewHolder.get(convertView, R.id.item_share_content_tv_user_name);
		TextView tvTime = ViewHolder.get(convertView, R.id.item_share_content_tv_time);
		TextView tvContent = ViewHolder.get(convertView, R.id.item_share_content_tv_content);
		TextView tvOriginalUserName = ViewHolder.get(convertView, R.id.item_share_content_tv_original_user_name);
		TextView tvOriginalContent = ViewHolder.get(convertView, R.id.item_share_content_tv_original_content);
		TextView tvLikeCount = ViewHolder.get(convertView, R.id.item_share_content_tv_like_count);
		TextView tvCommentsCount = ViewHolder.get(convertView, R.id.item_share_content_tv_comment_count);
		TextView tvRelayCount = ViewHolder.get(convertView, R.id.item_share_content_tv_replay_count);

		// ========================================imageviews===========================
		ImageView ivUserImage0 = ViewHolder.get(convertView, R.id.item_share_content_iv_image_0);
		ivUserImage0.setVisibility(View.GONE);

		ImageView ivUserImage1 = ViewHolder.get(convertView, R.id.item_share_content_iv_image_1);
		ivUserImage1.setVisibility(View.GONE);

		ImageView ivUserImage2 = ViewHolder.get(convertView, R.id.item_share_content_iv_image_2);
		ivUserImage2.setVisibility(View.GONE);

		ImageView ivReplayImage0 = ViewHolder.get(convertView, R.id.item_share_content_iv_original_image_0);
		ivReplayImage0.setVisibility(View.GONE);

		ImageView ivReplayImage1 = ViewHolder.get(convertView, R.id.item_share_content_iv_original_image_1);
		ivReplayImage1.setVisibility(View.GONE);

		ImageView ivReplayImage2 = ViewHolder.get(convertView, R.id.item_share_content_iv_original_image_2);
		ivReplayImage2.setVisibility(View.GONE);

		final UItemModel model = getItem(position);

		if (model != null)
		{
			SDViewBinder.setImageView(ivUserAvatar, model.getUser_avatar()); // 用户头像

			List<UItemExpressModel> listExpress = model.getParse_expres();
			ivUserAvatar.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					Intent intent = new Intent();
					intent.setClass(activity, UserCenterActivity.class);
					intent.putExtra(UserCenterActivity.EXTRA_UID_ID, model.getUid());
					activity.startActivity(intent);
				}
			});

			SDViewBinder.setTextView(tvUserName, model.getUser_name(), "未找到"); // 用户名
			SDViewBinder.setTextView(tvTime, model.getTime(), "未找到"); // 发表时间

			// 设置内容
			String userContent = model.getContent();
			if (listExpress.size() > 0) // 内容包含表情
			{
				for (int i = 0; i < listExpress.size(); i++)
				{
					String oldString = listExpress.get(i).getKey();
					String newString = "<img src='" + listExpress.get(i).getValue() + "'/>";
					userContent = userContent.replace(oldString, newString);
					Spanned text = Html.fromHtml(userContent, new SDMyImageGetterUtil(activity, tvContent), null);
					SDViewBinder.setTextView(tvContent, text);
				}
			} else
			{
				SDViewBinder.setTextView(tvContent, userContent);
			}
			bindShareImageContent(model.getImgs(), ivUserImage0, ivUserImage1, ivUserImage2); // 设置图片

			// ============================判断是否是回复的内容，并决定是否绑定数据=====================

			if (model.isIs_relay_format_boolean()) // 是回复内容
			{
				llOriginal.setVisibility(View.VISIBLE);
				final UItemModel originalModel = model.getRelay_share(); // 被回复的model
				if (originalModel != null)
				{
					String replayUsername = originalModel.getUser_name(); // 被回复的用户名
					String replayContent = originalModel.getContent(); // 被回复的内容
					if (replayUsername != null && replayContent != null)
					{
						String replayUsernameAndContent = "<font color=#41598a>" + replayUsername + "</font>" + "<font color=#000000>" + " : " + replayContent + "</font>";

						List<UItemExpressModel> listExpressReplay = originalModel.getParse_expres();
						if (listExpressReplay.size() > 0) // 被回复的内容中包含表情
						{
							for (int i = 0; i < listExpressReplay.size(); i++)
							{
								String oldString = listExpressReplay.get(i).getKey();
								String newString = "<img src='" + listExpressReplay.get(i).getValue() + "'/>";
								replayUsernameAndContent = replayUsernameAndContent.replace(oldString, newString);
								Spanned replayTextSpan = Html.fromHtml(replayUsernameAndContent, new SDMyImageGetterUtil(activity, tvOriginalUserName), null);
								SDViewBinder.setTextView(tvOriginalUserName, replayTextSpan);
							}

						} else
						{
							tvOriginalUserName.setText(Html.fromHtml(replayUsernameAndContent));
						}
					}
					bindShareImageContent(originalModel.getImgs(), ivReplayImage0, ivReplayImage1, ivReplayImage2); // 设置被回复中的图片
				}

				llOriginal.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View arg0)
					{
						Intent intent = new Intent();
						intent.setClass(mActivity, ShareDetailActivity.class);
						intent.putExtra(ShareDetailActivity.EXTRA_SHARE_ID, originalModel.getShare_id());
						mActivity.startActivity(intent);
					}
				});
			} else
			{
				llOriginal.setVisibility(View.GONE);
			}

			// 底部转发次数和喜欢评论次数设置
			// 喜欢数
			SDViewBinder.setTextView(tvLikeCount, model.getCollect_count());
			// 评论数
			SDViewBinder.setTextView(tvCommentsCount, model.getComment_count());
			// 转发数
			SDViewBinder.setTextView(tvRelayCount, model.getRelay_count());

			convertView.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0)
				{
					Intent intent = new Intent();
					intent.setClass(activity, ShareDetailActivity.class);
					intent.putExtra(ShareDetailActivity.EXTRA_SHARE_ID, model.getShare_id());
					activity.startActivity(intent);
				}
			});

		}
		return convertView;
	}

	private void bindShareImageContent(List<UItemImageModel> listShareImages, ImageView... ivImages)
	{
		if (listShareImages != null && listShareImages.size() > 0) // 被回复的内容中包含图片
		{
			UItemImageModel imageModel = null;
			ImageView ivImage = null;
			for (int i = 0; i < listShareImages.size(); i++)
			{
				imageModel = listShareImages.get(i);
				ivImage = ivImages[i];
				if (ivImage != null)
				{
					ivImage.setVisibility(View.VISIBLE);
					SDViewBinder.setImageView(ivImage, imageModel.getSmall_img());
				}
			}
		}
	}

}

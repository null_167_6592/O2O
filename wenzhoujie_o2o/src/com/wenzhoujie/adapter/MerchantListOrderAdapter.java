package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.model.MerchantlistActItemModel;
import com.wenzhoujie.utils.ViewHolder;

public class MerchantListOrderAdapter extends SDBaseAdapter<MerchantlistActItemModel>
{

	public MerchantListOrderAdapter(List<MerchantlistActItemModel> listModel, Activity activity)
	{
		super(listModel, activity);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_lv_merchant_list_order, null);
		}
		ImageView ivLogo = ViewHolder.get(convertView, R.id.item_lv_merchant_list_order_iv_logo);
		TextView tvName = ViewHolder.get(convertView, R.id.item_lv_merchant_list_order_tv_name);
		TextView tvbrief = ViewHolder.get(convertView, R.id.item_lv_merchant_list_order_tv_brief);
		TextView tvAddress = ViewHolder.get(convertView, R.id.item_lv_merchant_list_order_tv_address);
		TextView tvDistance = ViewHolder.get(convertView, R.id.item_lv_merchant_list_order_tv_distance);

		MerchantlistActItemModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setImageView(ivLogo, model.getLogo());
			SDViewBinder.setTextView(tvName, model.getName());
			SDViewBinder.setTextView(tvbrief, model.getBrief());
			SDViewBinder.setTextView(tvAddress, model.getApi_address());
			if (model.getDistance_format_int() == 0)
			{
				tvDistance.setVisibility(View.GONE);
			} else
			{
				tvDistance.setVisibility(View.VISIBLE);
				SDViewBinder.setTextView(tvDistance, model.getDistance_format_string());
			}
		}

		return convertView;
	}

}

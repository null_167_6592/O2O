package com.wenzhoujie.adapter;

import java.text.DecimalFormat;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.wenzhoujie.library.utils.SDToast;

import com.umeng.socialize.facebook.controller.utils.ToastUtil;
import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.SuperMarketDetailActivity;
import com.wenzhoujie.TuanDetailActivity;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.model.MerchantdetailClassifyModel;
import com.wenzhoujie.utils.ViewHolder;

public class ChaoshiCateAdapter extends SDBaseAdapter<MerchantdetailClassifyModel>{


	public ChaoshiCateAdapter(List<MerchantdetailClassifyModel> listModel, Activity activity)
	{

		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_chaoshi_cate, null);
		}

//		View viewDiv = ViewHolder.get(convertView, R.id.item_home_recommend_goods_view_div);
		ImageView ivImage = ViewHolder.get(convertView, R.id.biz_cate_image);
		TextView tvName = ViewHolder.get(convertView, R.id.biz_cate_name);

		if (position == 0)
		{
	//		viewDiv.setVisibility(View.GONE);
		}

		final MerchantdetailClassifyModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setImageView(ivImage, model.getImg());
			SDViewBinder.setTextView(tvName, model.getName());

			convertView.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if (model.getId() != 0)
					{
						SDToast.showToast(model.getName(), Toast.LENGTH_SHORT);
						
//						Intent intent = new Intent(App.getApplication(), TuanDetailActivity.class);
//						intent.putExtra(TuanDetailActivity.EXTRA_GOODS_ID, model.getId());
//						intent.putExtra(TuanDetailActivity.EXTRA_GOODS_TYPE, 0);
//						mActivity.startActivity(intent);
					}
				}
			});
		}

		return convertView;
	}

	public String stringTo2double(String num) {
		Double total_price_double = Double.parseDouble(num);
		if(total_price_double>0){
		DecimalFormat df = new DecimalFormat("###.00");
		return df.format(total_price_double);
		}else
		{
			return "0.00";
		}
	}
}
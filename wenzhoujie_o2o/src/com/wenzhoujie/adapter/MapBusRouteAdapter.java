package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.model.MapBusRouteModel;
import com.wenzhoujie.utils.ViewHolder;

public class MapBusRouteAdapter extends SDBaseAdapter<MapBusRouteModel>
{

	public MapBusRouteAdapter(List<MapBusRouteModel> listModel, Activity activity)
	{
		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_routeinfo, null);
		}
		TextView tvIndex = ViewHolder.get(convertView, R.id.item_routeinfo_tv_index);
		TextView tvName = ViewHolder.get(convertView, R.id.item_routeinfo_tv_name);
		TextView tvTime = ViewHolder.get(convertView, R.id.item_routeinfo_tv_time);
		TextView tvDistance = ViewHolder.get(convertView, R.id.item_routeinfo_tv_distance);

		MapBusRouteModel model = getItem(position);
		if (model != null)
		{
			tvIndex.setText(String.valueOf(position + 1));
			SDViewBinder.setTextView(tvName, model.getName());
			SDViewBinder.setTextView(tvTime, model.getTime());
			SDViewBinder.setTextView(tvDistance, model.getDistance());
		}

		return convertView;
	}

}

package com.wenzhoujie.adapter;

import java.util.List;

import android.app.Activity;
import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wenzhoujie.library.adapter.SDBaseAdapter;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.model.TuanGoodsModel;
import com.wenzhoujie.utils.ViewHolder;

public class TuanListAdapter extends SDBaseAdapter<TuanGoodsModel>
{

	public TuanListAdapter(List<TuanGoodsModel> listModel, Activity activity)
	{
		super(listModel, activity);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.item_home_recommend_deals, null);
		}

		View viewDiv = ViewHolder.get(convertView, R.id.item_home_recommend_deals_view_div);
		ImageView ivImage = ViewHolder.get(convertView, R.id.item_home_recommend_deals_iv_image);
		ImageView ivDoNotNeedOrder = ViewHolder.get(convertView, R.id.item_home_recommend_deals_iv_do_not_need_order);
		ImageView ivIsNew = ViewHolder.get(convertView, R.id.item_home_recommend_deals_iv_is_new);
		TextView tvName = ViewHolder.get(convertView, R.id.item_home_recommend_deals_tv_name);
		TextView tvBrief = ViewHolder.get(convertView, R.id.item_home_recommend_deals_tv_brief);
		TextView tvCurrentPrice = ViewHolder.get(convertView, R.id.item_home_recommend_deals_tv_current_price);
		TextView tvOriginalPrice = ViewHolder.get(convertView, R.id.item_home_recommend_deals_tv_original_price);
		TextView tvBuyCount = ViewHolder.get(convertView, R.id.item_home_recommend_deals_tv_buy_count);
		TextView tvDistance = ViewHolder.get(convertView, R.id.item_home_recommend_deals_tv_distance);
		if (position == 0)
		{
			viewDiv.setVisibility(View.GONE);
		}

		TuanGoodsModel model = getItem(position);
		if (model != null)
		{
			SDViewBinder.setImageView(ivImage, model.getIcon());
			SDViewBinder.setTextView(tvName, model.getSub_name());
			SDViewBinder.setTextView(tvBrief, model.getName());
			SDViewBinder.setTextView(tvCurrentPrice, model.getCurrent_price_fromat_string());
			tvOriginalPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG); // 设置中划线效果
			SDViewBinder.setTextView(tvOriginalPrice, model.getOrigin_price_fromat_string());
			SDViewBinder.setTextView(tvBuyCount, model.getBuy_count());
			SDViewBinder.setTextView(tvDistance, model.getDistance_format());

			// SDViewBinder.setViewsVisibility(ivDoNotNeedOrder,
			// model.getAuto_order_fromat_int());
			// SDViewBinder.setViewsVisibility(ivIsNew,
			// model.getIs_taday_format_int());
		}

		return convertView;
	}

}

package com.wenzhoujie.listener;

import com.lidroid.xutils.http.ResponseInfo;
import com.wenzhoujie.model.Mobile_qrcodeActModel;

public interface RequestScanResultListener
{

	void onStart();

	void onFailure();

	void onFinish();

	void onSuccess(ResponseInfo<String> responseInfo, Mobile_qrcodeActModel model);

}

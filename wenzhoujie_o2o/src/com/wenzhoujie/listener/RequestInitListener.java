package com.wenzhoujie.listener;

import com.wenzhoujie.model.act.InitActModel;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;

public interface RequestInitListener
{

	public void onStart();

	public void onSuccess(ResponseInfo<String> responseInfo, InitActModel model);

	public void onFailure(HttpException error, String msg);

	public void onFinish();
}

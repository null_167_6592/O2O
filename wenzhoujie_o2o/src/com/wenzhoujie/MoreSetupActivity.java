package com.wenzhoujie;

import android.os.Bundle;
import android.view.View;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.fragment.MoreFragment;
import com.wenzhoujie.fragment.WebViewFragment;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.Show_articleActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;

/**
 * 更多设置
 * 
 * @author song
 * 
 */
public class MoreSetupActivity extends BaseActivity
{
	private MoreFragment mFragMore;
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		mIsNeedSlideFinishLayout = false;
		setmTitleType(TitleType.TITLE_SIMPLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_more_setup);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		requestData();
	}

	private void requestData()
	{
		if (mFragMore == null)
		{
			mFragMore = new MoreFragment();
			replaceFragment(mFragMore, R.id.act_more_setup_content);
		}
		//toggleFragment(mFragMore);
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("更多");
	}

}
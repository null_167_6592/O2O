package com.wenzhoujie;

import android.os.Bundle;

import com.lingou.www.R;
import com.wenzhoujie.fragment.ECshopListFragment;

/**
 * 商城
 * 
 * @author js02
 * 
 */
public class ECshopListActivity extends BaseActivity
{

	private ECshopListFragment mFragECshop;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_ecshop_list);
		init();

	}

	private void init()
	{
		mFragECshop = new ECshopListFragment();
		replaceFragment(mFragECshop, R.id.act_echsop_fl_fragment_content);
	}

}
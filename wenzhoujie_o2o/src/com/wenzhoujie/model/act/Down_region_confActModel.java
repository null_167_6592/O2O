package com.wenzhoujie.model.act;


public class Down_region_confActModel extends BaseActModel
{

	private String file_exists = null;
	private String region_num = null;
	private String file_url = null;
	private String file_size = null;

	public String getFile_exists()
	{
		return file_exists;
	}

	public void setFile_exists(String file_exists)
	{
		this.file_exists = file_exists;
	}

	public String getRegion_num()
	{
		return region_num;
	}

	public void setRegion_num(String region_num)
	{
		this.region_num = region_num;
	}

	public String getFile_url()
	{
		return file_url;
	}

	public void setFile_url(String file_url)
	{
		this.file_url = file_url;
	}

	public String getFile_size()
	{
		return file_size;
	}

	public void setFile_size(String file_size)
	{
		this.file_size = file_size;
	}

}

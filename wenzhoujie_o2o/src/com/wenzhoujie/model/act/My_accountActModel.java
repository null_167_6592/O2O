package com.wenzhoujie.model.act;

public class My_accountActModel extends BaseActModel
{
	private String user_name;
	private String nick_name;
	private String user_money;
	private String user_money_format;
	private String user_score;
	private String coupon_count;
	private String youhui_count;
	private String not_pay_order_count;

	public String getUser_name()
	{
		return user_name;
	}

	public void setUser_name(String user_name)
	{
		this.user_name = user_name;
	}

	public String getNick_name()
	{
		return nick_name;
	}

	public void setNick_name(String nick_name)
	{
		this.nick_name = nick_name;
	}
	
	public String getUser_money()
	{
		return user_money;
	}

	public void setUser_money(String user_money)
	{
		this.user_money = user_money;
	}

	public String getUser_money_format()
	{
		return user_money_format;
	}

	public void setUser_money_format(String user_money_format)
	{
		this.user_money_format = user_money_format;
	}

	public String getUser_score()
	{
		return user_score;
	}

	public void setUser_score(String user_score)
	{
		this.user_score = user_score;
	}

	public String getCoupon_count()
	{
		return coupon_count;
	}

	public void setCoupon_count(String coupon_count)
	{
		this.coupon_count = coupon_count;
	}

	public String getYouhui_count()
	{
		return youhui_count;
	}

	public void setYouhui_count(String youhui_count)
	{
		this.youhui_count = youhui_count;
	}

	public String getNot_pay_order_count()
	{
		return not_pay_order_count;
	}

	public void setNot_pay_order_count(String not_pay_order_count)
	{
		this.not_pay_order_count = not_pay_order_count;
	}

}

package com.wenzhoujie.model.act;

import java.io.Serializable;
import java.util.List;

import com.wenzhoujie.model.GoodsCommentModel;
import com.wenzhoujie.model.GoodsdescSupplier_location_listModel;
import com.wenzhoujie.model.IndexActAdvsModel;
import com.wenzhoujie.model.MerchantShopItem;
import com.wenzhoujie.model.MerchantdetailClassifyModel;
import com.wenzhoujie.model.IndexActYouhui_listModel;
import com.wenzhoujie.model.SmCatesModel;

public class MerchantitemChaoshiActModel extends BaseActModel implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<IndexActAdvsModel> advs;
	private List<SmCatesModel> cates;
	private List<MerchantShopItem> recommend_items;

	
	public List<IndexActAdvsModel> getAdvs()
	{
		return advs;
	}

	public void setAdvs(List<IndexActAdvsModel> advs)
	{
		this.advs = advs;
	}
	
	public List<SmCatesModel> getCates()
	{
		return cates;
	}

	public void setCates(List<SmCatesModel> cates)
	{
		this.cates = cates;
	}

	public List<MerchantShopItem> getRecommend_items() {
		return recommend_items;
	}

	public void setRecommend_items(List<MerchantShopItem> recommend_items) {
		this.recommend_items = recommend_items;
	}	
}

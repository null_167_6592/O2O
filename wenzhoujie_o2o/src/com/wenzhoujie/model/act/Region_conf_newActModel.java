package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.Region_confModel;

public class Region_conf_newActModel extends BaseActModel
{

	private List<Region_confModel> region_list;

	public List<Region_confModel> getRegion_list()
	{
		return region_list;
	}

	public void setRegion_list(List<Region_confModel> region_list)
	{
		this.region_list = region_list;
	}

}

package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.InitActAddr_tlistModel;
import com.wenzhoujie.model.InitActCitylistModel;
import com.wenzhoujie.model.InitActDeal_cate_listModel;
import com.wenzhoujie.model.InitActNewslistModel;
import com.wenzhoujie.model.QuansModel;

/**
 * 
 * @author yhz
 * @create time 2014-7-16
 */
public class InitActModel extends BaseActModel
{
	private int _id;

	private int city_id;

	private String city_name = null;

	private String catalog_id = null;

	private String catalog_id_name = null;

	private String shop_cate_id = null;

	private String shop_cate_id_name = null;

	private String event_cate_id = null;

	private String event_cate_id_name = null;

	private String region_version = null;

	private String only_one_delivery = null;

	private String kf_phone = null;

	private String kf_email = null;

	private String about_info = null;

	private String version = null;

	private String page_size = null;

	private String has_region = null;

	private String program_title = null;

	private String index_logo = null;

	private String api_sina = null;

	private String sina_app_key = null;

	private String sina_app_secret = null;

	private String sina_bind_url = null;

	private String api_tencent = null;

	private String tencent_app_key = null;

	private String tencent_app_secret = null;

	private String tencent_bind_url = null;

	private String api_qq = null;

	private String qq_app_key = null;

	private String qq_app_secret = null;

	private List<InitActCitylistModel> citylist = null;

	private List<InitActCitylistModel> hotList = null;

	private List<InitActNewslistModel> newslist = null;

	private List<InitActAddr_tlistModel> addr_tlist = null;

	private List<QuansModel> quanlist = null;

	private List<InitActDeal_cate_listModel> deal_cate_list = null;

	private String wx_app_key = null;

	private String wx_app_secret = null;

	public String getWx_app_key()
	{
		return wx_app_key;
	}

	public void setWx_app_key(String wx_app_key)
	{
		this.wx_app_key = wx_app_key;
	}

	public String getWx_app_secret()
	{
		return wx_app_secret;
	}

	public void setWx_app_secret(String wx_app_secret)
	{
		this.wx_app_secret = wx_app_secret;
	}

	public int get_id()
	{
		return _id;
	}

	public void set_id(int _id)
	{
		this._id = _id;
	}

	public String getApi_qq()
	{
		return api_qq;
	}

	public void setApi_qq(String api_qq)
	{
		this.api_qq = api_qq;
	}

	public String getQq_app_key()
	{
		return qq_app_key;
	}

	public void setQq_app_key(String qq_app_key)
	{
		this.qq_app_key = qq_app_key;
	}

	public String getQq_app_secret()
	{
		return qq_app_secret;
	}

	public void setQq_app_secret(String qq_app_secret)
	{
		this.qq_app_secret = qq_app_secret;
	}

	public String getApi_tencent()
	{
		return api_tencent;
	}

	public void setApi_tencent(String api_tencent)
	{
		this.api_tencent = api_tencent;
	}

	public String getTencent_app_key()
	{
		return tencent_app_key;
	}

	public void setTencent_app_key(String tencent_app_key)
	{
		this.tencent_app_key = tencent_app_key;
	}

	public String getTencent_app_secret()
	{
		return tencent_app_secret;
	}

	public void setTencent_app_secret(String tencent_app_secret)
	{
		this.tencent_app_secret = tencent_app_secret;
	}

	public String getTencent_bind_url()
	{
		return tencent_bind_url;
	}

	public void setTencent_bind_url(String tencent_bind_url)
	{
		this.tencent_bind_url = tencent_bind_url;
	}

	public String getApi_sina()
	{
		return api_sina;
	}

	public void setApi_sina(String api_sina)
	{
		this.api_sina = api_sina;
	}

	public String getSina_app_key()
	{
		return sina_app_key;
	}

	public void setSina_app_key(String sina_app_key)
	{
		this.sina_app_key = sina_app_key;
	}

	public String getSina_app_secret()
	{
		return sina_app_secret;
	}

	public void setSina_app_secret(String sina_app_secret)
	{
		this.sina_app_secret = sina_app_secret;
	}

	public String getSina_bind_url()
	{
		return sina_bind_url;
	}

	public void setSina_bind_url(String sina_bind_url)
	{
		this.sina_bind_url = sina_bind_url;
	}

	public int getCity_id()
	{
		return city_id;
	}

	public void setCity_id(int city_id)
	{
		this.city_id = city_id;
	}

	public String getCity_name()
	{
		return city_name;
	}

	public void setCity_name(String city_name)
	{
		this.city_name = city_name;
	}

	public String getCatalog_id()
	{
		return catalog_id;
	}

	public void setCatalog_id(String catalog_id)
	{
		this.catalog_id = catalog_id;
	}

	public String getCatalog_id_name()
	{
		return catalog_id_name;
	}

	public void setCatalog_id_name(String catalog_id_name)
	{
		this.catalog_id_name = catalog_id_name;
	}

	public String getShop_cate_id()
	{
		return shop_cate_id;
	}

	public void setShop_cate_id(String shop_cate_id)
	{
		this.shop_cate_id = shop_cate_id;
	}

	public String getShop_cate_id_name()
	{
		return shop_cate_id_name;
	}

	public void setShop_cate_id_name(String shop_cate_id_name)
	{
		this.shop_cate_id_name = shop_cate_id_name;
	}

	public String getEvent_cate_id()
	{
		return event_cate_id;
	}

	public void setEvent_cate_id(String event_cate_id)
	{
		this.event_cate_id = event_cate_id;
	}

	public String getEvent_cate_id_name()
	{
		return event_cate_id_name;
	}

	public void setEvent_cate_id_name(String event_cate_id_name)
	{
		this.event_cate_id_name = event_cate_id_name;
	}

	public String getRegion_version()
	{
		return region_version;
	}

	public void setRegion_version(String region_version)
	{
		this.region_version = region_version;
	}

	public String getOnly_one_delivery()
	{
		return only_one_delivery;
	}

	public void setOnly_one_delivery(String only_one_delivery)
	{
		this.only_one_delivery = only_one_delivery;
	}

	public String getKf_phone()
	{
		return kf_phone;
	}

	public void setKf_phone(String kf_phone)
	{
		this.kf_phone = kf_phone;
	}

	public String getKf_email()
	{
		return kf_email;
	}

	public void setKf_email(String kf_email)
	{
		this.kf_email = kf_email;
	}

	public String getAbout_info()
	{
		return about_info;
	}

	public void setAbout_info(String about_info)
	{
		this.about_info = about_info;
	}

	public String getVersion()
	{
		return version;
	}

	public void setVersion(String version)
	{
		this.version = version;
	}

	public String getPage_size()
	{
		return page_size;
	}

	public void setPage_size(String page_size)
	{
		this.page_size = page_size;
	}

	public String getHas_region()
	{
		return has_region;
	}

	public void setHas_region(String has_region)
	{
		this.has_region = has_region;
	}

	public String getProgram_title()
	{
		return program_title;
	}

	public void setProgram_title(String program_title)
	{
		this.program_title = program_title;
	}

	public String getIndex_logo()
	{
		return index_logo;
	}

	public void setIndex_logo(String index_logo)
	{
		this.index_logo = index_logo;
	}

	public List<InitActCitylistModel> getCitylist()
	{
		return citylist;
	}

	public void setCitylist(List<InitActCitylistModel> citylist)
	{
		this.citylist = citylist;
	}

	public List<InitActNewslistModel> getNewslist()
	{
		return newslist;
	}

	public void setNewslist(List<InitActNewslistModel> newslist)
	{
		this.newslist = newslist;
	}

	public List<InitActAddr_tlistModel> getAddr_tlist()
	{
		return addr_tlist;
	}

	public void setAddr_tlist(List<InitActAddr_tlistModel> addr_tlist)
	{
		this.addr_tlist = addr_tlist;
	}

	public List<QuansModel> getQuanlist()
	{
		return quanlist;
	}

	public void setQuanlist(List<QuansModel> quanlist)
	{
		this.quanlist = quanlist;
	}

	public List<InitActDeal_cate_listModel> getDeal_cate_list()
	{
		return deal_cate_list;
	}

	public void setDeal_cate_list(List<InitActDeal_cate_listModel> deal_cate_list)
	{
		this.deal_cate_list = deal_cate_list;
	}

	public List<InitActCitylistModel> getHoCitytList() {
		return hotList;
	}

	public void setHotCityList(List<InitActCitylistModel> hotList) {
		this.hotList = hotList;
	}

}

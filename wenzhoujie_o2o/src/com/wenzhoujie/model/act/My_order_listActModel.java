package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.My_order_listActItemModel;
import com.wenzhoujie.model.PageModel;

public class My_order_listActModel extends BaseActModel
{
	private String totalPages = null;
	private String pageRows = null;
	private String nowPage = null;
	private String totalRows = null;
	private List<My_order_listActItemModel> item = null;
	private String not_pay_count = null;
	private PageModel page = null;

	public String getTotalPages()
	{
		return totalPages;
	}

	public void setTotalPages(String totalPages)
	{
		this.totalPages = totalPages;
	}

	public String getPageRows()
	{
		return pageRows;
	}

	public void setPageRows(String pageRows)
	{
		this.pageRows = pageRows;
	}

	public String getNowPage()
	{
		return nowPage;
	}

	public void setNowPage(String nowPage)
	{
		this.nowPage = nowPage;
	}

	public String getTotalRows()
	{
		return totalRows;
	}

	public void setTotalRows(String totalRows)
	{
		this.totalRows = totalRows;
	}

	public List<My_order_listActItemModel> getItem()
	{
		return item;
	}

	public void setItem(List<My_order_listActItemModel> item)
	{
		this.item = item;
	}

	public String getNot_pay_count()
	{
		return not_pay_count;
	}

	public void setNot_pay_count(String not_pay_count)
	{
		this.not_pay_count = not_pay_count;
	}

	public PageModel getPage()
	{
		return page;
	}

	public void setPage(PageModel page)
	{
		this.page = page;
	}

}
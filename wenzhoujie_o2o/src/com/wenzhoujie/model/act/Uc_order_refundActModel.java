package com.wenzhoujie.model.act;

import java.util.ArrayList;
import java.util.List;

import com.wenzhoujie.model.DealOrderItemModel;

public class Uc_order_refundActModel extends BaseActModel
{

	private DealOrderItemModel item;

	public List<DealOrderItemModel> getListItem()
	{
		List<DealOrderItemModel> listItem = new ArrayList<DealOrderItemModel>();
		if (item != null)
		{
			listItem.add(item);
		}
		return listItem;
	}

	public DealOrderItemModel getItem()
	{
		return item;
	}

	public void setItem(DealOrderItemModel item)
	{
		this.item = item;
	}

}

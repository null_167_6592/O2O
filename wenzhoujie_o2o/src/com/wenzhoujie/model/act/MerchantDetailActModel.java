package com.wenzhoujie.model.act;

import java.io.Serializable;
import java.util.List;

import android.text.TextUtils;

import com.wenzhoujie.model.MerchantDetailActComment_listModel;
import com.wenzhoujie.model.MerchantDetailActTuan_listModel;
import com.wenzhoujie.utils.SDTypeParseUtil;

public class MerchantDetailActModel extends BaseActModel implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id = null;
	private String name = null;
	private String logo = null;
	private String xpoint = null;
	private String ypoint = null;
	private String api_address = null;
	private String address = null;
	private String dp_count = null;
	private String good_rate = null;
	private String deal_cate_id = null;
	private String tel = null;
	private String group_point = null;

	private String city_name = null;

	private String event_count = null;
	private String youhui_count = null;
	private String brand_id = null;
	private String brief = null;
	private List<MerchantDetailActTuan_listModel> tuan_list = null;

	private List<MerchantDetailActComment_listModel> comment_list = null;
	private int user_login_status;
	private String is_auto_order = null;

	// ------add------
	private float avg_point;
	private String distance = null;
	private String is_dy;
	private int tuan_count;
	private String comment_count;
	private int is_auto_order_format_int = 0;

	public String getIs_auto_order()
	{
		return is_auto_order;
	}

	public void setIs_auto_order(String is_auto_order)
	{
		this.is_auto_order = is_auto_order;
		if (!TextUtils.isEmpty(is_auto_order))
		{
			this.is_auto_order_format_int = SDTypeParseUtil.getIntFromString(is_auto_order, 0);
		}
	}

	public int getIs_auto_order_format_int()
	{
		return is_auto_order_format_int;
	}

	public void setIs_auto_order_format_int(int is_auto_order_format_int)
	{
		this.is_auto_order_format_int = is_auto_order_format_int;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public float getAvg_point()
	{
		return avg_point;
	}

	public void setAvg_point(float avg_point)
	{
		this.avg_point = avg_point;
	}

	public String getLogo()
	{
		return logo;
	}

	public void setLogo(String logo)
	{
		this.logo = logo;
	}

	public String getXpoint()
	{
		return xpoint;
	}

	public void setXpoint(String xpoint)
	{
		this.xpoint = xpoint;
	}

	public String getYpoint()
	{
		return ypoint;
	}

	public void setYpoint(String ypoint)
	{
		this.ypoint = ypoint;
	}

	public String getApi_address()
	{
		return api_address;
	}

	public void setApi_address(String api_address)
	{
		this.api_address = api_address;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public String getDp_count()
	{
		return dp_count;
	}

	public void setDp_count(String dp_count)
	{
		this.dp_count = dp_count;
	}

	public String getGood_rate()
	{
		return good_rate;
	}

	public void setGood_rate(String good_rate)
	{
		this.good_rate = good_rate;
	}

	public String getDeal_cate_id()
	{
		return deal_cate_id;
	}

	public void setDeal_cate_id(String deal_cate_id)
	{
		this.deal_cate_id = deal_cate_id;
	}

	public String getTel()
	{
		return tel;
	}

	public void setTel(String tel)
	{
		this.tel = tel;
	}

	public String getGroup_point()
	{
		return group_point;
	}

	public void setGroup_point(String group_point)
	{
		this.group_point = group_point;
	}

	public String getIs_dy()
	{
		return is_dy;
	}

	public void setIs_dy(String is_dy)
	{
		this.is_dy = is_dy;
	}

	public String getCity_name()
	{
		return city_name;
	}

	public void setCity_name(String city_name)
	{
		this.city_name = city_name;
	}

	public String getComment_count()
	{
		return comment_count;
	}

	public void setComment_count(String comment_count)
	{
		this.comment_count = "相关评论(" + comment_count + ")";
	}

	public String getEvent_count()
	{
		return event_count;
	}

	public void setEvent_count(String event_count)
	{
		this.event_count = event_count;
	}

	public String getYouhui_count()
	{
		return youhui_count;
	}

	public void setYouhui_count(String youhui_count)
	{
		this.youhui_count = youhui_count;
	}

	public String getBrand_id()
	{
		return brand_id;
	}

	public void setBrand_id(String brand_id)
	{
		this.brand_id = brand_id;
	}

	public String getDistance()
	{
		return distance;
	}

	public void setDistance(String distance)
	{
		this.distance = distance + "米";
	}

	public String getBrief()
	{
		return brief;
	}

	public void setBrief(String brief)
	{
		this.brief = brief;
	}

	public List<MerchantDetailActTuan_listModel> getTuan_list()
	{
		return tuan_list;
	}

	public void setTuan_list(List<MerchantDetailActTuan_listModel> tuan_list)
	{
		this.tuan_list = tuan_list;
	}

	public int getTuan_count()
	{
		return tuan_count;
	}

	public void setTuan_count(int tuan_count)
	{
		this.tuan_count = tuan_count;
	}

	public List<MerchantDetailActComment_listModel> getComment_list()
	{
		return comment_list;
	}

	public void setComment_list(List<MerchantDetailActComment_listModel> comment_list)
	{
		this.comment_list = comment_list;
	}

	public int getUser_login_status()
	{
		return user_login_status;
	}

	public void setUser_login_status(int user_login_status)
	{
		this.user_login_status = user_login_status;
	}
}
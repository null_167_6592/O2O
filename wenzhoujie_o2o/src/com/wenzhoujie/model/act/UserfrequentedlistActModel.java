package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.UserfrequentedlistActItemModel;

public class UserfrequentedlistActModel extends BaseActModel
{

	private List<UserfrequentedlistActItemModel> item = null;

	public List<UserfrequentedlistActItemModel> getItem()
	{
		return item;
	}

	public void setItem(List<UserfrequentedlistActItemModel> item)
	{
		this.item = item;
	}

}

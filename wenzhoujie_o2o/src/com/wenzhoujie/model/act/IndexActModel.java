package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.F_link_dataModel;
import com.wenzhoujie.model.HomeAdvsModel;
import com.wenzhoujie.model.HomeCatesModel;
import com.wenzhoujie.model.IndexActCate_type_listModel;
import com.wenzhoujie.model.IndexActDeal_listModel;
import com.wenzhoujie.model.IndexActIndexsModel;
import com.wenzhoujie.model.IndexActShop_cate_listModel;
import com.wenzhoujie.model.IndexActSupplier_listModel;

public class IndexActModel extends BaseActModel
{
	private String email = null;
	private String city_id = null;
	private String city_name = null;
	private List<HomeAdvsModel> advs = null;
	private List<HomeAdvsModel> advs_hot = null;
	private List<IndexActIndexsModel> indexs = null;
	private List<HomeCatesModel> cates = null;
	private List<IndexActCate_type_listModel> cate_type_list = null;
	private String event_list = null;
	private List<IndexActSupplier_listModel> supplier_list = null;
	private String deal_list = null;
	private List<IndexActShop_cate_listModel> shop_cate_list = null;
	private List<IndexActDeal_listModel> supplier_deal_list = null;
	private List<F_link_dataModel> f_link_data = null;

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getCity_id()
	{
		return city_id;
	}

	public void setCity_id(String city_id)
	{
		this.city_id = city_id;
	}

	public String getCity_name()
	{
		return city_name;
	}

	public void setCity_name(String city_name)
	{
		this.city_name = city_name;
	}

	public List<HomeAdvsModel> getAdvs()
	{
		return advs;
	}

	public void setAdvs(List<HomeAdvsModel> advs)
	{
		this.advs = advs;
	}

	public List<HomeAdvsModel> getAdvs_hot()
	{
		return advs_hot;
	}

	public void setAdvs_hot(List<HomeAdvsModel> advs_hot)
	{
		this.advs_hot = advs_hot;
	}

	public List<IndexActIndexsModel> getIndexs()
	{
		return indexs;
	}

	public void setIndexs(List<IndexActIndexsModel> indexs)
	{
		this.indexs = indexs;
	}

	public List<HomeCatesModel> getCates()
	{
		return cates;
	}

	public void setCates(List<HomeCatesModel> cates)
	{
		this.cates = cates;
	}

	public List<IndexActCate_type_listModel> getCate_type_list()
	{
		return cate_type_list;
	}

	public void setCate_type_list(List<IndexActCate_type_listModel> cate_type_list)
	{
		this.cate_type_list = cate_type_list;
	}

	public String getEvent_list()
	{
		return event_list;
	}

	public void setEvent_list(String event_list)
	{
		this.event_list = event_list;
	}

	public List<IndexActSupplier_listModel> getSupplier_list()
	{
		return supplier_list;
	}

	public void setSupplier_list(List<IndexActSupplier_listModel> supplier_list)
	{
		this.supplier_list = supplier_list;
	}

	public String getDeal_list()
	{
		return deal_list;
	}

	public void setDeal_list(String deal_list)
	{
		this.deal_list = deal_list;
	}

	public List<IndexActShop_cate_listModel> getShop_cate_list()
	{
		return shop_cate_list;
	}

	public void setShop_cate_list(List<IndexActShop_cate_listModel> shop_cate_list)
	{
		this.shop_cate_list = shop_cate_list;
	}

	public List<IndexActDeal_listModel> getSupplier_deal_list()
	{
		return supplier_deal_list;
	}

	public void setSupplier_deal_list(List<IndexActDeal_listModel> supplier_deal_list)
	{
		this.supplier_deal_list = supplier_deal_list;
	}

	public List<F_link_dataModel> getF_link_data()
	{
		return f_link_data;
	}

	public void setF_link_data(List<F_link_dataModel> f_link_data)
	{
		this.f_link_data = f_link_data;
	}

}
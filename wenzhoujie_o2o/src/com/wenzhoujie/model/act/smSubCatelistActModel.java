package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.Bcate_listModel;
import com.wenzhoujie.model.MerchantlistActF_link_dataModel;
import com.wenzhoujie.model.MerchantlistActItemModel;
import com.wenzhoujie.model.PageModel;
import com.wenzhoujie.model.Quan_listModel;
import com.wenzhoujie.model.smSubCateItemModel;

public class smSubCatelistActModel extends BaseActModel
{
	private List<smSubCateItemModel> cate_items;

	
	public List<smSubCateItemModel> getCate_items() {
		return cate_items;
	}
	public void setCate_items(List<smSubCateItemModel> cate_items) {
		this.cate_items = cate_items;
	}

}
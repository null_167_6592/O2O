package com.wenzhoujie.model.act;

public class SyncloginActModel extends BaseActModel
{

	private String user_pwd = null;
	private String uid = null;
	private String email = null;
	private String user_money = null;
	private String user_money_format = null;
	private String user_avatar = null;
	private String resulttype = null;
	private String access_token = null;
	private String sina_id = null;
	private String qq_id = null;
	private String user_name = null;
	private String login_type = null;
	private String user_score = null;
	private String nick_name;

	public String getQq_id()
	{
		return qq_id;
	}

	public void setQq_id(String qq_id)
	{
		this.qq_id = qq_id;
	}

	public String getUser_pwd()
	{
		return user_pwd;
	}

	public void setUser_pwd(String user_pwd)
	{
		this.user_pwd = user_pwd;
	}

	public String getUid()
	{
		return uid;
	}

	public void setUid(String uid)
	{
		this.uid = uid;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getUser_money()
	{
		return user_money;
	}

	public void setUser_money(String user_money)
	{
		this.user_money = user_money;
	}

	public String getUser_money_format()
	{
		return user_money_format;
	}

	public void setUser_money_format(String user_money_format)
	{
		this.user_money_format = user_money_format;
	}

	public String getUser_avatar()
	{
		return user_avatar;
	}

	public void setUser_avatar(String user_avatar)
	{
		this.user_avatar = user_avatar;
	}

	public String getResulttype()
	{
		return resulttype;
	}

	public void setResulttype(String resulttype)
	{
		this.resulttype = resulttype;
	}

	public String getAccess_token()
	{
		return access_token;
	}

	public void setAccess_token(String access_token)
	{
		this.access_token = access_token;
	}

	public String getSina_id()
	{
		return sina_id;
	}

	public void setSina_id(String sina_id)
	{
		this.sina_id = sina_id;
	}

	public String getUser_name()
	{
		return user_name;
	}

	public void setUser_name(String user_name)
	{
		this.user_name = user_name;
	}
	public String getNick_name()
	{
		return nick_name;
	}

	public void setNick_name(String nick_name)
	{
		this.nick_name = nick_name;
	}
	public String getLogin_type()
	{
		return login_type;
	}

	public void setLogin_type(String login_type)
	{
		this.login_type = login_type;
	}

	public String getUser_score()
	{
		return user_score;
	}

	public void setUser_score(String user_score)
	{
		this.user_score = user_score;
	}

}

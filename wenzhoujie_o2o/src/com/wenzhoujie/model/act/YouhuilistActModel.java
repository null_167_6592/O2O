package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.Bcate_listModel;
import com.wenzhoujie.model.PageModel;
import com.wenzhoujie.model.Quan_listModel;
import com.wenzhoujie.model.YouhuilistItemModel;

public class YouhuilistActModel extends BaseActModel
{
	private List<Bcate_listModel> bcate_list = null;
	private List<Quan_listModel> quan_list = null;

	private List<YouhuilistItemModel> item = null;

	private PageModel page = null;

	public List<Bcate_listModel> getBcate_list()
	{
		return bcate_list;
	}

	public void setBcate_list(List<Bcate_listModel> bcate_list)
	{
		this.bcate_list = bcate_list;
	}

	public List<Quan_listModel> getQuan_list()
	{
		return quan_list;
	}

	public void setQuan_list(List<Quan_listModel> quan_list)
	{
		this.quan_list = quan_list;
	}

	public List<YouhuilistItemModel> getItem()
	{
		return item;
	}

	public void setItem(List<YouhuilistItemModel> item)
	{
		this.item = item;
	}

	public PageModel getPage()
	{
		return page;
	}

	public void setPage(PageModel page)
	{
		this.page = page;
	}

}

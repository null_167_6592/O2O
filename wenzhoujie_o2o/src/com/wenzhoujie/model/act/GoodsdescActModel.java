package com.wenzhoujie.model.act;

import java.io.Serializable;
import java.util.List;

import android.text.TextUtils;

import com.wenzhoujie.model.F_link_dataModel;
import com.wenzhoujie.model.GoodsAttrsModel;
import com.wenzhoujie.model.GoodsCommentModel;
import com.wenzhoujie.model.GoodsExt_labelModel;
import com.wenzhoujie.model.GoodsdescSupplier_location_listModel;
import com.wenzhoujie.utils.SDTypeParseUtil;

public class GoodsdescActModel extends BaseActModel implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String city_name = null;
	private String goods_id = null;
	private String title = null;
	private String sub_name = null;
	private String image = null;
	private String buy_count = null;
	private String start_date = null;
	private String end_date = null;
	private String ori_price = null;
	private String cur_price = null;
	private String goods_brief = null;
	private String ori_price_format = null;
	private String cur_price_format = null;
	private String discount = null;
	private String address = null;
	private String num_unit = null;
	private String limit_num = null;
	private String goods_desc = null;
	private String is_shop = null;
	private String notes = null;
	// private String package = null;
	private String supplier_location_id = null;
	private String any_refund = null;
	private String expire_refund = null;
	private String time_status = null;
	private String end_time = null;
	private String begin_time = null;
	private String sp_detail = null;
	private String sp_tel = null;
	private String saving_format = null;
	private String less_time = null;
	private String has_attr = null;
	private String has_delivery = null;
	private String has_mcod = null;
	private String has_cart = null;
	private String change_cart_request_server = null;
	private String is_refund = null;
	private String avg_point = null;
	private String distance = null;
	private GoodsAttrsModel attr = null;
	private String share_content = null;
	private List<GoodsCommentModel> message_list = null;
	private String message_count = null;
	private List<String> gallery = null;
	private List<String> big_gallery = null;
	private List<GoodsdescActModel> deal_other = null;
	private String comment_list = null;
	private String point = null;
	private String width = null;
	private String comment_count = null;
	private List<F_link_dataModel> f_link_data = null;
	private String email = null;
	private String page_title = null;
	private String sp_location_id;
	private List<GoodsdescSupplier_location_listModel> supplier_location_list = null;
	private List<GoodsExt_labelModel> ext_label = null;

	// ----------add--------------
	private int has_attr_format_int = 0;

	private boolean is_refund_format_boolean = false;

	private String xpoint = null;

	private String ypoint = null;

	private String cate_id = null;

	private String id = null;

	private String name = null;

	public List<GoodsExt_labelModel> getExt_label()
	{
		return ext_label;
	}

	public void setExt_label(List<GoodsExt_labelModel> ext_label)
	{
		this.ext_label = ext_label;
	}

	public List<GoodsdescSupplier_location_listModel> getSupplier_location_list()
	{
		return supplier_location_list;
	}

	public void setSupplier_location_list(List<GoodsdescSupplier_location_listModel> supplier_location_list)
	{
		this.supplier_location_list = supplier_location_list;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getSp_location_id()
	{
		return sp_location_id;
	}

	public void setSp_location_id(String sp_location_id)
	{
		this.sp_location_id = sp_location_id;
	}

	public String getCate_id()
	{
		return cate_id;
	}

	public void setCate_id(String cate_id)
	{
		this.cate_id = cate_id;
	}

	public String getXpoint()
	{
		return xpoint;
	}

	public void setXpoint(String xpoint)
	{
		this.xpoint = xpoint;
	}

	public String getYpoint()
	{
		return ypoint;
	}

	public void setYpoint(String ypoint)
	{
		this.ypoint = ypoint;
	}

	public int getHas_attr_format_int()
	{
		return has_attr_format_int;
	}

	public void setHas_attr_format_int(int has_attr_format_int)
	{
		this.has_attr_format_int = has_attr_format_int;
	}

	public boolean isIs_refund_format_boolean()
	{
		return is_refund_format_boolean;
	}

	public void setIs_refund_format_boolean(boolean is_refund_format_boolean)
	{
		this.is_refund_format_boolean = is_refund_format_boolean;
	}

	public String getCity_name()
	{
		return city_name;
	}

	public void setCity_name(String city_name)
	{
		this.city_name = city_name;
	}

	public String getGoods_id()
	{
		return goods_id;
	}

	public void setGoods_id(String goods_id)
	{
		this.goods_id = goods_id;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getSub_name()
	{
		return sub_name;
	}

	public void setSub_name(String sub_name)
	{
		this.sub_name = sub_name;
	}

	public String getImage()
	{
		return image;
	}

	public void setImage(String image)
	{
		this.image = image;
	}

	public String getBuy_count()
	{
		return buy_count;
	}

	public void setBuy_count(String buy_count)
	{
		this.buy_count = buy_count;
	}

	public String getStart_date()
	{
		return start_date;
	}

	public void setStart_date(String start_date)
	{
		this.start_date = start_date;
	}

	public String getEnd_date()
	{
		return end_date;
	}

	public void setEnd_date(String end_date)
	{
		this.end_date = end_date;
	}

	public String getOri_price()
	{
		return ori_price;
	}

	public void setOri_price(String ori_price)
	{
		this.ori_price = ori_price;
	}

	public String getCur_price()
	{
		return cur_price;
	}

	public void setCur_price(String cur_price)
	{
		this.cur_price = cur_price;
	}

	public String getGoods_brief()
	{
		return goods_brief;
	}

	public void setGoods_brief(String goods_brief)
	{
		this.goods_brief = goods_brief;
	}

	public String getOri_price_format()
	{
		return ori_price_format;
	}

	public void setOri_price_format(String ori_price_format)
	{
		this.ori_price_format = ori_price_format;
	}

	public String getCur_price_format()
	{
		return cur_price_format;
	}

	public void setCur_price_format(String cur_price_format)
	{
		this.cur_price_format = cur_price_format;
	}

	public String getDiscount()
	{
		return discount;
	}

	public void setDiscount(String discount)
	{
		this.discount = discount;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public String getNum_unit()
	{
		return num_unit;
	}

	public void setNum_unit(String num_unit)
	{
		this.num_unit = num_unit;
	}

	public String getLimit_num()
	{
		return limit_num;
	}

	public void setLimit_num(String limit_num)
	{
		this.limit_num = limit_num;
	}

	public String getGoods_desc()
	{
		return goods_desc;
	}

	public void setGoods_desc(String goods_desc)
	{
		this.goods_desc = goods_desc;
	}

	public String getIs_shop()
	{
		return is_shop;
	}

	public void setIs_shop(String is_shop)
	{
		this.is_shop = is_shop;
	}

	public String getNotes()
	{
		return notes;
	}

	public void setNotes(String notes)
	{
		this.notes = notes;
	}

	public String getSupplier_location_id()
	{
		return supplier_location_id;
	}

	public void setSupplier_location_id(String supplier_location_id)
	{
		this.supplier_location_id = supplier_location_id;
	}

	public String getAny_refund()
	{
		return any_refund;
	}

	public void setAny_refund(String any_refund)
	{
		this.any_refund = any_refund;
	}

	public String getExpire_refund()
	{
		return expire_refund;
	}

	public void setExpire_refund(String expire_refund)
	{
		this.expire_refund = expire_refund;
	}

	public String getTime_status()
	{
		return time_status;
	}

	public void setTime_status(String time_status)
	{
		this.time_status = time_status;
	}

	public String getEnd_time()
	{
		return end_time;
	}

	public void setEnd_time(String end_time)
	{
		this.end_time = end_time;
	}

	public String getBegin_time()
	{
		return begin_time;
	}

	public void setBegin_time(String begin_time)
	{
		this.begin_time = begin_time;
	}

	public String getSp_detail()
	{
		return sp_detail;
	}

	public void setSp_detail(String sp_detail)
	{
		this.sp_detail = sp_detail;
	}

	public String getSp_tel()
	{
		return sp_tel;
	}

	public void setSp_tel(String sp_tel)
	{
		this.sp_tel = sp_tel;
	}

	public String getSaving_format()
	{
		return saving_format;
	}

	public void setSaving_format(String saving_format)
	{
		this.saving_format = saving_format;
	}

	public String getLess_time()
	{
		return less_time;
	}

	public void setLess_time(String less_time)
	{
		this.less_time = less_time;
	}

	public String getHas_attr()
	{
		return has_attr;
	}

	public void setHas_attr(String has_attr)
	{
		this.has_attr = has_attr;
		this.has_attr_format_int = SDTypeParseUtil.getIntFromString(has_attr, 0);
	}

	public String getHas_delivery()
	{
		return has_delivery;
	}

	public void setHas_delivery(String has_delivery)
	{
		this.has_delivery = has_delivery;
	}

	public String getHas_mcod()
	{
		return has_mcod;
	}

	public void setHas_mcod(String has_mcod)
	{
		this.has_mcod = has_mcod;
	}

	public String getHas_cart()
	{
		return has_cart;
	}

	public void setHas_cart(String has_cart)
	{
		this.has_cart = has_cart;
	}

	public String getChange_cart_request_server()
	{
		return change_cart_request_server;
	}

	public void setChange_cart_request_server(String change_cart_request_server)
	{
		this.change_cart_request_server = change_cart_request_server;
	}

	public String getIs_refund()
	{
		return is_refund;
	}

	public void setIs_refund(String is_refund)
	{
		this.is_refund = is_refund;
		if (!TextUtils.isEmpty(is_refund))
		{
			if (this.is_refund.equals("0"))
			{
				this.is_refund_format_boolean = false;
			} else if (this.is_refund.equals("1"))
			{
				this.is_refund_format_boolean = true;
			}
		}
	}

	public String getAvg_point()
	{
		return avg_point;
	}

	public void setAvg_point(String avg_point)
	{
		this.avg_point = avg_point;
	}

	public String getDistance()
	{
		return distance;
	}

	public void setDistance(String distance)
	{
		this.distance = distance;
	}

	public GoodsAttrsModel getAttr()
	{
		return attr;
	}

	public void setAttr(GoodsAttrsModel attr)
	{
		this.attr = attr;
	}

	public String getShare_content()
	{
		return share_content;
	}

	public void setShare_content(String share_content)
	{
		this.share_content = share_content;
	}

	public List<GoodsCommentModel> getMessage_list()
	{
		return message_list;
	}

	public void setMessage_list(List<GoodsCommentModel> message_list)
	{
		this.message_list = message_list;
	}

	public String getMessage_count()
	{
		return message_count;
	}

	public void setMessage_count(String message_count)
	{
		this.message_count = message_count;
	}

	public List<String> getGallery()
	{
		return gallery;
	}

	public void setGallery(List<String> gallery)
	{
		this.gallery = gallery;
	}

	public List<String> getBig_gallery()
	{
		return big_gallery;
	}

	public void setBig_gallery(List<String> big_gallery)
	{
		this.big_gallery = big_gallery;
	}

	public List<GoodsdescActModel> getDeal_other()
	{
		return deal_other;
	}

	public void setDeal_other(List<GoodsdescActModel> deal_other)
	{
		this.deal_other = deal_other;
	}

	public String getComment_list()
	{
		return comment_list;
	}

	public void setComment_list(String comment_list)
	{
		this.comment_list = comment_list;
	}

	public String getPoint()
	{
		return point;
	}

	public void setPoint(String point)
	{
		this.point = point;
	}

	public String getWidth()
	{
		return width;
	}

	public void setWidth(String width)
	{
		this.width = width;
	}

	public String getComment_count()
	{
		return comment_count;
	}

	public void setComment_count(String comment_count)
	{
		this.comment_count = comment_count;
	}

	public List<F_link_dataModel> getF_link_data()
	{
		return f_link_data;
	}

	public void setF_link_data(List<F_link_dataModel> f_link_data)
	{
		this.f_link_data = f_link_data;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getPage_title()
	{
		return page_title;
	}

	public void setPage_title(String page_title)
	{
		this.page_title = page_title;
	}

}
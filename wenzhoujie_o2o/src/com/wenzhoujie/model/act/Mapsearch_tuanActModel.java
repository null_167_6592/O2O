package com.wenzhoujie.model.act;

import java.util.List;

/**
 * 
 * @author yhz
 * @create time 2014-8-15
 */
public class Mapsearch_tuanActModel extends BaseActModel
{
	List<GoodsdescActModel> item = null;

	public List<GoodsdescActModel> getItem()
	{
		return item;
	}

	public void setItem(List<GoodsdescActModel> item)
	{
		this.item = item;
	}

}

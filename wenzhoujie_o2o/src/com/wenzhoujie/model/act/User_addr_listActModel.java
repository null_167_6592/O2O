package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.DeliveryModel;

public class User_addr_listActModel extends BaseActModel
{
	private List<DeliveryModel> item = null;

	public List<DeliveryModel> getItem()
	{
		return item;
	}

	public void setItem(List<DeliveryModel> item)
	{
		this.item = item;
	}

}

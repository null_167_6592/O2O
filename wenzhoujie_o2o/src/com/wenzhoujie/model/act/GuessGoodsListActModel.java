package com.wenzhoujie.model.act;

import java.io.Serializable;
import java.util.List;

public class GuessGoodsListActModel extends BaseActModel implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<GuessGoodsActModel> gglist;
	
	public List<GuessGoodsActModel> getGGlist() {
		return gglist;
	}
	public void setGGlist(List<GuessGoodsActModel> gglist) {
		this.gglist = gglist;
	}
}

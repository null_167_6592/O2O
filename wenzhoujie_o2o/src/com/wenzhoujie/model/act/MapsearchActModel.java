package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.MapsearchDaijin_listModel;
import com.wenzhoujie.model.MapsearchEvent_listModel;
import com.wenzhoujie.model.MapsearchMerchant_listModel;
import com.wenzhoujie.model.MapsearchTuan_listModel;
import com.wenzhoujie.model.MapsearchYouhui_listModel;

public class MapsearchActModel extends BaseActModel
{

	private List<MapsearchYouhui_listModel> youhui_list = null;

	private List<MapsearchEvent_listModel> event_list = null;

	private List<MapsearchTuan_listModel> tuan_list = null;

	private List<MapsearchDaijin_listModel> daijin_list = null;

	private List<MapsearchMerchant_listModel> merchant_list = null;

	public List<MapsearchYouhui_listModel> getYouhui_list()
	{
		return youhui_list;
	}

	public void setYouhui_list(List<MapsearchYouhui_listModel> youhui_list)
	{
		this.youhui_list = youhui_list;
	}

	public List<MapsearchEvent_listModel> getEvent_list()
	{
		return event_list;
	}

	public void setEvent_list(List<MapsearchEvent_listModel> event_list)
	{
		this.event_list = event_list;
	}

	public List<MapsearchTuan_listModel> getTuan_list()
	{
		return tuan_list;
	}

	public void setTuan_list(List<MapsearchTuan_listModel> tuan_list)
	{
		this.tuan_list = tuan_list;
	}

	public List<MapsearchDaijin_listModel> getDaijin_list()
	{
		return daijin_list;
	}

	public void setDaijin_list(List<MapsearchDaijin_listModel> daijin_list)
	{
		this.daijin_list = daijin_list;
	}

	public List<MapsearchMerchant_listModel> getMerchant_list()
	{
		return merchant_list;
	}

	public void setMerchant_list(List<MapsearchMerchant_listModel> merchant_list)
	{
		this.merchant_list = merchant_list;
	}

}

package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.Index2ActCity_listModel;
import com.wenzhoujie.model.Index2ActF_link_dataModel;
import com.wenzhoujie.model.QuansModel;
import com.wenzhoujie.model.TuanGoodsModel;

public class Index2ActModel extends BaseActModel
{
	private String email = null;
	private String city_id = null;
	private List<QuansModel> quans = null;
	private String quan_id = null;
	private List<Index2ActCity_listModel> city_list = null;
	private List<Index2ActF_link_dataModel> f_link_data = null;
	private List<TuanGoodsModel> deal_list = null;

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getCity_id()
	{
		return city_id;
	}

	public void setCity_id(String city_id)
	{
		this.city_id = city_id;
	}

	public List<QuansModel> getQuans()
	{
		return quans;
	}

	public void setQuans(List<QuansModel> quans)
	{
		this.quans = quans;
	}

	public String getQuan_id()
	{
		return quan_id;
	}

	public void setQuan_id(String quan_id)
	{
		this.quan_id = quan_id;
	}

	public List<Index2ActCity_listModel> getCity_list()
	{
		return city_list;
	}

	public void setCity_list(List<Index2ActCity_listModel> city_list)
	{
		this.city_list = city_list;
	}

	public List<Index2ActF_link_dataModel> getF_link_data()
	{
		return f_link_data;
	}

	public void setF_link_data(List<Index2ActF_link_dataModel> f_link_data)
	{
		this.f_link_data = f_link_data;
	}

	public List<TuanGoodsModel> getDeal_list()
	{
		return deal_list;
	}

	public void setDeal_list(List<TuanGoodsModel> deal_list)
	{
		this.deal_list = deal_list;
	}

}
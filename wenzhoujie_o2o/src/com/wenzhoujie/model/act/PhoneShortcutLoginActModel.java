package com.wenzhoujie.model.act;

import com.wenzhoujie.model.PhoneShortcutLoginActItemHomeUserModel;

public class PhoneShortcutLoginActModel extends BaseActModel
{
	private String uid = null;
	private String user_name = null;
	private String user_email = null;
	private String user_avatar = null;
	private String user_pwd = null;
	private String user_money_format = null;
	private String user_money = null;
	private String user_score = null;
	private PhoneShortcutLoginActItemHomeUserModel home_user = null;
	private String mobile = null;
	private String nick_name;

	public String getMobile()
	{
		return mobile;
	}

	public void setMobile(String mobile)
	{
		this.mobile = mobile;
	}

	public PhoneShortcutLoginActItemHomeUserModel getHome_user()
	{
		return home_user;
	}

	public void setHome_user(PhoneShortcutLoginActItemHomeUserModel home_user)
	{
		this.home_user = home_user;
	}

	public String getUser_money_format()
	{
		return user_money_format;
	}

	public void setUser_money_format(String user_money_format)
	{
		this.user_money_format = user_money_format;
	}

	public String getUser_money()
	{
		return user_money;
	}

	public void setUser_money(String user_money)
	{
		this.user_money = user_money;
	}

	public String getUid()
	{
		return uid;
	}

	public void setUid(String uid)
	{
		this.uid = uid;
	}

	public String getUser_name()
	{
		return user_name;
	}

	public void setUser_name(String user_name)
	{
		this.user_name = user_name;
	}
	public String getNick_name()
	{
		return nick_name;
	}

	public void setNick_name(String nick_name)
	{
		this.nick_name = nick_name;
	}
	public String getUser_email()
	{
		return user_email;
	}

	public void setUser_email(String user_email)
	{
		this.user_email = user_email;
	}

	public String getUser_avatar()
	{
		return user_avatar;
	}

	public void setUser_avatar(String user_avatar)
	{
		this.user_avatar = user_avatar;
	}

	public String getUser_pwd()
	{
		return user_pwd;
	}

	public void setUser_pwd(String user_pwd)
	{
		this.user_pwd = user_pwd;
	}

	public String getUser_score()
	{
		return user_score;
	}

	public void setUser_score(String user_score)
	{
		this.user_score = user_score;
	}

}

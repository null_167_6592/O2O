package com.wenzhoujie.model.act;

import java.io.Serializable;

public class GuessGoodsActModel extends BaseActModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String name;
	private String address;
	private String tel;
	private String good_dp_count;
	private String preview;
	private int shop_type;
	
	public int getShop_type() {
		return shop_type;
	}
	public void setShop_type(int shop_type) {
		this.shop_type = shop_type;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getGood_dp_count() {
		return good_dp_count;
	}
	public void setGood_dp_count(String good_dp_count) {
		this.good_dp_count = good_dp_count;
	}
	public String getPreview() {
		return preview;
	}
	public void setPreview(String preview) {
		this.preview = preview;
	}
	
	
}

package com.wenzhoujie.model.act;

public class BaseActModel
{
	protected String act = null;
	protected String act_2 = null;
	protected int response_code = -999;
	protected String info = null;
	protected int user_login_status = 0;
	protected String status = null;

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public int getUser_login_status()
	{
		return user_login_status;
	}

	public void setUser_login_status(int user_login_status)
	{
		this.user_login_status = user_login_status;
	}

	public String getInfo()
	{
		return info;
	}

	public void setInfo(String show_err)
	{
		this.info = show_err;
	}

	public int getResponse_code()
	{
		return response_code;
	}

	public void setResponse_code(int response_code)
	{
		this.response_code = response_code;
	}

	public String getAct()
	{
		return act;
	}

	public void setAct(String act)
	{
		this.act = act;
	}

	public String getAct_2()
	{
		return act_2;
	}

	public void setAct_2(String act_2)
	{
		this.act_2 = act_2;
	}

}

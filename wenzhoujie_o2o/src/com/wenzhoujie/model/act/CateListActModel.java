package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.CateListActItemModel;
import com.wenzhoujie.model.PageModel;

public class CateListActModel extends BaseActModel
{
	private List<CateListActItemModel> item = null;

	public List<CateListActItemModel> getItem()
	{
		return item;
	}

	public void setItem(List<CateListActItemModel> item)
	{
		this.item = item;
	}

	public PageModel getPage()
	{
		return page;
	}

	public void setPage(PageModel page)
	{
		this.page = page;
	}

	private PageModel page = null;
}
package com.wenzhoujie.model.act;

public class InfoModel extends BaseActModel
{
	private String info;

	public String getInfo()
	{
		return info;
	}

	public void setInfo(String info)
	{
		this.info = info;
	}
}
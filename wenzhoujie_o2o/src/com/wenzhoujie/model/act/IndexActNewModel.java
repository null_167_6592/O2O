package com.wenzhoujie.model.act;

import java.io.Serializable;
import java.sql.Array;
import java.util.ArrayList;
import java.util.List;

import android.content.Intent;

import com.sunday.eventbus.SDEventManager;
import com.wenzhoujie.AboutUsActivity;
import com.wenzhoujie.ECshopListActivity;
import com.wenzhoujie.EventListActivity;
import com.wenzhoujie.EventsDetailActivity;
import com.wenzhoujie.HomeSearchActivity;
import com.wenzhoujie.MerchantDetailNewActivity;
import com.wenzhoujie.MerchantListActivity;
import com.wenzhoujie.MerchantListOrderActivity;
import com.wenzhoujie.MyCaptureActivity;
import com.wenzhoujie.NearbyVipActivity;
import com.wenzhoujie.NewsListActivity;
import com.wenzhoujie.ShareDetailActivity;
import com.wenzhoujie.SuperMarketDetailActivity;
import com.wenzhoujie.TuanDetailActivity;
import com.wenzhoujie.TuanListActivity;
import com.wenzhoujie.WebViewActivity;
import com.wenzhoujie.YouHuiDetailActivity;
import com.wenzhoujie.YouHuiListActivity;
import com.wenzhoujie.app.App;
import com.wenzhoujie.event.EnumEventTag;
import com.wenzhoujie.fragment.ECshopListFragment;
import com.wenzhoujie.fragment.MerchantListFragment;
import com.wenzhoujie.model.IndexActAdvsDataModel;
import com.wenzhoujie.model.IndexActAdvsModel;
import com.wenzhoujie.model.IndexActCatesModel;
import com.wenzhoujie.model.IndexActDeal_listModel;
import com.wenzhoujie.model.IndexActEvent_listModel;
import com.wenzhoujie.model.IndexActIndexsModel;
import com.wenzhoujie.model.IndexActSupplier_listModel;
import com.wenzhoujie.model.IndexActYouhui_listModel;

public class IndexActNewModel extends BaseActModel implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<IndexActAdvsModel> advs;
	private List<IndexActIndexsModel> indexs;
	private List<IndexActSupplier_listModel> supplier_list;
	private List<IndexActDeal_listModel> deal_list;
	private List<IndexActDeal_listModel> supplier_deal_list;
	private List<IndexActEvent_listModel> event_list;
	private List<IndexActYouhui_listModel> youhui_list;
	private List<IndexActCatesModel> cates;
	private List<GuessGoodsActModel> guesses_list;
	private List<ShangQuanActModel> shangquan_list;
	
	/*private String more_list;
	
	public String getMore_list() {
		return more_list;
	}
	public void setMore_list(String more_list) {
		this.more_list = more_list;
	}*/
	private List<MoreCateActModel> more_list_a;
	
	public List<MoreCateActModel> getMore_list_a() {
		return more_list_a;
	}
	public void setMore_list_a(List<MoreCateActModel> more_list_a) {
		this.more_list_a = more_list_a;
	}
	
	public List<ShangQuanActModel> getShangquan_list() {
		return shangquan_list;
	}
	public void setShangquan_list(List<ShangQuanActModel> shangquan_list) {
		this.shangquan_list = shangquan_list;
	}

	public List<GuessGoodsActModel> getGuesses_list() {
		return guesses_list;
	}
	public void setGuesses_list(List<GuessGoodsActModel> guesses_list) {
		this.guesses_list = guesses_list;
	}

	public List<IndexActAdvsModel> getAdvs()
	{
		return advs;
	}

	public void setAdvs(List<IndexActAdvsModel> advs)
	{
		this.advs = advs;
	}

	public List<IndexActIndexsModel> getIndexs()
	{
		return indexs;
	}

	public void setIndexs(List<IndexActIndexsModel> indexs)
	{
		this.indexs = indexs;
	}

	public List<IndexActSupplier_listModel> getSupplier_list()
	{
		return supplier_list;
	}

	public void setSupplier_list(List<IndexActSupplier_listModel> supplier_list)
	{
		this.supplier_list = supplier_list;
	}

	public List<IndexActDeal_listModel> getDeal_list()
	{
		return deal_list;
	}

	public void setDeal_list(List<IndexActDeal_listModel> deal_list)
	{
		this.deal_list = deal_list;
	}

	public List<IndexActDeal_listModel> getSupplier_deal_list()
	{
		return supplier_deal_list;
	}

	public void setSupplier_deal_list(List<IndexActDeal_listModel> supplier_deal_list)
	{
		this.supplier_deal_list = supplier_deal_list;
	}

	public List<IndexActEvent_listModel> getEvent_list()
	{
		return event_list;
	}

	public void setEvent_list(List<IndexActEvent_listModel> event_list)
	{
		this.event_list = event_list;
	}

	public List<IndexActYouhui_listModel> getYouhui_list()
	{
		return youhui_list;
	}

	public void setYouhui_list(List<IndexActYouhui_listModel> youhui_list)
	{
		this.youhui_list = youhui_list;
	}

	public List<IndexActCatesModel> getCates()
	{
		return cates;
	}

	public void setCates(List<IndexActCatesModel> cates)
	{
		this.cates = cates;
	}
	
	private String zt_html;

	public String getZt_html()
	{
		return zt_html;
	}

	public void setZt_html(String zt_html)
	{
		this.zt_html = zt_html;
	}

	public static Intent createIntentByType(int type, IndexActAdvsDataModel data, boolean createDefaultIntent)
	{
		Intent intent = null;
		switch (type)
		{
		case 2: // url
			if (data != null)
			{
				intent = new Intent(App.getApplication(), WebViewActivity.class);
				intent.putExtra(WebViewActivity.EXTRA_URL, data.getUrl());
			}
			break;
		case 5: // 搜索页
			intent = new Intent(App.getApplication(), HomeSearchActivity.class);
			break;
		case 7: // 分享列表
			SDEventManager.post(EnumEventTag.OPEN_SHARE_LIST.ordinal());
			break;
		case 8: // 分享详细
			if (data != null)
			{
				intent = new Intent(App.getApplication(), ShareDetailActivity.class);
				intent.putExtra(ShareDetailActivity.EXTRA_SHARE_ID, data.getShare_id());
			}
			break;
		case 9:// 团购列表
			intent = new Intent(App.getApplication(), TuanListActivity.class);
			if (data != null)
			{
				intent.putExtra(TuanListActivity.EXTRA_CATE_ID, data.getCate_id());
			}
			break;
		case 10:// 商城列表
			intent = new Intent(App.getApplication(), ECshopListActivity.class);
			if (data != null)
			{
				intent.putExtra(ECshopListFragment.EXTRA_CATE_ID, data.getCate_id());
			}
			break;
		case 11:// 活动列表
			intent = new Intent(App.getApplication(), EventListActivity.class);
			if (data != null)
			{
				intent.putExtra(EventListActivity.EXTRA_CATE_ID, data.getCate_id());
			}
			break;
		case 12:// 优惠列表
			intent = new Intent(App.getApplication(), YouHuiListActivity.class);
			if (data != null)
			{
				intent.putExtra(YouHuiListActivity.EXTRA_CATE_ID, data.getCate_id());
			}
			break;
		case 14:// 团购明细
			if (data != null)
			{
				intent = new Intent(App.getApplication(), TuanDetailActivity.class);
				intent.putExtra(TuanDetailActivity.EXTRA_GOODS_ID, data.getData_id());
			}
			break;
		case 15:// 商品明细
			if (data != null)
			{
				intent = new Intent(App.getApplication(), TuanDetailActivity.class);
				intent.putExtra(TuanDetailActivity.EXTRA_GOODS_ID, data.getData_id());
			}
			break;
		case 16: // 活动明细
			if (data != null)
			{
				intent = new Intent(App.getApplication(), EventsDetailActivity.class);
				intent.putExtra(EventsDetailActivity.EXTRA_EVENTS_ID, data.getData_id());
			}
			break;
		case 17:// 优惠明细
			if (data != null)
			{
				intent = new Intent(App.getApplication(), YouHuiDetailActivity.class);
				intent.putExtra(YouHuiDetailActivity.EXTRA_YOUHUI_ID, data.getData_id());
			}
			break;
		case 19: // 关于我们
			intent = new Intent(App.getApplication(), AboutUsActivity.class);
			break;
		case 21: // 公告列表
			intent = new Intent(App.getApplication(), NewsListActivity.class);
			break;
		case 22: // 商家列表
			intent = new Intent(App.getApplication(), MerchantListActivity.class);
			if (data != null)
			{
				intent.putExtra(MerchantListFragment.EXTRA_CATE_ID, data.getCate_id());
			}
			break;
		case 23: // 商家明细
			if (data != null)
			{
				intent = new Intent(App.getApplication(), MerchantDetailNewActivity.class);
				intent.putExtra(MerchantDetailNewActivity.EXTRA_MERCHANT_ID, data.getData_id());
			}
			break;
		case 24: // 自主下单
			intent = new Intent(App.getApplication(), MerchantListOrderActivity.class);
			break;
		case 25: // 扫一扫
			intent = new Intent(App.getApplication(), MyCaptureActivity.class);
			break;
		case 26: // 附近的人
			intent = new Intent(App.getApplication(), NearbyVipActivity.class);
			break;
		case 27: //chaoshi
			intent = new Intent(App.getApplication(), SuperMarketDetailActivity.class);
			intent.putExtra(SuperMarketDetailActivity.EXTRA_MERCHANT_ID, data.getCate_id());
			intent.putExtra(SuperMarketDetailActivity.EXTRA_MERCHANT_TITLE, data.getName());
		
			break;
		case 28: //shangpinerji
			intent = new Intent(App.getApplication(), MerchantListActivity.class);
			if (data != null)
			{
				intent.putExtra(MerchantListFragment.EXTRA_CATE_TYPE_ID, data.getCate_id());
				intent.putExtra(MerchantListFragment.EXTRA_CATE_NAME, data.getCate_name());
			}
			break;
	
		default:
			if (createDefaultIntent)
			{
				intent = new Intent(App.getApplication(), TuanListActivity.class);
			}
			break;
		}
		return intent;
	}
	
	public static Intent createIntentByType(int type, int id, boolean createDefaultIntent)
	{
		Intent intent = null;
		switch (type)
		{
		case 5: // 搜索页
			intent = new Intent(App.getApplication(), HomeSearchActivity.class);
			break;
		case 7: // 分享列表
			SDEventManager.post(EnumEventTag.OPEN_SHARE_LIST.ordinal());
			break;
		case 8: // 分享详细
			intent = new Intent(App.getApplication(), ShareDetailActivity.class);
			intent.putExtra(ShareDetailActivity.EXTRA_SHARE_ID, id);
			break;
		case 9:// 团购列表
			intent = new Intent(App.getApplication(), TuanListActivity.class);
			intent.putExtra(TuanListActivity.EXTRA_CATE_ID, id);
			break;
		case 10:// 商城列表
			intent = new Intent(App.getApplication(), ECshopListActivity.class);
			intent.putExtra(ECshopListFragment.EXTRA_CATE_ID, id);
			break;
		case 11:// 活动列表
			intent = new Intent(App.getApplication(), EventListActivity.class);
			intent.putExtra(EventListActivity.EXTRA_CATE_ID, id);
			break;
		case 12:// 优惠列表
			intent = new Intent(App.getApplication(), YouHuiListActivity.class);
			intent.putExtra(YouHuiListActivity.EXTRA_CATE_ID, id);
			break;
		case 14:// 团购明细
			intent = new Intent(App.getApplication(), TuanDetailActivity.class);
			intent.putExtra(TuanDetailActivity.EXTRA_GOODS_ID, id);
			break;
		case 15:// 商品明细
			intent = new Intent(App.getApplication(), TuanDetailActivity.class);
			intent.putExtra(TuanDetailActivity.EXTRA_GOODS_ID, id);
			break;
		case 16: // 活动明细
			intent = new Intent(App.getApplication(), EventsDetailActivity.class);
			intent.putExtra(EventsDetailActivity.EXTRA_EVENTS_ID, id);
			break;
		case 17:// 优惠明细
			intent = new Intent(App.getApplication(), YouHuiDetailActivity.class);
			intent.putExtra(YouHuiDetailActivity.EXTRA_YOUHUI_ID, id);
			break;
		case 19: // 关于我们
			intent = new Intent(App.getApplication(), AboutUsActivity.class);
			break;
		case 21: // 公告列表
			intent = new Intent(App.getApplication(), NewsListActivity.class);
			break;
		case 22: // 商家列表
			intent = new Intent(App.getApplication(), MerchantListActivity.class);
			intent.putExtra(MerchantListFragment.EXTRA_CATE_ID, id);
			break;
		case 23: // 商家明细
			intent = new Intent(App.getApplication(), MerchantDetailNewActivity.class);
			intent.putExtra(MerchantDetailNewActivity.EXTRA_MERCHANT_ID, id);
			break;
		case 24: // 自主下单
			intent = new Intent(App.getApplication(), MerchantListOrderActivity.class);
			break;
		case 25: // 扫一扫
			intent = new Intent(App.getApplication(), MyCaptureActivity.class);
			break;
		case 26: // 附近的人
			intent = new Intent(App.getApplication(), NearbyVipActivity.class);
			break;
		case 27: //chaoshi
			intent = new Intent(App.getApplication(), SuperMarketDetailActivity.class);
			intent.putExtra(SuperMarketDetailActivity.EXTRA_MERCHANT_ID, id);
			intent.putExtra(SuperMarketDetailActivity.EXTRA_MERCHANT_TITLE, "");
		
			break;
		case 28: //shangpinerji
			intent = new Intent(App.getApplication(), MerchantListActivity.class);
			intent.putExtra(MerchantListFragment.EXTRA_CATE_TYPE_ID, id);
			intent.putExtra(MerchantListFragment.EXTRA_CATE_NAME, "");
			break;
	
		default:
			if (createDefaultIntent)
			{
				intent = new Intent(App.getApplication(), TuanListActivity.class);
			}
			break;
		}
		return intent;
	}

}

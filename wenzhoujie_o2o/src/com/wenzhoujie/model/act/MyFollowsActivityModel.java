package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.MyFollowsActivityHome_userModel;
import com.wenzhoujie.model.MyFollowsActivityItemModel;
import com.wenzhoujie.model.MyFollowsActivityUserModel;
import com.wenzhoujie.model.PageModel;

public class MyFollowsActivityModel extends BaseActModel
{
	private MyFollowsActivityUserModel user = null;
	private MyFollowsActivityHome_userModel home_user = null;
	private PageModel page = null;
	private List<MyFollowsActivityItemModel> item = null;

	public MyFollowsActivityUserModel getUser()
	{
		return user;
	}

	public void setUser(MyFollowsActivityUserModel user)
	{
		this.user = user;
	}

	public MyFollowsActivityHome_userModel getHome_user()
	{
		return home_user;
	}

	public void setHome_user(MyFollowsActivityHome_userModel home_user)
	{
		this.home_user = home_user;
	}

	public PageModel getPage()
	{
		return page;
	}

	public void setPage(PageModel page)
	{
		this.page = page;
	}

	public List<MyFollowsActivityItemModel> getItem()
	{
		return item;
	}

	public void setItem(List<MyFollowsActivityItemModel> item)
	{
		this.item = item;
	}
}
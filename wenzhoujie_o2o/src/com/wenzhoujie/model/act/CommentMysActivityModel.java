package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.CommentMysActivityItemModel;
import com.wenzhoujie.model.PageModel;

public class CommentMysActivityModel extends BaseActModel
{
	private List<CommentMysActivityItemModel> item = null;
	private PageModel page = null;

	public List<CommentMysActivityItemModel> getItem()
	{
		return item;
	}

	public void setItem(List<CommentMysActivityItemModel> item)
	{
		this.item = item;
	}

	public PageModel getPage()
	{
		return page;
	}

	public void setPage(PageModel page)
	{
		this.page = page;
	}
}
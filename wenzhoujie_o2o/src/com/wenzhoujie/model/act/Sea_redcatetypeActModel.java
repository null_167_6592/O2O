package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.CateTypeModel;

public class Sea_redcatetypeActModel extends BaseActModel
{

	private List<CateTypeModel> cate_type_list = null;

	private String page_title = null;

	public List<CateTypeModel> getCate_type_list()
	{
		return cate_type_list;
	}

	public void setCate_type_list(List<CateTypeModel> cate_type_list)
	{
		this.cate_type_list = cate_type_list;
	}

	public String getPage_title()
	{
		return page_title;
	}

	public void setPage_title(String page_title)
	{
		this.page_title = page_title;
	}

}

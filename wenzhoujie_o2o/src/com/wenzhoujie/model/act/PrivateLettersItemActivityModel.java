package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.PageModel;
import com.wenzhoujie.model.PrivateLettersItemActivityMsg_listModel;

public class PrivateLettersItemActivityModel extends BaseActModel
{
	private String lid = null;
	private String title = null;
	private String t_name = null;
	private PageModel page = null;
	private List<PrivateLettersItemActivityMsg_listModel> msg_list = null;

	public String getLid()
	{
		return lid;
	}

	public void setLid(String lid)
	{
		this.lid = lid;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getT_name()
	{
		return t_name;
	}

	public void setT_name(String t_name)
	{
		this.t_name = t_name;
	}

	public PageModel getPage()
	{
		return page;
	}

	public void setPage(PageModel page)
	{
		this.page = page;
	}

	public List<PrivateLettersItemActivityMsg_listModel> getMsg_list()
	{
		return msg_list;
	}

	public void setMsg_list(List<PrivateLettersItemActivityMsg_listModel> msg_list)
	{
		this.msg_list = msg_list;
	}
}
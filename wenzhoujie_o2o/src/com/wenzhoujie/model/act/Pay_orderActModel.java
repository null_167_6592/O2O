package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.MalipayModel;
import com.wenzhoujie.model.McodMode;
import com.wenzhoujie.model.Pay_orderActCouponlistModel;
import com.wenzhoujie.model.WalipayModel;
import com.wenzhoujie.model.WtenpayModel;
import com.wenzhoujie.model.WxAppModel;
import com.wenzhoujie.model.WxpayModel;

public class Pay_orderActModel extends BaseActModel
{
	private String pay_status = null;
	private String pay_code = null;
	private String order_id = null;
	private String order_sn = null;
	private String show_pay_btn = null;
	private String pay_money_format = null;
	private String pay_money = null;
	private String pay_info = null;
	private MalipayModel malipay = null; // 支付宝/各银行
	private McodMode mcod = null; // 货到付款
	private WalipayModel walipay = null; // 支付宝
	private WtenpayModel wtenpay = null; // 财付通
	private WxAppModel wxapp = null;
	private WxpayModel wxpay = null; // 微信支付

	private List<Pay_orderActCouponlistModel> couponlist = null;
	private String is_wap = null;// 1：直接跳转到pay_wap的url
	private String pay_wap = null;

	public String getPay_wap()
	{
		return pay_wap;
	}

	public void setPay_wap(String pay_wap)
	{
		this.pay_wap = pay_wap;
	}

	public String getIs_wap()
	{
		return is_wap;
	}

	public void setIs_wap(String is_wap)
	{
		this.is_wap = is_wap;
	}

	public List<Pay_orderActCouponlistModel> getCouponlist()
	{
		return couponlist;
	}

	public void setCouponlist(List<Pay_orderActCouponlistModel> couponlist)
	{
		this.couponlist = couponlist;
	}

	public MalipayModel getMalipay()
	{
		return malipay;
	}

	public void setMalipay(MalipayModel malipay)
	{
		this.malipay = malipay;
	}

	public McodMode getMcod()
	{
		return mcod;
	}

	public void setMcod(McodMode mcod)
	{
		this.mcod = mcod;
	}

	public WtenpayModel getWtenpay()
	{
		return wtenpay;
	}

	public void setWtenpay(WtenpayModel wtenpay)
	{
		this.wtenpay = wtenpay;
	}

	public String getPay_status()
	{
		return pay_status;
	}

	public void setPay_status(String pay_status)
	{
		this.pay_status = pay_status;
	}

	public String getPay_code()
	{
		return pay_code;
	}

	public void setPay_code(String pay_code)
	{
		this.pay_code = pay_code;
	}

	public String getOrder_id()
	{
		return order_id;
	}

	public void setOrder_id(String order_id)
	{
		this.order_id = order_id;
	}

	public String getOrder_sn()
	{
		return order_sn;
	}

	public void setOrder_sn(String order_sn)
	{
		this.order_sn = order_sn;
	}

	public String getShow_pay_btn()
	{
		return show_pay_btn;
	}

	public void setShow_pay_btn(String show_pay_btn)
	{
		this.show_pay_btn = show_pay_btn;
	}

	public String getPay_money_format()
	{
		return pay_money_format;
	}

	public void setPay_money_format(String pay_money_format)
	{
		this.pay_money_format = pay_money_format;
	}

	public String getPay_money()
	{
		return pay_money;
	}

	public void setPay_money(String pay_money)
	{
		this.pay_money = pay_money;
	}

	public String getPay_info()
	{
		return pay_info;
	}

	public void setPay_info(String pay_info)
	{
		this.pay_info = pay_info;
	}

	public WalipayModel getWalipay()
	{
		return walipay;
	}

	public void setWalipay(WalipayModel walipay)
	{
		this.walipay = walipay;
	}

	public WxpayModel getWxpay() {
		return wxpay;
	}

	public void setWxpay(WxpayModel wxpay) {
		this.wxpay = wxpay;
	}

	public WxAppModel getWxapp() {
		return wxapp;
	}

	public void setWxapp(WxAppModel wxapp) {
		this.wxapp = wxapp;
	}

}

package com.wenzhoujie.model.act;

import java.io.Serializable;
import java.util.List;

import com.wenzhoujie.model.GoodsCommentModel;
import com.wenzhoujie.model.GoodsdescSupplier_location_listModel;
import com.wenzhoujie.model.IndexActAdvsModel;
import com.wenzhoujie.model.MerchantShopItem;
import com.wenzhoujie.model.MerchantdetailClassifyModel;
import com.wenzhoujie.model.IndexActYouhui_listModel;

public class SmMoreActModel extends BaseActModel implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<MerchantShopItem> items;

	public List<MerchantShopItem> getItems() {
		return items;
	}

	public void setItems(List<MerchantShopItem> items) {
		this.items = items;
	}	
}

package com.wenzhoujie.model.act;

import java.util.List;

import com.wenzhoujie.model.EventsListActBigcate_listModel;
import com.wenzhoujie.model.EventsListActF_link_dataModel;
import com.wenzhoujie.model.EventsListActItemModel;
import com.wenzhoujie.model.PageModel;

public class EventsListActModel extends BaseActModel
{
	private List<EventsListActBigcate_listModel> bigcate_list = null;
	private List<EventsListActItemModel> item = null;
	private PageModel page = null;

	public List<EventsListActBigcate_listModel> getBigcate_list()
	{
		return bigcate_list;
	}

	public void setBigcate_list(List<EventsListActBigcate_listModel> bigcate_list)
	{
		this.bigcate_list = bigcate_list;
	}

	public List<EventsListActItemModel> getItem()
	{
		return item;
	}

	public void setItem(List<EventsListActItemModel> item)
	{
		this.item = item;
	}

	public PageModel getPage()
	{
		return page;
	}

	public void setPage(PageModel page)
	{
		this.page = page;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public List<EventsListActF_link_dataModel> getF_link_data()
	{
		return f_link_data;
	}

	public void setF_link_data(List<EventsListActF_link_dataModel> f_link_data)
	{
		this.f_link_data = f_link_data;
	}

	public String getPage_title()
	{
		return page_title;
	}

	public void setPage_title(String page_title)
	{
		this.page_title = page_title;
	}

	private String email = null;
	private List<EventsListActF_link_dataModel> f_link_data = null;
	private String page_title = null;
}
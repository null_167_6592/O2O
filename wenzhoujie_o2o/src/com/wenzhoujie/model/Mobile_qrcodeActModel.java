package com.wenzhoujie.model;

import com.wenzhoujie.model.act.BaseActModel;
import com.wenzhoujie.utils.SDTypeParseUtil;

public class Mobile_qrcodeActModel extends BaseActModel
{

	private String type;

	private IndexActAdvsDataModel data;

	// ===================add
	private int typeFormat;

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
		setTypeFormat(SDTypeParseUtil.getIntFromString(type, 0));
	}

	public IndexActAdvsDataModel getData()
	{
		return data;
	}

	public void setData(IndexActAdvsDataModel data)
	{
		this.data = data;
	}

	public int getTypeFormat()
	{
		return typeFormat;
	}

	public void setTypeFormat(int typeFormat)
	{
		this.typeFormat = typeFormat;
	}

}

package com.wenzhoujie.model;

import java.util.List;

//超市专页，
public class smSubCateListModel {

	private List<smSubCateItemModel> cate_items;

	
	public List<smSubCateItemModel> getCate_items() {
		return cate_items;
	}
	public void setCate_items(List<smSubCateItemModel> cate_items) {
		this.cate_items = cate_items;
	}

}

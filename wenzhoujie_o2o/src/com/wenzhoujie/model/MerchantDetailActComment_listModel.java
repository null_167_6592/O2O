package com.wenzhoujie.model;

import java.io.Serializable;

public class MerchantDetailActComment_listModel implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id = null;
	private String content = null;

	private String avg_price = null;
	private String create_time = null;
	private String user_id = null;
	private String user_name = null;
	private String time = null;

	// ------add------
	private float point;

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public float getPoint()
	{
		return point;
	}

	public void setPoint(float point)
	{
		this.point = point;
	}

	public String getAvg_price()
	{
		return avg_price;
	}

	public void setAvg_price(String avg_price)
	{
		this.avg_price = " 人均：￥" + avg_price;
	}

	public String getCreate_time()
	{
		return create_time;
	}

	public void setCreate_time(String create_time)
	{
		this.create_time = create_time;
	}

	public String getUser_id()
	{
		return user_id;
	}

	public void setUser_id(String user_id)
	{
		this.user_id = user_id;
	}

	public String getUser_name()
	{
		return user_name;
	}

	public void setUser_name(String user_name)
	{
		this.user_name = "用户：" + user_name;
	}

	public String getTime()
	{
		return time;
	}

	public void setTime(String time)
	{
		this.time = time;
	}
}
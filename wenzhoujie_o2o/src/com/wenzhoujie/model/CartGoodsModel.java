package com.wenzhoujie.model;

import java.io.Serializable;
import java.util.List;

import android.text.TextUtils;

import com.alibaba.fastjson.annotation.JSONField;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.model.act.GoodsdescActModel;
import com.wenzhoujie.utils.SDFormatUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;

public class CartGoodsModel implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private GoodsdescActModel goods;

	private int cart_id;// 本地购物车的唯一关键字
	private String goods_id;
	private String attr_id_1;// 属性类型一关键字(如10，23),与attr_title_1是对应的，一个是关键字，一个是显示名称
	private String attr_id_2;// 属性类型二关键字(如10，23),与attr_title_2是对应的，一个是关键字，一个是显示名称
	private String attr_id_a = "";
	private String attr_id_b = "";
	private String attr_value_a;
	private String attr_value_b;

	private String num;
	private String limit_num;// 购买限制数量

	private String score;
	private String price;
	private String total;

	// -------------add

	private String attr_title_a;
	private String attr_title_b;
	private boolean has_attr = false;
	private boolean has_cart = false;
	private boolean has_attr_1 = false;
	private boolean has_attr_2 = false;
	private int num_format_int = 0;
	private int limit_num_format_int = 0;
	private String price_format_string = null;
	private String total_format_string = null;

	private String attr_contents = "";

	@JSONField(serialize = false)
	public String getAttr_contents()
	{
		if (!TextUtils.isEmpty(attr_value_a))
		{
			attr_contents = attr_value_a;
			if (!TextUtils.isEmpty(attr_value_b))
			{
				attr_contents = attr_contents + "," + attr_value_b;
			}
		} else
		{
			if (!TextUtils.isEmpty(attr_value_b))
			{
				attr_contents = attr_value_b;
			}
		}
		// if (!TextUtils.isEmpty(attr_value_a))
		// {
		// attr_contents = "(" + attr_title_a + ":" + attr_value_a;
		// if (!TextUtils.isEmpty(attr_value_b))
		// {
		// attr_contents = attr_contents + ";" + attr_title_b + ":" +
		// attr_value_b;
		// }
		// attr_contents = attr_contents + ")";
		// } else
		// {
		// if (!TextUtils.isEmpty(attr_value_b))
		// {
		// attr_contents = "(" + attr_title_b + ":" + attr_value_b + ")";
		// }
		// }
		return attr_contents;
	}

	@JSONField(serialize = false)
	public String getPrice_format_string()
	{
		return price_format_string;
	}

	public void setPrice_format_string(String price_format_string)
	{
		this.price_format_string = price_format_string;
	}

	@JSONField(serialize = false)
	public String getTotal_format_string()
	{
		total_format_string = "￥" + getTotal();
		return total_format_string;
	}

	public void setTotal_format_string(String total_format_string)
	{
		this.total_format_string = total_format_string;
	}

	@JSONField(serialize = false)
	public boolean isHas_attr_1()
	{
		return has_attr_1;
	}

	public void setHas_attr_1(boolean has_attr_1)
	{
		this.has_attr_1 = has_attr_1;
	}

	@JSONField(serialize = false)
	public boolean isHas_attr_2()
	{
		return has_attr_2;
	}

	public void setHas_attr_2(boolean has_attr_2)
	{
		this.has_attr_2 = has_attr_2;
	}

	@JSONField(serialize = false)
	public boolean isHas_cart()
	{
		return has_cart;
	}

	public void setHas_cart(boolean has_cart)
	{
		this.has_cart = has_cart;
	}

	@JSONField(serialize = false)
	public boolean isHas_attr()
	{
		return has_attr;
	}

	public void setHas_attr(boolean has_attr)
	{
		this.has_attr = has_attr;
	}

	@JSONField(serialize = false)
	public int getLimit_num_format_int()
	{
		return limit_num_format_int;
	}

	public void setLimit_num_format_int(int limit_num_format_int)
	{
		this.limit_num_format_int = limit_num_format_int;
	}

	@JSONField(serialize = false)
	public String getAttr_title_a()
	{
		return attr_title_a;
	}

	public void setAttr_title_a(String attr_title_a)
	{
		this.attr_title_a = attr_title_a;
	}

	@JSONField(serialize = false)
	public String getAttr_title_b()
	{
		return attr_title_b;
	}

	public void setAttr_title_b(String attr_title_b)
	{
		this.attr_title_b = attr_title_b;
	}

	@JSONField(serialize = false)
	public int getNum_format_int()
	{
		return num_format_int;
	}

	public void setNum_format_int(int num_format_int)
	{
		this.num_format_int = num_format_int;
	}

	@JSONField(serialize = false)
	public GoodsdescActModel getGoods()
	{
		return goods;
	}

	public boolean setGoods(GoodsdescActModel goods, boolean validate)
	{
		this.goods = goods;
		if (goods == null)
		{
			return false;
		}
		this.setGoods_id(goods.getGoods_id());
		if ("1".equals(goods.getHas_cart())) // 可以加入购物车
		{
			this.setHas_cart(true);
		}

		if ("1".equals(goods.getHas_attr())) // 有属性
		{
			GoodsAttrsModel attrsModel = goods.getAttr();
			if (attrsModel != null)
			{
				int selectedAttr1Index = -1;
				int selectedAttr2Index = -1;
				this.setHas_attr(true); // 设置有属性
				if ("1".equals(attrsModel.getHas_attr_1())) // 有属性1
				{
					this.setHas_attr_1(true); // 设置有属性1
					this.setAttr_title_a(attrsModel.getAttr_title_1());// 设置属性1的类型名字
					this.setAttr_id_1(attrsModel.getAttr_id_1());// 设置属性1的类型id
					List<GoodsAttrsAttrModel> listAttr1 = attrsModel.getAttr_1();
					if (listAttr1 != null && listAttr1.size() > 0)
					{
						selectedAttr1Index = SDTypeParseUtil.getIntFromString(attrsModel.getSelected_attr_1(), -1);
						if (selectedAttr1Index >= 0)
						{
							if (selectedAttr1Index < listAttr1.size())
							{
								GoodsAttrsAttrModel attrModel = listAttr1.get(selectedAttr1Index);
								this.setAttr_id_a(attrModel.getAttr_id());// 设置属性1的id
								this.setAttr_value_a(attrModel.getAttr_name());// 设置属性1的名字
							}
						} else
						{
							if (validate)
							{
								SDToast.showToast("请选择" + getAttr_title_a());
								return false;
							}
						}
					}
				}
				if ("1".equals(attrsModel.getHas_attr_2())) // 有属性2
				{
					this.setHas_attr_2(true); // 设置有属性2
					this.setAttr_title_b(attrsModel.getAttr_title_2());// 设置属性2的类型名字
					this.setAttr_id_2(attrsModel.getAttr_id_2());// 设置属性2的类型id
					List<GoodsAttrsAttrModel> listAttr2 = attrsModel.getAttr_2();
					if (listAttr2 != null && listAttr2.size() > 0)
					{
						selectedAttr2Index = SDTypeParseUtil.getIntFromString(attrsModel.getSelected_attr_2(), -1);
						if (selectedAttr2Index >= 0)
						{
							if (selectedAttr2Index < listAttr2.size())
							{
								GoodsAttrsAttrModel attrModel = listAttr2.get(selectedAttr2Index);
								this.setAttr_id_b(attrModel.getAttr_id());
								this.setAttr_value_b(attrModel.getAttr_name());
							}
						} else
						{
							if (validate)
							{
								SDToast.showToast("请选择" + getAttr_title_b());
								return false;
							}
						}
					}
				}

				this.setPrice(attrsModel.getAttr_1_2Price(selectedAttr1Index, selectedAttr2Index));
				this.setLimit_num(attrsModel.getAttr_1_2Limit_num(selectedAttr1Index, selectedAttr2Index));
				this.setScore(attrsModel.getAttr_1_2Score(selectedAttr1Index, selectedAttr2Index));
			}
		} else
		{
			this.setHas_attr(false); // 设置无属性
			this.setPrice(goods.getCur_price() == null ? "0" : goods.getCur_price());
			this.setLimit_num(goods.getLimit_num() == null ? "0" : goods.getLimit_num());
		}
		this.setNum("1");
		return true;
	}

	public int addNumber()
	{
		int numInt = -1;
		if (!TextUtils.isEmpty(num))
		{
			numInt = SDTypeParseUtil.getIntFromString(num, -1);
			if (numInt > 0)
			{
				numInt++;
				if (!TextUtils.isEmpty(limit_num))
				{
					int limitNumInt = SDTypeParseUtil.getIntFromString(limit_num, 0);
					if (limitNumInt > 0 && limitNumInt >= numInt)
					{
						this.setNum(String.valueOf(numInt));
					}
				}
			}
		}
		return numInt;
	}

	public int minusNumber()
	{
		int numInt = -1;
		if (!TextUtils.isEmpty(num))
		{
			numInt = SDTypeParseUtil.getIntFromString(num, -1);
			if (numInt > 0)
			{
				numInt--;
				if (numInt <= 0)
				{
					numInt = 1;
				}
				this.setNum(String.valueOf(numInt));
			}
		}
		return numInt;
	}

	public int getCart_id()
	{
		return cart_id;
	}

	public void setCart_id(int cart_id)
	{
		this.cart_id = cart_id;
	}

	public String getGoods_id()
	{
		return goods_id;
	}

	public void setGoods_id(String goods_id)
	{
		this.goods_id = goods_id;
	}

	@JSONField(serialize = false)
	public String getAttr_id_1()
	{
		return attr_id_1;
	}

	public void setAttr_id_1(String attr_id_1)
	{
		this.attr_id_1 = attr_id_1;
	}

	@JSONField(serialize = false)
	public String getAttr_id_2()
	{
		return attr_id_2;
	}

	public void setAttr_id_2(String attr_id_2)
	{
		this.attr_id_2 = attr_id_2;
	}

	public String getAttr_id_a()
	{
		return attr_id_a;
	}

	public void setAttr_id_a(String attr_id_a)
	{
		if (attr_id_a == null)
		{
			attr_id_a = "";
		}
		this.attr_id_a = attr_id_a;
	}

	public String getAttr_id_b()
	{
		if (attr_id_b == null)
		{
			attr_id_b = "";
		}
		return attr_id_b;
	}

	public void setAttr_id_b(String attr_id_b)
	{
		this.attr_id_b = attr_id_b;
	}

	public String getAttr_value_a()
	{
		return attr_value_a;
	}

	public void setAttr_value_a(String attr_value_a)
	{
		this.attr_value_a = attr_value_a;
	}

	public String getAttr_value_b()
	{
		return attr_value_b;
	}

	public void setAttr_value_b(String attr_value_b)
	{
		this.attr_value_b = attr_value_b;
	}

	public String getNum()
	{
		return num;
	}

	public void setNum(String num)
	{
		this.num = num;
		this.num_format_int = SDTypeParseUtil.getIntFromString(num, 1);
	}

	public String getLimit_num()
	{
		return limit_num;
	}

	public void setLimit_num(String limit_num)
	{
		this.limit_num = limit_num;
		this.limit_num_format_int = SDTypeParseUtil.getIntFromString(limit_num, 0);
	}

	public String getScore()
	{
		return score;
	}

	public void setScore(String score)
	{
		this.score = score;
	}

	public String getPrice()
	{
		return price;
	}

	public void setPrice(String price)
	{
		this.price = price;
		if (!TextUtils.isEmpty(price))
		{
			this.price_format_string = "￥" + price;
		}
	}

	public String getTotal()
	{
		double num = SDTypeParseUtil.getDoubleFromString(this.num, 1);
		double price = SDTypeParseUtil.getDoubleFromString(this.price, 0);
		double total = num * price;
		setTotal(String.valueOf(SDFormatUtil.formatNumberDouble(total, 2)));
		return this.total;
	}

	public void setTotal(String total)
	{
		this.total = total;
	}

	@Override
	public boolean equals(Object o)
	{
		if (o != null && (o instanceof CartGoodsModel))
		{
			CartGoodsModel model = (CartGoodsModel) o;
			String goodsId = model.getGoods_id();
			String attrIdA = model.getAttr_id_a();
			String attrIdB = model.getAttr_id_b();
			if (!TextUtils.isEmpty(goodsId))
			{
				if (goodsId.equals(this.getGoods_id()) && attrIdA.equals(this.getAttr_id_a()) && attrIdB.equals(this.getAttr_id_b()))
				{
					return true;
				}
			}
		}
		return false;
	}

}

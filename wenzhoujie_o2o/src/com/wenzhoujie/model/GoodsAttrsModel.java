package com.wenzhoujie.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import android.text.TextUtils;

import com.wenzhoujie.utils.SDTypeParseUtil;

public class GoodsAttrsModel implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String has_attr_1 = null;
	private String has_attr_2 = null;
	private String attr_title_1 = null;
	private String selected_attr_1 = null;
	private List<GoodsAttrsAttrModel> attr_1 = null;
	private String attr_title_2 = null;
	private String selected_attr_2 = null;
	private List<GoodsAttrsAttrModel> attr_2 = null;
	private Map<String, String> attr_1_2 = null;

	// --------------
	private String attr_id_1 = null;
	private String attr_id_2 = null;

	private int has_attr_1_format_int = 0;
	private int has_attr_2_format_int = 0;

	public int getHas_attr_1_format_int()
	{
		return has_attr_1_format_int;
	}

	public void setHas_attr_1_format_int(int has_attr_1_format_int)
	{
		this.has_attr_1_format_int = has_attr_1_format_int;
	}

	public int getHas_attr_2_format_int()
	{
		return has_attr_2_format_int;
	}

	public void setHas_attr_2_format_int(int has_attr_2_format_int)
	{
		this.has_attr_2_format_int = has_attr_2_format_int;
	}

	public String getAttr_id_1()
	{
		return attr_id_1;
	}

	public void setAttr_id_1(String attr_id_1)
	{
		this.attr_id_1 = attr_id_1;
	}

	public String getAttr_id_2()
	{
		return attr_id_2;
	}

	public void setAttr_id_2(String attr_id_2)
	{
		this.attr_id_2 = attr_id_2;
	}

	public String getAttr_1_2Data(String preString, int attr1Index, int attr2Index)
	{
		String data = null;
		if (attr_1_2 != null)
		{
			String key = preString;
			if (attr_1 != null && attr_1.size() > 0 && attr1Index >= 0 && attr_1.size() > attr1Index)
			{
				GoodsAttrsAttrModel attr_1Model = attr_1.get(attr1Index);
				if (attr_1Model != null)
				{
					String attr1_id = attr_1Model.getAttr_id();
					if (attr1_id != null)
					{
						key = key + attr1_id;
						if (attr_1_2.containsKey(key))
						{
							data = attr_1_2.get(key);
							return data;
						}
					}
				}
			} else
			{
				key = key + "0";
				if (attr_1_2.containsKey(key))
				{
					data = attr_1_2.get(key);
					return data;
				}
			}

			if (attr_2 != null && attr_2.size() > 0 && attr2Index >= 0 && attr_2.size() > attr2Index)
			{
				GoodsAttrsAttrModel attr_2Model = attr_2.get(attr2Index);
				if (attr_2Model != null)
				{
					String attr2_id = attr_2Model.getAttr_id();
					if (attr2_id != null)
					{
						key = key + "_" + attr2_id;
						if (attr_1_2.containsKey(key))
						{
							data = attr_1_2.get(key);
							return data;
						}
					}
				}
			} else
			{
				key = key + "_0";
				if (attr_1_2.containsKey(key))
				{
					data = attr_1_2.get(key);
					return data;
				}
			}
		}
		return null;
	}

	public String getAttr_1_2Price(int attr1Index, int attr2Index)
	{
		String price = getAttr_1_2Data("attr_price_", attr1Index, attr2Index);
		return price;
	}

	public String getAttr_1_2Price_format(int attr1Index, int attr2Index)
	{
		String price = getAttr_1_2Data("attr_price_", attr1Index, attr2Index);
		if (TextUtils.isEmpty(price))
		{
			return null;
		} else
		{
			return "￥" + price;
		}
	}

	public String getAttr_1_2Score(int attr1Index, int attr2Index)
	{
		String price = getAttr_1_2Data("attr_score_", attr1Index, attr2Index);
		return price;
	}

	public String getAttr_1_2Limit_num(int attr1Index, int attr2Index)
	{
		String price = getAttr_1_2Data("attr_limit_num_", attr1Index, attr2Index);
		return price;
	}

	public String getHas_attr_1()
	{
		return has_attr_1;
	}

	public void setHas_attr_1(String has_attr_1)
	{
		this.has_attr_1 = has_attr_1;
		this.has_attr_1_format_int = SDTypeParseUtil.getIntFromString(has_attr_1, 0);
	}

	public String getHas_attr_2()
	{
		return has_attr_2;
	}

	public void setHas_attr_2(String has_attr_2)
	{
		this.has_attr_2 = has_attr_2;
		this.has_attr_2_format_int = SDTypeParseUtil.getIntFromString(has_attr_2, 0);
	}

	public String getAttr_title_1()
	{
		return attr_title_1;
	}

	public void setAttr_title_1(String attr_title_1)
	{
		this.attr_title_1 = attr_title_1;
	}

	public String getSelected_attr_1()
	{
		return selected_attr_1;
	}

	public void setSelected_attr_1(String selected_attr_1)
	{
		this.selected_attr_1 = selected_attr_1;
	}

	public List<GoodsAttrsAttrModel> getAttr_1()
	{
		return attr_1;
	}

	public void setAttr_1(List<GoodsAttrsAttrModel> attr_1)
	{
		this.attr_1 = attr_1;
	}

	public String getAttr_title_2()
	{
		return attr_title_2;
	}

	public void setAttr_title_2(String attr_title_2)
	{
		this.attr_title_2 = attr_title_2;
	}

	public String getSelected_attr_2()
	{
		return selected_attr_2;
	}

	public void setSelected_attr_2(String selected_attr_2)
	{
		this.selected_attr_2 = selected_attr_2;
	}

	public List<GoodsAttrsAttrModel> getAttr_2()
	{
		return attr_2;
	}

	public void setAttr_2(List<GoodsAttrsAttrModel> attr_2)
	{
		this.attr_2 = attr_2;
	}

	public Map<String, String> getAttr_1_2()
	{
		return attr_1_2;
	}

	public void setAttr_1_2(Map<String, String> attr_1_2)
	{
		this.attr_1_2 = attr_1_2;
	}

}
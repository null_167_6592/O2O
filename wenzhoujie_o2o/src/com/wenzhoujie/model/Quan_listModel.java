package com.wenzhoujie.model;

import java.util.List;

public class Quan_listModel
{
	// TODO id类型
	private String id = null;
	private String name = null;
	private int city_id;
	private String sort = null;
	private String pid = null;
	private List<Quan_listQuan_subModel> quan_sub = null;

	// /////////////////////////add
	private boolean isSelect = false;
	private boolean isHasChild = false;

	public boolean isHasChild()
	{
		return isHasChild;
	}

	public void setHasChild(boolean isHasChild)
	{
		this.isHasChild = isHasChild;
	}

	public boolean isSelect()
	{
		return isSelect;
	}

	public void setSelect(boolean isSelect)
	{
		this.isSelect = isSelect;
	}

	public int getCity_id()
	{
		return city_id;
	}

	public void setCity_id(int city_id)
	{
		this.city_id = city_id;
	}

	public String getSort()
	{
		return sort;
	}

	public void setSort(String sort)
	{
		this.sort = sort;
	}

	public String getPid()
	{
		return pid;
	}

	public void setPid(String pid)
	{
		this.pid = pid;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public List<Quan_listQuan_subModel> getQuan_sub()
	{
		return quan_sub;
	}

	public void setQuan_sub(List<Quan_listQuan_subModel> quan_sub)
	{
		this.quan_sub = quan_sub;
		if (quan_sub != null && quan_sub.size() > 1)
		{
			setHasChild(true);
		} else
		{
			setHasChild(false);
		}
	}

}
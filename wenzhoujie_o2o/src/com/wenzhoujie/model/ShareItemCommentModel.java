package com.wenzhoujie.model;

import java.util.List;

public class ShareItemCommentModel
{
	private List<ShareItemCommentListModel> list = null;
	private PageModel page = null;

	public List<ShareItemCommentListModel> getList()
	{
		return list;
	}

	public void setList(List<ShareItemCommentListModel> list)
	{
		this.list = list;
	}

	public PageModel getPage()
	{
		return page;
	}

	public void setPage(PageModel page)
	{
		this.page = page;
	}
}
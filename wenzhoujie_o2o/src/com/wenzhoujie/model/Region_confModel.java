package com.wenzhoujie.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.lidroid.xutils.db.annotation.Id;

@DatabaseTable(tableName = "region_conf")
public class Region_confModel
{

	//@DatabaseField(generatedId = true)
	//private int _id;
	@DatabaseField
	private String id;
	@DatabaseField
	private String pid;
	@DatabaseField
	private String name;
	@DatabaseField
	private String postcode;
	@DatabaseField
	private String py;

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getPid()
	{
		return pid;
	}

	public void setPid(String pid)
	{
		this.pid = pid;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getPostcode()
	{
		return postcode;
	}

	public void setPostcode(String postcode)
	{
		this.postcode = postcode;
	}

	public String getPy()
	{
		return py;
	}

	public void setPy(String py)
	{
		this.py = py;
	}

	@Override
	public String toString()
	{
		if (getName() != null)
		{
			return getName();
		} else
		{
			return "";
		}
	}
	
}

package com.wenzhoujie.model;

import com.wenzhoujie.utils.SDDistanceUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;

public class MerchantlistActItemModel
{
	private String id = null;
	private String name = null;
	private String avg_point = null;

	private String logo = null;
	private String xpoint = null;
	private String ypoint = null;
	private String api_address = null;
	private String address = null;
	private String dp_count = null;
	private String good_rate = null;
	private String deal_cate_id = null;
	private String tel = null;
	// private List<MerchantlistActItemGroup_pointModel> group_point = null;

	private String is_dy = null;
	private String city_name = null;
	private String comment_count = null;
	private String event_count = null;
	private String youhui_count = null;
	private String brand_id = null;
	private String distance = null;
	private String brief = null;
	private String l_area = null;
	private String l_cate_type = null;
	private String cate_name = null;
	private String width = null;

	// ------add----
	private float avg_point_fromat_float = 0;
	private String distance_format_string = null;
	private int distance_format_int = 0;
	private String mobile_brief = null;
	private String user_score = null;
	private int shop_type = 0;
	
	public int getShop_type()
	{
		return shop_type;
	}

	public void setShop_type(int shop_type)
	{
		this.shop_type = shop_type;
	}
	
	public int getDistance_format_int()
	{
		return distance_format_int;
	}

	public void setDistance_format_int(int distance_format_int)
	{
		this.distance_format_int = distance_format_int;
	}

	public String getDistance_format_string()
	{
		return distance_format_string;
	}

	public void setDistance_format_string(String distance_format_string)
	{
		this.distance_format_string = distance_format_string;
	}

	public float getAvg_point_fromat_float()
	{
		return avg_point_fromat_float;
	}

	public void setAvg_point_fromat_float(float avg_point_fromat_float)
	{
		this.avg_point_fromat_float = avg_point_fromat_float;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getAvg_point()
	{
		return avg_point;
	}

	public void setAvg_point(String avg_point)
	{
		this.avg_point = avg_point;
		this.avg_point_fromat_float = SDTypeParseUtil.getFloatFromString(avg_point, 0);
	}

	public String getLogo()
	{
		return logo;
	}

	public void setLogo(String logo)
	{
		this.logo = logo;
	}

	public String getXpoint()
	{
		return xpoint;
	}

	public void setXpoint(String xpoint)
	{
		this.xpoint = xpoint;
	}

	public String getYpoint()
	{
		return ypoint;
	}

	public void setYpoint(String ypoint)
	{
		this.ypoint = ypoint;
	}

	public String getApi_address()
	{
		return api_address;
	}

	public void setApi_address(String api_address)
	{
		this.api_address = api_address;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public String getDp_count()
	{
		return dp_count;
	}

	public void setDp_count(String dp_count)
	{
		this.dp_count = dp_count;
	}

	public String getGood_rate()
	{
		return good_rate;
	}

	public void setGood_rate(String good_rate)
	{
		this.good_rate = good_rate;
	}

	public String getDeal_cate_id()
	{
		return deal_cate_id;
	}

	public void setDeal_cate_id(String deal_cate_id)
	{
		this.deal_cate_id = deal_cate_id;
	}

	public String getTel()
	{
		return tel;
	}

	public void setTel(String tel)
	{
		this.tel = tel;
	}

	// public List<MerchantlistActItemGroup_pointModel> getGroup_point()
	// {
	// return group_point;
	// }
	// public void setGroup_point(List<MerchantlistActItemGroup_pointModel>
	// group_point)
	// {
	// this.group_point = group_point;
	// }
	public String getIs_dy()
	{
		return is_dy;
	}

	public void setIs_dy(String is_dy)
	{
		this.is_dy = is_dy;
	}

	public String getCity_name()
	{
		return city_name;
	}

	public void setCity_name(String city_name)
	{
		this.city_name = city_name;
	}

	public String getComment_count()
	{
		return comment_count;
	}

	public void setComment_count(String comment_count)
	{
		this.comment_count = comment_count + "评价";
	}

	public String getEvent_count()
	{
		return event_count;
	}

	public void setEvent_count(String event_count)
	{
		this.event_count = event_count;
	}

	public String getYouhui_count()
	{
		return youhui_count;
	}

	public void setYouhui_count(String youhui_count)
	{
		this.youhui_count = youhui_count;
	}

	public String getBrand_id()
	{
		return brand_id;
	}

	public void setBrand_id(String brand_id)
	{
		this.brand_id = brand_id;
	}

	public String getDistance()
	{
		return distance;
	}

	public void setDistance(String distance)
	{
		this.distance = distance;
		setDistance_format_string(SDDistanceUtil.getFormatDistance(SDTypeParseUtil.getDoubleFromString(distance, 0)));
		setDistance_format_int(SDTypeParseUtil.getIntFromString(distance, 0));
	}

	public String getBrief()
	{
		return brief;
	}

	public void setBrief(String brief)
	{
		this.brief = brief;
	}

	public String getL_area()
	{
		return l_area;
	}

	public void setL_area(String l_area)
	{
		this.l_area = l_area;
	}

	public String getL_cate_type()
	{
		return l_cate_type;
	}

	public void setL_cate_type(String l_cate_type)
	{
		this.l_cate_type = l_cate_type;
	}

	public String getCate_name()
	{
		return cate_name;
	}

	public void setCate_name(String cate_name)
	{
		this.cate_name = cate_name;
	}

	public String getWidth()
	{
		return width;
	}

	public void setWidth(String width)
	{
		this.width = width;
	}

	public String getMobile_brief()
	{
		return mobile_brief;
	}

	public void setMobile_brief(String mobile_brief)
	{
		this.mobile_brief = mobile_brief;
	}

	public String getUser_score()
	{
		return user_score;
	}

	public void setUser_score(String user_score)
	{
		this.user_score = user_score;
	}

}
package com.wenzhoujie.model;

import java.io.Serializable;

import com.wenzhoujie.baidumap.BaiduMapManager;
import com.wenzhoujie.utils.SDDistanceUtil;

public class IndexActYouhui_listModel implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String merchant_id;
	private String description;
	private String begin_time;
	private String youhui_type;
	private String total_num;
	private String end_time;
	private String title;
	private String content;
	private String merchant_logo;
	private String create_time;
	private String xpoint;
	private String ypoint;
	private String api_address;
	private String image_1;
	private String distance;

	// ================add
	private String distance_format;

	public String getDistance()
	{
		return distance;
	}

	public void setDistance(String distance)
	{
		this.distance = distance;
	}

	public void calculateDistance()
	{
		double calDistance = BaiduMapManager.getInstance().getDistanceFromMyLocation(ypoint, xpoint);
		setDistance_format(SDDistanceUtil.getFormatDistance(calDistance));
	}

	public String getDistance_format()
	{
		return distance_format;
	}

	public void setDistance_format(String distance_format)
	{
		this.distance_format = distance_format;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getMerchant_id()
	{
		return merchant_id;
	}

	public void setMerchant_id(String merchant_id)
	{
		this.merchant_id = merchant_id;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getBegin_time()
	{
		return begin_time;
	}

	public void setBegin_time(String begin_time)
	{
		this.begin_time = begin_time;
	}

	public String getYouhui_type()
	{
		return youhui_type;
	}

	public void setYouhui_type(String youhui_type)
	{
		this.youhui_type = youhui_type;
	}

	public String getTotal_num()
	{
		return total_num;
	}

	public void setTotal_num(String total_num)
	{
		this.total_num = total_num;
	}

	public String getEnd_time()
	{
		return end_time;
	}

	public void setEnd_time(String end_time)
	{
		this.end_time = end_time;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public String getMerchant_logo()
	{
		return merchant_logo;
	}

	public void setMerchant_logo(String merchant_logo)
	{
		this.merchant_logo = merchant_logo;
	}

	public String getCreate_time()
	{
		return create_time;
	}

	public void setCreate_time(String create_time)
	{
		this.create_time = create_time;
	}

	public String getXpoint()
	{
		return xpoint;
	}

	public void setXpoint(String xpoint)
	{
		this.xpoint = xpoint;
		calculateDistance();
	}

	public String getYpoint()
	{
		return ypoint;
	}

	public void setYpoint(String ypoint)
	{
		this.ypoint = ypoint;
		calculateDistance();
	}

	public String getApi_address()
	{
		return api_address;
	}

	public void setApi_address(String api_address)
	{
		this.api_address = api_address;
	}

	public String getImage_1()
	{
		return image_1;
	}

	public void setImage_1(String image_1)
	{
		this.image_1 = image_1;
	}

}

package com.wenzhoujie.model;

import java.util.List;

public class ShareItemModel
{
	private String share_id = null;
	private String uid = null;
	private String user_name = null;
	private String nick_name = null;
	private String content = null;
	private String share_content = null;
	private String relay_count = null;
	private String click_count = null;
	private String title = null;
	private String type = null;
	private String share_data = null;
	private String source = null;
	private String time = null;
	private List<ShareItemExpressModel> parse_expres = null;
	private String parse_user = null;
	private String user_avatar = null;
	private List<ShareItemImageModel> imgs = null;
	private ShareItemUserModel user = null;
	private ShareItemCommentModel comments = null;
	private List<ShareItemCollectModel> collects = null;

	private Integer is_follow_user;
	private Integer is_collect_share;
	private Integer comment_count;
	private Integer collect_count;

	public Integer getIs_follow_user()
	{
		return is_follow_user;
	}

	public void setIs_follow_user(Integer is_follow_user)
	{
		this.is_follow_user = is_follow_user;
	}

	public Integer getIs_collect_share()
	{
		return is_collect_share;
	}

	public void setIs_collect_share(Integer is_collect_share)
	{
		this.is_collect_share = is_collect_share;
	}

	public Integer getComment_count()
	{
		return comment_count;
	}

	public void setComment_count(Integer comment_count)
	{
		this.comment_count = comment_count;
	}

	public Integer getCollect_count()
	{
		return collect_count;
	}

	public void setCollect_count(Integer collect_count)
	{
		this.collect_count = collect_count;
	}

	public String getShare_id()
	{
		return share_id;
	}

	public void setShare_id(String share_id)
	{
		this.share_id = share_id;
	}

	public String getUid()
	{
		return uid;
	}

	public void setUid(String uid)
	{
		this.uid = uid;
	}

	public String getUser_name()
	{
		return user_name;
	}

	public void setUser_name(String user_name)
	{
		this.user_name = user_name;
	}
	
	public String getNick_name()
	{
		return nick_name;
	}

	public void setNick_name(String nick_name)
	{
		this.nick_name = nick_name;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public String getShare_content()
	{
		return share_content;
	}

	public void setShare_content(String share_content)
	{
		this.share_content = share_content;
	}

	public String getRelay_count()
	{
		return relay_count;
	}

	public void setRelay_count(String relay_count)
	{
		this.relay_count = relay_count;
	}

	public String getClick_count()
	{
		return click_count;
	}

	public void setClick_count(String click_count)
	{
		this.click_count = click_count;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public String getShare_data()
	{
		return share_data;
	}

	public void setShare_data(String share_data)
	{
		this.share_data = share_data;
	}

	public String getSource()
	{
		return source;
	}

	public void setSource(String source)
	{
		this.source = source;
	}

	public String getTime()
	{
		return time;
	}

	public void setTime(String time)
	{
		this.time = time;
	}

	public List<ShareItemExpressModel> getParse_expres()
	{
		return parse_expres;
	}

	public void setParse_expres(List<ShareItemExpressModel> parse_expres)
	{
		this.parse_expres = parse_expres;
	}

	public String getParse_user()
	{
		return parse_user;
	}

	public void setParse_user(String parse_user)
	{
		this.parse_user = parse_user;
	}

	public String getUser_avatar()
	{
		return user_avatar;
	}

	public void setUser_avatar(String user_avatar)
	{
		this.user_avatar = user_avatar;
	}

	public List<ShareItemImageModel> getImgs()
	{
		return imgs;
	}

	public void setImgs(List<ShareItemImageModel> imgs)
	{
		this.imgs = imgs;
	}

	public ShareItemUserModel getUser()
	{
		return user;
	}

	public void setUser(ShareItemUserModel user)
	{
		this.user = user;
	}

	public ShareItemCommentModel getComments()
	{
		return comments;
	}

	public void setComments(ShareItemCommentModel comments)
	{
		this.comments = comments;
	}

	public List<ShareItemCollectModel> getCollects()
	{
		return collects;
	}

	public void setCollects(List<ShareItemCollectModel> collects)
	{
		this.collects = collects;
	}
}
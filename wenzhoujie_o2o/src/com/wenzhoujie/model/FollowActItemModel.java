package com.wenzhoujie.model;

import java.util.List;

import com.wenzhoujie.utils.SDTypeParseUtil;

public class FollowActItemModel
{
	private String share_id = null;
	private String uid = null;
	private String user_name = null;
	private String content = null;
	private String share_content = null;
	private String collect_count = null;
	private String comment_count = null;
	private String relay_count = null;
	private String click_count = null;
	private String title = null;
	private String type = null;
	private String share_data = null;
	private String source = null;
	private String time = null;
	private List<UItemExpressModel> parse_expres = null;
	private String parse_user = null;
	private String user_avatar = null;
	private List<UItemImageModel> imgs = null;
	private String is_relay = null;
	private FollowActItemUserModel user = null;
	private FollowActItemRelay_shareModel relay_share = null;

	// ====================add

	private boolean is_relay_format_boolean = false;

	public boolean isIs_relay_format_boolean()
	{
		return is_relay_format_boolean;
	}

	public void setIs_relay_format_boolean(boolean is_relay_format_boolean)
	{
		this.is_relay_format_boolean = is_relay_format_boolean;
	}

	public String getShare_id()
	{
		return share_id;
	}

	public void setShare_id(String share_id)
	{
		this.share_id = share_id;
	}

	public String getUid()
	{
		return uid;
	}

	public void setUid(String uid)
	{
		this.uid = uid;
	}

	public String getUser_name()
	{
		return user_name;
	}

	public void setUser_name(String user_name)
	{
		this.user_name = user_name;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public String getShare_content()
	{
		return share_content;
	}

	public void setShare_content(String share_content)
	{
		this.share_content = share_content;
	}

	public String getCollect_count()
	{
		return collect_count;
	}

	public void setCollect_count(String collect_count)
	{
		this.collect_count = "喜欢(" + collect_count + ")";
	}

	public String getComment_count()
	{
		return comment_count;
	}

	public void setComment_count(String comment_count)
	{
		this.comment_count = "评论(" + comment_count + ")";
	}

	public String getRelay_count()
	{
		return relay_count;
	}

	public void setRelay_count(String relay_count)
	{
		this.relay_count = "转发(" + relay_count + ")";
	}

	public String getClick_count()
	{
		return click_count;
	}

	public void setClick_count(String click_count)
	{
		this.click_count = click_count;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public String getShare_data()
	{
		return share_data;
	}

	public void setShare_data(String share_data)
	{
		this.share_data = share_data;
	}

	public String getSource()
	{
		return source;
	}

	public void setSource(String source)
	{
		this.source = source;
	}

	public String getTime()
	{
		return time;
	}

	public void setTime(String time)
	{
		this.time = time;
	}

	public List<UItemExpressModel> getParse_expres()
	{
		return parse_expres;
	}

	public void setParse_expres(List<UItemExpressModel> parse_expres)
	{
		this.parse_expres = parse_expres;
	}

	public String getParse_user()
	{
		return parse_user;
	}

	public void setParse_user(String parse_user)
	{
		this.parse_user = parse_user;
	}

	public String getUser_avatar()
	{
		return user_avatar;
	}

	public void setUser_avatar(String user_avatar)
	{
		this.user_avatar = user_avatar;
	}

	public List<UItemImageModel> getImgs()
	{
		return imgs;
	}

	public void setImgs(List<UItemImageModel> imgs)
	{
		this.imgs = imgs;
	}

	public String getIs_relay()
	{
		return is_relay;
	}

	public void setIs_relay(String is_relay)
	{
		this.is_relay = is_relay;
		int state = SDTypeParseUtil.getIntFromString(is_relay, 0);
		if (state == 0)
		{
			this.is_relay_format_boolean = false;
		} else if (state == 1)
		{
			this.is_relay_format_boolean = true;
		}
	}

	public FollowActItemUserModel getUser()
	{
		return user;
	}

	public void setUser(FollowActItemUserModel user)
	{
		this.user = user;
	}

	public FollowActItemRelay_shareModel getRelay_share()
	{
		return relay_share;
	}

	public void setRelay_share(FollowActItemRelay_shareModel relay_share)
	{
		this.relay_share = relay_share;
	}
}
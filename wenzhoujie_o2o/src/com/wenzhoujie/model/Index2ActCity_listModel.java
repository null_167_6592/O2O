package com.wenzhoujie.model;

public class Index2ActCity_listModel
{
	private String id = null;
	private String name = null;
	private String uname = null;
	private String is_effect = null;
	private String is_delete = null;
	private String pid = null;
	private String is_open = null;
	private String is_default = null;
	private String description = null;
	private String notice = null;
	private String seo_title = null;
	private String seo_keyword = null;
	private String seo_description = null;
	private String sort = null;

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getUname()
	{
		return uname;
	}

	public void setUname(String uname)
	{
		this.uname = uname;
	}

	public String getIs_effect()
	{
		return is_effect;
	}

	public void setIs_effect(String is_effect)
	{
		this.is_effect = is_effect;
	}

	public String getIs_delete()
	{
		return is_delete;
	}

	public void setIs_delete(String is_delete)
	{
		this.is_delete = is_delete;
	}

	public String getPid()
	{
		return pid;
	}

	public void setPid(String pid)
	{
		this.pid = pid;
	}

	public String getIs_open()
	{
		return is_open;
	}

	public void setIs_open(String is_open)
	{
		this.is_open = is_open;
	}

	public String getIs_default()
	{
		return is_default;
	}

	public void setIs_default(String is_default)
	{
		this.is_default = is_default;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getNotice()
	{
		return notice;
	}

	public void setNotice(String notice)
	{
		this.notice = notice;
	}

	public String getSeo_title()
	{
		return seo_title;
	}

	public void setSeo_title(String seo_title)
	{
		this.seo_title = seo_title;
	}

	public String getSeo_keyword()
	{
		return seo_keyword;
	}

	public void setSeo_keyword(String seo_keyword)
	{
		this.seo_keyword = seo_keyword;
	}

	public String getSeo_description()
	{
		return seo_description;
	}

	public void setSeo_description(String seo_description)
	{
		this.seo_description = seo_description;
	}

	public String getSort()
	{
		return sort;
	}

	public void setSort(String sort)
	{
		this.sort = sort;
	}

}
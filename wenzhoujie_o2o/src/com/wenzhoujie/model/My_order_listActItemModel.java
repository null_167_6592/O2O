package com.wenzhoujie.model;

import java.util.List;

import com.wenzhoujie.utils.SDCollectionUtil;

public class My_order_listActItemModel {
	private String id = null;
	private String order_sn = null;
	private String create_time = null;
	private String create_time_format = null;
	private String total_price = null;
	private String money = null;
	private String total_money_format = null;
	private String money_format = null;
	private String status = null;
	private String c = null;
	private List<DealOrderItemModel> deal_order_item = null;
	private int delivery_status;
	private int pay_status;
	private int order_status;
	private int refund_status;

	// ===================add
	private String status_format = null;

	public String getStatus_format() {
		status_format = "";
		// 付款状态
		switch (pay_status) {
		case 0:
			status_format += "未支付";
			break;
		case 1:
			status_format += "部分付款";
			break;
		case 2:
			status_format += "全部付款";
			break;

		default:
			break;
		}
		// 发货状态
		switch (delivery_status) {
		case 0:
			status_format += "、未发货";
			break;
		case 1:
			status_format += "、部分发货";
			break;
		case 2:
			status_format += "、全部发货";
			break;
		case 3:
			status_format += "、无需发货的订单";
			break;
		default:
			break;
		}
		// 订单状态
		switch (order_status) {
		/*
		 * case 0: status_format+="、未结单"; break; case 1: status_format+="、已结单";
		 * break; default: break;
		 */
		}
		// 退款状态
		switch (refund_status) {
		case 1:
			status_format += "、退款申请中";
			break;
		case 2:
			status_format += "、退款已处理";
			break;
		default:
			break;
		}
		return status_format;
	}

	public boolean hasCancelButton()
	{
		boolean has = false;
		if (order_status == 1)
		{
			has = true;
		} else
		{
			if (pay_status == 0)
			{
				has = true;
			}
		}
		return has;
	}

	public String getCancelButtonText()
	{
		String text = null;
		if (order_status == 1)
		{
			text = "删除订单";
		} else
		{
			if (pay_status == 0)
			{
				text = "取消订单";
			}
		}
		return text;
	}
	
	public void setStatus_format(String status_format) {
		this.status_format = status_format;
	}

	public int getDelivery_status() {
		return delivery_status;
	}

	public void setDelivery_status(int delivery_status) {
		this.delivery_status = delivery_status;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCreate_time() {
		return create_time;
	}

	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}

	public String getCreate_time_format() {
		return create_time_format;
	}

	public void setCreate_time_format(String create_time_format) {
		this.create_time_format = create_time_format;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getTotal_money_format() {
		return total_money_format;
	}

	public void setTotal_money_format(String total_money_format) {
		this.total_money_format = total_money_format;
	}

	public String getMoney_format() {
		return money_format;
	}

	public void setMoney_format(String money_format) {
		this.money_format = money_format;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getOrder_sn() {
		return order_sn;
	}

	public void setOrder_sn(String order_sn) {
		this.order_sn = order_sn;
	}

	public String getTotal_price() {
		return total_price;
	}

	public void setTotal_price(String total_price) {
		this.total_price = total_price;
	}

	public String getC() {
		return c;
	}

	public void setC(String c) {
		this.c = c;
	}

	public int getPay_status() {
		return pay_status;
	}

	public void setPay_status(int pay_status) {
		this.pay_status = pay_status;
	}

	public int getOrder_status() {
		return order_status;
	}

	public void setOrder_status(int order_status) {
		this.order_status = order_status;
	}

	public int getRefund_status() {
		return refund_status;
	}

	public void setRefund_status(int refund_status) {
		this.refund_status = refund_status;
	}

	public List<DealOrderItemModel> getDeal_order_item() {
		return deal_order_item;
	}

	public void setDeal_order_item(List<DealOrderItemModel> deal_order_item) {
		this.deal_order_item = deal_order_item;
		updateGoodsStatus();
	}

	private void updateGoodsStatus() {
		if (!SDCollectionUtil.isEmpty(deal_order_item)) {
			for (DealOrderItemModel model : deal_order_item) {
				model.setOrder_status(order_status);
				model.setPay_status(pay_status);
			}
		}
	}

}
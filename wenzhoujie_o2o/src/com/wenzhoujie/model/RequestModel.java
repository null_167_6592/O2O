package com.wenzhoujie.model;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.wenzhoujie.app.App;
import com.wenzhoujie.baidumap.BaiduMapManager;

public class RequestModel
{
	public static final class RequestDataType
	{
		public static final int BASE64 = 0;
		public static final int REQUEST = 1;
	}

	public static final class ResponseDataType
	{
		public static final int BASE64 = 0;
		public static final int JSON = 1;
		public static final int ARRAY = 2;
	}

	// -----------------------------------------
	private int mRequestDataType = RequestDataType.BASE64;
	private int mResponseDataType = ResponseDataType.JSON;

	private Map<String, Object> mData = new HashMap<String, Object>();
	private Map<String, File> mDataFile = new HashMap<String, File>();

	private boolean mIsNeedCache = true;
	private boolean mIsNeedCheckLoginState0 = true;

	private Class<?> mActClass;

	public Class<?> getmActClass()
	{
		return mActClass;
	}

	public void setmActClass(Class<?> mActClass)
	{
		this.mActClass = mActClass;
	}

	public RequestModel(Map<String, Object> mData)
	{
		super();
		this.mData = mData;
	}

	public RequestModel()
	{
		super();
	}

	public RequestModel(boolean isNeedCheckLoginState0)
	{
		super();
		this.mIsNeedCheckLoginState0 = isNeedCheckLoginState0;
	}

	public boolean ismIsNeedCheckLoginState0()
	{
		return mIsNeedCheckLoginState0;
	}

	public void setmIsNeedCheckLoginState0(boolean mIsNeedCheckLoginState0)
	{
		this.mIsNeedCheckLoginState0 = mIsNeedCheckLoginState0;
	}

	public boolean ismIsNeedCache()
	{
		return mIsNeedCache;
	}

	public void setmIsNeedCache(boolean mIsNeedCache)
	{
		this.mIsNeedCache = mIsNeedCache;
	}

	public Map<String, File> getmDataFile()
	{
		return mDataFile;
	}

	public void setmDataFile(Map<String, File> mDataFile)
	{
		this.mDataFile = mDataFile;
	}

	public Map<String, Object> getmData()
	{
		return mData;
	}

	public void setmData(Map<String, Object> mData)
	{
		this.mData = mData;
	}

	public int getmRequestDataType()
	{
		return mRequestDataType;
	}

	public void setmRequestDataType(int mRequestDataType)
	{
		this.mRequestDataType = mRequestDataType;
	}

	public int getmResponseDataType()
	{
		return mResponseDataType;
	}

	public void setmResponseDataType(int mResponseDataType)
	{
		this.mResponseDataType = mResponseDataType;
	}

	public void put(String key, Object value)
	{
		mData.put(key, value);
	}

	public void putFile(String key, File file)
	{
		mDataFile.put(key, file);
	}

	public void putUser()
	{
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (user != null)
		{
			put("email", user.getUser_name());
			put("pwd", user.getUser_pwd());
		}
	}

	public void putLocation()
	{
		put("m_latitude", BaiduMapManager.getInstance().getLatitude());
		put("m_longitude", BaiduMapManager.getInstance().getLongitude());
	}

	public void putPage(int page)
	{
		put("page", page);
	}

	public void putAct(String act)
	{
		put("act", act);
	}

}

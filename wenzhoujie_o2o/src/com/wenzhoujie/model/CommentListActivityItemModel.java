package com.wenzhoujie.model;

public class CommentListActivityItemModel
{
	private String id = null;
	private String merchant_id = null;
	private String yh_id = null;
	private String content = null;
	private String user_name = null;
	private String create_time = null;
	private String create_time_format = null;

	public String getYh_id()
	{
		return yh_id;
	}

	public void setYh_id(String yh_id)
	{
		this.yh_id = yh_id;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getMerchant_id()
	{
		return merchant_id;
	}

	public void setMerchant_id(String merchant_id)
	{
		this.merchant_id = merchant_id;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public String getUser_name()
	{
		return user_name;
	}

	public void setUser_name(String user_name)
	{
		this.user_name = user_name;
	}

	public String getCreate_time()
	{
		return create_time;
	}

	public void setCreate_time(String create_time)
	{
		this.create_time = create_time;
	}

	public String getCreate_time_format()
	{
		return create_time_format;
	}

	public void setCreate_time_format(String create_time_format)
	{
		this.create_time_format = create_time_format;
	}
}
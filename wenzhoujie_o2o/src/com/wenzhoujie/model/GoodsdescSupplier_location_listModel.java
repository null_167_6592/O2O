package com.wenzhoujie.model;

import java.io.Serializable;

import com.wenzhoujie.utils.SDDistanceUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;

public class GoodsdescSupplier_location_listModel implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String name;
	private String address;
	private String tel;
	private String xpoint;
	private String ypoint;
	private String supplier_id;
	private String distance;
	private int shop_type;
	public int getShop_type()
{
return shop_type;
}

public void setShop_type(int shop_type)
{
this.shop_type = shop_type;
}

	// ===================add
	private String distance_format;

	public String getDistance_format()
	{
		return distance_format;
	}

	public void setDistance_format(String distance_format)
	{
		this.distance_format = distance_format;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public String getTel()
	{
		return tel;
	}

	public void setTel(String tel)
	{
		this.tel = tel;
	}

	public String getXpoint()
	{
		return xpoint;
	}

	public void setXpoint(String xpoint)
	{
		this.xpoint = xpoint;
	}

	public String getYpoint()
	{
		return ypoint;
	}

	public void setYpoint(String ypoint)
	{
		this.ypoint = ypoint;
	}

	public String getSupplier_id()
	{
		return supplier_id;
	}

	public void setSupplier_id(String supplier_id)
	{
		this.supplier_id = supplier_id;
	}

	public String getDistance()
	{
		return distance;
	}

	public void setDistance(String distance)
	{
		this.distance = distance;
		setDistance_format(SDDistanceUtil.getFormatDistance(SDTypeParseUtil.getDoubleFromString(distance, 0)));
	}

}

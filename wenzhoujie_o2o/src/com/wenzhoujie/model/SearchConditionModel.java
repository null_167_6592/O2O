package com.wenzhoujie.model;

import com.wenzhoujie.app.App;
import com.wenzhoujie.baidumap.BaiduMapManager;
import com.wenzhoujie.work.AppRuntimeWorker;

public class SearchConditionModel
{

	public static final class Order_type
	{
		public static final String DEFAULT = "default";
		public static final String AVG_POINT = "avg_point";
		public static final String NEARBY = "nearby";
		public static final String NEWEST = "newest";
		public static final String BUY_COUNT = "buy_count";
		public static final String PRICE_ASC = "price_asc";
		public static final String PRICE_DESC = "price_desc";
	}

	private String email = null;
	private String pwd = null;
	private String uid = null;
	private String keyword = null;
	private double m_latitude;
	private double m_longitude;
	private String order_type = Order_type.DEFAULT;
	private String cate_id = "0";
	private String cata_type_id = "0";
	private String brand_id = "0";
	private int city_id;
	private String quan_id = "0";
	private String merchant_id = "0";
	private String catalog_id = "0";
	private int cate_type = 0;
	private int type = 1;
	private String content = null;

	public String getUid()
	{
		refreshLoginState();
		return uid;
	}

	public void setUid(String uid)
	{
		this.uid = uid;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public String getCatalog_id()
	{
		return catalog_id;
	}

	public void setCatalog_id(String catalog_id)
	{
		this.catalog_id = catalog_id;
	}

	public int getCate_type()
	{
		return cate_type;
	}

	public void setCate_type(int cate_type)
	{
		this.cate_type = cate_type;
	}

	public int getType()
	{
		return type;
	}

	public void setType(int type)
	{
		this.type = type;
	}

	private PageModel pageModel = null;

	public SearchConditionModel()
	{
		super();
		this.pageModel = new PageModel();
	}

	private void refreshLocation()
	{
		this.m_latitude = BaiduMapManager.getInstance().getLatitude();
		this.m_longitude = BaiduMapManager.getInstance().getLongitude();
	}

	private boolean refreshLoginState()
	{
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (user != null)
		{
			this.email = user.getUser_name();
			this.pwd = user.getUser_pwd();
			this.uid = user.getUser_id();
			return true;
		} else
		{
			return false;
		}
	}

	private void refreshCityId()
	{
		this.city_id = AppRuntimeWorker.getCity_id();
	}

	public String getMerchant_id()
	{
		return merchant_id;
	}

	public void setMerchant_id(String merchant_id)
	{
		this.merchant_id = merchant_id;
	}

	public String getEmail()
	{
		refreshLoginState();
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getPwd()
	{
		refreshLoginState();
		return pwd;
	}

	public void setPwd(String pwd)
	{
		this.pwd = pwd;
	}

	public String getKeyword()
	{
		return keyword;
	}

	public void setKeyword(String keyword)
	{
		this.keyword = keyword;
	}

	public double getM_latitude()
	{
		refreshLocation();
		return m_latitude;
	}

	public void setM_latitude(double m_latitude)
	{
		this.m_latitude = m_latitude;
	}

	public double getM_longitude()
	{
		refreshLocation();
		return m_longitude;
	}

	public void setM_longitude(double m_longitude)
	{
		this.m_longitude = m_longitude;
	}

	public String getOrder_type()
	{
		return order_type;
	}

	public void setOrder_type(String order_type)
	{
		this.order_type = order_type;
	}

	public String getCate_id()
	{
		return cate_id;
	}

	public void setCate_id(String cate_id)
	{
		this.cate_id = cate_id;
	}

	public String getCata_type_id()
	{
		return cata_type_id;
	}

	public void setCata_type_id(String cata_type_id)
	{
		this.cata_type_id = cata_type_id;
	}

	public String getBrand_id()
	{
		return brand_id;
	}

	public void setBrand_id(String brand_id)
	{
		this.brand_id = brand_id;
	}

	public int getCity_id()
	{
		refreshCityId();
		return city_id;
	}

	public void setCity_id(int city_id)
	{
		this.city_id = city_id;
	}

	public String getQuan_id()
	{
		return quan_id;
	}

	public void setQuan_id(String quan_id)
	{
		this.quan_id = quan_id;
	}

	public PageModel getPageModel()
	{
		return pageModel;
	}

	public void setPageModel(PageModel pageModel)
	{
		this.pageModel = pageModel;
	}

}

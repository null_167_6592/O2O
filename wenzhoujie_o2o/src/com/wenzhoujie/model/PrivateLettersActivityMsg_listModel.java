package com.wenzhoujie.model;

import java.util.List;

public class PrivateLettersActivityMsg_listModel
{
	private String content = null;
	private String uid = null;
	private String user_name = null;
	private String user_avatar = null;
	private String tuid = null;
	private String tuser_name = null;
	private String tuser_avatar = null;
	private String time = null;
	private String msg_count = null;
	private String mlid = null;
	private List<UItemExpressModel> parse_expres = null;

	public List<UItemExpressModel> getParse_expres()
	{
		return parse_expres;
	}

	public void setParse_expres(List<UItemExpressModel> parse_expres)
	{
		this.parse_expres = parse_expres;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public String getUid()
	{
		return uid;
	}

	public void setUid(String uid)
	{
		this.uid = uid;
	}

	public String getUser_name()
	{
		return user_name;
	}

	public void setUser_name(String user_name)
	{
		this.user_name = user_name;
	}

	public String getUser_avatar()
	{
		return user_avatar;
	}

	public void setUser_avatar(String user_avatar)
	{
		this.user_avatar = user_avatar;
	}

	public String getTuid()
	{
		return tuid;
	}

	public void setTuid(String tuid)
	{
		this.tuid = tuid;
	}

	public String getTuser_name()
	{
		return tuser_name;
	}

	public void setTuser_name(String tuser_name)
	{
		this.tuser_name = tuser_name;
	}

	public String getTuser_avatar()
	{
		return tuser_avatar;
	}

	public void setTuser_avatar(String tuser_avatar)
	{
		this.tuser_avatar = tuser_avatar;
	}

	public String getTime()
	{
		return time;
	}

	public void setTime(String time)
	{
		this.time = time;
	}

	public String getMsg_count()
	{
		return msg_count;
	}

	public void setMsg_count(String msg_count)
	{
		this.msg_count = "共" + msg_count + "封";
	}

	public String getMlid()
	{
		return mlid;
	}

	public void setMlid(String mlid)
	{
		this.mlid = mlid;
	}
}
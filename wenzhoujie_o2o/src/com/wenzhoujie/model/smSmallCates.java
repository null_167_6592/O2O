package com.wenzhoujie.model;

import java.util.List;

public class smSmallCates
{
	
		private String parent_id = null;
		private String id = null;
		private String name = null;

		// ////////////////////////add
		private boolean isSelect = false;

		public boolean isSelect()
		{
			return isSelect;
		}

		public void setSelect(boolean isSelect)
		{
			this.isSelect = isSelect;
		}

		public String getId()
		{
			return id;
		}

		public void setId(String id)
		{
			this.id = id;
		}

		public String getParent_id()
		{
			return parent_id;
		}

		public void setParent_id(String parent_id)
		{
			this.parent_id = parent_id;
		}

		public String getName()
		{
			return name;
		}

		public void setName(String name)
		{
			this.name = name;
		}

	}
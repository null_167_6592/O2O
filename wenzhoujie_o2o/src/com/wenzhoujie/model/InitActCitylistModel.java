package com.wenzhoujie.model;

import java.io.Serializable;

import android.text.TextUtils;

import com.wenzhoujie.utils.CharacterParser;

/**
 * init citylist
 * 
 * @author yhz
 * @create time 2014-7-16
 */
public class InitActCitylistModel implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;

	private String name = null;

	private String pid = null;

	private String image = null;

	private String has_child = null;

	// ===========add
	private String sortLetters = null;

	public String getSortLetters()
	{
		return sortLetters;
	}

	public void setSortLetters(String sortLetters)
	{
		this.sortLetters = sortLetters;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
		if (!TextUtils.isEmpty(name))
		{
			String pinyin = CharacterParser.getInstance().getSelling(name);
			String sortString = pinyin.substring(0, 1).toUpperCase();

			// 正则表达式，判断首字母是否是英文字母
			if (sortString.matches("[A-Z]"))
			{
				sortLetters = sortString.toUpperCase();
			} else
			{
				sortLetters = "#";
			}
		} else
		{
			sortLetters = "#";
		}
	}

	public String getPid()
	{
		return pid;
	}

	public void setPid(String pid)
	{
		this.pid = pid;
	}

	public String getImage()
	{
		return image;
	}

	public void setImage(String image)
	{
		this.image = image;
	}

	public String getHas_child()
	{
		return has_child;
	}

	public void setHas_child(String has_child)
	{
		this.has_child = has_child;
	}

}

package com.wenzhoujie.model;

import java.io.Serializable;

import android.text.TextUtils;

/**
 * 配送地址实体
 * 
 * @author js02
 * 
 */
public class DeliveryModel implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id = null;
	private String consignee = null;
	private String delivery = null;
	private String region_lv1 = null;
	private String region_lv2 = null;
	private String region_lv3 = null;
	private String region_lv4 = null;
	private String delivery_detail = null;
	private String phone = null;
	private String postcode = null;

	// ==============add
	private String delivery_long = null;
	private boolean isSelect = false;

	public String getDelivery_long()
	{
		if (!TextUtils.isEmpty(delivery))
		{
			delivery_long = delivery;
			if (!TextUtils.isEmpty(delivery_detail))
			{
				delivery_long = delivery_long + " " + delivery_detail;
			}
		} else
		{
			if (!TextUtils.isEmpty(delivery_detail))
			{
				delivery_long = delivery_detail;
			}
		}

		return delivery_long;
	}

	public void setDelivery_long(String delivery_long)
	{
		this.delivery_long = delivery_long;
	}

	public boolean isSelect()
	{
		return isSelect;
	}

	public void setSelect(boolean isSelect)
	{
		this.isSelect = isSelect;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getConsignee()
	{
		return consignee;
	}

	public void setConsignee(String consignee)
	{
		this.consignee = consignee;
	}

	public String getDelivery()
	{
		return delivery;
	}

	public void setDelivery(String delivery)
	{
		this.delivery = delivery;
	}

	public String getRegion_lv1()
	{
		return region_lv1;
	}

	public void setRegion_lv1(String region_lv1)
	{
		this.region_lv1 = region_lv1;
	}

	public String getRegion_lv2()
	{
		return region_lv2;
	}

	public void setRegion_lv2(String region_lv2)
	{
		this.region_lv2 = region_lv2;
	}

	public String getRegion_lv3()
	{
		return region_lv3;
	}

	public void setRegion_lv3(String region_lv3)
	{
		this.region_lv3 = region_lv3;
	}

	public String getRegion_lv4()
	{
		return region_lv4;
	}

	public void setRegion_lv4(String region_lv4)
	{
		this.region_lv4 = region_lv4;
	}

	public String getDelivery_detail()
	{
		return delivery_detail;
	}

	public void setDelivery_detail(String delivery_detail)
	{
		this.delivery_detail = delivery_detail;
	}

	public String getPhone()
	{
		return phone;
	}

	public void setPhone(String phone)
	{
		this.phone = phone;
	}

	public String getPostcode()
	{
		return postcode;
	}

	public void setPostcode(String postcode)
	{
		this.postcode = postcode;
	}

}

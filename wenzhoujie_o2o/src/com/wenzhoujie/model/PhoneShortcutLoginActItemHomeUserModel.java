package com.wenzhoujie.model;

public class PhoneShortcutLoginActItemHomeUserModel
{
	private String follows = null;
	private String goods = null;
	private String photos = null;
	private String user_avatar = null;
	private String fans = null;
	private String favs = null;

	public String getFollows()
	{
		return follows;
	}

	public void setFollows(String follows)
	{
		this.follows = follows;
	}

	public String getGoods()
	{
		return goods;
	}

	public void setGoods(String goods)
	{
		this.goods = goods;
	}

	public String getPhotos()
	{
		return photos;
	}

	public void setPhotos(String photos)
	{
		this.photos = photos;
	}

	public String getUser_avatar()
	{
		return user_avatar;
	}

	public void setUser_avatar(String user_avatar)
	{
		this.user_avatar = user_avatar;
	}

	public String getFans()
	{
		return fans;
	}

	public void setFans(String fans)
	{
		this.fans = fans;
	}

	public String getFavs()
	{
		return favs;
	}

	public void setFavs(String favs)
	{
		this.favs = favs;
	}

}

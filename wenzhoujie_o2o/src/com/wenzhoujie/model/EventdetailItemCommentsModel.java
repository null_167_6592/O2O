package com.wenzhoujie.model;

import java.util.List;

public class EventdetailItemCommentsModel
{
	private String content = null;
	private String create_time = null;
	private String user_name = null;
	private String user_avatar = null;
	private String time = null;
	private List<EventdetailItemCommentsExpressModel> parse_expres = null;
	private String parse_user = null;
	private String user_id = null;

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public String getCreate_time()
	{
		return create_time;
	}

	public void setCreate_time(String create_time)
	{
		this.create_time = create_time;
	}

	public String getUser_name()
	{
		return user_name;
	}

	public void setUser_name(String user_name)
	{
		this.user_name = user_name;
	}

	public String getUser_avatar()
	{
		return user_avatar;
	}

	public void setUser_avatar(String user_avatar)
	{
		this.user_avatar = user_avatar;
	}

	public String getTime()
	{
		return time;
	}

	public void setTime(String time)
	{
		this.time = time;
	}

	public List<EventdetailItemCommentsExpressModel> getParse_expres()
	{
		return parse_expres;
	}

	public void setParse_expres(List<EventdetailItemCommentsExpressModel> parse_expres)
	{
		this.parse_expres = parse_expres;
	}

	public String getParse_user()
	{
		return parse_user;
	}

	public void setParse_user(String parse_user)
	{
		this.parse_user = parse_user;
	}

	public String getUser_id()
	{
		return user_id;
	}

	public void setUser_id(String user_id)
	{
		this.user_id = user_id;
	}
}
package com.wenzhoujie.model;

/**
 * 支付方式实体
 * 
 * @author js02
 * 
 */
public class Payment_listModel
{

	private String id = null;
	private String code = null;
	private String name = null;
	private String has_calc = null;

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(String code)
	{
		this.code = code;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getHas_calc()
	{
		return has_calc;
	}

	public void setHas_calc(String has_calc)
	{
		this.has_calc = has_calc;
	}

}

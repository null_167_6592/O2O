package com.wenzhoujie.model;

public class MyFollowsActivityItemModel
{
	private String uid = null;
	private String user_name = null;
	private String fans = null;
	private String user_avatar = null;

	// ------add------
	private int is_follow;

	public String getUid()
	{
		return uid;
	}

	public void setUid(String uid)
	{
		this.uid = uid;
	}

	public String getUser_name()
	{
		return user_name;
	}

	public void setUser_name(String user_name)
	{
		this.user_name = user_name;
	}

	public String getFans()
	{
		return fans;
	}

	public void setFans(String fans)
	{
		this.fans = "粉丝数  " + fans + "人";
	}

	public String getUser_avatar()
	{
		return user_avatar;
	}

	public void setUser_avatar(String user_avatar)
	{
		this.user_avatar = user_avatar;
	}

	public int getIs_follow()
	{
		return is_follow;
	}

	public void setIs_follow(int is_follow)
	{
		this.is_follow = is_follow;
	}
}
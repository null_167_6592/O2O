package com.wenzhoujie.model;

public class Pay_orderActCouponlistModel
{
	private String couponSn;
	private String couponPw;
	private String qrcode;

	public String getCouponSn()
	{
		return couponSn;
	}

	public void setCouponSn(String couponSn)
	{
		this.couponSn = couponSn;
	}

	public String getCouponPw()
	{
		return couponPw;
	}

	public void setCouponPw(String couponPw)
	{
		this.couponPw = couponPw;
	}

	public String getQrcode()
	{
		return qrcode;
	}

	public void setQrcode(String qrcode)
	{
		this.qrcode = qrcode;
	}

}

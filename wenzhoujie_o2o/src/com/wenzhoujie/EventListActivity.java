package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.wenzhoujie.library.title.SDTitleTwoRightButton.SDTitleTwoRightButtonListener;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.adapter.EventsAdapter;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.baidumap.BaiduMapManager;
import com.wenzhoujie.constant.Constant.SearchTypeMap;
import com.wenzhoujie.constant.Constant.SearchTypeNormal;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.EventsListActItemModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.EventsListActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;
import com.wenzhoujie.work.AppRuntimeWorker;

/**
 * 活动列表
 * 
 * @author js02
 * 
 */
public class EventListActivity extends BaseActivity
{
	public static String EXTRA_KEY_WORD = "extra_key_word";
	public static String EXTRA_CATE_ID = "extra_cate_id";

	@ViewInject(id = R.id.act_event_list_ptrlv_content)
	private PullToRefreshListView mPtrlvContent = null;

	@ViewInject(id = R.id.act_event_list_ll_empty)
	private LinearLayout mLlEmpty = null;

	@ViewInject(id = R.id.act_event_list_ll_current_search)
	private LinearLayout mLlCurrentSearch = null;

	@ViewInject(id = R.id.act_event_list_tv_current_keyword)
	private TextView mTvCurrentKeyword = null;

	private List<EventsListActItemModel> mListModel = new ArrayList<EventsListActItemModel>();
	private EventsAdapter mAdapter = null;

	private int pageTotal;
	// =================提交服务器参数
	private int page;
	private String keyword;
	private String cate_id;
	private String city_id;
	private String city_name;
	private String latitude_top;
	private String latitude_bottom;
	private String longitude_left;
	private String longitude_right;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_TWO_RIGHT_BUTTON);
		setContentView(R.layout.act_event_list);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		getIntentData();
		bindDefaultData();
		initPullListView();
	}

	private void bindDefaultData()
	{
		mAdapter = new EventsAdapter(mListModel, this);
		mPtrlvContent.setAdapter(mAdapter);
	}

	private void getIntentData()
	{
		Intent intent = getIntent();
		keyword = intent.getStringExtra(EXTRA_KEY_WORD);
		cate_id = intent.getStringExtra(EXTRA_CATE_ID);

		if (AppHelper.isEmptyString(keyword))
		{
			mLlCurrentSearch.setVisibility(View.GONE);
		} else
		{
			mLlCurrentSearch.setVisibility(View.VISIBLE);
			mTvCurrentKeyword.setText(keyword);
		}

	}

	protected void requestNearByVip(final boolean isLoadMore)
	{

		RequestModel model = new RequestModel();
		model.put("act", "eventlist");
		model.put("city_id", AppRuntimeWorker.getCity_id());
		model.put("keyword", keyword);
		model.put("cate_id", cate_id);
		model.put("m_latitude", BaiduMapManager.getInstance().getLatitude());
		model.put("m_longitude", BaiduMapManager.getInstance().getLongitude());
		model.put("page", page);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{

			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				EventsListActModel model = JsonUtil.json2Object(responseInfo.result, EventsListActModel.class);
				if (!SDInterfaceUtil.isActModelNull(model))
				{
					switch (model.getResponse_code())
					{
					case 0:

						break;
					case 1:
						if (model.getPage() != null)
						{
							page = model.getPage().getPage();
							pageTotal = model.getPage().getPage_total();
						}
						SDViewUtil.updateAdapterByList(mListModel, model.getItem(), mAdapter, isLoadMore);
						break;
					default:
						break;
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				AppHelper.hideLoadingDialog();
				mPtrlvContent.onRefreshComplete();
				SDViewUtil.toggleEmptyMsgByList(mListModel, mLlEmpty);
			}
		};

		InterfaceServer.getInstance().requestInterface(model, handler);

	}

	private void initPullListView()
	{
		mPtrlvContent.setMode(Mode.BOTH);
		mPtrlvContent.setOnRefreshListener(new OnRefreshListener2<ListView>()
		{

			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				page = 1;
				requestNearByVip(false);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				if (++page > pageTotal && pageTotal > 0)
				{
					SDToast.showToast("没有更多数据了");
					mPtrlvContent.onRefreshComplete();
				} else
				{
					requestNearByVip(true);
				}
			}
		});
		mPtrlvContent.setRefreshing();
	}

	private void initTitle()
	{

		mTitleTwoRightBtns.setmListener(new SDTitleTwoRightButtonListener()
		{

			@Override
			public void onRightButton2CLick_SDTitleTwoRightButton(View v)
			{
				Intent intent = new Intent(EventListActivity.this, HomeSearchActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra(HomeSearchActivity.EXTRA_SEARCH_TYPE, SearchTypeNormal.EVENT);
				startActivity(intent);
			}

			@Override
			public void onRightButton1CLick_SDTitleTwoRightButton(View v)
			{
				Intent intent = new Intent(EventListActivity.this, MapSearchActivity.class);
				intent.putExtra(MapSearchActivity.EXTRA_SEARCH_TYPE, SearchTypeMap.EVENT);
				startActivity(intent);
			}

			@Override
			public void onLeftButtonCLick_SDTitleTwoRightButton(View v)
			{
				finish();
			}
		});

		mTitleTwoRightBtns.setTitleTop("活动列表");
		mTitleTwoRightBtns.setRightImage1(R.drawable.ic_location_home_top);
		mTitleTwoRightBtns.setRightImage2(R.drawable.ic_search_home_top);
	}

}
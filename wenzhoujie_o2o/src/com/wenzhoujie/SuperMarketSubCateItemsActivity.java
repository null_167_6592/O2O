package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.sunday.eventbus.SDBaseEvent;
import com.wenzhoujie.adapter.SuperMarketCateGoodOneAdapter;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.event.EventTagJpush;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.wenzhoujie.model.MerchantShopItem;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.SmMoreActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 
 * 
 *
 * 
 */
public class SuperMarketSubCateItemsActivity extends BaseActivity
{

	/** 商家id */
	public static final String EXTRA_MERCHANT_ID = "extra_sm_id";
	public static final String EXTRA_MERCHANT_TITLE = "extra_sm_title";
	//public static final String EXTRA_MERCHANT_SUPPLYER_ID = "extra_merchant_sid";
	public static final String EXTRA_CATE_ID = "extra_sm_cate";
	public static final String EXTRA_SUBCATE_ID = "extra_sm_subcate";
	public static final String EXTRA_SUBCATE_NAME = "extra_sm_subcatename";

	//private PopupWindow pop;
	private String id = null;
	private String cateid = null;
	private String subcateid = null;
	private String subcatename = null;
	//从item中获取吧
	//private String supplyer_id = null;
	private String title = "超市";
	
	@ViewInject(id = R.id.frag_mssi_cate_name)
	private TextView mssiTitle = null;
	
	//@ViewInject(id = R.id.frag_mssi_list_ptrlv_content)
	//private PullToRefreshListView mPtrlvContent = null;
	
	private SuperMarketCateGoodOneAdapter mGoodsAdapter = null;
	private List<MerchantShopItem> mListGoodsModel = null;
	@ViewInject(id = R.id.frag_mssi_gv)
	private com.wenzhoujie.customview.SDGridViewInScroll mGvTodayGoods = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.frag_merchant_supermarket_subcate_items);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init() {
		initIntentData(getIntent());
		initTitle();
		bindDefaultGoods();
		requestData();
		//initPullRefreshLv();
		//
	}

	private void initTitle() {
		
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
				
					//mFragWebview.startLoadData();
				gotocart();
		
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
					finish();
			}
		});
		//mTitleSimple.setRightText("刷新");
		//mTitleSimple.setRightText("");
		mTitleSimple.setRightImage(R.drawable.ic_shopcart_new);

		if (title != null)
		{
			mTitleSimple.setTitleTop(title);
		}
		
	}
	
	private void gotocart()
	{
		startActivity(new Intent(this, ShopCartActivity.class));
	}
	
	public String getSmName()
	{ //返回超市名稱
		return title;
	}
	public String getSmId()
	{ //返回超市id
		return id;
	}
	
	
	private void initIntentData(Intent intent) {
		id = intent.getStringExtra(EXTRA_MERCHANT_ID);
		cateid = intent.getStringExtra(EXTRA_CATE_ID);
		subcateid = intent.getStringExtra(EXTRA_SUBCATE_ID);
		subcatename= intent.getStringExtra(EXTRA_SUBCATE_NAME);
		if (intent.getStringExtra(EXTRA_MERCHANT_TITLE) != null)
			title = intent.getStringExtra(EXTRA_MERCHANT_TITLE);
		//supplyer_id = intent.getStringExtra(EXTRA_MERCHANT_SUPPLYER_ID);
		mssiTitle.setText(subcatename);
	}

	private void bindDefaultGoods(){
		this.mListGoodsModel = new ArrayList<MerchantShopItem>();
		this.mGoodsAdapter = new SuperMarketCateGoodOneAdapter(this.mListGoodsModel, this);
		this.mGvTodayGoods.setAdapter(mGoodsAdapter);
	}
	
	private void requestData()
	{
		RequestModel model = new RequestModel();
		model.put("act", "get_cate_item");
		model.put("id", id);
		model.put("cate_id", subcateid);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{
				AppHelper.showLoadingDialog("请稍候...");
			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				SmMoreActModel actModel = JsonUtil.json2Object(responseInfo.result, SmMoreActModel.class);
				if (!SDInterfaceUtil.isActModelNull(actModel))
				{
					if (actModel.getResponse_code() == 1)
					{
						SDViewUtil.updateAdapterByList(mListGoodsModel, actModel.getItems(), mGoodsAdapter, false);
						
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				dealFinishRequest();
			}
		};
		InterfaceServer.getInstance().requestInterface(model, handler);
	}

	protected void dealFinishRequest()
	{
		AppHelper.hideLoadingDialog();
		//mPtrlvContent.onRefreshComplete();
	}
	
	
	
	@Override
	protected void onNewIntent(Intent intent) {
		setIntent(intent);
		init();
		super.onNewIntent(intent);
	}
	
	@Override
	public void onEventMainThread(SDBaseEvent event) {
		super.onEventMainThread(event);
		if (EventTagJpush.EVENT_MERCHANTDETAIL_ID.equals(event
				.getEventTagString())) {
			finish();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (App.getApplication().mRuntimeConfig.isNeedRefreshMerchantDetail()) {
			init();
			App.getApplication().mRuntimeConfig
					.setNeedRefreshMerchantDetail(false);
		}
		
	}
	
	@Override
	protected void onPause(){
		
		super.onPause();
	}

}
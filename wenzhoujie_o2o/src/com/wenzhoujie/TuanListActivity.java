package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.lingou.www.R.color;
import com.sunday.eventbus.SDBaseEvent;
import com.wenzhoujie.adapter.CategoryCateLeftAdapter;
import com.wenzhoujie.adapter.CategoryCateRightAdapter;
import com.wenzhoujie.adapter.CategoryOrderAdapter;
import com.wenzhoujie.adapter.CategoryQuanLeftAdapterNew;
import com.wenzhoujie.adapter.CategoryQuanRightAdapterNew;
import com.wenzhoujie.adapter.TuanListAdapter;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.baidumap.BaiduMapManager;
import com.wenzhoujie.constant.Constant;
import com.wenzhoujie.constant.Constant.CategoryOrderTypeName;
import com.wenzhoujie.constant.Constant.CategoryOrderTypeValue;
import com.wenzhoujie.constant.Constant.SearchTypeMap;
import com.wenzhoujie.constant.Constant.SearchTypeNormal;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.customview.SD2LvCategoryView;
import com.wenzhoujie.customview.SDBottomNavigatorBaseItem;
import com.wenzhoujie.customview.SDLvCategoryView;
import com.wenzhoujie.customview.SDLvCategoryView.SDLvCategoryViewListener;
import com.wenzhoujie.event.EnumEventTag;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.library.title.SDTitleTwoRightButton.SDTitleTwoRightButtonListener;
import com.wenzhoujie.library.utils.SDResourcesUtil;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.wenzhoujie.model.Bcate_listBcate_typeModel;
import com.wenzhoujie.model.Bcate_listModel;
import com.wenzhoujie.model.CategoryOrderModel;
import com.wenzhoujie.model.Quan_listModel;
import com.wenzhoujie.model.Quan_listQuan_subModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.TuanGoodsModel;
import com.wenzhoujie.model.act.TuanlistActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.SDViewNavigatorManager;
import com.wenzhoujie.utils.ViewInject;
import com.wenzhoujie.work.AppRuntimeWorker;

/**
 * 团购界面
 * 
 * @author js02
 * 
 */
public class TuanListActivity extends BaseActivity implements OnClickListener
{
	// 标题
	public static final String EXTRA_TITLE_TXT = "extra_title_txt";
	/** 关键字 */
	public static final String EXTRA_KEY_WORD = "extra_key_word";

	/** 大分类id */
	public static final String EXTRA_CATE_ID = "extra_cate_id";
	public static final String EXTRA_CATE_NAME = "extra_cate_name";

	/** 小分类id */
	public static final String EXTRA_CATE_TYPE_ID = "extra_cate_type_id";

	@ViewInject(id = R.id.act_tuan_list_cv_left)
	private SD2LvCategoryView mCvLeft = null;

	@ViewInject(id = R.id.act_tuan_list_cv_middle)
	private SD2LvCategoryView mCvMiddle = null;

	@ViewInject(id = R.id.act_tuan_list_cv_right)
	private SDLvCategoryView mCvRight = null;

	@ViewInject(id = R.id.act_tuan_list_ll_empty)
	private LinearLayout mLlEmpty = null;

	@ViewInject(id = R.id.act_tuan_list_ll_current_location)
	private LinearLayout mLlCurrentLocation = null;

	@ViewInject(id = R.id.act_tuan_list_tv_current_address)
	private TextView mTvAddress = null;

	@ViewInject(id = R.id.act_tuan_list_tv_location)
	private ImageView mIvLocation = null;

	@ViewInject(id = R.id.act_tuan_list_ll_current_search)
	private LinearLayout mLlCurrentSearch = null;

	@ViewInject(id = R.id.act_tuan_list_tv_current_keyword)
	private TextView mTvCurrentKeyword = null;

	@ViewInject(id = R.id.act_tuan_list_ptrlv_content)
	private PullToRefreshListView mPtrlvContent = null;

	private TuanListAdapter mAdapter = null;
	private List<TuanGoodsModel> mListModel = new ArrayList<TuanGoodsModel>();
	private SDViewNavigatorManager mViewManager = new SDViewNavigatorManager();

	private boolean isFirstBindCategoryViewData = true;

	private int pageTotal;
	private String title_txt;
	private String cata_type_name;
	// ====================提交服务端参数
	private String catalog_id;
	private String cata_type_id;
	private int city_id;
	private int page;
	private String keyword;
	private String quan_id;
	private String order_type;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_TWO_RIGHT_BUTTON);
		setContentView(R.layout.act_tuan_list);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		getIntentData();
		initTitle();
		bindDefaultLvData();
		initCategoryView();
		initCategoryViewNavigatorManager();
		registeClick();
		initPullRefreshLv();

	}

	private void initTitle()
	{
		mTitleTwoRightBtns.setmListener(new SDTitleTwoRightButtonListener()
		{

			@Override
			public void onRightButton2CLick_SDTitleTwoRightButton(View v)
			{
				Intent intent = new Intent(TuanListActivity.this, HomeSearchActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra(HomeSearchActivity.EXTRA_SEARCH_TYPE, SearchTypeNormal.TUAN);
				startActivity(intent);
			}

			@Override
			public void onRightButton1CLick_SDTitleTwoRightButton(View v)
			{
				Intent intent = new Intent(TuanListActivity.this, MapSearchActivity.class);
				intent.putExtra(MapSearchActivity.EXTRA_SEARCH_TYPE, SearchTypeMap.TUAN);
				startActivity(intent);
			}

			@Override
			public void onLeftButtonCLick_SDTitleTwoRightButton(View v)
			{
				finish();
			}
		});

		String cityName = AppRuntimeWorker.getCity_name();
		if (title_txt != null)
		{
			mTitleTwoRightBtns.setTitleTop(title_txt + " - " + cityName);
		}
		else
		{
			if (!AppHelper.isEmptyString(cityName))
			{
				mTitleTwoRightBtns.setTitleTop(SDResourcesUtil.getString(R.string.tuan_gou) + " - " + cityName);
			} else
			{
				mTitleTwoRightBtns.setTitleTop(SDResourcesUtil.getString(R.string.tuan_gou));
			}
		}
		mTitleTwoRightBtns.setRightImage1(R.drawable.ic_location_home_top);
		mTitleTwoRightBtns.setRightImage2(R.drawable.ic_search_home_top);
	}

	/**
	 * 从intent获取数据
	 */
	private void getIntentData()
	{
		Intent intent = getIntent();
		catalog_id = intent.getStringExtra(EXTRA_CATE_ID);
		cata_type_id = intent.getStringExtra(EXTRA_CATE_TYPE_ID);
		keyword = intent.getStringExtra(EXTRA_KEY_WORD);
		title_txt = intent.getStringExtra(EXTRA_TITLE_TXT);
		cata_type_name = intent.getStringExtra(EXTRA_CATE_NAME);

		if (TextUtils.isEmpty(keyword))
		{
			mLlCurrentLocation.setVisibility(View.VISIBLE);
			mLlCurrentSearch.setVisibility(View.GONE);
			if (BaiduMapManager.getInstance().getCurAddress() != null)
			{
				mTvAddress.setText(BaiduMapManager.getInstance().getCurAddressShort());
			}
		} else
		{
			mLlCurrentLocation.setVisibility(View.GONE);
			mLlCurrentSearch.setVisibility(View.VISIBLE);
			mTvCurrentKeyword.setText(keyword);
		}

	}

	/**
	 * 初始化下拉刷新控件
	 */
	private void initPullRefreshLv()
	{
		mPtrlvContent.setMode(Mode.BOTH);
		mPtrlvContent.setOnRefreshListener(new OnRefreshListener2<ListView>()
		{

			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				page = 1;
				requestData(false);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				page++;
				if (page > pageTotal && pageTotal > 0)
				{
					SDToast.showToast("没有更多数据了");
					mPtrlvContent.onRefreshComplete();
				} else
				{
					requestData(true);
				}
			}
		});

		mPtrlvContent.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				TuanGoodsModel goodsModel = mAdapter.getItem((int) id);
				if (goodsModel != null && goodsModel.getId() != null)
				{
					Intent intent = new Intent(getApplicationContext(), TuanDetailActivity.class);
					intent.putExtra(TuanDetailActivity.EXTRA_GOODS_ID, goodsModel.getId());
					intent.putExtra(TuanDetailActivity.EXTRA_GOODS_TYPE, 1);
					startActivity(intent);
				}

			}
		});

		mPtrlvContent.setRefreshing();
	}

	private void bindDefaultLvData()
	{
		mAdapter = new TuanListAdapter(mListModel, TuanListActivity.this);
		mPtrlvContent.setAdapter(mAdapter);
	}

	private void initCategoryViewNavigatorManager()
	{
		SDBottomNavigatorBaseItem[] items = new SDBottomNavigatorBaseItem[] { mCvLeft, mCvMiddle, mCvRight };
		mViewManager.setItems(items);
		mViewManager.setmMode(SDViewNavigatorManager.Mode.CAN_NONE_SELECT);
	}

	private void initCategoryView()
	{

		mCvLeft.setmBackgroundNormal(R.drawable.bg_choosebar_press_down);
		mCvLeft.setmBackgroundSelect(R.drawable.bg_choosebar_press_up);
		mCvLeft.setmTextColorNormal(getResources().getColor(color.text_home_item_content));
		mCvLeft.setmTextColorSelect(getResources().getColor(color.main_color));
		mCvLeft.setmListener(new SD2LvCategoryView.SD2LvCategoryViewListener()
		{

			@Override
			public void onRightItemSelect(int leftIndex, int rightIndex, Object leftModel, Object rightModel)
			{
				Bcate_listModel left = (Bcate_listModel) leftModel;
				Bcate_listBcate_typeModel right = (Bcate_listBcate_typeModel) rightModel;
				catalog_id = left.getId();
				cata_type_id = right.getId();
				mPtrlvContent.setRefreshing();
				cata_type_name = null;
			}

			@Override
			public void onLeftItemSelect(int leftIndex, Object leftModel, boolean isNotifyDirect)
			{
				if (isNotifyDirect)
				{
					Bcate_listModel left = (Bcate_listModel) leftModel;
					Bcate_listBcate_typeModel right = left.getBcate_type().get(0);
					catalog_id = left.getId();
					cata_type_id = right.getId();
					mPtrlvContent.setRefreshing();
					cata_type_name = null;
				}
			}
		});

		mCvMiddle.setmBackgroundNormal(R.drawable.bg_choosebar_press_down_2);
		mCvMiddle.setmBackgroundSelect(R.drawable.bg_choosebar_press_up_2);
		mCvMiddle.setmTextColorNormal(getResources().getColor(color.text_home_item_content));
		mCvMiddle.setmTextColorSelect(getResources().getColor(color.main_color));
		mCvMiddle.setmListener(new SD2LvCategoryView.SD2LvCategoryViewListener()
		{

			@Override
			public void onRightItemSelect(int leftIndex, int rightIndex, Object leftModel, Object rightModel)
			{
				Quan_listQuan_subModel right = (Quan_listQuan_subModel) rightModel;
				quan_id = right.getId();
				mPtrlvContent.setRefreshing();
			}

			@Override
			public void onLeftItemSelect(int leftIndex, Object leftModel, boolean isNotifyDirect)
			{
				if (isNotifyDirect)
				{
					Quan_listModel left = (Quan_listModel) leftModel;
					Quan_listQuan_subModel right = left.getQuan_sub().get(0);
					quan_id = right.getId();
					mPtrlvContent.setRefreshing();
				}
			}
		});

		mCvRight.setmBackgroundNormal(R.drawable.bg_choosebar_press_down_3);
		mCvRight.setmBackgroundSelect(R.drawable.bg_choosebar_press_up_3);
		mCvRight.setmTextColorNormal(getResources().getColor(color.text_home_item_content));
		mCvRight.setmTextColorSelect(getResources().getColor(color.main_color));
		mCvRight.setmListener(new SDLvCategoryViewListener()
		{
			@Override
			public void onItemSelect(int index, Object model)
			{
				if (model instanceof CategoryOrderModel)
				{
					CategoryOrderModel orderModel = (CategoryOrderModel) model;
					order_type = orderModel.getValue();
					mPtrlvContent.setRefreshing();
				}
			}
		});

		List<CategoryOrderModel> listOrderModel = new ArrayList<CategoryOrderModel>();
		CategoryOrderModel orderModelDefault = new CategoryOrderModel();
		orderModelDefault.setName(CategoryOrderTypeName.DEFAULT);
		orderModelDefault.setValue(CategoryOrderTypeValue.DEFAULT);
		listOrderModel.add(orderModelDefault);

		CategoryOrderModel orderModelNearBy = new CategoryOrderModel();
		orderModelNearBy.setName(CategoryOrderTypeName.NEARBY);
		orderModelNearBy.setValue(CategoryOrderTypeValue.NEARBY);
		listOrderModel.add(orderModelNearBy);

		CategoryOrderModel orderModelPoint = new CategoryOrderModel();
		orderModelPoint.setName(CategoryOrderTypeName.AVG_POINT);
		orderModelPoint.setValue(CategoryOrderTypeValue.AVG_POINT);
		listOrderModel.add(orderModelPoint);

		CategoryOrderModel orderModelNewest = new CategoryOrderModel();
		orderModelNewest.setName(CategoryOrderTypeName.NEWEST);
		orderModelNewest.setValue(CategoryOrderTypeValue.NEWEST);
		listOrderModel.add(orderModelNewest);

		CategoryOrderModel orderModelBuyCount = new CategoryOrderModel();
		orderModelBuyCount.setName(CategoryOrderTypeName.BUY_COUNT);
		orderModelBuyCount.setValue(CategoryOrderTypeValue.BUY_COUNT);
		listOrderModel.add(orderModelBuyCount);

		CategoryOrderModel orderModelPriceAsc = new CategoryOrderModel();
		orderModelPriceAsc.setName(CategoryOrderTypeName.PRICE_ASC);
		orderModelPriceAsc.setValue(CategoryOrderTypeValue.PRICE_ASC);
		listOrderModel.add(orderModelPriceAsc);

		CategoryOrderModel orderModelPriceDesc = new CategoryOrderModel();
		orderModelPriceDesc.setName(CategoryOrderTypeName.PRICE_DESC);
		orderModelPriceDesc.setValue(CategoryOrderTypeValue.PRICE_DESC);
		listOrderModel.add(orderModelPriceDesc);

		CategoryOrderAdapter adapter = new CategoryOrderAdapter(listOrderModel, this);
		mCvRight.setAdapter(adapter);
	}

	private void requestData(final boolean isLoadMore)
	{
		RequestModel model = new RequestModel();
		model.put("act", "tuanlist");
		model.put("order_type", order_type); // 排序类型
		model.put("cata_type_id", cata_type_id);// 小分类ID
		model.put("catalog_id", catalog_id);// 大分类ID
		model.put("quan_id", quan_id); // 商圈
		model.put("city_id", AppRuntimeWorker.getCity_id());
		model.put("page", page);
		model.put("keyword", keyword);
		model.put("m_latitude", BaiduMapManager.getInstance().getLatitude());
		model.put("m_longitude", BaiduMapManager.getInstance().getLongitude());
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{
				AppHelper.showLoadingDialog("请稍候...");
			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				TuanlistActModel actModel = JsonUtil.json2Object(responseInfo.result, TuanlistActModel.class);
				if (!SDInterfaceUtil.isActModelNull(actModel))
				{
					if (isFirstBindCategoryViewData)
					{
						bindLeftCategoryViewData(actModel.getBcate_list());
						bindMiddleCategoryViewData(actModel.getQuan_list());
						isFirstBindCategoryViewData = false;
					}

					if (actModel.getPage() != null)
					{
						page = actModel.getPage().getPage();
						pageTotal = actModel.getPage().getPage_total();
					}
					SDViewUtil.updateAdapterByList(mListModel, actModel.getItem(), mAdapter, isLoadMore);
				}
				if (cata_type_name != null)
				{
					mCvLeft.setTitle(cata_type_name);
				}

			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				dealFinishRequest();
			}
		};
		InterfaceServer.getInstance().requestInterface(model, handler);

	}

	protected void dealFinishRequest()
	{
		AppHelper.hideLoadingDialog();
		mPtrlvContent.onRefreshComplete();
		SDViewUtil.toggleEmptyMsgByList(mListModel, mLlEmpty);
	}

	private void bindLeftCategoryViewData(List<Bcate_listModel> listModel)
	{
		if (listModel != null && listModel.size() > 0)
		{
			Bcate_listModel leftModel = listModel.get(0);
			if (leftModel != null)
			{
				List<Bcate_listBcate_typeModel> listRight = leftModel.getBcate_type();

				CategoryCateLeftAdapter adapterLeft = new CategoryCateLeftAdapter(listModel, TuanListActivity.this);
				adapterLeft.setDefaultSelectId(catalog_id);

				CategoryCateRightAdapter adapterRight = new CategoryCateRightAdapter(listRight, TuanListActivity.this);

				mCvLeft.setLeftAdapter(adapterLeft);
				mCvLeft.setRightAdapter(adapterRight);
				mCvLeft.setAdapterFinish();
			}
		}
	}

	private void bindMiddleCategoryViewData(List<Quan_listModel> listModel)
	{
		if (listModel != null && listModel.size() > 0)
		{
			Quan_listModel leftModel = listModel.get(0);
			if (leftModel != null)
			{
				List<Quan_listQuan_subModel> listRight = leftModel.getQuan_sub();

				CategoryQuanLeftAdapterNew adapterLeft = new CategoryQuanLeftAdapterNew(listModel,
						TuanListActivity.this);
				CategoryQuanRightAdapterNew adapterRight = new CategoryQuanRightAdapterNew(listRight,
						TuanListActivity.this);
				mCvMiddle.setLeftAdapter(adapterLeft);
				mCvMiddle.setRightAdapter(adapterRight);
				mCvMiddle.setAdapterFinish();
			}
		}
	}

	private void registeClick()
	{
		mIvLocation.setOnClickListener(this);

	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{

		case R.id.act_tuan_list_tv_location:
			clickTv_locaiton();
			break;

		default:
			break;
		}
	}

	private void clickTv_locaiton()
	{
		locationAddress();
	}

	/**
	 * 定位地址
	 */
	private void locationAddress()
	{
		// 开始定位
		mIvLocation.setVisibility(View.GONE);
		setCurrentLocationCity("定位中...", false);
		BaiduMapManager.getInstance().startLocation(new BDLocationListener()
		{
			@Override
			public void onReceivePoi(BDLocation arg0)
			{

			}

			@Override
			public void onReceiveLocation(BDLocation location)
			{

				if (location != null)
				{
					String strAddress = location.getAddrStr();
					if (!AppHelper.isEmptyString(strAddress) && strAddress.contains(Constant.EARN_SUB_CHAR))
					{
						strAddress = strAddress.substring(strAddress.indexOf(Constant.EARN_SUB_CHAR) + 1);
					}
					double lat = location.getLatitude();
					double lon = location.getLongitude();
					if (!TextUtils.isEmpty(strAddress) && lat > 0 && lon > 0)
					{
						setCurrentLocationCity(strAddress, true);
					}
				} else
				{
					setCurrentLocationCity("定位失败，点击重试", false);
				}
				// 定位回调完关闭，否则一直定位浪费电
				BaiduMapManager.getInstance().destoryLocation();
				mIvLocation.setVisibility(View.VISIBLE);
			}
		});
	}

	private void setCurrentLocationCity(String string, boolean isLocationSuccess)
	{
		if (!AppHelper.isEmptyString(string))
		{
			if (mTvAddress != null)
			{
				mTvAddress.setText(string);
				if (isLocationSuccess)
				{
					// 这个刷新有BUG会保留原始状态
					mPtrlvContent.setRefreshing();
				}
			}
		}
	}

	@Override
	public void onEventMainThread(SDBaseEvent event)
	{
		// TODO Auto-generated method stub
		super.onEventMainThread(event);
		switch (EnumEventTag.valueOf(event.getEventTagInt()))
		{
		case CITY_CHANGE:
			initTitle();
			city_id = AppRuntimeWorker.getCity_id();
			mPtrlvContent.setRefreshing();
			break;

		default:
			break;
		}
	}

}
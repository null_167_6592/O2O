package com.wenzhoujie.jshandler;

import android.app.Activity;
import android.content.Intent;
import android.webkit.JavascriptInterface;

import com.wenzhoujie.MainActivity;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.library.common.SDActivityManager;
import com.wenzhoujie.model.act.IndexActNewModel;
import com.wenzhoujie.utils.SDTypeParseUtil;

/**
 * app详情页js回调处理类
 * 
 * @author Administrator
 * 
 */
public class AppJsHandler extends BaseJsHandler
{
	private static final String DEFAULT_NAME = "App";

	public AppJsHandler(String name, Activity activity)
	{
		super(name, activity);
	}

	public AppJsHandler(Activity activity)
	{
		this(DEFAULT_NAME, activity);
	}

	@JavascriptInterface
	public void app_detail(int type, int id)
	{
		Intent intent = IndexActNewModel.createIntentByType(type, id, true);
		if (intent != null)
		{
			try
			{
				startActivity(intent);
			} catch (Exception e)
			{
				// TODO: handle exception
			}
		}
	}

	@JavascriptInterface
	public void close_page()
	{
		finish();
	}

	@JavascriptInterface
	public void login()
	{
		Activity activity = SDActivityManager.getInstance().getLastActivity();
		AppHelper.isLogin(activity);
	}

	@JavascriptInterface
	public void page_title(String title)
	{

	}

}

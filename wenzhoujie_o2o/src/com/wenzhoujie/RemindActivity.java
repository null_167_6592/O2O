package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.adapter.FavorableListAdapter;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.FavorableListActItemModel;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.FavorableListActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

public class RemindActivity extends BaseActivity implements OnClickListener
{

	@ViewInject(id = R.id.act_remind_ptrlv_list)
	private PullToRefreshListView mPtrlvList = null;

	@ViewInject(id = R.id.act_remind_iv_empty_pic)
	private ImageView mIvEmptyPic = null;

	private List<FavorableListActItemModel> mlistModel = new ArrayList<FavorableListActItemModel>();
	private FavorableListAdapter mAdapter = null;

	private int mCurPage = 1;
	private int mTotalPage = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_remind);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		bindDefaultData();
		initPullListView();
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
				startActivity(new Intent(RemindActivity.this, SubscribeActivity.class));
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("打折提醒");
		mTitleSimple.setRightText("提醒设置");
	}

	private void bindDefaultData()
	{
		mAdapter = new FavorableListAdapter(mlistModel, this);
		mPtrlvList.setAdapter(mAdapter);
	}

	private void initPullListView()
	{
		mPtrlvList.setMode(Mode.BOTH);
		mPtrlvList.setOnRefreshListener(new OnRefreshListener2<ListView>()
		{

			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				mCurPage = 1;
				requestFavorable(false);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				if (++mCurPage > mTotalPage && mTotalPage != -1)
				{
					SDToast.showToast("没有更多数据了");
					mPtrlvList.onRefreshComplete();
				} else
				{
					requestFavorable(true);
				}
			}
		});
		mPtrlvList.setRefreshing();
	}

	protected void requestFavorable(final boolean isLoadMore)
	{

		RequestModel model = new RequestModel();
		model.put("act", "remindyouhui");
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (user != null)
		{
			model.put("email", user.getUser_name());
			model.put("pwd", user.getUser_pwd());
		}
		model.put("page", mCurPage);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{

			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				FavorableListActModel model = JsonUtil.json2Object(responseInfo.result, FavorableListActModel.class);
				if (!SDInterfaceUtil.isActModelNull(model))
				{
					switch (model.getResponse_code())
					{
					case 0:

						break;
					case 1:
						if (model.getPage() != null)
						{
							mCurPage = model.getPage().getPage();
							mTotalPage = model.getPage().getPage_total();
						}
						if (!isLoadMore)
						{
							mlistModel.clear();
						}
						if (model.getItem() != null)
						{
							mlistModel.addAll(model.getItem());
						} else
						{
							SDToast.showToast("没有更多数据了");
						}
						mAdapter.updateListViewData(mlistModel);
						break;
					default:
						break;
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				AppHelper.hideLoadingDialog();
				mPtrlvList.onRefreshComplete();
			}
		};

		InterfaceServer.getInstance().requestInterface(model, handler);

	}

	@Override
	public void onClick(View v)
	{
	}

}
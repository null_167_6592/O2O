package com.wenzhoujie;

import android.os.Bundle;
import android.view.View;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.lingou.www.R;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.fragment.WebViewFragment;
import com.wenzhoujie.fragment.WebViewFragment.EnumProgressMode;
import com.wenzhoujie.utils.IocUtil;

/**
 * 项目详情web界面
 * 
 * @author js02
 * 
 */
public class WebViewActivity extends BaseActivity
{
	/** webview 要加载的链接 */
	public static final String EXTRA_URL = "extra_url";
	/** webview 界面标题 */
	public static final String EXTRA_TITLE = "extra_title";
	/** 要显示的HTML内容 */
	public static final String EXTRA_HTML_CONTENT = "extra_html_content";
	/** header中的referer */
	public static final String EXTRA_REFERER = "extra_referer";

	private String mStrUrl = null;
	private String mStrTitle = null;
	private String mStrHtmlContent = null;

	protected WebViewFragment mFragWebview = null;

	public void setUrl(String url)
	{
		this.mStrUrl = url;
	}

	public void setHtmlContent(String htmlContent)
	{
		this.mStrHtmlContent = htmlContent;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		mIsNeedSlideFinishLayout = false;
		setmTitleType(TitleType.TITLE_SIMPLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_webview);
		IocUtil.initInjectedView(this);
		init();
	}

	protected void init()
	{
		initIntentData();
		initTitle();
		addFragments();
	}

	private void addFragments()
	{
		mFragWebview = new WebViewFragment();
		mFragWebview.setHtmlContent(mStrHtmlContent);
		mFragWebview.setUrl(mStrUrl);
		replaceFragment(mFragWebview, R.id.act_webview_fl_content);
	}

	private void initIntentData()
	{
		mStrUrl = getIntent().getStringExtra(EXTRA_URL);
		mStrTitle = getIntent().getStringExtra(EXTRA_TITLE);
		mStrHtmlContent = getIntent().getStringExtra(EXTRA_HTML_CONTENT);
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
				if (mFragWebview != null)
				{
					//mFragWebview.startLoadData();
					finish();
				}
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				if (mFragWebview.canGoBack())
					mFragWebview.onGoBack();
				else
					finish();
			}
		});
		//mTitleSimple.setRightText("刷新");
		mTitleSimple.setRightText("关闭");

		if (mStrTitle != null)
		{
			mTitleSimple.setTitleTop(mStrTitle);
		}
	}
	
	protected void getIntentData()
	{
		String strUrl = getIntent().getStringExtra(EXTRA_URL);
		String strTitle = getIntent().getStringExtra(EXTRA_TITLE);
		String strHtmlContent = getIntent().getStringExtra(EXTRA_HTML_CONTENT);
		String strReferer = getIntent().getStringExtra(EXTRA_REFERER);

		// 初始化fragment
		mFragWebview.setHtmlContent(strHtmlContent);
		mFragWebview.setUrl(strUrl);
		mFragWebview.setReferer(strReferer);
	}
	
	protected WebViewFragment createFragment()
	{
		WebViewFragment frag = new WebViewFragment();
		frag.setmProgressMode(EnumProgressMode.HORIZONTAL);
		return frag;
	}

}
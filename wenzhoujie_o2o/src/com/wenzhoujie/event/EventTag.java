package com.wenzhoujie.event;

public class EventTag
{
	/** 退出app事件 */
	public static final int EVENT_EXIT_APP = 0;
	/** 正常登录成功事件 */
	public static final int EVENT_LOGIN_NORMAL_SUCCESS = 1;
	/** 刷新用户金额成功事件 */
	public static final int EVENT_REFRESH_USER_MONEY_SUCCESS = 2;
	/** 退出登录事件 */
	public static final int EVENT_LOGOUT = 3;
	/** 刷新区域，城市名字，城市id成功事件 */
	public static final int EVENT_REFRESH_QUANLIST_CITYID_CITYNAME_SUCCESS = 4;
	/** 返回我发表界面 */
	public static final int EVENT_RETURN_PERSONCENTER_SUCCESS = 5;

}

package com.wenzhoujie.event;

public enum EnumEventTag
{
	/** 退出app事件 */
	EXIT_APP,

	/** 正常登录成功事件 */
	LOGIN_NORMAL_SUCCESS,

	/** 刷新用户金额成功事件 */
	REFRESH_USER_MONEY_SUCCESS,

	/** 退出登录事件 */
	LOGOUT,

	/** 刷新区域，城市名字，城市id成功事件 */
	REFRESH_QUANLIST_CITYID_CITYNAME_SUCCESS,

	/** 购物车下单成功 */
	DONE_CART_SUCCESS,
	
	/** 刷新订单列表 */
	REFRESH_ORDER_LIST,
	
	/** 订单支付成功 */
	PAY_ORDER_SUCCESS,

	/** 城市改变事件 */
	CITY_CHANGE,

	/** 定位成功事件 */
	LOCATION_SUCCESS,

	/** 打开分享列表 */
	OPEN_SHARE_LIST,

	/** 扫描二维码成功 */
	SCAN_CODE_SUCCESS,

	/** 用户配送地址改变（删除，修改等） */
	USER_DELIVERY_CHANGE,

	/** 重试初始化成功 */
	RETRY_INIT_SUCCESS;

	public static EnumEventTag valueOf(int index)
	{
		if (index >= 0 && index < values().length)
		{
			return values()[index];
		} else
		{
			return null;
		}
	}
}

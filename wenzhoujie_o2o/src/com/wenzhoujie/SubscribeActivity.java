package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.adapter.SubscribeAdapter;
import com.wenzhoujie.adapter.SubscribeAdapter.SubscribeAdapterListener;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.GetbrandlistActItemModel;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.BaseActModel;
import com.wenzhoujie.model.act.GetbrandlistActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

public class SubscribeActivity extends BaseActivity
{

	@ViewInject(id = R.id.act_subscribe_ptrlv_subscribes)
	private PullToRefreshListView mPtrlvSubscribes = null;

	@ViewInject(id = R.id.act_subscribe_iv_empty)
	private ImageView mIvEmpty = null;

	private SubscribeAdapter mAdapter = null;
	private List<GetbrandlistActItemModel> mListModel = new ArrayList<GetbrandlistActItemModel>();

	private int mPage = 1;
	private int mPageTotal = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_subscribe);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		bindDefaultData();
		initPullToRefreshListView();
	}

	private void bindDefaultData()
	{
		mAdapter = new SubscribeAdapter(mListModel, this, new SubscribeAdapterListener()
		{
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked, GetbrandlistActItemModel model)
			{
				// TODO 请求订阅接口
				requestSubscribe(model);
			}
		});
		mPtrlvSubscribes.setAdapter(mAdapter);
	}

	private void initPullToRefreshListView()
	{
		mPtrlvSubscribes.setMode(Mode.BOTH);
		mPtrlvSubscribes.setOnRefreshListener(new OnRefreshListener2<ListView>()
		{
			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				refreshData();
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				loadMoreData();
			}
		});

		mPtrlvSubscribes.setRefreshing();
	}

	protected void refreshData()
	{
		mPage = 1;
		requestSubscribeList(false);

	}

	protected void loadMoreData()
	{
		mPage++;
		if (mPage > mPageTotal && mPageTotal != 0)
		{
			SDToast.showToast("没有更多数据了");
			mPtrlvSubscribes.onRefreshComplete();
		} else
		{
			requestSubscribeList(true);
		}

	}

	/**
	 * 订阅
	 * 
	 * @param model
	 */
	protected void requestSubscribe(final GetbrandlistActItemModel itemModel)
	{
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (user != null && itemModel != null && itemModel.getId() != null)
		{
			RequestModel model = new RequestModel();
			model.put("act", "dybrand");
			// model.put("page", mPage);
			model.put("email", user.getUser_name());
			model.put("pwd", user.getUser_pwd());
			model.put("brand_id", itemModel.getId());
			RequestCallBack<String> handler = new RequestCallBack<String>()
			{

				@Override
				public void onStart()
				{
					AppHelper.showLoadingDialog("请稍候...");
				}

				@Override
				public void onSuccess(ResponseInfo<String> responseInfo)
				{
					BaseActModel actModel = JsonUtil.json2Object(responseInfo.result, BaseActModel.class);
					if (!SDInterfaceUtil.isActModelNull(actModel))
					{
						if (actModel.getResponse_code() == 1)
						{
							if (!TextUtils.isEmpty(actModel.getStatus()))
							{
								itemModel.setIs_checked(actModel.getStatus());
							}
						}
						if (mAdapter != null)
						{
							mAdapter.notifyDataSetChanged();
						}
					}
				}

				@Override
				public void onFailure(HttpException error, String msg)
				{

				}

				@Override
				public void onFinish()
				{
					AppHelper.hideLoadingDialog();
				}
			};
			InterfaceServer.getInstance().requestInterface(model, handler);
		}
	}

	/**
	 * 请求可以订阅的列表
	 * 
	 * @param isLoadMore
	 */
	private void requestSubscribeList(final boolean isLoadMore)
	{
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (user != null)
		{
			RequestModel model = new RequestModel();
			model.put("act", "getbrandlist");
			model.put("page", mPage);
			model.put("email", user.getUser_name());
			model.put("pwd", user.getUser_pwd());
			RequestCallBack<String> handler = new RequestCallBack<String>()
			{

				@Override
				public void onStart()
				{
					AppHelper.showLoadingDialog("请稍候...");
				}

				@Override
				public void onSuccess(ResponseInfo<String> responseInfo)
				{
					GetbrandlistActModel actModel = JsonUtil.json2Object(responseInfo.result, GetbrandlistActModel.class);
					if (!SDInterfaceUtil.isActModelNull(actModel))
					{
						if (actModel.getResponse_code() == 1)
						{
							if (actModel.getPage() != null)
							{
								mPage = actModel.getPage().getPage();
								mPageTotal = actModel.getPage().getPage_total();
							}

							if (actModel.getItem() != null && actModel.getItem().size() > 0)
							{
								if (!isLoadMore)
								{
									mListModel.clear();
								}
								mListModel.addAll(actModel.getItem());
							} else
							{
								mListModel.clear();
								SDToast.showToast("未找到数据");
							}
							mAdapter.updateListViewData(mListModel);

						}

					}
				}

				@Override
				public void onFailure(HttpException error, String msg)
				{

				}

				@Override
				public void onFinish()
				{
					AppHelper.hideLoadingDialog();
					mPtrlvSubscribes.onRefreshComplete();
					SDViewUtil.toggleEmptyMsgByList(mListModel, mIvEmpty);
				}
			};
			InterfaceServer.getInstance().requestInterface(model, handler);
		}

	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("设置提醒");
	}

}
package com.wenzhoujie;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.BaseActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

public class SendlettersActivity extends BaseActivity implements OnClickListener
{

	public static final int RESULT_CODE_SEND_SUCCESS = 10;

	/** fansname */
	public static final String EXTRA_FANS_NAME = "extra_fans_name";

	@ViewInject(id = R.id.act_sendletters_et_fansNameText)
	private EditText mEtFansnametext = null;

	@ViewInject(id = R.id.act_sendletters_btn_selectFans)
	private Button mBtnSelectfans = null;

	@ViewInject(id = R.id.act_sendletters_et_lettersContentText)
	private EditText mEtLetterscontenttext = null;

	private String fansName = null;
	private String ContentText = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_sendletters);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		registeClick();

	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
				publishData();
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("发私信");
		mTitleSimple.setRightText("发送");

	}

	protected void requestSendMSG()
	{

		RequestModel model = new RequestModel();
		model.put("act", "sendmsg");
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (user != null)
		{
			model.put("email", user.getUser_name());
			model.put("pwd", user.getUser_pwd());
		}
		model.put("user_name", fansName);
		model.put("message", ContentText);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{
				AppHelper.showLoadingDialog("发送中");
			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				BaseActModel actModel = JsonUtil.json2Object(responseInfo.result, BaseActModel.class);
				if (!SDInterfaceUtil.isActModelNull(actModel))
				{
					if (actModel.getResponse_code() == 1)
					{
						setResult(RESULT_CODE_SEND_SUCCESS);
						finish();
					}
				}
			}

			@Override
			public void onFinish()
			{
				AppHelper.hideLoadingDialog();
			}
		};

		InterfaceServer.getInstance().requestInterface(model, handler);

	}

	private void registeClick()
	{
		mBtnSelectfans.setOnClickListener(this);

	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.act_sendletters_btn_selectFans:
			clickSelectFans();
			break;

		default:
			break;
		}
	}

	/**
	 * 选择粉丝
	 */
	private void clickSelectFans()
	{
		LocalUserModel user = App.getApplication().getmLocalUser();
		Intent intent = new Intent();
		intent.setClass(SendlettersActivity.this, UserFansAndMyFollowsActivity.class);
		intent.putExtra(UserFansAndMyFollowsActivity.EXTRA_ACT, "fanslist");
		intent.putExtra(UserFansAndMyFollowsActivity.EXTRA_TYPE, true);
		intent.putExtra(UserFansAndMyFollowsActivity.EXTRA_UID, user.getUser_id());
		startActivityForResult(intent, 0);
	}

	/**
	 * 发送
	 */
	private void publishData()
	{
		if (mEtFansnametext.getText() != null)
		{
			fansName = mEtFansnametext.getText().toString();
			ContentText = mEtLetterscontenttext.getText().toString();
			requestSendMSG();
		} else
		{
			SDToast.showToast("收件人不能为空");
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == 1)
		{
			Bundle d = data.getExtras();
			String a = d.getString(EXTRA_FANS_NAME);
			mEtFansnametext.setText(a);
		}

	}

}
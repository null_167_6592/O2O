package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.adapter.PrivateLettersItemAdapter;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.PrivateLettersItemActivityMsg_listModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.SearchConditionModel;
import com.wenzhoujie.model.act.PrivateLettersItemActivityModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

public class PrivatelettersItemListActivity extends BaseActivity
{
	/** mid **/
	public static final String EXTRA_MID = "extra_mid";

	@ViewInject(id = R.id.act_privateletters_item_ptrlv_list)
	private PullToRefreshListView mList = null;

	private SearchConditionModel mSearcher = new SearchConditionModel();
	private PrivateLettersItemAdapter mAdapter = null;
	private List<PrivateLettersItemActivityMsg_listModel> mMsg_listModel = new ArrayList<PrivateLettersItemActivityMsg_listModel>();

	private String mid = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_privateletters_item_list);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initIntentData();
		initTitle();
		bindDefaultLvData();
		initPullRefreshLv();
	}

	private void initIntentData()
	{
		Intent intent = getIntent();
		mid = intent.getStringExtra(EXTRA_MID);

	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});

	}

	private void bindDefaultLvData()
	{
		mAdapter = new PrivateLettersItemAdapter(mMsg_listModel, this);
		mList.setAdapter(mAdapter);
	}

	private void initPullRefreshLv()
	{
		mList.setMode(Mode.BOTH);
		mList.setOnRefreshListener(new OnRefreshListener2<ListView>()
		{

			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				mSearcher.getPageModel().setPage(1);
				requestPrivateLetters(false);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				int page = mSearcher.getPageModel().getPage();
				int pageTotal = mSearcher.getPageModel().getPage_total();
				page++;
				if (page > pageTotal && pageTotal > 0)
				{
					SDToast.showToast("没有更多数据了");
					mList.onRefreshComplete();
				} else
				{
					mSearcher.getPageModel().setPage(page);
					requestPrivateLetters(true);
				}
			}
		});
		mList.setRefreshing();
	}

	private void requestPrivateLetters(final boolean isLoadMore)
	{
		RequestModel model = new RequestModel();
		model.put("act", "msgview");
		model.put("email", mSearcher.getEmail());
		model.put("pwd", mSearcher.getPwd());
		model.put("page", mSearcher.getPageModel().getPage());
		model.put("mid", mid);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{

			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				PrivateLettersItemActivityModel model = JsonUtil.json2Object(responseInfo.result, PrivateLettersItemActivityModel.class);
				if (!SDInterfaceUtil.isActModelNull(model))
				{
					switch (model.getResponse_code())
					{
					case 0:

						break;
					case 1:
						if (model.getPage() != null)
						{
							mSearcher.getPageModel().setPage(model.getPage().getPage());
							mSearcher.getPageModel().setPage_total(model.getPage().getPage_total());
						} else
						{
							SDToast.showToast("未收到任何私信");
						}
						mTitleSimple.setTitleTop(model.getTitle());
						mMsg_listModel.addAll(model.getMsg_list());
						mAdapter.updateListViewData(mMsg_listModel);
						break;
					default:
						break;
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				AppHelper.hideLoadingDialog();
				mList.onRefreshComplete();
			}
		};

		InterfaceServer.getInstance().requestInterface(model, handler);
	}

}
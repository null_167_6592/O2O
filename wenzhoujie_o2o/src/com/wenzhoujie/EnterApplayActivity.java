package com.wenzhoujie;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.library.customview.ClearEditText;
import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.BaseActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;
import com.wenzhoujie.utils.ViewInject;

public class EnterApplayActivity extends BaseActivity implements OnClickListener
{
	@ViewInject(id = R.id.act_enter_apply_et_shop_name)
	private ClearEditText mEtShopName = null;
	
	@ViewInject(id = R.id.act_enter_apply_et_shop_address)
	private ClearEditText mEtShopAddress = null;
	
	@ViewInject(id = R.id.act_enter_apply_et_user_name)
	private ClearEditText mEtUserName = null;
	
	@ViewInject(id = R.id.act_enter_apply_et_mobile)
	private ClearEditText mEtMobile = null;

	@ViewInject(id = R.id.act_enter_applay_tv_submit)
	private TextView mTvSubmit = null;

	private String mStrShopName = null;
	private String mStrShopAddress = null;
	private String mStrUserName = null;
	private String mStrMobile = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_enter_applay);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		registeClick();

	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("商家入驻申请");
	}

	private void registeClick()
	{
		mTvSubmit.setOnClickListener(this);

	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.act_enter_applay_tv_submit:
			clickSubmit();
			break;

		default:
			break;
		}
	}

	private void clickSubmit()
	{
		if (validateParams())
		{
			final LocalUserModel user = App.getApplication().getmLocalUser();
			if (user != null)
			{
				RequestModel model = new RequestModel(false);
				model.put("act", "biz_apply");
				model.put("shop_name", mStrShopName);
				model.put("shop_address", mStrShopAddress);
				model.put("user_name", mStrUserName);
				model.put("moblie", mStrMobile);
				RequestCallBack<String> handler = new RequestCallBack<String>()
				{

					@Override
					public void onStart()
					{
						AppHelper.showLoadingDialog("请稍候...");
					}

					@Override
					public void onSuccess(ResponseInfo<String> responseInfo)
					{
						BaseActModel actModel = JsonUtil.json2Object(responseInfo.result, BaseActModel.class);
						if (!SDInterfaceUtil.isActModelNull(actModel))
						{
							int status = SDTypeParseUtil.getIntFromString(
									actModel.getStatus(), 0);
							if (status == 1) {
								finish();
							}
						}
					}

					@Override
					public void onFailure(HttpException error, String msg)
					{

					}

					@Override
					public void onFinish()
					{
						AppHelper.hideLoadingDialog();
					}
				};
				InterfaceServer.getInstance().requestInterface(model, handler);
			}

		}

	}

	private boolean validateParams()
	{
		mStrShopName = mEtShopName.getText().toString().trim();
		if (TextUtils.isEmpty(mStrShopName))
		{
			SDToast.showToast("店铺名称不能为空");
			mEtShopName.requestFocus();
			return false;
		}
		
		mStrShopAddress = mEtShopAddress.getText().toString().trim();
		if (TextUtils.isEmpty(mStrShopAddress))
		{
			SDToast.showToast("店铺地址不能为空");
			mEtShopAddress.requestFocus();
			return false;
		}
		
		mStrUserName = mEtUserName.getText().toString().trim();
		if (TextUtils.isEmpty(mStrUserName))
		{
			SDToast.showToast("真实姓名不能为空");
			mEtUserName.requestFocus();
			return false;
		}
		
		mStrMobile = mEtMobile.getText().toString().trim();
		if (TextUtils.isEmpty(mStrMobile))
		{
			SDToast.showToast("联系方式不能为空");
			mEtMobile.requestFocus();
			return false;
		}
		return true;
	}

}
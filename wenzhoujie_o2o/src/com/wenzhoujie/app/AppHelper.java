package com.wenzhoujie.app;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Looper;
import android.text.TextUtils;

import com.facebook.LoginActivity;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.utils.SDDialogUtil;
import com.wenzhoujie.utils.SDHandlerUtil;

public class AppHelper
{

	public static void showLoadingDialog(final String msg)
	{
		if (Looper.myLooper() == Looper.getMainLooper())
		{
			SDDialogUtil.showLoading(msg);
		} else
		{
			SDHandlerUtil.runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					SDDialogUtil.showLoading(msg);
				}
			});
		}
	}

	public static void hideLoadingDialog()
	{
		if (Looper.myLooper() == Looper.getMainLooper())
		{
			SDDialogUtil.dismissLoadingDialog();
		} else
		{
			SDHandlerUtil.runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					SDDialogUtil.dismissLoadingDialog();
				}
			});
		}
	}

	public static boolean isLogin()
	{
		return getLocalUser() != null;
	}
	
	public static boolean isLogin(Activity activity)
	{
		if (getLocalUser() == null)
		{
			if (activity != null)
			{
				Intent intent = new Intent(activity, LoginActivity.class);
				activity.startActivity(intent);
			}
			return false;
		} else
		{
			return true;
		}
	}

	public static LocalUserModel getLocalUser()
	{
		return App.getApplication().getmLocalUser();
	}

	public static boolean isEmptyString(String string)
	{
		return TextUtils.isEmpty(string);
	}

	public static boolean isIndexLegalInList(List<?> list, int index)
	{
		if (list != null && index >= 0 && index < list.size())
		{
			return true;
		} else
		{
			return false;
		}
	}

}

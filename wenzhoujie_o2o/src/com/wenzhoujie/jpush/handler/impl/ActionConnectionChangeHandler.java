package com.wenzhoujie.jpush.handler.impl;

import android.content.Intent;
import cn.jpush.android.api.JPushInterface;

import com.wenzhoujie.app.App;
import com.wenzhoujie.jpush.handler.IJpushActionHandler;
import com.wenzhoujie.runnable.ReConnectThread;

public class ActionConnectionChangeHandler implements IJpushActionHandler
{
	@Override
	public void handle(Intent intent)
	{
		if (JPushInterface.isPushStopped(App.getApplication()))
		{
			new Thread(new ReConnectThread()).start();
		}
	}
}

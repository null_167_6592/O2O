package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.View;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lingou.www.R;
import com.wenzhoujie.adapter.QuanListAdapter;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.dao.InitActModelDao;
import com.wenzhoujie.model.QuansModel;
import com.wenzhoujie.model.act.InitActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

public class QuanListActivity extends BaseActivity
{

	/** select_result */
	public static String EXTRA_SELECT_RESULT = "EXTRA_SELECT_RESULT";
	/** quan_id */
	public static String EXTRA_QUAN_ID = "EXTRA_QUAN_ID";
	/** is_merchant */
	public static String EXTRA_IS_MERCHANT = "EXTRA_IS_MERCHANT";
	/** key */
	public static String EXTRA_KEY = "EXTRA_KEY";

	@ViewInject(id = R.id.act_cate_lv)
	private PullToRefreshListView mLv = null;

	private QuanListAdapter mAdapter = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_cate);
		IocUtil.initInjectedView(this);
		init();

	}

	private void init()
	{
		initTitle();
		bindDefaultData();
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("商圈列表");
	}

	private void bindDefaultData()
	{
		InitActModel model = InitActModelDao.queryModel();
		QuansModel q = new QuansModel();
		List<QuansModel> quanModel = new ArrayList<QuansModel>();
		q.setId("0");
		q.setName("全部商圈");
		quanModel.add(q);
		quanModel.addAll(model.getQuanlist());
		mAdapter = new QuanListAdapter(quanModel, this);
		mLv.setAdapter(mAdapter);
		mAdapter.updateListViewData(quanModel);
	}

}
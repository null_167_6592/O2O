package com.wenzhoujie.common;

import android.graphics.Bitmap;

import com.lingou.www.R;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.wenzhoujie.app.App;

public class ImageLoaderManager
{

	public static void initImageLoader()
	{
		if (!ImageLoader.getInstance().isInited())
		{
			ImageLoaderConfiguration config = getConfigDefault();
			ImageLoader.getInstance().init(config);
		}
	}

	private static ImageLoaderConfiguration getConfigDefault()
	{
		//
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(App.getApplication())
				.denyCacheImageMultipleSizesInMemory()
				.memoryCache(new WeakMemoryCache())
				.memoryCacheSize(4 * 1024 * 1024)
				.threadPoolSize(2)
				.discCacheFileNameGenerator(new Md5FileNameGenerator())
				.defaultDisplayImageOptions(getOptionsDefault()).build();
		return config;
	}

	private static DisplayImageOptions getOptionsDefault()
	{
		DisplayImageOptions options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.nopic)
				.showImageForEmptyUri(R.drawable.nopic)
				.showImageOnFail(R.drawable.nopic)
				.resetViewBeforeLoading(true)
				.cacheInMemory(true)
				.cacheOnDisc(true)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true).displayer(new FadeInBitmapDisplayer(300)).build();
		return options;
	}

	public static DisplayImageOptions getOptionsNoCache()
	{
		DisplayImageOptions options = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.nopic).showImageForEmptyUri(R.drawable.nopic)
				.showImageOnFail(R.drawable.nopic).resetViewBeforeLoading(true).cacheInMemory(false).cacheOnDisc(false)
				.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true).displayer(new FadeInBitmapDisplayer(300)).build();
		return options;
	}

}

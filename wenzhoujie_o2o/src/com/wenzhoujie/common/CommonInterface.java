package com.wenzhoujie.common;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import android.text.TextUtils;
import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;

import com.alibaba.fastjson.JSON;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.sunday.eventbus.SDEventManager;
import com.wenzhoujie.app.App;
import com.wenzhoujie.dao.InitActModelDao;
import com.wenzhoujie.event.EnumEventTag;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.library.dialog.SDDialogManager;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.listener.GetUserShareByUidListener;
import com.wenzhoujie.listener.RequestInitListener;
import com.wenzhoujie.listener.RequestScanResultListener;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.Mobile_qrcodeActModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.InitActModel;
import com.wenzhoujie.model.act.LoginActModel;
import com.wenzhoujie.model.act.Quan_listActModel;
import com.wenzhoujie.model.act.UActModel;
import com.wenzhoujie.umeng.UmengSocialManager;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.work.AppRuntimeWorker;

public class CommonInterface
{

	public static void requestInit(final RequestInitListener listener)
	{
		RequestModel model = new RequestModel();
		model.put("act", "init");
		model.put("device_type", "android");
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{
				if (listener != null)
				{
					listener.onStart();
				}
			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				boolean initSuccess = false;
				InitActModel model = JsonUtil.json2Object(responseInfo.result, InitActModel.class);
				if (!SDInterfaceUtil.isActModelNull(model))
				{
					// TODO:对初始化返回结果进行处理
					switch (model.getResponse_code())
					{
					case 1:
						InitActModelDao.insertOrUpdateModel(model);
						initSuccess = true;
						UmengSocialManager.initHandler();
						break;

					default:
						break;
					}
				}
				if (listener != null)
				{
					if (initSuccess)
					{
						listener.onSuccess(responseInfo, model);
					} else
					{
						listener.onFailure(null, null);
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{
				if (listener != null)
				{
					listener.onFailure(error, msg);
				}
			}

			@Override
			public void onFinish()
			{
				UmengSocialManager.initDisplay();
				if (listener != null)
				{
					listener.onFinish();
				}
			}
		};
		InterfaceServer.getInstance().requestInterface(model, handler);
	}

	public static void refreshLocalUser()
	{
		final LocalUserModel user = App.getApplication().getmLocalUser();
		if (user != null)
		{
			RequestModel model = new RequestModel();
			model.put("act", "login");
			model.put("email", user.getUser_name());
			model.put("pwd", user.getUser_pwd());
			RequestCallBack<String> handler = new RequestCallBack<String>()
			{

				@Override
				public void onStart()
				{

				}

				@Override
				public void onSuccess(ResponseInfo<String> responseInfo)
				{
					LoginActModel actModel = JSON.parseObject(responseInfo.result, LoginActModel.class);
					if (actModel != null)
					{
						if (actModel.getResponse_code() == 1)
						{
							LocalUserModel userModel = App.getApplication().getmLocalUser();
							userModel.setUser_money(actModel.getUser_money());
							userModel.setUser_money_format(actModel.getUser_money_format());
							userModel.setUser_score(actModel.getUser_score());
							App.getApplication().setmLocalUser(userModel);
							SDEventManager.post(EnumEventTag.REFRESH_USER_MONEY_SUCCESS.ordinal());
						}
					}
				}

				@Override
				public void onFailure(HttpException error, String msg)
				{

				}

				@Override
				public void onFinish()
				{

				}
			};
			InterfaceServer.getInstance().requestInterface(model, handler);
		}
	}

	public static void refresh_Quanlist_cityId_cityName_ByCityName(final String cityName)
	{
		if (!TextUtils.isEmpty(cityName))
		{
			final int cityId = AppRuntimeWorker.getCityIdByCityName(cityName);
			RequestModel model = new RequestModel();
			model.put("act", "quan_list");
			model.put("city_id", cityId);
			RequestCallBack<String> handler = new RequestCallBack<String>()
			{

				@Override
				public void onStart()
				{

				}

				@Override
				public void onSuccess(ResponseInfo<String> responseInfo)
				{
					Quan_listActModel actModel = JsonUtil.json2Object(responseInfo.result, Quan_listActModel.class);
					if (actModel != null)
					{
						InitActModel model = InitActModelDao.queryModel();
						if (model != null)
						{
							model.setCity_name(cityName);
							model.setCity_id(cityId);
							model.setQuanlist(actModel.getQuanlist());
							InitActModelDao.insertOrUpdateModel(model);
							SDEventManager.post(EnumEventTag.REFRESH_QUANLIST_CITYID_CITYNAME_SUCCESS.ordinal());
						}
					}
				}

				@Override
				public void onFailure(HttpException error, String msg)
				{

				}

				@Override
				public void onFinish()
				{

				}
			};
			InterfaceServer.getInstance().requestInterface(model, handler);
		}
	}

	public static void submitRegistrationID()
	{
		String regId = JPushInterface.getRegistrationID(App.getApplication());
		if (!TextUtils.isEmpty(regId))
		{
			final LocalUserModel user = App.getApplication().getmLocalUser();
			if (user != null && user.get_id() != 0)
			{
				JPushInterface.setAlias(App.getApplication(), String.valueOf(user.get_id()), new TagAliasCallback()
				{
					@Override
					public void gotResult(int arg0, String arg1, Set<String> arg2)
					{
						switch (arg0)
						{
						case 0: // 设置别名成功
							requestSubmitRegistrationID(user);
							break;

						default:
							break;
						}
					}
				});
			}
		} else
		{
			SDToast.showToast("获取设备推送ID失败，您的设备暂无法收到推送消息!");
		}
	}

	private static void requestSubmitRegistrationID(LocalUserModel user)
	{
		Map<String, Object> mapData = new HashMap<String, Object>();
		mapData.put("act", "apns");
		mapData.put("email", user.getUser_name());
		mapData.put("pwd", user.getUser_pwd());
		mapData.put("apns_code", user.get_id());
		RequestModel model = new RequestModel(mapData);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{

			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{

			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{

			}
		};
		InterfaceServer.getInstance().requestInterface(model, handler);
	}

	public static void getUserShareByUid(String uid, int page, final GetUserShareByUidListener listener)
	{
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (user == null)
		{
			if (listener != null)
			{
				listener.onUnLogin();
			}
			return;
		}

		if (TextUtils.isEmpty(uid))
		{
			if (listener != null)
			{
				listener.onUidIsEmpty();
			}
			return;
		}

		RequestModel model = new RequestModel();
		model.put("act", "u");
		model.put("uid", uid);
		model.put("email", user.getUser_name());
		model.put("pwd", user.getUser_pwd());
		model.put("page", page);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{
				if (listener != null)
				{
					listener.onStart();
				}
			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				UActModel model = JsonUtil.json2Object(responseInfo.result, UActModel.class);
				if (listener != null)
				{
					listener.onSuccess(model);
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{
				if (listener != null)
				{
					listener.onFailure();
				}
			}

			@Override
			public void onFinish()
			{
				if (listener != null)
				{
					listener.onFinish();
				}
			}
		};
		InterfaceServer.getInstance().requestInterface(model, handler);

	}

	public static void requestScanResult(String scanResult, final RequestScanResultListener listener)
	{
		RequestModel model = new RequestModel();
		model.put("act", "mobile_qrcode");
		model.put("pc_url", scanResult);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{
				if (listener != null)
				{
					listener.onStart();
				}
			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				Mobile_qrcodeActModel actModel = JsonUtil.json2Object(responseInfo.result, Mobile_qrcodeActModel.class);
				if (!SDInterfaceUtil.isActModelNull(actModel))
				{
					if (listener != null)
					{
						listener.onSuccess(responseInfo, actModel);
					}

				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				SDDialogManager.hideProgressDialog();
			}
		};
		InterfaceServer.getInstance().requestInterface(model, handler);

	}

}

package com.wenzhoujie.common;

import com.ta.util.config.TAPreferenceConfig;
import com.wenzhoujie.app.App;

/**
 * 
 * shareperference管理类
 * 
 */
public class ConfigManager
{

	private static TAPreferenceConfig mConfig = null;

	public static TAPreferenceConfig getConfig()
	{
		if (mConfig == null)
		{
			mConfig = (TAPreferenceConfig) TAPreferenceConfig.getPreferenceConfig(App.getApplication());
			if (!mConfig.isLoadConfig())
			{
				mConfig.loadConfig();
			}
		}
		return mConfig;
	}
}

package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.wenzhoujie.library.customview.SDTabItemCorner;
import com.wenzhoujie.library.customview.SDViewBase.EnumTabPosition;
import com.wenzhoujie.library.customview.SDViewNavigatorManager;
import com.wenzhoujie.library.customview.SDViewNavigatorManager.SDViewNavigatorManagerListener;
import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.lingou.www.R;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.fragment.LoginFragment;
import com.wenzhoujie.fragment.LoginPhoneFragment;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

public class LoginNewActivity extends BaseActivity
{
	public static final int REQUEST_CODE_REGISTER = 1;

	public static final int RESULT_CODE_LOGIN_SUCCESS = 10;

	public static final String EXTRA_SELECT_TAG_INDEX = "extra_select_tag_index";

	@ViewInject(id = R.id.act_login_new_tab_login_phone)
	private SDTabItemCorner mTabLoginPhone = null;
	
	@ViewInject(id = R.id.act_login_new_tab_login_normal)
	private SDTabItemCorner mTabLoginNormal = null;

	private SDViewNavigatorManager mViewManager = new SDViewNavigatorManager();

	private LoginFragment mFragLogin = null;
	private LoginPhoneFragment mFragLoginPhone = null;

	private int mSelectTabIndex = 0;

	private List<Integer> mListSelectIndex = new ArrayList<Integer>();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_login_new);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		getIntentData();
		initTitle();
		initTabs();
		addFragments();
	}

	private void getIntentData()
	{
		mListSelectIndex.add(0);
		mListSelectIndex.add(1);

		mSelectTabIndex = getIntent().getIntExtra(EXTRA_SELECT_TAG_INDEX, 0);

		if (!mListSelectIndex.contains(mSelectTabIndex))
		{
			mSelectTabIndex = 0;
		}
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
				//startRegisterActivity();
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("登录");
		//mTitleSimple.setRightText("注册");
		mTitleSimple.setRightText("");
	}

	private void addFragments()
	{
		mFragLoginPhone = new LoginPhoneFragment();
		addFragment(mFragLoginPhone, R.id.act_login_new_fl_login_phone);
		
		
		mFragLogin = new LoginFragment();
		addFragment(mFragLogin, R.id.act_login_new_fl_login_normal);

		hideFragment(mFragLogin);

		mViewManager.setSelectIndex(mSelectTabIndex, null, true);
	}

	private void initTabs()
	{
		mTabLoginPhone.setTabName("快捷登录");
		mTabLoginPhone.setTabTextSizeSp(18);
		mTabLoginPhone.setmPosition(EnumTabPosition.FIRST);
		
		mTabLoginNormal.setTabName("登录");
		mTabLoginNormal.setTabTextSizeSp(18);
		mTabLoginNormal.setmPosition(EnumTabPosition.LAST);
		
		mViewManager.setItems(mTabLoginPhone, mTabLoginNormal);

		mViewManager.setmListener(new SDViewNavigatorManagerListener()
		{
			@Override
			public void onItemClick(View v, int index)
			{
				switch (index)
				{
				case 0: // 快捷登录
					clickLoginPhone();
					break;
				case 1: // 正常登录
					clickLoginNormal();
					break;
				
				default:
					break;
				}
			}
		});

		mViewManager.setSelectIndex(0, mTabLoginPhone, false);

	}

	/**
	 * 正常登录的选项卡被选中
	 */
	protected void clickLoginNormal()
	{
		showFragment(mFragLogin);
		hideFragment(mFragLoginPhone);
	}

	/**
	 * 手机号快捷登录的选项卡被选中
	 */
	protected void clickLoginPhone()
	{
		showFragment(mFragLoginPhone);
		hideFragment(mFragLogin);
	}

	protected void startRegisterActivity()
	{
		Intent intent = new Intent(LoginNewActivity.this, RegisterActivity.class);
		startActivityForResult(intent, REQUEST_CODE_REGISTER);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (mFragLogin != null)
		{
			mFragLogin.onActivityResult(requestCode, resultCode, data);
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

}
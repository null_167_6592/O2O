package com.wenzhoujie;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import cn.jpush.android.api.JPushInterface;

import com.wenzhoujie.library.utils.SDResourcesUtil;
import com.wenzhoujie.library.utils.SDToast;
import com.lingou.www.R;
import com.sunday.eventbus.SDBaseEvent;
import com.wenzhoujie.app.App;
import com.wenzhoujie.customview.SDBottomTabItem;
import com.wenzhoujie.event.EnumEventTag;
import com.wenzhoujie.fragment.BaseFragment;
import com.wenzhoujie.fragment.HomeFragment2;
import com.wenzhoujie.fragment.MerchantListFragment;
import com.wenzhoujie.fragment.MoreFragment;
import com.wenzhoujie.fragment.MyAccountFragment;
import com.wenzhoujie.fragment.ShareFragment;
import com.wenzhoujie.model.CartGoodsArrayList;
import com.wenzhoujie.model.ShopGoodsModel;
import com.wenzhoujie.service.AppUpgradeService;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.LogUtil;
import com.wenzhoujie.utils.SDActivityUtil;
import com.wenzhoujie.utils.SDViewNavigatorManager;
import com.wenzhoujie.utils.ViewInject;
import com.wenzhoujie.utils.SDViewNavigatorManager.SDViewNavigatorManagerListener;

public class MainActivity extends BaseActivity
{

	@ViewInject(id = R.id.act_main_tab_0)
	private SDBottomTabItem mTab0 = null;

	@ViewInject(id = R.id.act_main_tab_1)
	private SDBottomTabItem mTab1 = null;

	@ViewInject(id = R.id.act_main_tab_2)
	private SDBottomTabItem mTab2 = null;

	@ViewInject(id = R.id.act_main_tab_3)
	private SDBottomTabItem mTab3 = null;

	@ViewInject(id = R.id.act_main_tab_4)
	private SDBottomTabItem mTab4 = null;

	private SDViewNavigatorManager mViewManager = new SDViewNavigatorManager();

	private HomeFragment2 mFragHomeNew;
	private MerchantListFragment mFragStores;
	private MyAccountFragment mFragMyAccount;
	private ShareFragment mFragShare;
	//private MoreFragment mFragMore;

	private BaseFragment mFragLast = null;

	private long mExitTime = 0;

	private SDBottomTabItem[] items = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		mIsNeedSlideFinishLayout = false;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_main);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		startUpgradeService();
		initTitle();
		initBottom();
	}

	private void startUpgradeService()
	{
		startService(new Intent(MainActivity.this, AppUpgradeService.class));
	}

	private void initTitle()
	{
		// TODO Auto-generated method stub

	}

	private void initBottom()
	{
		mTab0.setTitleText(SDResourcesUtil.getString(R.string.home));
		mTab1.setTitleText(SDResourcesUtil.getString(R.string.supplier));
		mTab2.setTitleText(SDResourcesUtil.getString(R.string.share));
		mTab3.setTitleText(SDResourcesUtil.getString(R.string.mine));
		mTab4.setTitleText(SDResourcesUtil.getString(R.string.more));

		mTab0.setmImageNormalId(R.drawable.tab_0_normal);
		mTab1.setmImageNormalId(R.drawable.tab_1_normal);
		mTab2.setmImageNormalId(R.drawable.tab_2_normal);
		mTab3.setmImageNormalId(R.drawable.tab_3_normal);
		mTab4.setmImageNormalId(R.drawable.tab_4_normal);

		mTab0.setmImagePressId(R.drawable.tab_0_press);
		mTab1.setmImagePressId(R.drawable.tab_1_press);
		mTab2.setmImagePressId(R.drawable.tab_2_press);
		mTab3.setmImagePressId(R.drawable.tab_3_press);
		mTab4.setmImagePressId(R.drawable.tab_4_press);

		items = new SDBottomTabItem[] { mTab0, mTab1, mTab2, mTab3, mTab4 };

		mViewManager.setItems(items);
		mViewManager.setmListener(new SDViewNavigatorManagerListener()
		{
			@Override
			public void onItemClick(View v, int index)
			{
				if (index != 2)
				{
					items[2].setSelectedState(false);
				}
				switch (index)
				{
				case 0:
					click0();
					break;
				case 1:
					click1();
					break;
				case 2:
					click2();
					break;
				case 3:
					click3();
					break;
				case 4:
					click4();
					break;

				default:
					break;
				}
			}
		});

		mViewManager.setSelectIndex(0, mTab0, true);
	}

	public void setSelectIndex(int index, View view, boolean notify)
	{
		if (mViewManager != null)
		{
			mViewManager.setSelectIndex(index, view, notify);
		}
	}

	/**
	 * 团购
	 */
	protected void click0()
	{
		if (mFragHomeNew == null)
		{
			mFragHomeNew = new HomeFragment2();
			replaceFragment(mFragHomeNew, R.id.act_main_fl_tab0);
		}
		toggleFragment(mFragHomeNew);
	}

	/**
	 * 商家
	 */
	protected void click1()
	{
		if (mFragStores == null)
		{
			mFragStores = new MerchantListFragment();
			replaceFragment(mFragStores, R.id.act_main_fl_tab1);
		}
		toggleFragment(mFragStores);
	}

	/**
	 * 分享
	 */
	protected void click2()
	{
		if (mFragShare == null)
		{
			mFragShare = new ShareFragment();
			replaceFragment(mFragShare, R.id.act_main_fl_tab2);
		}
		toggleFragment(mFragShare);
	}

	/**
	 * 我的账户
	 */
	protected void click3()
	{
		if (App.getApplication().getmLocalUser() == null) // 未登录
		{
			mViewManager.setSelectIndexLast(false);
			SDActivityUtil.startActivity(MainActivity.this, LoginNewActivity.class);
		} else
		{
			if (mFragMyAccount == null)
			{
				mFragMyAccount = new MyAccountFragment();
				replaceFragment(mFragMyAccount, R.id.act_main_fl_tab3);
			}
			toggleFragment(mFragMyAccount);
		}
	}

	/**
	 * 购物车(原更多)
	 */
	protected void click4()
	{
		/*if (mFragMore == null)
		{
			mFragMore = new MoreFragment();
			replaceFragment(mFragMore, R.id.act_main_fl_tab4);
		}
		toggleFragment(mFragMore);*/
		startActivity(new Intent(this, ShopCartActivity.class));

	}

	private void toggleFragment(BaseFragment fragment)
	{
		if (mFragLast != null)
		{
			hideFragment(mFragLast);
		}
		showFragment(fragment);
		mFragLast = fragment;
	}
	

	@Override
	public void onEventMainThread(SDBaseEvent event)
	{
		super.onEventMainThread(event);
		switch (EnumEventTag.valueOf(event.getEventTagInt()))
		{
		case LOGIN_NORMAL_SUCCESS:
			mViewManager.setSelectIndex(0, mTab0, true);
			break;
		case LOGOUT:
			mFragMyAccount = null;
			mFragShare = null;
			mViewManager.setSelectIndex(0, mTab0, true);
			break;
		case LOCATION_SUCCESS:
			LogUtil.e("location success");
			break;
		case OPEN_SHARE_LIST:
			mViewManager.setSelectIndex(2, mTab2, true);
			break;
		default:
			break;
		}
	}

	@Override
	public void onBackPressed()
	{
		exitApp();
	}

	private void exitApp()
	{
		if (System.currentTimeMillis() - mExitTime > 2000)
		{
			SDToast.showToast("再按一次退出!");
		} else
		{
			App.getApplication().exitApp(true);
		}
		mExitTime = System.currentTimeMillis();
	}

	@Override
	protected void onResume()
	{
		CartGoodsArrayList cartlist = App.getApplication().getListCartGoodsModel();
		if (cartlist != null)
		{
			if (cartlist.size() > 0)
			{
				mTab4.setTitleNumber(cartlist.size() + "");
			} else
			{
				mTab4.setTitleNumber(null);
			}

		}
		
		
		super.onResume();
		JPushInterface.onResume(this);
	}
	@Override
	protected void onPause()
	{
		
		super.onPause();
		JPushInterface.onPause(this);
		
	}

}

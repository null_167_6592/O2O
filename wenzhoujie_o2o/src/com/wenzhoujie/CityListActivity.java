package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.lingou.www.R;
import com.sunday.eventbus.SDBaseEvent;
import com.wenzhoujie.adapter.CityListAdapter;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.baidumap.BaiduMapManager;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.customview.SideBar;
import com.wenzhoujie.customview.SideBar.OnTouchingLetterChangedListener;
import com.wenzhoujie.event.EnumEventTag;
import com.wenzhoujie.library.customview.ClearEditText;
import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.model.InitActCitylistModel;
import com.wenzhoujie.utils.CharacterParser;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;
import com.wenzhoujie.work.AppRuntimeWorker;

/**
 * 城市列表
 * 
 * @author js02
 * 
 */
public class CityListActivity extends BaseActivity implements OnClickListener
{

	@ViewInject(id = R.id.ll_location_city)
	private LinearLayout mLl_location_city;

	@ViewInject(id = R.id.act_city_list_et_search)
	private ClearEditText mEtSearch = null;

	@ViewInject(id = R.id.act_city_list_lv_citys)
	private ListView mLvCitys = null;

	@ViewInject(id = R.id.act_city_list_tv_touched_letter)
	private TextView mTvTouchedLetter = null;

	@ViewInject(id = R.id.act_city_list_sb_letters)
	private SideBar mSbLetters = null;

	@ViewInject(id = R.id.tv_location)
	private TextView mTv_location = null;

	private List<InitActCitylistModel> mListModel = new ArrayList<InitActCitylistModel>();
	private List<InitActCitylistModel> mListFilterModel = new ArrayList<InitActCitylistModel>();

	private CityListAdapter mAdapter = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_city_list);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		bindDefaultData();
		bindDataFromDb();
		initSlideBar();
		initCurrentLocation();
		registeEtSearchListener();
		registeClick();

	}

	private void registeEtSearchListener()
	{
		mEtSearch.addTextChangedListener(new TextWatcher()
		{

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after)
			{

			}

			@Override
			public void afterTextChanged(Editable s)
			{
				filterData(s.toString());
			}
		});
	}

	protected void filterData(String key)
	{
		mListFilterModel.clear();
		if (TextUtils.isEmpty(key))
		{
			mListFilterModel.addAll(mListModel);
		} else
		{
			for (InitActCitylistModel city : mListModel)
			{
				String name = city.getName();
				if (name.indexOf(key) != -1 || CharacterParser.getInstance().getSelling(name).startsWith(key))
				{
					mListFilterModel.add(city);
				}
			}
		}
		mAdapter.updateListViewData(mListFilterModel);
	}

	private void initSlideBar()
	{
		mSbLetters.setTextView(mTvTouchedLetter);
		mSbLetters.setOnTouchingLetterChangedListener(new CityListActivity_OnTouchingLetterChangedListener());
	}

	private void bindDataFromDb()
	{
		List<InitActCitylistModel> listDbModel = AppRuntimeWorker.getCitylist();
		if (listDbModel != null && listDbModel.size() > 0)
		{
			mListModel.addAll(listDbModel);
		} else
		{
			mListModel.clear();
		}
		mAdapter.updateListViewData(mListModel);
	}

	// private List<InitActCitylistModel> getListTestData()
	// {
	// List<InitActCitylistModel> listDbModel = new
	// ArrayList<InitActCitylistModel>();
	// listDbModel.add(getTestData("厦门"));
	// listDbModel.add(getTestData("泉州"));
	// listDbModel.add(getTestData("漳州"));
	// listDbModel.add(getTestData("南平"));
	// listDbModel.add(getTestData("宁德"));
	// listDbModel.add(getTestData("海口"));
	// listDbModel.add(getTestData("温州"));
	// return listDbModel;
	// }
	// private InitActCitylistModel getTestData(String name)
	// {
	// InitActCitylistModel model = new InitActCitylistModel();
	// model.setName(name);
	// return model;
	// }

	private void bindDefaultData()
	{
		mAdapter = new CityListAdapter(mListModel, this);
		mLvCitys.setAdapter(mAdapter);
		// mLvCitys.setOnItemClickListener(new OnItemClickListener()
		// {
		// @Override
		// public void onItemClick(AdapterView<?> parent, View view, int
		// position, long id)
		// {
		// InitActCitylistModel model = mAdapter.getItem((int) id);
		// if (model != null)
		// {
		// AppHelper.showLoadingDialog("请稍候...");
		// CommonInterface.refresh_Quanlist_cityId_cityName_ByCityName(model.getName());
		// }
		// }
		// });
	}

	private void initTitle() {
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{
			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		String title = null;
		String city = AppRuntimeWorker.getCity_name();
		if (TextUtils.isEmpty(city)) {
			title = "城市列表";

		} else {
			title = "当前城市 - " + city;
		}
		mTitleSimple.setTitleTop(title);
	}

	private void registeClick()
	{
		mLl_location_city.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (BaiduMapManager.getInstance().hasLocationSuccess())
				{
					String locationCity = mTv_location.getText().toString();
					int cityId = AppRuntimeWorker.getCityIdByCityName(locationCity);
					if (cityId < 0)
					{
						SDToast.showToast("不支持当前城市:" + locationCity);

					} else {
						AppRuntimeWorker.setCity_name(locationCity);
						finish();
					}

				} else {
					locationCity();
				}
			}
		});
	}

	private void initCurrentLocation()
	{
		if (!BaiduMapManager.getInstance().hasLocationSuccess())
		{
			locationCity();
		} else
		{
			updateLocationTextView();
		}
	}

	protected void locationCity()
	{
		mTv_location.setText("定位中");
		BaiduMapManager.getInstance().startLocation(new BDLocationListener()
		{

			@Override
			public void onReceiveLocation(BDLocation location)
			{
				updateLocationTextView();
				BaiduMapManager.getInstance().stopLocation();
			}

			@Override
			public void onReceivePoi(BDLocation arg0) {
				// TODO Auto-generated method stub

			}
		});
	}

	private void updateLocationTextView()
	{
		if (BaiduMapManager.getInstance().hasLocationSuccess())
		{
			String dist = BaiduMapManager.getInstance().getDistrictShort();
			if (AppRuntimeWorker.getCityIdByCityName(dist) > 0)
			{
				mTv_location.setText(dist);
			} else
			{
				String city = BaiduMapManager.getInstance().getCityShort();
				mTv_location.setText(city);
			}
		} else
		{
			mTv_location.setText("定位失败，点击重试");
		}
	}

	@Override
	public void onClick(View v)
	{
		// TODO Auto-generated method stub

	}

	class CityListActivity_OnTouchingLetterChangedListener implements OnTouchingLetterChangedListener
	{
		@Override
		public void onTouchingLetterChanged(String s)
		{
			int position = mAdapter.getLettersAsciisFirstPosition(s.charAt(0));
			if (position != -1)
			{
				mLvCitys.setSelection(position);
			}
		}

	}

	@Override
	public void onEventMainThread(SDBaseEvent event)
	{
		super.onEventMainThread(event);
		switch (EnumEventTag.valueOf(event.getEventTagInt()))
		{
		case REFRESH_QUANLIST_CITYID_CITYNAME_SUCCESS:
			AppHelper.hideLoadingDialog();
			finish();
			break;

		default:
			break;
		}
	}

}
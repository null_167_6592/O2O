package com.wenzhoujie.proxy;

import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import android.text.TextUtils;

import com.alibaba.fastjson.JSONException;
import com.wenzhoujie.library.utils.SDToast;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.HttpHandler;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.sunday.eventbus.SDEventManager;
import com.wenzhoujie.event.EnumEventTag;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.LogUtil;

public class RequestCallBackProxy extends RequestCallBack<String>
{
	private RequestCallBack<String> mCallBack;
	private RequestModel mRequestModel;

	public RequestCallBackProxy(RequestCallBack<String> callBack, RequestModel model)
	{
		this.mCallBack = callBack;
		this.mRequestModel = model;
	}

	@Override
	public void setmHttpHandler(HttpHandler<String> httpHandler)
	{
		if (mCallBack != null)
		{
			mCallBack.setmHttpHandler(httpHandler);
		}
	}

	@Override
	public HttpHandler<String> getmHttpHandler()
	{
		if (mCallBack != null)
		{
			return mCallBack.getmHttpHandler();
		}
		return super.getmHttpHandler();
	}

	@Override
	public void onStart()
	{
		if (mCallBack != null)
		{
			mCallBack.onStart();
		}
	}

	@Override
	public void onLoading(long total, long current, boolean isUploading)
	{
		if (mCallBack != null)
		{
			mCallBack.onLoading(total, current, isUploading);
		}
	}

	private void beforeOnSuccessBack(ResponseInfo<String> responseInfo)
	{
		String content = responseInfo.result;
		if (content != null && content.length() > 0)
		{
			// ---------------替换return为response_code
			if (content.contains("\"return\":"))
			{
				content = content.replace("\"return\":", "\"response_code\":");
			}
			// ----------------判断是否需要检查登录状态
			if (mRequestModel.ismIsNeedCheckLoginState0())
			{
				if (content.contains("\"user_login_status\":0"))
				{
					SDToast.showToast("您已经处于离线状态!");
					SDEventManager.post(EnumEventTag.LOGOUT.ordinal());
				}
			}
			// ----------------替换false为null
			if (content.contains("\":false"))
			{
				content = content.replace("\":false", "\":null");
			}

			if (mCallBack != null)
			{
				// ------------------如果Class不为空进行json解析
				if (mRequestModel.getmActClass() != null)
				{
					mCallBack.setmData(JsonUtil.json2Object(content, mRequestModel.getmActClass()));
				}
			}
		}
		responseInfo.result = content;
	}

	@Override
	public void onSuccessBack(ResponseInfo<String> responseInfo)
	{
		try
		{
			beforeOnSuccessBack(responseInfo);
			if (mCallBack != null)
			{
				mCallBack.onSuccessBack(responseInfo);
			}
		} catch (Exception e)
		{
			showErrorTipByThrowable(e.getCause());
		}
	}

	@Override
	public void onSuccess(ResponseInfo<String> responseInfo)
	{
		try
		{
			if (mCallBack != null)
			{
				mCallBack.onSuccess(responseInfo);
			}
		} catch (Exception e)
		{
			showErrorTipByThrowable(e.getCause());
		}
	}

	private void beforeOnFailure(HttpException exception, String content)
	{
		if (exception == null)
		{

			if (!TextUtils.isEmpty(content))
			{
				SDToast.showToast("错误:" + content);
			} else
			{
				SDToast.showToast("未知错误,请求失败.");
			}
		} else
		{
			showErrorTipByThrowable(exception.getCause());
		}
	}

	private void showErrorTipByThrowable(Throwable error)
	{
		if (error != null)
		{
			if (error instanceof JSONException)
			{
				SDToast.showToast("错误:" + "数据解析异常!");
			} else if (error instanceof UnknownHostException)
			{
				SDToast.showToast("错误:" + "无法访问的服务器地址!");
			} else if (error instanceof ConnectException)
			{
				SDToast.showToast("错误:" + "连接服务器失败!");
			} else if (error instanceof SocketTimeoutException)
			{
				SDToast.showToast("错误:" + "连接超时!");
			} else if (error instanceof SocketException)
			{
				SDToast.showToast("错误:" + "连接服务器失败!");
			} else
			{
				SDToast.showToast("未知错误,请求失败!");
			}
			LogUtil.e("error:" + error.toString());
		} else
		{
			SDToast.showToast("未知错误,请求失败!");
		}
	}

	@Override
	public void onFailure(HttpException error, String msg)
	{
		beforeOnFailure(error, msg);
		if (mCallBack != null)
		{
			mCallBack.onFailure(error, msg);
		}
	}

	@Override
	public void onFinish()
	{
		if (mCallBack != null)
		{
			mCallBack.onFinish();
		}
	}

	@Override
	public void onCancelled()
	{
		if (mCallBack != null)
		{
			mCallBack.onCancelled();
		}
	}

}

package com.wenzhoujie.proxy;

import org.apache.http.Header;

import android.text.TextUtils;
import android.util.Log;


import com.sunday.eventbus.SDBaseEvent;
import com.ta.sunday.proxy.ISDAsyncHttpResponseHandlerProxy;
import com.wenzhoujie.event.EventTag;
import com.wenzhoujie.library.utils.SDToast;

import de.greenrobot.event.EventBus;

public class O2oSDAsyncHttpResponseHandlerSimpleProxy implements ISDAsyncHttpResponseHandlerProxy
{

	@Override
	public Object beforeOnStartInRequestThread()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object afterOnStartInRequestThread()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object beforeOnStartInMainThread(Object result)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object afterOnStartInMainThread(Object result)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object beforeOnProgressInRequestThread(long totalSize, long currentSize, long speed)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object afterOnProgressInRequestThread(long totalSize, long currentSize, long speed)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object beforeOnProgressInMainThread(long totalSize, long currentSize, long speed, Object result)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object afterOnProgressInMainThread(long totalSize, long currentSize, long speed, Object result)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object beforeOnFailureInRequestThread(Throwable error, String content)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object afterOnFailureInRequestThread(Throwable error, String content)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object beforeOnFailureInMainThread(Throwable error, String content, Object result)
	{
		if (error == null)
		{
			if (!TextUtils.isEmpty(content))
			{
				SDToast.showToast("错误:" + content);
			} else
			{
				SDToast.showToast("网络请求失败!");
			}
		} else
		{
			SDToast.showToast("错误:" + "网络请求异常!");
			Log.e("O2oSDAsyncHttpResponseHandlerSimpleProxy", "error:" + error.toString());
		}
		return null;
	}

	@Override
	public Object afterOnFailureInMainThread(Throwable error, String content, Object result)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object beforeOnSuccessInRequestThread(int statusCode, Header[] headers, String content)
	{
		if (content != null && content.length() > 0)
		{
			if (content.contains("\"return\":"))
			{
				content = content.replace("\"return\":", "\"response_code\":");
			}
		}
		if (content.contains("\"user_login_status\":0"))
		{
			SDToast.showToast("您已经处于离线状态!");
			EventBus.getDefault().post(new SDBaseEvent(null, EventTag.EVENT_LOGOUT));
		}
		return content;
	}

	@Override
	public Object afterOnSuccessInRequestThread(int statusCode, Header[] headers, String content)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object beforeOnSuccessInMainThread(int statusCode, Header[] headers, String content, Object result)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object afterOnSuccessInMainThread(int statusCode, Header[] headers, String content, Object result)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object beforeOnFinishInRequestThread()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object afterOnFinishInRequestThread()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object beforeOnFinishInMainThread(Object result)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object afterOnFinishInMainThread(Object result)
	{
		// TODO Auto-generated method stub
		return null;
	}

}

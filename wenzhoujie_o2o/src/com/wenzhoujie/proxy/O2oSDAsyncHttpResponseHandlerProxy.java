package com.wenzhoujie.proxy;

import org.apache.http.Header;

import com.ta.sunday.http.ISDAsyncHttpResponseHandler;
import com.ta.sunday.http.impl.SDAsyncHttpResponseHandler;
import com.ta.sunday.proxy.ISDAsyncHttpResponseHandlerProxy;

public class O2oSDAsyncHttpResponseHandlerProxy extends SDAsyncHttpResponseHandler
{

	private ISDAsyncHttpResponseHandlerProxy mProxyImpl = null;
	private ISDAsyncHttpResponseHandler mHandler = null;

	public O2oSDAsyncHttpResponseHandlerProxy(ISDAsyncHttpResponseHandlerProxy proxyImpl, ISDAsyncHttpResponseHandler handler)
	{
		this.mProxyImpl = proxyImpl;
		this.mHandler = handler;
	}

	@Override
	public Object onStartInRequestThread()
	{
		Object result = null;
		if (mProxyImpl != null)
		{
			mProxyImpl.beforeOnStartInRequestThread();
		}
		if (mHandler != null)
		{
			result = mHandler.onStartInRequestThread();
		}
		if (mProxyImpl != null)
		{
			mProxyImpl.afterOnStartInRequestThread();
		}
		return result;
	}

	@Override
	public Object onProgressInRequestThread(long totalSize, long currentSize, long speed)
	{
		Object result = null;
		if (mProxyImpl != null)
		{
			mProxyImpl.beforeOnProgressInRequestThread(totalSize, currentSize, speed);
		}
		if (mHandler != null)
		{
			result = mHandler.onProgressInRequestThread(totalSize, currentSize, speed);
		}
		if (mProxyImpl != null)
		{
			mProxyImpl.afterOnProgressInRequestThread(totalSize, currentSize, speed);
		}
		return result;
	}

	@Override
	public Object onSuccessInRequestThread(int statusCode, Header[] headers, String content)
	{
		Object result = null;
		if (mProxyImpl != null)
		{
			result = mProxyImpl.beforeOnSuccessInRequestThread(statusCode, headers, content);
		}
		if (mHandler != null)
		{
			result = mHandler.onSuccessInRequestThread(statusCode, headers, String.valueOf(result));
		}
		if (mProxyImpl != null)
		{
			mProxyImpl.afterOnSuccessInRequestThread(statusCode, headers, content);
		}
		return result;
	}

	@Override
	public Object onFailureInRequestThread(Throwable error, String content)
	{
		Object result = null;
		if (mProxyImpl != null)
		{
			mProxyImpl.beforeOnFailureInRequestThread(error, content);
		}
		if (mHandler != null)
		{
			result = mHandler.onFailureInRequestThread(error, content);
		}
		if (mProxyImpl != null)
		{
			mProxyImpl.afterOnFailureInRequestThread(error, content);
		}
		return result;
	}

	@Override
	public Object onFinishInRequestThread()
	{
		Object result = null;
		if (mProxyImpl != null)
		{
			mProxyImpl.beforeOnFinishInRequestThread();
		}
		if (mHandler != null)
		{
			result = mHandler.onFinishInRequestThread();
		}
		if (mProxyImpl != null)
		{
			mProxyImpl.afterOnFinishInRequestThread();
		}
		return result;
	}

	@Override
	public void onStartInMainThread(Object result)
	{
		if (mProxyImpl != null)
		{
			mProxyImpl.beforeOnStartInMainThread(result);
		}
		if (mHandler != null)
		{
			mHandler.onStartInMainThread(result);
		}
		if (mProxyImpl != null)
		{
			mProxyImpl.afterOnStartInMainThread(result);
		}
	}

	@Override
	public void onProgressInMainThread(long totalSize, long currentSize, long speed, Object result)
	{
		if (mProxyImpl != null)
		{
			mProxyImpl.beforeOnProgressInMainThread(totalSize, currentSize, speed, result);
		}
		if (mHandler != null)
		{
			mHandler.onProgressInMainThread(totalSize, currentSize, speed, result);
		}
		if (mProxyImpl != null)
		{
			mProxyImpl.afterOnProgressInMainThread(totalSize, currentSize, speed, result);
		}
	}

	@Override
	public void onSuccessInMainThread(int statusCode, Header[] headers, String content, Object result)
	{
		if (mProxyImpl != null)
		{
			mProxyImpl.beforeOnSuccessInMainThread(statusCode, headers, content, result);
		}
		if (mHandler != null)
		{
			mHandler.onSuccessInMainThread(statusCode, headers, content, result);
		}
		if (mProxyImpl != null)
		{
			mProxyImpl.afterOnSuccessInMainThread(statusCode, headers, content, result);
		}
	}

	@Override
	public void onFailureInMainThread(Throwable error, String content, Object result)
	{
		if (mProxyImpl != null)
		{
			mProxyImpl.beforeOnFailureInMainThread(error, content, result);
		}
		if (mHandler != null)
		{
			mHandler.onFailureInMainThread(error, content, result);
		}
		if (mProxyImpl != null)
		{
			mProxyImpl.afterOnFailureInMainThread(error, content, result);
		}
	}

	@Override
	public void onFinishInMainThread(Object result)
	{
		if (mProxyImpl != null)
		{
			mProxyImpl.beforeOnFinishInMainThread(result);
		}
		if (mHandler != null)
		{
			mHandler.onFinishInMainThread(result);
		}
		if (mProxyImpl != null)
		{
			mProxyImpl.afterOnFinishInMainThread(result);
		}
	}

	@Override
	public Object onUploadProgressInRequestThread(long totalSize, long currentSize, long speed)
	{
//		Object result = null;
//		if (mHandler != null)
//		{
//			result = mHandler.onUploadProgressInRequestThread(totalSize, currentSize, speed);
//		}
//		return result;
		return null;
	}

	@Override
	public void onUploadProgressInMainThread(long totalSize, long currentSize, long speed, Object result)
	{
//		if (mHandler != null)
//		{
//			mHandler.onUploadProgressInMainThread(totalSize, currentSize, speed, result);
//		}
	}

}

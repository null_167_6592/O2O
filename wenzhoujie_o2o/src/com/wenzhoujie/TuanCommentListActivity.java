package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.sunday.eventbus.SDBaseEvent;
import com.wenzhoujie.adapter.TuanDetailCommontsAdapter;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.event.EnumEventTag;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.GoodsCommentModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.Tuan_message_listActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 商品团购评论列表
 * 
 * @author js02
 * 
 */
public class TuanCommentListActivity extends BaseActivity
{
	public static final int RESULT_CODE_COMMENT_SUCCESS = 10;

	/** 团购商品的id */
	public static final String EXTRA_TUAN_ID = "EXTRA_TUAN_ID";

	@ViewInject(id = R.id.act_tuan_comment_list_ptrlv_comments)
	private PullToRefreshListView mPtrlvComments = null;

	@ViewInject(id = R.id.act_tuan_comment_tv_buy_dp_avg)
	private TextView mTvByDpAvg = null;

	@ViewInject(id = R.id.act_tuan_comment_tv_buy_dp_count)
	private TextView mTvByDpCount = null;

	@ViewInject(id = R.id.act_tuan_comment_rb_star)
	private RatingBar mRbStar = null;

	@ViewInject(id = R.id.act_tuan_comment_pb_start5)
	private ProgressBar mPbStar5 = null;

	@ViewInject(id = R.id.act_tuan_comment_tv_start5)
	private TextView mTvStar5 = null;

	@ViewInject(id = R.id.act_tuan_comment_pb_start4)
	private ProgressBar mPbStar4 = null;

	@ViewInject(id = R.id.act_tuan_comment_tv_start4)
	private TextView mTvStar4 = null;

	@ViewInject(id = R.id.act_tuan_comment_pb_start3)
	private ProgressBar mPbStar3 = null;

	@ViewInject(id = R.id.act_tuan_comment_tv_start3)
	private TextView mTvStar3 = null;

	@ViewInject(id = R.id.act_tuan_comment_pb_start2)
	private ProgressBar mPbStar2 = null;

	@ViewInject(id = R.id.act_tuan_comment_tv_start2)
	private TextView mTvStar2 = null;

	@ViewInject(id = R.id.act_tuan_comment_pb_start1)
	private ProgressBar mPbStar1 = null;

	@ViewInject(id = R.id.act_tuan_comment_tv_start1)
	private TextView mTvStar1 = null;

	@ViewInject(id = R.id.act_tuan_comment_btn_publish)
	private Button mBtnPublish = null;

	private List<GoodsCommentModel> mListModel = new ArrayList<GoodsCommentModel>();
	private TuanDetailCommontsAdapter mAdapter = null;

	private int mCurPage = 1;
	private int mTotalPage = -1;

	private String mStrTuanId = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_tuan_comment_list);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		getIntentData(getIntent());
		initTitle();
		bindDefaultData();
		initPullToRefreshListView();
	}

	private void bindDefaultData()
	{
		mAdapter = new TuanDetailCommontsAdapter(mListModel, this);
		mPtrlvComments.setAdapter(mAdapter);
	}

	private void getIntentData(Intent intent)
	{
		String strTuanId = intent.getStringExtra(EXTRA_TUAN_ID);
		if (strTuanId != null)
		{
			this.mStrTuanId = strTuanId;
		}
	}

	@Override
	protected void onNewIntent(Intent intent)
	{
		setIntent(intent);
		init();
		super.onNewIntent(intent);
	}

	private void initPullToRefreshListView()
	{
		mPtrlvComments.setMode(Mode.BOTH);
		mPtrlvComments.setOnRefreshListener(new OnRefreshListener2<ListView>()
		{

			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				mCurPage = 1;
				requestComments(false);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				mCurPage++;
				if (mCurPage > mTotalPage && mTotalPage > 0)
				{
					SDToast.showToast("没有更多内容");
					mPtrlvComments.onRefreshComplete();
				} else
				{
					requestComments(true);
				}
			}
		});
		mPtrlvComments.setRefreshing();
	}

	protected void requestComments(final boolean isLoadMore)
	{
		if (mStrTuanId != null)
		{
			RequestModel model = new RequestModel();
			model.put("act", "tuan_message_list");
			model.put("tuan_id", mStrTuanId);
			model.put("page", mCurPage);
			model.putUser();
			RequestCallBack<String> handler = new RequestCallBack<String>()
			{

				@Override
				public void onStart()
				{
					AppHelper.showLoadingDialog("请稍候...");
				}

				@Override
				public void onSuccess(ResponseInfo<String> responseInfo)
				{
					Tuan_message_listActModel actModel = JSON.parseObject(responseInfo.result, Tuan_message_listActModel.class);
					if (!SDInterfaceUtil.isActModelNull(actModel))
					{
						if (actModel != null)
						{
							pointInfo(actModel);
						}

						if (actModel.getMessage_list() != null)
						{
							if (actModel.getMessage_list().size() <= 0)
							{
								SDToast.showToast("未找到评论");
							}
							if (actModel.getPage() != null)
							{
								mCurPage = actModel.getPage().getPage();
								mTotalPage = actModel.getPage().getPage_total();
							}
							if (!isLoadMore)
							{
								mListModel.clear();
							}
							mListModel.addAll(actModel.getMessage_list());
							mAdapter.updateListViewData(mListModel);
						} else
						{
							SDToast.showToast("暂无评论");
						}
					}
				}

				@Override
				public void onFailure(HttpException error, String msg)
				{

				}

				@Override
				public void onFinish()
				{
					AppHelper.hideLoadingDialog();
					mPtrlvComments.onRefreshComplete();
				}
			};
			InterfaceServer.getInstance().requestInterface(model, handler);
		}

	}

	/**
	 * 评分栏
	 * 
	 * @param actModel
	 */
	private void pointInfo(Tuan_message_listActModel actModel)
	{

		if (AppHelper.isLogin())
		{
			if ("1".equals(actModel.getAllow_dp()))
			{
				mBtnPublish.setVisibility(View.VISIBLE);
			} else
			{
				mBtnPublish.setVisibility(View.GONE);
				SDToast.showToast("购买后可点评");
			}
		} else
		{
			mBtnPublish.setVisibility(View.VISIBLE);
		}

		mBtnPublish.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				if (!AppHelper.isLogin())
				{
					startActivity(new Intent(TuanCommentListActivity.this, LoginNewActivity.class));
				} else
				{
					Intent i = new Intent(TuanCommentListActivity.this, AddTuanCommentActivity.class);
					i.putExtra(AddTuanCommentActivity.EXTRA_TUAN_ID, mStrTuanId);
					startActivity(i);
				}
			}
		});

		mTvByDpAvg.setText(actModel.getBuy_dp_avg());
		mTvByDpCount.setText(actModel.getMessage_count());
		mRbStar.setRating(SDTypeParseUtil.getFloatFromString(actModel.getBuy_dp_avg(), 0));

		mTvStar5.setText(actModel.getStar_5());
		mTvStar4.setText(actModel.getStar_4());
		mTvStar3.setText(actModel.getStar_3());
		mTvStar2.setText(actModel.getStar_2());
		mTvStar1.setText(actModel.getStar_1());

		int numCount = SDTypeParseUtil.getIntFromString(actModel.getMessage_count(), 0);

		if (numCount > 0)
		{
			mPbStar5.setMax(numCount);
			mPbStar5.setProgress(SDTypeParseUtil.getIntFromString(actModel.getStar_5(), 0));
			mPbStar4.setMax(numCount);
			mPbStar4.setProgress(SDTypeParseUtil.getIntFromString(actModel.getStar_4(), 0));
			mPbStar3.setMax(numCount);
			mPbStar3.setProgress(SDTypeParseUtil.getIntFromString(actModel.getStar_3(), 0));
			mPbStar2.setMax(numCount);
			mPbStar2.setProgress(SDTypeParseUtil.getIntFromString(actModel.getStar_2(), 0));
			mPbStar1.setMax(numCount);
			mPbStar1.setProgress(SDTypeParseUtil.getIntFromString(actModel.getStar_1(), 0));
		} else
		{
			mPbStar5.setProgress(0);
			mPbStar4.setProgress(0);
			mPbStar3.setProgress(0);
			mPbStar2.setProgress(0);
			mPbStar1.setProgress(0);
		}

	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("全部评论");
	}

	@Override
	public void onEventMainThread(SDBaseEvent event)
	{
		// TODO Auto-generated method stub
		super.onEventMainThread(event);
		switch (EnumEventTag.valueOf(event.getEventTagInt()))
		{
		case LOGIN_NORMAL_SUCCESS:
			mPtrlvComments.setRefreshing();
			break;

		default:
			break;
		}
	}

}
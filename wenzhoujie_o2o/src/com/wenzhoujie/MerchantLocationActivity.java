package com.wenzhoujie;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baidu.mapapi.model.LatLng;
import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.model.act.MerchantitemActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 商家地图
 * 
 * @author js02
 * 
 */
public class MerchantLocationActivity extends BaseBaiduMapActivity implements OnClickListener
{

	public static final String EXTRA_MODEL_MERCHANTITEMACTMODEL = "extra_model_merchantitemactmodel";

	@ViewInject(id = R.id.act_merchant_location_ll_bot)
	private LinearLayout mLlBot = null;

	private TextView mTvTitle = null;
	private TextView mTvContent = null;
	private Button mBtnSearch = null;

	private MerchantitemActModel mModel = null;

	private LatLng mLlEnd = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		setmTitleType(TitleType.TITLE_SIMPLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_merchant_location);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		getIntentData();
		initTitle();
		inflateViews();
		registeClick();
		bindData();
		addEndMarker();
	}

	private void addEndMarker()
	{
		if (mLlEnd != null)
		{
			addMarkerToMap(mLlEnd, getMarkerFromResource(R.drawable.ic_map_merchant));
			focusMapTo(mLlEnd, true);
		}
	}

	@Override
	public void onMapLoaded()
	{
		super.onMapLoaded();
		startLocation(false);
	}

	private void inflateViews()
	{
		getLayoutInflater().inflate(R.layout.include_map_bot_info, mLlBot);
		mLlBot.setOnClickListener(this);
		mTvTitle = (TextView) findViewById(R.id.include_map_bot_info_tv_title);
		mTvContent = (TextView) findViewById(R.id.include_map_bot_info_tv_content);
		mBtnSearch = (Button) findViewById(R.id.include_map_bot_info_btn_search);
	}

	private void bindData()
	{
		if (mModel != null)
		{
			SDViewBinder.setTextView(mTvTitle, mModel.getName());
			SDViewBinder.setTextView(mTvContent, mModel.getApi_address());

			double lat = SDTypeParseUtil.getDoubleFromString(mModel.getYpoint(), 0);
			double lon = SDTypeParseUtil.getDoubleFromString(mModel.getXpoint(), 0);
			mLlEnd = new LatLng(lat, lon);
		}
	}

	private void getIntentData()
	{
		mModel = (MerchantitemActModel) getIntent().getSerializableExtra(EXTRA_MODEL_MERCHANTITEMACTMODEL);
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
				startLocation(true);
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("商家位置");
		mTitleSimple.setRightText("当前位置");
	}

	private void registeClick()
	{
		mBtnSearch.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.include_map_bot_info_btn_search:
			clickSearch();
			break;
		case R.id.act_merchant_location_ll_bot:
			clickLlBot();
			break;
		default:
			break;
		}
	}

	private void clickLlBot()
	{
		focusMapTo(mLlEnd, true);
	}

	private void clickSearch()
	{
		if (mModel != null)
		{
			SDViewBinder.setTextView(mTvTitle, mModel.getName());
			SDViewBinder.setTextView(mTvContent, mModel.getApi_address());

			double lat = SDTypeParseUtil.getDoubleFromString(mModel.getYpoint(), 0);
			double lon = SDTypeParseUtil.getDoubleFromString(mModel.getXpoint(), 0);

			Intent intent = new Intent(getApplicationContext(), RouteInformationActivity.class);
			intent.putExtra(RouteInformationActivity.EXTRA_END_LAT, lat);
			intent.putExtra(RouteInformationActivity.EXTRA_END_LON, lon);

			intent.putExtra(RouteInformationActivity.EXTRA_END_NAME, mModel.getApi_address());
			startActivity(intent);
		}

	}
}
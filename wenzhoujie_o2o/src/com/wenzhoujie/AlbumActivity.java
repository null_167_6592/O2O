package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.wenzhoujie.library.utils.SDViewBinder;
import com.lingou.www.R;
import com.wenzhoujie.adapter.AbViewPagerAdapter;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

public class AlbumActivity extends BaseActivity
{
	/** 图片url集合 */
	public static final String EXTRA_LIST_IMAGES = "extra_list_images";
	/** 第一次进入页面的时候选中的图片位置 */
	public static final String EXTRA_IMAGES_INDEX = "extra_images_index";

	@ViewInject(id = R.id.act_album_vp_content)
	private ViewPager mVpContent = null;

	@ViewInject(id = R.id.act_album_tv_index)
	private TextView mTvIndex = null;

	@ViewInject(id = R.id.act_album_iv_back)
	private ImageView mIvBack = null;

	private AbViewPagerAdapter mAdapter = null;

	private List<String> mListUrl = null;

	/**
	 * 第一次进入页面的时候选中的图片位置
	 */
	private int mFirstSelectIndex;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		mIsNeedSlideFinishLayout = false;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_album);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		getIntentData();
		initViewPager();
		bindData();
		registerClick();
	}

	private void registerClick()
	{
		mIvBack.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				finish();
			}
		});
	}

	private void getIntentData()
	{
		mListUrl = getIntent().getStringArrayListExtra(EXTRA_LIST_IMAGES);
		mFirstSelectIndex = getIntent().getIntExtra(EXTRA_IMAGES_INDEX, 0);
	}

	private void initViewPager()
	{
		mVpContent.setOnPageChangeListener(new OnPageChangeListener()
		{

			@Override
			public void onPageSelected(int position)
			{
				setPositionData(position);
			}

			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
			{

			}

			@Override
			public void onPageScrollStateChanged(int state)
			{

			}
		});

	}

	private void setPositionData(int position)
	{
		if (AppHelper.isIndexLegalInList(mListUrl, position))
		{
			mTvIndex.setText(position + 1 + " of " + mListUrl.size());
		}
	}

	private void bindData()
	{
		if (mListUrl != null && mListUrl.size() > 0)
		{
			ArrayList<View> listViews = new ArrayList<View>();
			for (int i = 0; i < mListUrl.size(); i++)
			{
				// GestureImageView image = new
				// GestureImageView(getApplicationContext());
				ImageView image = new ImageView(getApplicationContext());
				listViews.add(image);

				String imageUrl = mListUrl.get(i);
				SDViewBinder.setImageView(image, imageUrl);
			}

			mAdapter = new AbViewPagerAdapter(getApplicationContext(), listViews);
			mVpContent.setAdapter(mAdapter);
			setSelectState();
		}
	}

	private void setSelectState()
	{
		if (mVpContent != null && mAdapter != null)
		{
			if (mFirstSelectIndex >= 0 && mAdapter.getCount() > mFirstSelectIndex)
			{
				mVpContent.setCurrentItem(mFirstSelectIndex);
				setPositionData(mFirstSelectIndex);
			}
		}
	}

}
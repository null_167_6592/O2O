package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.adapter.CateListAdapter;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.CateListActItemModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.SearchConditionModel;
import com.wenzhoujie.model.act.CateListActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 分类列表
 * 
 * @author js02
 * 
 */
public class CateListActivity extends BaseActivity
{

	/** cate_type */
	public static String EXTRA_CATE_TYPE = "EXTRA_CATE_TYPE";
	/** type */
	public static String EXTRA_TYPE = "EXTRA_TYPE";
	/** pid */
	public static String EXTRA_PID = "EXTRA_PID";

	@ViewInject(id = R.id.act_cate_lv)
	private PullToRefreshListView mLv = null;

	private List<CateListActItemModel> mlistModel = new ArrayList<CateListActItemModel>();
	private CateListAdapter mAdapter = null;

	private SearchConditionModel mSearcher = new SearchConditionModel();

	private int mCurPage = 1;
	private int mTotalPage = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_cate);
		IocUtil.initInjectedView(this);
		init();

	}

	private void init()
	{
		initTitle();
		igetIntent();
		bindDefaultData();
		initPullListView();
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("分类列表");
	}

	private void bindDefaultData()
	{
		mAdapter = new CateListAdapter(mlistModel, this, mSearcher.getCate_type(), mSearcher.getType());
		mLv.setAdapter(mAdapter);
	}

	private void igetIntent()
	{
		Intent i = getIntent();
		if (i.hasExtra(EXTRA_CATE_TYPE))
		{
			mSearcher.setCate_type(i.getIntExtra(EXTRA_CATE_TYPE, 0)); // 分类类型
																		// 0:代金券,团购分类;
																		// 1:商品分类;2:活动分类
		}
		if (i.hasExtra(EXTRA_TYPE))
		{
			mSearcher.setType(i.getIntExtra(EXTRA_TYPE, 1));// 获取分类
		}
		if (i.hasExtra(EXTRA_PID))
		{
			mSearcher.setCata_type_id(i.getStringExtra(EXTRA_PID));// 获取分类
		}
	}

	private void initPullListView()
	{
		mLv.setMode(Mode.BOTH);
		mLv.setOnRefreshListener(new OnRefreshListener2<ListView>()
		{

			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				mCurPage = 1;
				requestCate(false);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				if (++mCurPage > mTotalPage && mTotalPage != -1)
				{
					SDToast.showToast("没有更多数据了");
					mLv.onRefreshComplete();
				} else
				{
					requestCate(true);
				}
			}
		});
		mLv.setRefreshing();
	}

	protected void requestCate(final boolean isLoadMore)
	{

		RequestModel model = new RequestModel();
		model.put("act", "cate_list");
		model.put("cate_type", mSearcher.getCate_type());
		model.put("pid", mSearcher.getCata_type_id());
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{

			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				CateListActModel model = JsonUtil.json2Object(responseInfo.result, CateListActModel.class);
				if (!SDInterfaceUtil.isActModelNull(model))
				{
					if (model.getPage() != null)
					{
						mCurPage = model.getPage().getPage();
						mTotalPage = model.getPage().getPage_total();
					}
					if (!isLoadMore)
					{
						mlistModel.clear();
					}
					if (model.getItem() != null)
					{
						mlistModel.addAll(model.getItem());
					} else
					{
						SDToast.showToast("没有更多数据了");
					}
					mAdapter.updateListViewData(mlistModel);
					switch (model.getResponse_code())
					{
					case 0:

						break;
					case 1:
						// if (model.getPage() != null)
						// {
						// mCurPage = model.getPage().getPage();
						// mTotalPage = model.getPage().getPage_total();
						// }
						// if (!isLoadMore)
						// {
						// mlistModel.clear();
						// }
						// if (model.getItem() != null)
						// {
						// mlistModel.addAll(model.getItem());
						// } else
						// {
						// SDToast.showToast("没有优惠了");
						// }
						// mAdapter.updateListViewData(mlistModel);
						break;
					default:
						break;
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				AppHelper.hideLoadingDialog();
				mLv.onRefreshComplete();
			}
		};

		InterfaceServer.getInstance().requestInterface(model, handler);

	}

}
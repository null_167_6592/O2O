package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.adapter.CommentListAdapter;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.CommentListActivityItemModel;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.SearchConditionModel;
import com.wenzhoujie.model.act.CommentListActivityModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

public class CommentListActivity extends BaseActivity implements OnClickListener
{
	/** merchant_id */
	public static String EXTRA_MERCHANT_ID = "EXTRA_MERCHANT_ID";
	/** yh_id */
	public static String EXTRA_YH_ID = "EXTRA_YH_ID";

	@ViewInject(id = R.id.act_comment_list_content)
	private PullToRefreshListView mContent = null;

	private List<CommentListActivityItemModel> mlistModel = new ArrayList<CommentListActivityItemModel>();
	private CommentListAdapter mAdapter = null;

	private SearchConditionModel mSearcher = new SearchConditionModel();

	private String merchant_id = null;
	private String yh_id = null;
	private int mCurPage = 1;
	private int mTotalPage = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_comment_list);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		registeClick();
		igetIntent();
		initTitle();
		bindDefaultData();
		initPullListView();

	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
				if (App.getApplication().getmLocalUser() != null)
				{
					Dialog dialog = new MyDialog(CommentListActivity.this, R.style.MyDialog);
					// 设置触摸对话框以外的地方取消对话框
					dialog.setCanceledOnTouchOutside(true);
					dialog.show();
				} else
				{
					Toast.makeText(CommentListActivity.this, "请先登录", Toast.LENGTH_SHORT).show();
					Intent intent = new Intent();
					intent.setClass(CommentListActivity.this, LoginNewActivity.class);
					startActivity(intent);
				}
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("点评列表");
		mTitleSimple.setRightText("点评");
	}

	private void bindDefaultData()
	{
		mAdapter = new CommentListAdapter(mlistModel, this);
		mContent.setAdapter(mAdapter);
	}

	private void igetIntent()
	{
		Intent i = getIntent();
		if (i.hasExtra(EXTRA_MERCHANT_ID))
		{
			merchant_id = i.getStringExtra(EXTRA_MERCHANT_ID);
		}

		if (i.hasExtra(EXTRA_YH_ID))
		{
			yh_id = i.getStringExtra(EXTRA_YH_ID);
		}

	}

	private void initPullListView()
	{
		mContent.setMode(Mode.BOTH);
		mContent.setOnRefreshListener(new OnRefreshListener2<ListView>()
		{

			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				mCurPage = 1;
				requestComment(false);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				if (++mCurPage > mTotalPage && mTotalPage != -1)
				{
					SDToast.showToast("没有更多数据了");
					mContent.onRefreshComplete();
				} else
				{
					requestComment(true);
				}
			}
		});
		mContent.setRefreshing();
	}

	protected void requestComment(final boolean isLoadMore)
	{

		RequestModel model = new RequestModel();
		model.put("act", "youhui_comment_list");
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (user != null)
		{
			model.put("email", user.getUser_name());
			model.put("pwd", user.getUser_pwd());
		}
		model.put("yh_id", yh_id);
		model.put("merchant_id", merchant_id);
		model.put("content", mSearcher.getContent());
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{

			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				CommentListActivityModel model = JsonUtil.json2Object(responseInfo.result, CommentListActivityModel.class);
				if (!SDInterfaceUtil.isActModelNull(model))
				{

					switch (model.getResponse_code())
					{
					case 0:

						break;
					case 1:
						if (model.getPage() != null)
						{
							mCurPage = model.getPage().getPage();
							mTotalPage = model.getPage().getPage_total();
						}
						if (!isLoadMore)
						{
							mlistModel.clear();
						}
						if (model.getItem() != null)
						{
							mlistModel.addAll(model.getItem());
						} else
						{
							SDToast.showToast("没有更多数据了");
						}
						mAdapter.updateListViewData(mlistModel);
						break;
					default:
						break;
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				AppHelper.hideLoadingDialog();
				mContent.onRefreshComplete();
			}
		};

		InterfaceServer.getInstance().requestInterface(model, handler);

	}

	private void registeClick()
	{

	}

	@Override
	public void onClick(View arg0)
	{

	}

	/**
	 * 自定义对话框的类
	 */
	class MyDialog extends Dialog
	{

		public MyDialog(Context context, int theme)
		{
			super(context, theme);
		}

		@Override
		protected void onCreate(Bundle savedInstanceState)
		{
			super.onCreate(savedInstanceState);
			this.setContentView(R.layout.dialog);
			// 退出按钮和返回按钮
			Button submit_button, return_button;
			final EditText content_text;
			// 初始化
			submit_button = (Button) findViewById(R.id.submit_button);
			return_button = (Button) findViewById(R.id.return_button);
			content_text = (EditText) findViewById(R.id.dlg_input_content);

			// 发表
			submit_button.setOnClickListener(new View.OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					if (content_text.getText().toString() == null || "".equals(content_text.getText().toString()))
					{
						Toast.makeText(CommentListActivity.this, "点评内容为空,发布失败!", Toast.LENGTH_LONG).show();
					} else
					{
						mSearcher.setContent(content_text.getText().toString());
						mContent.setRefreshing();
						dismiss();
					}
				}
			});

			// 返回
			return_button.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					// 关闭对话框
					dismiss();
				}
			});
		}

	}

}
package com.wenzhoujie.baidumap;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.text.TextUtils;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.RouteLine;
import com.baidu.mapapi.search.core.RouteNode;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.route.DrivingRouteLine;
import com.baidu.mapapi.search.route.DrivingRouteLine.DrivingStep;
import com.baidu.mapapi.search.route.DrivingRoutePlanOption;
import com.baidu.mapapi.search.route.DrivingRouteResult;
import com.baidu.mapapi.search.route.OnGetRoutePlanResultListener;
import com.baidu.mapapi.search.route.PlanNode;
import com.baidu.mapapi.search.route.RoutePlanSearch;
import com.baidu.mapapi.search.route.TransitRouteLine;
import com.baidu.mapapi.search.route.TransitRouteLine.TransitStep;
import com.baidu.mapapi.search.route.TransitRoutePlanOption;
import com.baidu.mapapi.search.route.TransitRouteResult;
import com.baidu.mapapi.search.route.WalkingRouteLine;
import com.baidu.mapapi.search.route.WalkingRouteLine.WalkingStep;
import com.baidu.mapapi.search.route.WalkingRoutePlanOption;
import com.baidu.mapapi.search.route.WalkingRouteResult;
import com.baidu.mapapi.utils.DistanceUtil;
import com.sunday.eventbus.SDEventManager;
import com.wenzhoujie.constant.Constant;
import com.wenzhoujie.event.EnumEventTag;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.utils.LogUtil;
import com.wenzhoujie.utils.SDHandlerUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;

public class BaiduMapManager
{

	private static BaiduMapManager mManager = null;

	private Context mCtx = null;

	private boolean isInit = false;

	private LocationClient mClient = null;
	private List<BDLocationListener> mListLocationListener = new ArrayList<BDLocationListener>();

	private DefaultBDLocationListener mDdefaultBDLocationListener = new DefaultBDLocationListener();

	private BDLocation mBDLocation = null;

	private List<WalkingRouteLine> mListWalkingRouteLine;
	private List<TransitRouteLine> mListTransitRouteLine;
	private List<DrivingRouteLine> mListDrivingRouteLine;

	/** 是否查询线路失败 */
	private boolean mIsSearchFail = false;

	public List<DrivingRouteLine> getmListDrivingRouteLine()
	{
		return mListDrivingRouteLine;
	}

	public List<TransitRouteLine> getmListTransitRouteLine()
	{
		return mListTransitRouteLine;
	}

	public List<WalkingRouteLine> getmListWalkingRouteLine()
	{
		return mListWalkingRouteLine;
	}

	public String getCurAddressShort()
	{
		String address = getCurAddress();
		if (!TextUtils.isEmpty(address))
		{
			if (address.contains(Constant.EARN_SUB_CHAR))
			{
				address = address.substring(address.indexOf(Constant.EARN_SUB_CHAR) + 1);
			}
		}
		return address;
	}

	public String getCurAddress()
	{
		String address = null;
		if (mBDLocation != null)
		{
			address = mBDLocation.getAddrStr();
		}
		return address;
	}

	public String getCity()
	{
		String city = null;
		if (mBDLocation != null)
		{
			city = mBDLocation.getCity();
		}
		return city;
	}

	public String getCityShort()
	{
		String city = getCity();
		if (!TextUtils.isEmpty(city))
		{
			if (city.contains("市"))
			{
				city = city.replace("市", "");
			}
		}
		return city;
	}

	public String getDistrict()
	{
		String district = null;
		if (mBDLocation != null)
		{
			district = mBDLocation.getDistrict();
		}
		return district;
	}

	public String getDistrictShort()
	{
		String district = getDistrict();
		if (!TextUtils.isEmpty(district) && district.length() > 1)
		{
			district = district.substring(0, district.length() - 1);
		}
		return district;
	}

	/**
	 * 结束定位
	 */
	public void stopLocation()
	{
		SDHandlerUtil.runOnUiThreadDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				for (int i = 0; i < mListLocationListener.size(); i++)
				{
					unRegisterLocationListener(mListLocationListener.get(i));
				}
				mClient.stop();
				LogUtil.e("stopLocation");
			}
		}, 2000);
	}

	public BDLocation getBDLocation()
	{
		return mBDLocation;
	}

	public boolean hasLocationSuccess()
	{
		return mBDLocation != null;
	}

	public double getLatitude()
	{
		double latitude = 0;
		if (mBDLocation != null)
		{
			latitude = mBDLocation.getLatitude();
		}
		return latitude;
	}

	public double getLongitude()
	{
		double longitude = 0;
		if (mBDLocation != null)
		{
			longitude = mBDLocation.getLongitude();
		}
		return longitude;
	}

	public float getRadius()
	{
		float radius = 0;
		if (mBDLocation != null)
		{
			radius = mBDLocation.getRadius();
		}
		return radius;
	}

	public static BaiduMapManager getInstance()
	{
		if (mManager == null)
		{
			mManager = new BaiduMapManager();
		}
		return mManager;
	}

	public void init(Context context)
	{
		this.mCtx = context;
		if (!isInit)
		{
			SDKInitializer.initialize(mCtx);
			mClient = new LocationClient(mCtx);
			mClient.setLocOption(getLocationClientOption());
			mClient.registerLocationListener(mDdefaultBDLocationListener);
			isInit = true;
		}
	}

	public double getDistance(LatLng llStart, LatLng llEnd)
	{
		if (llStart != null && llEnd != null)
		{
			return DistanceUtil.getDistance(llStart, llEnd);
		} else
		{
			return 0;
		}
	}

	public double getDistanceFromMyLocation(LatLng llEnd)
	{
		return getDistance(getCurrentLatLng(), llEnd);
	}

	public double getDistanceFromMyLocation(double latitude, double longitude)
	{
		return getDistance(getCurrentLatLng(), new LatLng(latitude, longitude));
	}

	public double getDistanceFromMyLocation(String latitude, String longitude)
	{
		return getDistance(getCurrentLatLng(), new LatLng(SDTypeParseUtil.getDoubleFromString(latitude, 0),
				SDTypeParseUtil.getDoubleFromString(longitude, 0)));
	}

	public LatLng getLatLngFromRouteLineStep(RouteLine line, int index)
	{
		LatLng nodeLocation = null;
		if (line != null)
		{
			List listStep = line.getAllStep();
			if (listStep != null && listStep.size() > 0 && listStep.size() > index)
			{
				Object step = null;
				if (index < 0)
				{
					step = listStep.get(listStep.size() - 1);
				} else
				{
					step = listStep.get(index);
				}

				if (step instanceof DrivingStep)
				{
					DrivingStep dStep = (DrivingStep) step;
					RouteNode node = dStep.getEntrace();
					if (node != null)
					{
						nodeLocation = node.getLocation();
					}
				} else if (step instanceof WalkingStep)
				{
					WalkingStep dStep = (WalkingStep) step;
					RouteNode node = dStep.getEntrace();
					if (node != null)
					{
						nodeLocation = node.getLocation();
					}
				} else if (step instanceof TransitStep)
				{
					TransitStep dStep = (TransitStep) step;
					RouteNode node = dStep.getEntrace();
					if (node != null)
					{
						nodeLocation = node.getLocation();
					}
				}
			}
		}
		return nodeLocation;
	}

	public String getTitleFromRouteLineStep(RouteLine line, int index)
	{
		String nodeTitle = null;
		if (line != null)
		{
			List listStep = line.getAllStep();
			if (listStep != null && listStep.size() > 0 && listStep.size() > index)
			{
				Object step = null;
				if (index < 0)
				{
					step = listStep.get(listStep.size() - 1);
				} else
				{
					step = listStep.get(index);
				}

				if (step instanceof DrivingStep)
				{
					DrivingStep dStep = (DrivingStep) step;
					nodeTitle = dStep.getInstructions();
				} else if (step instanceof WalkingStep)
				{
					WalkingStep dStep = (WalkingStep) step;
					nodeTitle = dStep.getInstructions();
				} else if (step instanceof TransitStep)
				{
					TransitStep dStep = (TransitStep) step;
					nodeTitle = dStep.getInstructions();
				}
			}
		}
		return nodeTitle;
	}

	/**
	 * 定位
	 * 
	 */
	public void startLocation(BDLocationListener listener)
	{
		registerLocationListener(listener);
		if (mClient != null && !mClient.isStarted())
		{
			mClient.start();
		}
	}

	/**
	 * 注册定位监听对象
	 * 
	 * @param listener
	 * @return
	 */
	public boolean registerLocationListener(BDLocationListener listener)
	{
		if (mClient != null && listener != null && !mListLocationListener.contains(listener))
		{
			mClient.registerLocationListener(listener);
			mListLocationListener.add(listener);
			return true;
		} else
		{
			return false;
		}
	}

	/**
	 * 取消定位监听对象
	 * 
	 * @param listener
	 */
	public void unRegisterLocationListener(BDLocationListener listener)
	{
		if (listener != null && mListLocationListener.contains(listener))
		{
			mListLocationListener.remove(listener);
			if (mClient != null)
			{
				mClient.unRegisterLocationListener(listener);
			}
		}
	}

	/**
	 * 获得默认定位配置
	 * 
	 * @return
	 */
	private LocationClientOption getLocationClientOption()
	{
		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(true);// 打开gps
		option.setCoorType("bd09ll"); // 设置坐标类型
		option.setScanSpan(5000);
		option.setAddrType("all");// 设置是否返回地理信息
		return option;
	}

	public LocationClient getLocationClient()
	{
		return mClient;
	}

	/**
	 * 结束定位
	 */
	public void destoryLocation()
	{
		for (int i = 0; i < mListLocationListener.size(); i++)
		{
			unRegisterLocationListener(mListLocationListener.get(i));
		}
		if (mClient != null && mClient.isStarted())
		{
			mClient.stop();
		}
	}

	/**
	 * 默认定位监听
	 * 
	 * @author js02
	 * 
	 */
	class DefaultBDLocationListener implements BDLocationListener
	{
		@Override
		public void onReceiveLocation(BDLocation location)
		{
			if (location != null)
			{
				if (mIsSearchFail)
				{
					mIsSearchFail = false;
					SDToast.showToast("为您定位成功，当前城市：" + location.getCity() + "您可以开始查询线路");
				}
				mBDLocation = location;
				SDEventManager.post(EnumEventTag.LOCATION_SUCCESS.ordinal());
			}
		}

		@Override
		public void onReceivePoi(BDLocation location)
		{
		}
	}

	/**
	 * 获得当前位置的LatLng
	 * 
	 * @return
	 */
	public LatLng getCurrentLatLng()
	{
		return new LatLng(getLatitude(), getLongitude());
	}

	// =================================搜索公交路线=======================start

	public void searchBusRouteLine(LatLng start, LatLng end, RoutePlanSearch search,
			final OnGetBusRoutePlanResultListener listener)
	{
		if (start != null && end != null && search != null)
		{
			if (TextUtils.isEmpty(getCity()))
			{
				mIsSearchFail = true;
				SDToast.showToast("对不起未获取到当前城市，无法查询公交线路");
				return;
			}
			search.setOnGetRoutePlanResultListener(new OnGetRoutePlanResultListener()
			{
				@Override
				public void onGetWalkingRouteResult(WalkingRouteResult arg0)
				{
				}

				@Override
				public void onGetTransitRouteResult(TransitRouteResult result)
				{
					if (result != null && result.error == SearchResult.ERRORNO.NO_ERROR)
					{
						mListTransitRouteLine = result.getRouteLines();
						if (listener != null)
						{
							listener.onResult(result, true);
						}
					} else
					{
						if (listener != null)
						{
							listener.onResult(result, false);
						}
					}
					if (listener != null)
					{
						listener.onFinish();
					}
				}

				@Override
				public void onGetDrivingRouteResult(DrivingRouteResult arg0)
				{
				}
			});
			search.transitSearch(new TransitRoutePlanOption().city(getCity()).from(PlanNode.withLocation(start))
					.to(PlanNode.withLocation(end)));
		}
	}

	// =================================搜索公交路线=======================end

	// =================================搜索驾车路线=======================start

	public void searchDrivingRouteLine(LatLng start, LatLng end, RoutePlanSearch search,
			final OnGetDrivingRoutePlanResultListener listener)
	{
		if (start != null && end != null && search != null)
		{
			search.setOnGetRoutePlanResultListener(new OnGetRoutePlanResultListener()
			{

				@Override
				public void onGetWalkingRouteResult(WalkingRouteResult arg0)
				{
				}

				@Override
				public void onGetTransitRouteResult(TransitRouteResult arg0)
				{
				}

				@Override
				public void onGetDrivingRouteResult(DrivingRouteResult result)
				{
					if (result != null && result.error == SearchResult.ERRORNO.NO_ERROR)
					{
						mListDrivingRouteLine = result.getRouteLines();
						if (listener != null)
						{
							listener.onResult(result, true);
						}
					} else
					{
						if (listener != null)
						{
							listener.onResult(result, false);
						}
					}
					if (listener != null)
					{
						listener.onFinish();
					}
				}
			});
			search.drivingSearch(new DrivingRoutePlanOption().from(PlanNode.withLocation(start)).to(
					PlanNode.withLocation(end)));
		}
	}

	// =================================搜索驾车路线=======================end

	// =================================搜索步行路线=======================start

	public void searchWalkingRouteLine(LatLng start, LatLng end, RoutePlanSearch search,
			final OnGetWalkingRoutePlanResultListener listener)
	{
		if (start != null && end != null && search != null)
		{
			search.setOnGetRoutePlanResultListener(new OnGetRoutePlanResultListener()
			{

				@Override
				public void onGetWalkingRouteResult(WalkingRouteResult result)
				{
					if (result != null && result.error == SearchResult.ERRORNO.NO_ERROR)
					{
						mListWalkingRouteLine = result.getRouteLines();
						if (listener != null)
						{
							listener.onResult(result, true);
						}
					} else
					{
						if (listener != null)
						{
							listener.onResult(result, false);
						}
					}
					if (listener != null)
					{
						listener.onFinish();
					}
				}

				@Override
				public void onGetTransitRouteResult(TransitRouteResult arg0)
				{
				}

				@Override
				public void onGetDrivingRouteResult(DrivingRouteResult arg0)
				{
				}
			});
			search.walkingSearch(new WalkingRoutePlanOption().from(PlanNode.withLocation(start)).to(
					PlanNode.withLocation(end)));
		}
	}

	// =================================搜索步行路线=======================end

	/**
	 * 搜索公交线路监听
	 * 
	 * @author js02
	 * 
	 */
	public interface OnGetBusRoutePlanResultListener
	{
		public void onResult(TransitRouteResult result, boolean success);

		public void onFinish();
	}

	/**
	 * 搜索驾车线路监听
	 * 
	 * @author js02
	 * 
	 */
	public interface OnGetDrivingRoutePlanResultListener
	{
		public void onResult(DrivingRouteResult result, boolean success);

		public void onFinish();
	}

	/**
	 * 搜索步行线路监听
	 * 
	 * @author js02
	 * 
	 */
	public interface OnGetWalkingRoutePlanResultListener
	{
		public void onResult(WalkingRouteResult result, boolean success);

		public void onFinish();
	}

}

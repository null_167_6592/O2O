package com.wenzhoujie;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lingou.www.R;
import com.wenzhoujie.constant.Constant.SearchTypeMap;
import com.wenzhoujie.fragment.NearByEventsFragment;
import com.wenzhoujie.fragment.NearByFavorableFragment;
import com.wenzhoujie.fragment.NearByMerchantFragment;
import com.wenzhoujie.fragment.NearByTuanFragment;
import com.wenzhoujie.fragment.NearByVoucherFragment;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.ViewInject;

public class NearbyDiscountActivity extends BaseActivity implements OnClickListener
{
	@ViewInject(id = R.id.act_nearby_discount_rl_back_new)
	private RelativeLayout mRlBackNew = null;

	@ViewInject(id = R.id.act_nearby_discount_iv_arrow_left)
	private ImageView mIvArrowLeft = null;

	@ViewInject(id = R.id.act_nearby_discount_tv_area)
	private TextView mTvArea = null;

	@ViewInject(id = R.id.act_nearby_discount_tv_indexText)
	private TextView mTvIndextext = null;

	@ViewInject(id = R.id.act_nearby_discount_btn_next)
	private Button mBtnNext = null;

	private NearByVoucherFragment mFragNearByVoucher;
	private NearByEventsFragment mFragNearByEvents;
	private NearByMerchantFragment mFragNearByMerchant;
	private NearByFavorableFragment mFragNearByFavorable;
	private NearByTuanFragment mFragNearByTuan;

	private View mPopView;
	private PopupWindow mPopWindow;
	private int mType = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_nearby_discount);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{

		registeClick();
		loadFrag(mType);
	}

	private void loadFrag(int mType)
	{
		switch (mType)
		{
		case 0:
			mFragNearByTuan = new NearByTuanFragment();
			replaceFragment(mFragNearByTuan, R.id.act_nearby_discount_fl_fragment_content);
			break;
		case 1:
			mFragNearByFavorable = new NearByFavorableFragment();
			replaceFragment(mFragNearByFavorable, R.id.act_nearby_discount_fl_fragment_content);
			break;
		case 2:
			mFragNearByVoucher = new NearByVoucherFragment();
			replaceFragment(mFragNearByVoucher, R.id.act_nearby_discount_fl_fragment_content);
			break;
		case 3:
			mFragNearByEvents = new NearByEventsFragment();
			replaceFragment(mFragNearByEvents, R.id.act_nearby_discount_fl_fragment_content);
			break;
		case 4:
			mFragNearByMerchant = new NearByMerchantFragment();
			replaceFragment(mFragNearByMerchant, R.id.act_nearby_discount_fl_fragment_content);
			break;

		default:
			break;
		}

	}

	private void clickPopupWindow()
	{
		mPopView = LayoutInflater.from(this).inflate(R.layout.pop_nearby_type_lv, null);
		mPopWindow = new PopupWindow(mPopView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		mPopWindow.setFocusable(true);
		ColorDrawable dw = new ColorDrawable(Color.HSVToColor(0x11, new float[] { 00, 00, 0xff }));
		mPopWindow.setBackgroundDrawable(dw);
		mPopWindow.showAsDropDown(mTvIndextext);
		ListView mLvPop = (ListView) mPopView.findViewById(R.id.pop_category_single_lv_lv);
		final String[] strs = { "附近的团购", "附近的优惠券", "附近的代金券", "附近的活动", "附近的商家" };
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.my_simple_list_item, strs);
		mLvPop.setAdapter(adapter);
		mLvPop.setOnItemClickListener(new OnItemClickListener()
		{

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
			{
				loadFrag(arg2);
				mTvIndextext.setText(strs[(int) arg3]);
				mPopWindow.dismiss();
			}
		});
	}

	private void registeClick()
	{
		mRlBackNew.setOnClickListener(this);
		mTvArea.setOnClickListener(this);
		mTvIndextext.setOnClickListener(this);
		mBtnNext.setOnClickListener(this);

	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.act_nearby_discount_rl_back_new:
			finish();
			break;

		case R.id.act_nearby_discount_tv_indexText:
			clickPopupWindow();
			break;

		case R.id.act_nearby_discount_btn_next:
			clickLocation();
			break;

		default:
			break;
		}
	}

	/**
	 * 点击定位
	 */
	private void clickLocation()
	{
		Intent intent = new Intent(NearbyDiscountActivity.this, MapSearchActivity.class);
		intent.putExtra(MapSearchActivity.EXTRA_SEARCH_TYPE, SearchTypeMap.TUAN);
		startActivity(intent);
	}

}
package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.adapter.VoucherAdapter;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.SearchConditionModel;
import com.wenzhoujie.model.VoucherActItemModel;
import com.wenzhoujie.model.act.VoucherActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 代金券列表
 * 
 * @author js02
 * 
 */
public class VoucherActivity extends BaseActivity
{
	/** key */
	public static final String EXTRA_KEY = "EXTRA_KEY";
	/** catalog_id */
	public static final String EXTRA_CATALOG_ID = "EXTRA_CATALOG_ID";
	/** merchant_id */
	public static final String EXTRA_MERCHANT_ID = "EXTRA_MERCHANT_ID";
	/** title */
	public static final String EXTRA_TITLE = "EXTRA_TITLE";

	@ViewInject(id = R.id.act_nearby_vip_lv_vip)
	private PullToRefreshListView mIpLvVip = null;

	@ViewInject(id = R.id.act_nearby_vip_iv_empty)
	private ImageView mIvEmpty = null;

	private List<VoucherActItemModel> mlistModel = new ArrayList<VoucherActItemModel>();
	private VoucherAdapter mAdapter = null;

	private SearchConditionModel mSearcher = new SearchConditionModel();

	private int mCurPage = 1;
	private int mTotalPage = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_voucher);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		bindDefaultData();
		igetIntent();
		initPullListView();

	}

	private void bindDefaultData()
	{
		mAdapter = new VoucherAdapter(mlistModel, this);
		mIpLvVip.setAdapter(mAdapter);
	}

	private void igetIntent()
	{
		Intent i = getIntent();
		if (i.hasExtra(EXTRA_KEY))
		{
			mSearcher.setKeyword(i.getStringExtra(EXTRA_KEY));
		}
		if (i.hasExtra(EXTRA_CATALOG_ID))
		{
			mSearcher.setCatalog_id(i.getStringExtra(EXTRA_CATALOG_ID));
		}
		if (i.hasExtra(EXTRA_MERCHANT_ID))
		{
			mSearcher.setMerchant_id(i.getStringExtra(EXTRA_MERCHANT_ID));
		}
		if (i.hasExtra(EXTRA_TITLE))
		{
			mTitleSimple.setTitleTop((i.getStringExtra(EXTRA_TITLE)));
		}
	}

	protected void requestNearByVip(final boolean isLoadMore)
	{

		RequestModel model = new RequestModel();
		model.put("act", "daijinlist");
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (user != null)
		{
			model.put("city_id", mSearcher.getCity_id());
			model.put("email", user.getUser_name());
			model.put("pwd", user.getUser_pwd());
		}
		model.put("keyword", mSearcher.getKeyword());
		model.put("catalog_id", mSearcher.getCatalog_id());
		model.put("merchant_id", mSearcher.getMerchant_id());
		model.put("m_latitude", mSearcher.getM_latitude());
		model.put("m_longitude", mSearcher.getM_longitude());
		model.put("page", mCurPage);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{

			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				VoucherActModel model = JsonUtil.json2Object(responseInfo.result, VoucherActModel.class);
				if (!SDInterfaceUtil.isActModelNull(model))
				{
					switch (model.getResponse_code())
					{
					case 0:

						break;
					case 1:
						if (model.getPage() != null)
						{
							mCurPage = model.getPage().getPage();
							mTotalPage = model.getPage().getPage_total();
						}
						if (!isLoadMore)
						{
							mlistModel.clear();
						}
						if (model.getItem() != null)
						{
							mlistModel.addAll(model.getItem());
						} else
						{
							SDToast.showToast("没有更多数据了");
						}
						mAdapter.updateListViewData(mlistModel);
						break;
					default:
						break;
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				AppHelper.hideLoadingDialog();
				mIpLvVip.onRefreshComplete();
				SDViewUtil.toggleEmptyMsgByList(mlistModel, mIvEmpty);
			}
		};

		InterfaceServer.getInstance().requestInterface(model, handler);

	}

	private void initPullListView()
	{
		mIpLvVip.setMode(Mode.BOTH);
		mIpLvVip.setOnRefreshListener(new OnRefreshListener2<ListView>()
		{

			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				mCurPage = 1;
				requestNearByVip(false);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				if (++mCurPage > mTotalPage && mTotalPage != -1)
				{
					SDToast.showToast("没有更多数据了");
					mIpLvVip.onRefreshComplete();
				} else
				{
					requestNearByVip(true);
				}
			}
		});
		mIpLvVip.setRefreshing();
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
				Intent intent = new Intent();
				intent.setClass(VoucherActivity.this, CateListActivity.class);
				intent.putExtra(CateListActivity.EXTRA_TYPE, 3); // 1.为商城分类
																	// 2.为优惠分类
				startActivity(intent);
				finish();
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("代金券列表");
		mTitleSimple.setRightText("分类");

	}

}
package com.wenzhoujie;

import java.util.List;

import android.view.View;

import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.sunday.eventbus.SDEventManager;
import com.wenzhoujie.adapter.RefundCouponAdapter;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.event.EnumEventTag;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.wenzhoujie.model.OrderCoupon_listModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.Uc_order_refund_couponActModel;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDCollectionUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;

/**
 * 团购申请退款
 * 
 * @author Administrator
 * 
 */
public class RefundTuanActivity extends RefundGoodsActivity {

	private RefundCouponAdapter mAdapterCoupon;

	@Override
	protected void init() {
		super.init();
	}

	@Override
	protected void requestData() {
		RequestModel model = new RequestModel();
		model.putUser();
		model.putAct("order_refund_coupon");
		model.put("item_id", mId);

		RequestCallBack<String> handler = new RequestCallBack<String>() {

			@Override
			public void onStart() {
				AppHelper.showLoadingDialog("请稍候...");
			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo) {
				Uc_order_refund_couponActModel actModel = JsonUtil.json2Object(
						responseInfo.result,
						Uc_order_refund_couponActModel.class);
				if (!SDInterfaceUtil.isActModelNull(actModel)) {
					mListModel = actModel.getListItem();
					mAdapter.setData(mListModel);
					bindData();
					bindCoupons(actModel.getCoupon_list());
				}
			}

			@Override
			public void onFailure(HttpException error, String msg) {

			}

			@Override
			public void onFinish() {
				AppHelper.hideLoadingDialog();
				mSsv_all.onRefreshComplete();
			}
		};
		InterfaceServer.getInstance().requestInterface(model, handler);
	}

	protected void bindCoupons(List<OrderCoupon_listModel> listModel) {
		if (SDCollectionUtil.isEmpty(listModel)) {
			SDViewUtil.hide(mLl_coupon);
			return;
		}

		SDViewUtil.show(mLl_coupon);
		mLl_coupon.removeAllViews();
		mAdapterCoupon = new RefundCouponAdapter(listModel,
				RefundTuanActivity.this);
		for (int i = 0; i < mAdapterCoupon.getCount(); i++) {
			View view = mAdapterCoupon.getView(i, null, null);
			mLl_coupon.addView(view);
		}
	}

	@Override
	protected void requestSubmit() {
		if (mAdapter == null) {
			return;
		}

		List<String> listIds = mAdapterCoupon.getSelectedIds();
		if (SDCollectionUtil.isEmpty(listIds)) {
			SDToast.showToast("请选择要退款的团购券");
			return;
		}

		RequestModel model = new RequestModel();
		model.putUser();
		model.putAct("order_do_refund_coupon");
		model.put("content", mStrContent);
		model.put("item_id", listIds);

		RequestCallBack<String> handler = new RequestCallBack<String>() {

			@Override
			public void onStart() {
				AppHelper.showLoadingDialog("请稍候...");
			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo) {
				Uc_order_refund_couponActModel actModel = JsonUtil.json2Object(
						responseInfo.result,
						Uc_order_refund_couponActModel.class);
				if (!SDInterfaceUtil.isActModelNull(actModel)) {
					int status = SDTypeParseUtil.getIntFromString(
							actModel.getStatus(), 0);
					if (status == 1) {
						SDEventManager.post(EnumEventTag.REFRESH_ORDER_LIST
								.ordinal());
						finish();
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg) {

			}

			@Override
			public void onFinish() {
				AppHelper.hideLoadingDialog();
			}
		};
		InterfaceServer.getInstance().requestInterface(model, handler);
	}

}

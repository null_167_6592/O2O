package com.wenzhoujie;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.wenzhoujie.library.customview.ClearEditText;
import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.NickActModel;
import com.wenzhoujie.model.act.PwdActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 修改
 * 
 * @author js02
 * 
 */
public class ModifyNicknameActivity extends BaseActivity implements OnClickListener
{

	@ViewInject(id = R.id.act_modify_nickname_tv_username)
	private TextView mTvUsername = null;

	@ViewInject(id = R.id.act_modify_nickname_et_new_nickname)
	private ClearEditText mEtNewNickname = null;

	@ViewInject(id = R.id.act_modify_nickname_tv_submit)
	private TextView mTvSubmit = null;

	private String mStrNewNickname = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_modify_nickname);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		registeClick();

	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("修改昵称");
	}

	private void registeClick()
	{
		mTvSubmit.setOnClickListener(this);

	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.act_modify_nickname_tv_submit:
			clickSubmit();
			break;

		default:
			break;
		}
	}

	private void clickSubmit()
	{
		if (validateParams())
		{
			// TODO 请求更改密码接口
			final LocalUserModel user = App.getApplication().getmLocalUser();
			if (user != null)
			{
				RequestModel model = new RequestModel(false);
				model.put("act", "nick_name");
				model.put("email", user.getUser_email());
				model.put("nick_name", mStrNewNickname);
				model.put("pwd", user.getUser_pwd());
				model.put("user_name", user.getUser_name());
				RequestCallBack<String> handler = new RequestCallBack<String>()
				{

					@Override
					public void onStart()
					{
						AppHelper.showLoadingDialog("请稍候...");
					}

					@Override
					public void onSuccess(ResponseInfo<String> responseInfo)
					{
						NickActModel actModel = JsonUtil.json2Object(responseInfo.result, NickActModel.class);
						if (!SDInterfaceUtil.isActModelNull(actModel))
						{
							if (actModel.getResponse_code() == 1)
							{
								user.setNick_name(mStrNewNickname);
								App.getApplication().setmLocalUser(user);
								finish();
							} else
							{

							}
						}
					}

					@Override
					public void onFailure(HttpException error, String msg)
					{

					}

					@Override
					public void onFinish()
					{
						AppHelper.hideLoadingDialog();
					}
				};
				InterfaceServer.getInstance().requestInterface(model, handler);
			} else
			{

			}

		}

	}

	private boolean validateParams()
	{
		mStrNewNickname = mEtNewNickname.getText().toString().trim();
		if (TextUtils.isEmpty(mStrNewNickname))
		{
			SDToast.showToast("昵称不能为空");
			mEtNewNickname.requestFocus();
			return false;
		}
		return true;
	}

}
package com.wenzhoujie;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.BaseActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDActivityUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 添加商品团购评论
 * 
 * @author js02
 * 
 */
public class AddTuanCommentActivity extends BaseActivity implements OnClickListener
{
	/** 团购商品的id */
	public static final String EXTRA_TUAN_ID = "EXTRA_TUAN_ID";

	@ViewInject(id = R.id.act_tuan_add_deal_dp_rb_star)
	private RatingBar mRbStar = null;

	@ViewInject(id = R.id.act_tuan_add_deal_dp_tv_point)
	private TextView mTvPoint = null;

	@ViewInject(id = R.id.act_tuan_add_deal_dp_et_content)
	private EditText mEtContent = null;

	@ViewInject(id = R.id.act_tuan_add_deal_dp_btn_publish)
	private Button mBtnPublish = null;

	private float point = 5.0f;

	private String mStrTuanId = null;

	private String mStrContent = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_add_tuan_comment);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		getIntentData();
		bindViewState();
		registeClick();
		initTitle();
	}

	private void bindViewState()
	{
		mRbStar.setRating(point);
		mTvPoint.setText(point + "分");
	}

	private void getIntentData()
	{
		Intent intent = getIntent();
		String strTuanId = intent.getStringExtra(EXTRA_TUAN_ID);
		if (strTuanId != null)
		{
			this.mStrTuanId = strTuanId;
		}
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("我要评论");
	}

	private boolean validateParam()
	{
		mStrContent = mEtContent.getText().toString();
		if (TextUtils.isEmpty(mStrContent))
		{
			SDToast.showToast("评论内容不能为空");
			return false;
		}

		return true;
	}

	protected void requestComments()
	{
		if (!validateParam())
		{
			return;
		}
		if (AppHelper.isLogin())
		{
			RequestModel model = new RequestModel();
			model.put("act", "add_deal_dp");
			model.put("content", mEtContent.getText().toString());
			model.put("point", point);
			model.put("id", mStrTuanId);
			model.putUser();
			RequestCallBack<String> handler = new RequestCallBack<String>()
			{

				@Override
				public void onStart()
				{
					AppHelper.showLoadingDialog("请稍候...");
				}

				@Override
				public void onSuccess(ResponseInfo<String> responseInfo)
				{
					BaseActModel actModel = JsonUtil.json2Object(responseInfo.result, BaseActModel.class);
					if (!SDInterfaceUtil.isActModelNull(actModel))
					{

						int status = SDTypeParseUtil.getIntFromString(actModel.getStatus(), 0);
						if (status == 1)
						{
							App.getApplication().mRuntimeConfig.setNeedRefreshTuanDetail(true);
							Intent intent = new Intent(AddTuanCommentActivity.this, TuanCommentListActivity.class);
							intent.putExtra(TuanCommentListActivity.EXTRA_TUAN_ID, mStrTuanId);
							startActivity(intent);
							finish();
						} else
						{
							SDToast.showToast("发表评论失败！");
						}
					}
				}

				@Override
				public void onFailure(HttpException error, String msg)
				{

				}

				@Override
				public void onFinish()
				{
					AppHelper.hideLoadingDialog();
				}
			};
			InterfaceServer.getInstance().requestInterface(model, handler);
		} else
		{
			SDActivityUtil.startActivity(AddTuanCommentActivity.this, LoginNewActivity.class);
		}

	}

	private void registeClick()
	{
		mBtnPublish.setOnClickListener(this);
		mRbStar.setOnRatingBarChangeListener(listener);
	}

	public RatingBar.OnRatingBarChangeListener listener = new OnRatingBarChangeListener()
	{

		@Override
		public void onRatingChanged(RatingBar view, float rating, boolean arg2)
		{
			if (rating <= 1)
			{
				rating = 1.0f;
			}
			point = rating;
			view.setRating(point);
			mTvPoint.setText(point + "分");
		}
	};

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.act_tuan_add_deal_dp_btn_publish:
			requestComments();
			break;

		default:
			break;
		}
	}

}
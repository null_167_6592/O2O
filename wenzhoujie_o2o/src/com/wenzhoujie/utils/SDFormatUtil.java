package com.wenzhoujie.utils;

import java.text.NumberFormat;

import android.text.TextUtils;

public class SDFormatUtil
{

	public static String formatMoneyChina(String money)
	{
		if (!TextUtils.isEmpty(money))
		{
			return "￥" + money;
		} else
		{
			return null;
		}
	}

	public static String formatNumberString(String formatString, int number)
	{
		if (formatString != null && formatString.length() > 0)
		{
			NumberFormat format = NumberFormat.getNumberInstance();
			format.setMaximumFractionDigits(number);
			try
			{
				return format.format(Double.valueOf(formatString));
			} catch (Exception e)
			{
				return null;
			}

		} else
		{
			return formatString;
		}
	}

	public static String formatNumberDouble(double formatDouble, int number)
	{
		return formatNumberString(String.valueOf(formatDouble), number);
	}

	public static String formatDuring(long mss)
	{
		long days = getDuringDay(mss);
		long hours = getDuringHours(mss);
		long minutes = getDuringMinutes(mss);
		long seconds = getDuringSeconds(mss);
		StringBuilder sb = new StringBuilder();
		if (days > 0)
		{
			sb.append(days + "天");
		}
		if (hours > 0)
		{
			sb.append(hours + "小时");
		}
		if (minutes > 0)
		{
			sb.append(minutes + "分钟");
		}
		return sb.toString();
	}

	public static long getDuringDay(long mss)
	{
		return mss / getDaysMilliseconds();
	}

	public static long getDuringHours(long mss)
	{
		return (mss % (getDaysMilliseconds())) / (getHoursMilliseconds());
	}

	public static long getDuringMinutes(long mss)
	{
		return (mss % (getHoursMilliseconds())) / (getMinutesMilliseconds());
	}

	public static long getDuringSeconds(long mss)
	{
		return (mss % (getMinutesMilliseconds())) / getSecondsMilliseconds();
	}

	public static long getDaysMilliseconds()
	{
		return (1000 * 60 * 60 * 24);
	}

	public static long getHoursMilliseconds()
	{
		return (1000 * 60 * 60);
	}

	public static long getMinutesMilliseconds()
	{
		return (1000 * 60);
	}

	public static long getSecondsMilliseconds()
	{
		return (1000);
	}
}

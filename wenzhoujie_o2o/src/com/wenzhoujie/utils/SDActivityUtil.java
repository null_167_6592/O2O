package com.wenzhoujie.utils;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import com.lingou.www.R;

public class SDActivityUtil
{

	public static void startBrowserActivity(Activity oldActivity, String url)
	{
		Intent intent = new Intent();
		intent.setAction("android.intent.action.VIEW");
		Uri uri = Uri.parse(url);
		intent.setData(uri);
		oldActivity.startActivity(intent);
	}

	public static void startActivity(Activity oldActivity, Class<?> targetClazz)
	{
		Intent intent = new Intent();
		intent.setClass(oldActivity, targetClazz);
		startActivity(oldActivity, intent);
	}

	public static void startActivity(Activity oldActivity, Intent intent)
	{
		oldActivity.startActivity(intent);
	}

	public static void startActivityForResult(Activity oldActivity, Class<?> targetClazz, int requestCode)
	{
		Intent intent = new Intent();
		intent.setClass(oldActivity, targetClazz);
		startActivityForResult(oldActivity, intent, requestCode);
	}

	public static void startActivityForResult(Activity oldActivity, Intent intent, int requestCode)
	{
		oldActivity.startActivityForResult(intent, requestCode);
	}

	public static void startActivityAnimation(Activity oldActivity, int enterAnim, int exitAnim)
	{
		oldActivity.overridePendingTransition(enterAnim, exitAnim);
	}

	public static void startActivityAnimationDefault(Activity oldActivity)
	{
		startActivityAnimation(oldActivity, R.anim.anim_slide_in_from_right, R.anim.anim_slide_out_to_top);
	}

}

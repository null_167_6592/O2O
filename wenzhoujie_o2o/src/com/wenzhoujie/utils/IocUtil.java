package com.wenzhoujie.utils;

import java.lang.reflect.Field;

import android.app.Activity;
import android.view.View;

public class IocUtil
{

	public static void initInjectedView(Activity activity)
	{
		initInjectedView(activity, activity.getWindow().getDecorView());
	}

	public static void initInjectedView(Object injectedSource, View sourceView)
	{
		Field[] fields = injectedSource.getClass().getDeclaredFields();
		if (fields != null && fields.length > 0)
		{
			for (Field field : fields)
			{
				try
				{
					field.setAccessible(true);
					if (field.get(injectedSource) != null)
						continue;

					ViewInject viewInject = field.getAnnotation(ViewInject.class);
					if (viewInject != null)
					{
						int viewId = viewInject.id();
						//System.out.println("viewId="+viewId);
						field.set(injectedSource, sourceView.findViewById(viewId));
					}
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
	}

}

package com.wenzhoujie.utils;

import java.util.ArrayList;
import java.util.List;

public class SDCollectionUtil
{
	public static boolean isEmpty(List<?> list)
	{
		if (list != null && !list.isEmpty())
		{
			return false;
		} else
		{
			return true;
		}
	}
	/**
	 * 如果list有数据返回true
	 * 
	 * @param list
	 * @return
	 */
	public static boolean isListHasData(List list)
	{
		if (list != null && !list.isEmpty())
		{
			return true;
		} else
		{
			return false;
		}
	}

	public static <T> List<T> subListToSize(List<T> listModel, int size)
	{
		List<T> listReturn = null;
		if (listModel != null && listModel.size() >= size && size >= 0)
		{
			listReturn = new ArrayList<T>();
			for (int i = 0; i < size; i++)
			{
				listReturn.add(listModel.get(i));
			}
		}
		return listReturn;
	}

}

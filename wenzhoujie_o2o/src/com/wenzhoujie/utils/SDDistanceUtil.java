package com.wenzhoujie.utils;

import android.text.TextUtils;

public class SDDistanceUtil
{

	public static String getFormatDistance(double distance)
	{
		if (distance < 500)
		{
			return "<500m";
		} else if (distance <= 5000)
		{
			return SDNumberUtil.round(distance / 1000, 1) + "km";
		} else
		{
			return ">5km";
		}
	}

	public static String getKmDistanceString(int distance)
	{
		if (distance > 0)
		{
			double doubleDistance = ((double) distance) / 1000;
			String strKmDistance = SDFormatUtil.formatNumberDouble(doubleDistance, 1);
			if (!TextUtils.isEmpty(strKmDistance))
			{
				return strKmDistance + "km";
			}
		}
		return "0.0km";
	}
}

package com.wenzhoujie.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.huewu.pla.lib.MultiColumnListView;
import com.lingou.www.R;

public class SDMultiColumnListView extends PullToRefreshBase<MultiColumnListView>
{

	public SDMultiColumnListView(Context context)
	{
		super(context);
	}

	public SDMultiColumnListView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public SDMultiColumnListView(Context context, com.handmark.pulltorefresh.library.PullToRefreshBase.Mode mode,
			com.handmark.pulltorefresh.library.PullToRefreshBase.AnimationStyle animStyle)
	{
		super(context, mode, animStyle);
	}

	public SDMultiColumnListView(Context context, com.handmark.pulltorefresh.library.PullToRefreshBase.Mode mode)
	{
		super(context, mode);
	}

	@Override
	public com.handmark.pulltorefresh.library.PullToRefreshBase.Orientation getPullToRefreshScrollDirection()
	{
		return Orientation.VERTICAL;
	}

	@Override
	protected MultiColumnListView createRefreshableView(Context context, AttributeSet attrs)
	{
		MultiColumnListView mclv = new MultiColumnListView(context, attrs);
		mclv.setId(R.id.multicolumnlistview);
		return mclv;
	}

	@Override
	protected boolean isReadyForPullEnd()
	{
		MultiColumnListView view = getRefreshableView();
		View lastChild = view.getChildAt(view.getChildCount() - 1);
		if (lastChild != null)
		{
			return lastChild.getBottom() <= view.getHeight();
		} else
		{
			return false;
		}
	}

	@Override
	protected boolean isReadyForPullStart()
	{
		MultiColumnListView view = getRefreshableView();
		View childView = view.getChildAt(0);
		if (childView != null)
		{
			return childView.getTop() == 0;
		} else
		{
			if (view.getFirstVisiblePosition() > 0)
			{
				return false;
			} else
			{
				return true;
			}
		}
	}
}

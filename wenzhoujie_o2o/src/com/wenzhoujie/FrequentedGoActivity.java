package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.adapter.FrequentedGoAdapter;
import com.wenzhoujie.adapter.FrequentedGoAdapter.SubscribeMessageAdapterListener;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.UserfrequentedlistActItemModel;
import com.wenzhoujie.model.act.UserfrequentedlistActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 常去地点
 * 
 * @author js02
 * 
 */
public class FrequentedGoActivity extends BaseActivity
{

	@ViewInject(id = R.id.act_frequented_go_ptrlv_content)
	private PullToRefreshListView mPtrlvContent = null;

	@ViewInject(id = R.id.act_frequented_go_iv_empty)
	private ImageView mIvEmpty = null;

	private FrequentedGoAdapter mAdapter = null;

	private List<UserfrequentedlistActItemModel> mListModel = new ArrayList<UserfrequentedlistActItemModel>();

	private String mDeleteId = "0";

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_frequented_go);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		bindDefaultData();
		initPullToRefreshListView();
	}

	private void bindDefaultData()
	{
		mAdapter = new FrequentedGoAdapter(mListModel, this, new SubscribeMessageAdapterListener()
		{
			@Override
			public void onDeleteClick(UserfrequentedlistActItemModel model)
			{
				if (model != null && model.getId() != null)
				{
					mDeleteId = model.getId();
					requestUserFrequentedList();
				}

			}
		});
		mPtrlvContent.setAdapter(mAdapter);
	}

	private void initPullToRefreshListView()
	{
		mPtrlvContent.setMode(Mode.PULL_FROM_START);
		mPtrlvContent.setOnRefreshListener(new OnRefreshListener2<ListView>()
		{
			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				requestUserFrequentedList();
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
			{
			}
		});

		mPtrlvContent.setRefreshing();
	}

	private void requestUserFrequentedList()
	{
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (user != null)
		{
			RequestModel model = new RequestModel();
			model.put("act", "userfrequentedlist");
			model.put("email", user.getUser_name());
			model.put("pwd", user.getUser_pwd());
			model.put("del_id", mDeleteId);
			RequestCallBack<String> handler = new RequestCallBack<String>()
			{

				@Override
				public void onStart()
				{
					AppHelper.showLoadingDialog("请稍候...");
				}

				@Override
				public void onSuccess(ResponseInfo<String> responseInfo)
				{
					UserfrequentedlistActModel actModel = JsonUtil.json2Object(responseInfo.result, UserfrequentedlistActModel.class);
					if (!SDInterfaceUtil.isActModelNull(actModel))
					{
						if (actModel.getResponse_code() == 1)
						{
							if (actModel.getItem() != null && actModel.getItem().size() > 0)
							{
								mListModel.clear();
								mListModel.addAll(actModel.getItem());
							} else
							{
								mListModel.clear();
								SDToast.showToast("未找到数据");
							}
							mAdapter.updateListViewData(mListModel);
						}
					}
				}

				@Override
				public void onFailure(HttpException error, String msg)
				{

				}

				@Override
				public void onFinish()
				{
					AppHelper.hideLoadingDialog();
					mPtrlvContent.onRefreshComplete();
					SDViewUtil.toggleEmptyMsgByList(mListModel, mIvEmpty);
				}
			};
			InterfaceServer.getInstance().requestInterface(model, handler);
		}

	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
				startActivity(new Intent(getApplicationContext(), NearbyDiscountActivity.class));
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("常去地点");
		mTitleSimple.setRightText("附近");
	}

}
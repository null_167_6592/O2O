package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;

import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.adapter.CollectionTuanGoodsAdapter;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.TuanGoodsModel;
import com.wenzhoujie.model.act.Collect_listActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 收藏
 * 
 * @author js02
 * 
 */
public class CollectionTuanGoodsActivity extends BaseActivity
{

	@ViewInject(id = R.id.act_collection_tuan_goods_ptrlv_collection)
	private PullToRefreshListView mPtrlvCollection = null;

	@ViewInject(id = R.id.act_collection_tuan_goods_iv_empty)
	private ImageView mIvEmpty = null;

	private CollectionTuanGoodsAdapter mAdapter = null;
	private List<TuanGoodsModel> mListModel = new ArrayList<TuanGoodsModel>();

	private int mPage = 1;
	private int mPageTotal = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_collection_tuan_goods);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		bindDefaultData();
		initPullToRefreshListView();
	}

	private void bindDefaultData()
	{
		mAdapter = new CollectionTuanGoodsAdapter(mListModel, this);
		mPtrlvCollection.setAdapter(mAdapter);
	}

	private void initPullToRefreshListView()
	{
		mPtrlvCollection.setMode(Mode.BOTH);
		mPtrlvCollection.setOnRefreshListener(new OnRefreshListener2<ListView>()
		{
			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				refreshData();
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				loadMoreData();
			}
		});
		mPtrlvCollection.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				if (mAdapter != null)
				{
					TuanGoodsModel model = mAdapter.getItem((int) id);
					if (model != null)
					{
						if (!TextUtils.isEmpty(model.getDeal_id()))
						{
							Intent intent = new Intent(getApplicationContext(), TuanDetailActivity.class);
							intent.putExtra(TuanDetailActivity.EXTRA_GOODS_ID, model.getDeal_id());
							startActivity(intent);
						} else
						{
							SDToast.showToast("商品ID为null！");
						}
					}
				}

			}

		});
		mPtrlvCollection.setRefreshing();
	}

	protected void refreshData()
	{
		mPage = 1;
		requestSubscribe(false);

	}

	protected void loadMoreData()
	{
		mPage++;
		if (mPage > mPageTotal && mPageTotal != 0)
		{
			SDToast.showToast("没有更多数据了");
			mPtrlvCollection.onRefreshComplete();
		} else
		{
			requestSubscribe(true);
		}

	}

	private void requestSubscribe(final boolean isLoadMore)
	{
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (user != null)
		{
			RequestModel model = new RequestModel();
			model.put("act", "collect_list");
			model.put("page", mPage);
			model.put("email", user.getUser_name());
			model.put("pwd", user.getUser_pwd());
			RequestCallBack<String> handler = new RequestCallBack<String>()
			{

				@Override
				public void onStart()
				{
					AppHelper.showLoadingDialog("请稍候...");
				}

				@Override
				public void onSuccess(ResponseInfo<String> responseInfo)
				{
					Collect_listActModel actModel = JsonUtil.json2Object(responseInfo.result, Collect_listActModel.class);
					if (!SDInterfaceUtil.isActModelNull(actModel))
					{
						if (actModel.getResponse_code() == 1)
						{
							if (actModel.getPage() != null)
							{
								mPage = actModel.getPage().getPage();
								mPageTotal = actModel.getPage().getPage_total();
							}

							if (actModel.getCollect_list() != null && actModel.getCollect_list().size() > 0)
							{
								if (!isLoadMore)
								{
									mListModel.clear();
								}
								mListModel.addAll(actModel.getCollect_list());
							} else
							{
								mListModel.clear();
								SDToast.showToast("未找到数据");
							}
							mAdapter.updateListViewData(mListModel);

						}
					}
				}

				@Override
				public void onFailure(HttpException error, String msg)
				{

				}

				@Override
				public void onFinish()
				{
					AppHelper.hideLoadingDialog();
					mPtrlvCollection.onRefreshComplete();
					SDViewUtil.toggleEmptyMsgByList(mListModel, mIvEmpty);
				}
			};
			InterfaceServer.getInstance().requestInterface(model, handler);
		}

	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("收藏");
	}

}
package com.wenzhoujie;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.wenzhoujie.library.customview.ClearEditText;
import com.wenzhoujie.library.dialog.SDDialogBase;
import com.wenzhoujie.library.title.SDTitleSimple.SDTitleSimpleListener;
import com.wenzhoujie.library.utils.SDToast;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.wenzhoujie.library.utils.SDViewUtil;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lingou.www.R;
import com.wenzhoujie.adapter.MerchantListOrderAdapter;
import com.wenzhoujie.app.App;
import com.wenzhoujie.app.AppHelper;
import com.wenzhoujie.constant.Constant.TitleType;
import com.wenzhoujie.http.InterfaceServer;
import com.wenzhoujie.model.LocalUserModel;
import com.wenzhoujie.model.MerchantlistActItemModel;
import com.wenzhoujie.model.RequestModel;
import com.wenzhoujie.model.act.Auto_orderActModel;
import com.wenzhoujie.model.act.MerchantlistActModel;
import com.wenzhoujie.model.act.Wap_qrcodeActModel;
import com.wenzhoujie.utils.IocUtil;
import com.wenzhoujie.utils.JsonUtil;
import com.wenzhoujie.utils.SDInterfaceUtil;
import com.wenzhoujie.utils.SDTypeParseUtil;
import com.wenzhoujie.utils.ViewInject;

/**
 * 商家列表界面（可以自主下单的商家列表）
 * 
 * @author js02
 * 
 */
public class MerchantListOrderActivity extends BaseActivity implements OnClickListener
{

	public static final int REQUEST_CODE_REQUEST_SCAN = 1;

	@ViewInject(id = R.id.act_merchant_list_order_prlv_stores)
	private PullToRefreshListView mPrlvStores = null;

	@ViewInject(id = R.id.act_merchant_list_order_ll_empty)
	private LinearLayout mLlEmpty = null;

	@ViewInject(id = R.id.act_merchant_list_order_tv_search)
	private TextView mTvSearch = null;

	@ViewInject(id = R.id.act_merchant_list_order_et_search)
	private ClearEditText mEtSearch = null;

	private MerchantListOrderAdapter mAdapter = null;
	private List<MerchantlistActItemModel> mListMerchantModel = new ArrayList<MerchantlistActItemModel>();

	private int mPage = 1;
	private int mTotalPage = 0;

	private String mStrKeyword = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setmTitleType(TitleType.TITLE_SIMPLE);
		setContentView(R.layout.act_merchant_list_order);
		IocUtil.initInjectedView(this);
		init();
	}

	private void init()
	{
		initTitle();
		registeClick();
		bindDefaultData();
		initPullToRefreshListView();
	}

	private void bindDefaultData()
	{
		mAdapter = new MerchantListOrderAdapter(mListMerchantModel, this);
		mPrlvStores.setAdapter(mAdapter);
	}

	private void initTitle()
	{
		mTitleSimple.setmListener(new SDTitleSimpleListener()
		{

			@Override
			public void onRightButtonCLick_SDTitleSimple(View v)
			{
				if (App.getApplication().getmLocalUser() == null)
				{
					startActivity(new Intent(getApplicationContext(), LoginNewActivity.class));
				} else
				{
					Intent intent = new Intent(getApplicationContext(), MyCaptureActivity.class);
					intent.putExtra(MyCaptureActivity.EXTRA_IS_FINISH_ACTIVITY, 1);
					startActivityForResult(intent, REQUEST_CODE_REQUEST_SCAN);
				}
			}

			@Override
			public void onLeftButtonCLick_SDTitleSimple(View v)
			{
				finish();
			}
		});
		mTitleSimple.setTitleTop("商家");
		mTitleSimple.setRightImage(R.drawable.ic_scan_code);
	}

	private void registeClick()
	{
		mTvSearch.setOnClickListener(this);
	}

	private void initPullToRefreshListView()
	{
		mPrlvStores.setMode(Mode.BOTH);
		mPrlvStores.setOnRefreshListener(new OnRefreshListener2<ListView>()
		{

			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				refreshData();
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
			{
				loadMoreData();
			}

		});
		mPrlvStores.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				MerchantlistActItemModel model = mAdapter.getItem((int) id);
				if (model != null && !TextUtils.isEmpty(model.getId()))
				{
					Intent itemintent = new Intent();
					if (model.getShop_type()==5){
						//超市
						itemintent.putExtra(com.wenzhoujie.SuperMarketDetailActivity.EXTRA_MERCHANT_ID, model.getId());
						itemintent.putExtra(com.wenzhoujie.SuperMarketDetailActivity.EXTRA_MERCHANT_TITLE, model.getName());
						//itemintent.putExtra(com.wenzhoujie.SuperMarketDetailActivity.EXTRA_MERCHANT_SUPPLYER_ID, model.get);
						itemintent.setClass(MerchantListOrderActivity.this, com.wenzhoujie.SuperMarketDetailActivity.class);
					}else{
						itemintent.putExtra(com.wenzhoujie.MerchantDetailNewActivity.EXTRA_MERCHANT_ID, model.getId());
						itemintent.setClass(MerchantListOrderActivity.this, com.wenzhoujie.MerchantDetailNewActivity.class);
					}
					startActivity(itemintent);
				}

			}
		});
		mPrlvStores.setRefreshing();
	}

	protected void refreshData()
	{
		mPage = 1;
		requestMerchantList(false);
	}

	protected void loadMoreData()
	{
		if (++mPage > mTotalPage && mTotalPage > 0)
		{
			mPrlvStores.onRefreshComplete();
			SDToast.showToast("没有更多数据了");
		} else
		{
			requestMerchantList(true);
		}

	}

	private void requestMerchantList(final boolean isLoadMore)
	{
		RequestModel model = new RequestModel();
		model.put("act", "merchantlist");
		model.put("keyword", mStrKeyword);
		model.put("is_auto_order", 1);
		RequestCallBack<String> handler = new RequestCallBack<String>()
		{

			@Override
			public void onStart()
			{
				AppHelper.showLoadingDialog("请稍候...");
			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo)
			{
				MerchantlistActModel actModel = JsonUtil.json2Object(responseInfo.result, MerchantlistActModel.class);
				if (!SDInterfaceUtil.isActModelNull(actModel))
				{
					if (actModel.getResponse_code() == 1)
					{
						if (actModel.getPage() != null)
						{
							mPage = actModel.getPage().getPage();
							mTotalPage = actModel.getPage().getPage_total();
						}
						SDViewUtil.updateAdapterByList(mListMerchantModel, actModel.getItem(), mAdapter, isLoadMore);
					}
				}
			}

			@Override
			public void onFailure(HttpException error, String msg)
			{

			}

			@Override
			public void onFinish()
			{
				dealFinishRequest();
			}
		};
		InterfaceServer.getInstance().requestInterface(model, handler);

	}

	/**
	 * 提交二维码扫描结果，以获取商家信息
	 * 
	 * @param resultString
	 */
	private void requestMerchanMsgByCode(String resultString)
	{
		if (!TextUtils.isEmpty(resultString))
		{
			RequestModel model = new RequestModel();
			model.put("act", "wap_qrcode");
			model.put("wap_url", resultString);
			RequestCallBack<String> handler = new RequestCallBack<String>()
			{

				@Override
				public void onStart()
				{
					AppHelper.showLoadingDialog("请稍候...");
				}

				@Override
				public void onSuccess(ResponseInfo<String> responseInfo)
				{
					Wap_qrcodeActModel actModel = JsonUtil.json2Object(responseInfo.result, Wap_qrcodeActModel.class);
					if (!SDInterfaceUtil.isActModelNull(actModel))
					{
						if (actModel.getResponse_code() == 1)
						{
							int status = SDTypeParseUtil.getIntFromString(actModel.getStatus(), 0);
							if (status == 0)
							{

							} else if (status == 1)
							{
								int openType = SDTypeParseUtil.getIntFromString(actModel.getOp_type(), 0);
								switch (openType)
								{
								case 0: // 下单窗口
									showConfirmOrderDialog(actModel);
									break;
								case 1: // 商家详细页
									String merchantId = actModel.getId();
									if (!TextUtils.isEmpty(merchantId))
									{
										Intent intent = new Intent(getApplicationContext(), MerchantDetailNewActivity.class);
										intent.putExtra(MerchantDetailNewActivity.EXTRA_MERCHANT_ID, merchantId);
										startActivity(intent);
									}
									break;

								default:
									showConfirmOrderDialog(actModel);
									break;
								}
							}
						}
					}
				}

				@Override
				public void onFailure(HttpException error, String msg)
				{

				}

				@Override
				public void onFinish()
				{
					AppHelper.hideLoadingDialog();
				}
			};
			InterfaceServer.getInstance().requestInterface(model, handler);
		}
	}

	/**
	 * 显示下单窗口
	 */
	private void showConfirmOrderDialog(final Wap_qrcodeActModel model)
	{
		if (model != null)
		{
			View view = LayoutInflater.from(App.getApplication()).inflate(R.layout.dialog_merchant_submit_order, null);
			final Dialog dialog = new SDDialogBase().setDialogView(view);
			dialog.show();
			TextView tvName = (TextView) view.findViewById(R.id.dialog_merchant_submit_order_tv_name);
			TextView tvBrief = (TextView) view.findViewById(R.id.dialog_merchant_submit_order_tv_brief);
			final EditText etMoney = (EditText) view.findViewById(R.id.dialog_merchant_submit_order_et_money);
			TextView tvConfirm = (TextView) view.findViewById(R.id.dialog_merchant_submit_order_tv_confirm);
			TextView tvCancel = (TextView) view.findViewById(R.id.dialog_merchant_submit_order_tv_cancel);
			final TextView tvTip = (TextView) view.findViewById(R.id.dialog_merchant_submit_order_tv_tip);

			SDViewBinder.setTextView(tvName, model.getName());
			SDViewBinder.setTextView(tvBrief, model.getMobile_brief());
			etMoney.addTextChangedListener(new TextWatcher()
			{

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count)
				{
					// TODO Auto-generated method stub

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after)
				{
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s)
				{
					String money = s.toString();
					if (!TextUtils.isEmpty(money))
					{
						tvTip.setText("");
					}
				}
			});
			tvConfirm.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					String money = etMoney.getText().toString();
					if (TextUtils.isEmpty(money))
					{
						tvTip.setText("请输入金额");
						return;
					}
					requestAutoOrder(model.getId(), money, dialog);
				}
			});
			tvCancel.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					dialog.dismiss();
				}
			});
		}
	}

	/**
	 * 请求自主下单接口
	 */
	protected void requestAutoOrder(String id, String money, final Dialog dialog)
	{
		LocalUserModel user = App.getApplication().getmLocalUser();
		if (user != null)
		{
			RequestModel model = new RequestModel();
			model.put("act", "auto_order");
			model.put("location_id", id);
			model.put("money", money);
			model.put("email", user.getUser_name());
			model.put("pwd", user.getUser_pwd());
			RequestCallBack<String> handler = new RequestCallBack<String>()
			{

				@Override
				public void onStart()
				{
					AppHelper.showLoadingDialog("请稍候...");
				}

				@Override
				public void onSuccess(ResponseInfo<String> responseInfo)
				{
					Auto_orderActModel actModel = JsonUtil.json2Object(responseInfo.result, Auto_orderActModel.class);
					if (!SDInterfaceUtil.isActModelNull(actModel))
					{
						if (actModel.getResponse_code() == 1)
						{
							int status = SDTypeParseUtil.getIntFromString(actModel.getStatus(), 0);
							if (status == 1)
							{
								AppHelper.hideLoadingDialog();
								dialog.dismiss();
								Intent intent = new Intent(App.getApplication(), OrderDetailActivity.class);
								intent.putExtra(OrderDetailActivity.EXTRA_ORDER_ID, String.valueOf(actModel.getOrder_id()));
								startActivity(intent);
							}
						}
					}
				}

				@Override
				public void onFailure(HttpException error, String msg)
				{

				}

				@Override
				public void onFinish()
				{
					AppHelper.hideLoadingDialog();
				}
			};
			InterfaceServer.getInstance().requestInterface(model, handler);
		}
	}

	protected void dealFinishRequest()
	{
		AppHelper.hideLoadingDialog();
		mPrlvStores.onRefreshComplete();
		SDViewUtil.toggleEmptyMsgByList(mListMerchantModel, mLlEmpty);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{

		case R.id.act_merchant_list_order_tv_search:
			clickSearch();
			break;

		default:
			break;
		}
	}

	private void clickSearch()
	{
		mStrKeyword = mEtSearch.getText().toString();
		requestMerchantList(false);
	}

	@Override
	protected void onActivityResult(int request, int result, Intent data)
	{
		switch (request)
		{
		case REQUEST_CODE_REQUEST_SCAN:
			if (result == MyCaptureActivity.RESULT_CODE_SCAN_SUCCESS)
			{
				if (data != null)
				{
					String resultString = data.getStringExtra(MyCaptureActivity.EXTRA_RESULT_SUCCESS_STRING);
					requestMerchanMsgByCode(resultString);
				}
			}
			break;

		default:
			break;
		}
		super.onActivityResult(request, result, data);
	}

}
package com.wenzhoujie.wxpay;

import android.text.TextUtils;

import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.wenzhoujie.library.common.SDActivityManager;
import com.wenzhoujie.work.AppRuntimeWorker;

public class WXPayer {

	private static WXPayer mInstance;
	private String mAppId;
	private boolean mIsRegister = false;

	public static WXPayer getInstance()
	{
		if (mInstance == null)
		{
			mInstance = new WXPayer();
		}
		return mInstance;
	}

	private WXPayer()
	{
		String appId = AppRuntimeWorker.getWx_app_key();
		setAppId(appId);
	}

	public String getAppId()
	{
		return this.mAppId;
	}

	public void setAppId(String appId)
	{
		this.mAppId = appId;
		register();
	}

	public void register()
	{
		if (!mIsRegister && !TextUtils.isEmpty(mAppId))
		{
			mIsRegister = getWXAPI().registerApp(mAppId);
		}
	}

	public void pay(PayReq request)
	{
		if (request != null)
		{
			getWXAPI().sendReq(request);
		}
	}

	public IWXAPI getWXAPI()
	{
		return WXAPIFactory.createWXAPI(SDActivityManager.getInstance().getLastActivity(), mAppId);
	}

}

package com.wenzhoujie;

import android.content.Intent;

import com.sunday.eventbus.SDBaseEvent;
import com.wenzhoujie.event.EnumEventTag;
import com.wenzhoujie.fragment.AppWebViewFragment;
import com.wenzhoujie.fragment.WebViewFragment;
import com.wenzhoujie.fragment.WebViewFragment.EnumProgressMode;
import com.wenzhoujie.fragment.WebViewFragment.EnumWebviewHeightMode;

/**
 * webview界面
 * 
 * @author js02
 * 
 */
public class AppWebViewActivity extends WebViewActivity
{

	private boolean mIsStartByAdvs = false;

	@Override
	protected void onNewIntent(Intent intent)
	{
		setIntent(intent);
		init();
		super.onNewIntent(intent);
	}

	@Override
	protected void getIntentData()
	{
		mIsStartByAdvs = getIntent().getBooleanExtra(BaseActivity.EXTRA_IS_ADVS, false);
		super.getIntentData();
	}

	@Override
	protected WebViewFragment createFragment()
	{
		AppWebViewFragment fragment = new AppWebViewFragment();
		fragment.setmProgressMode(EnumProgressMode.HORIZONTAL);
		fragment.setmWebviewHeightMode(EnumWebviewHeightMode.MATCH_PARENT);
		return fragment;
	}

	@Override
	public void finish()
	{
		if (mIsStartByAdvs)
		{
			startActivity(new Intent(this, MainActivity.class));
		}
		super.finish();
	}

	@Override
	public void onEventMainThread(SDBaseEvent event)
	{
		switch (EnumEventTag.valueOf(event.getEventTagInt()))
		{
		case LOGIN_NORMAL_SUCCESS:
			if (mFragWebview != null)
			{
				mFragWebview.startLoadData();
			}
			break;

		default:
			break;
		}
		super.onEventMainThread(event);
	}

}
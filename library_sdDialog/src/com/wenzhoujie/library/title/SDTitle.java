package com.wenzhoujie.library.title;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wenzhoujie.library.R;
import com.wenzhoujie.library.SDLibrary;
import com.wenzhoujie.library.config.SDLibraryConfig;
import com.wenzhoujie.library.drawable.SDDrawable;
import com.wenzhoujie.library.drawable.SDDrawableManager;
import com.wenzhoujie.library.model.TitleItemModel;
import com.wenzhoujie.library.popupwindow.SDPWindowBase;
import com.wenzhoujie.library.title.TitleItem.EnumShowType;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.wenzhoujie.library.utils.SDViewUtil;

public class SDTitle extends LinearLayout implements OnClickListener
{
	private static final int MAX_ITEM_COUNT = 2;

	public View mView;
	public LinearLayout mLlLeft;
	public LinearLayout mLlMiddle;
	public LinearLayout mLlRight;
	private SDTitleListener mListener;
	private SDTitleCreaterListener mListenerCreater;

	public TitleItem mTlLeft;
	public TitleItem mTlMiddle;
	private List<TitleItem> mListTitleItem = new ArrayList<TitleItem>();
	private List<TitleItem> mListTitleItemNormal = new ArrayList<TitleItem>();
	private List<TitleItem> mListTitleItemMore = new ArrayList<TitleItem>();
	private List<TitleItemModel> mListTitleItemModel = new ArrayList<TitleItemModel>();
	private SDLibraryConfig mConfig = SDLibrary.getInstance().getmConfig();

	private int mIndexMore;
	private int mMaxItem;

	private SDPWindowBase mPop;
	private LinearLayout mLlMoreItem;
	private TitleItem mMoreView;

	public SDTitleCreaterListener getmListenerCreater()
	{
		return mListenerCreater;
	}

	public void setmListenerCreater(SDTitleCreaterListener mListenerCreater)
	{
		this.mListenerCreater = mListenerCreater;
	}

	public SDTitleListener getmListener()
	{
		return mListener;
	}

	public void setmListener(SDTitleListener mListener)
	{
		this.mListener = mListener;
	}

	public SDTitle(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	public SDTitle(Context context)
	{
		this(context, null);
	}

	private void init()
	{
		mView = LayoutInflater.from(getContext()).inflate(R.layout.title, null);
		setHeightTitle(LayoutParams.MATCH_PARENT);

		mLlLeft = (LinearLayout) mView.findViewById(R.id.title_ll_left);
		mLlMiddle = (LinearLayout) mView.findViewById(R.id.title_ll_middle);
		mLlRight = (LinearLayout) mView.findViewById(R.id.title_ll_right);

		setmListenerCreater(mDefaultCreater);
		createSimpleTitle();
		initPop();
		// -----------------------------
		registerClick();
		setDefaultConfig();
	}

	private void initPop()
	{
		mPop = new SDPWindowBase(false);
		mPop.setWidth(SDViewUtil.getScreenWidth() / 2);
		View pop = LayoutInflater.from(getContext()).inflate(R.layout.pop_title_more, null);
		mLlMoreItem = (LinearLayout) pop.findViewById(R.id.pop_title_more_ll_items);
		mPop.setContentView(pop);
	}

	public void removeAllItems()
	{
		mLlLeft.removeAllViews();
		mLlMiddle.removeAllViews();
		removeAllRightItems();
	}

	public void removeAllRightItems()
	{
		mLlRight.removeAllViews();
		mListTitleItem.clear();
	}

	private void createSimpleTitle()
	{
		removeAllItems();
		addItemLeftDefault();
		addItemMiddleDefault();
	}

	private TitleItem addItemLeftDefault()
	{
		mTlLeft = new TitleItem(getContext());
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
		mLlLeft.addView(mTlLeft, params);
		LinearLayout llView = (LinearLayout) mTlLeft.mView;
		llView.setPadding(SDViewUtil.dp2px(10), 0, SDViewUtil.dp2px(10), 0);

		SDViewBinder.setBackgroundDrawable(mTlLeft, getSelectorDrawable());
		mTlLeft.setOnClickListener(this);
		mTlLeft.setAllViewsVisibility(View.GONE);
		return mTlLeft;
	}

	private TitleItem addItemMiddleDefault()
	{
		mTlMiddle = new TitleItem(getContext());
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
		mLlMiddle.addView(mTlMiddle, params);

		mTlMiddle.setOnClickListener(this);
		mTlMiddle.setAllViewsVisibility(View.GONE);
		return mTlMiddle;
	}

	public TitleItemModel addItemRight()
	{
		TitleItem tlRight = new TitleItem(getContext());
		SDViewBinder.setBackgroundDrawable(tlRight, getSelectorDrawable());
		tlRight.setOnClickListener(this);
		tlRight.setAllViewsVisibility(View.GONE);

		TitleItemModel model = new TitleItemModel();
		tlRight.setmTitleItemModel(model);
		mListTitleItem.add(tlRight);
		mListTitleItemModel.add(model);
		return model;
	}

	public void removeItemRight(TitleItem item)
	{
		if (item != null && mListTitleItem.contains(item))
		{
			mListTitleItem.remove(item);
			mListTitleItemModel.remove(item.getmTitleItemModel());
			done();
		}
	}

	public void removeItemRight(int index)
	{
		removeItemRight(getItemRight(index));
	}

	public TitleItem getItemRight(int index)
	{
		if (index >= 0 && index < mListTitleItem.size())
		{
			return mListTitleItem.get(index);
		}
		return null;
	}

	public int indexOfItemRight(TitleItemModel model)
	{
		if (model != null)
		{
			return mListTitleItemModel.indexOf(model);
		}
		return -1;
	}

	/**
	 * 所有设置完成，根据设置改变视图
	 */
	public void done()
	{
		mLlRight.removeAllViews();
		mLlMoreItem.removeAllViews();
		findMoreView();
		splitList();

		for (TitleItem item : mListTitleItemNormal)
		{
			addItemToRight(item);
		}
		if (mMoreView != null)
		{
			addItemToRight(mMoreView);
		}
		for (TitleItem item : mListTitleItemMore)
		{
			View view = mDefaultCreater.getItemView(item.getmTitleItemModel());
			view.setTag(item.getmTitleItemModel());
			addItemToMore(view);
		}
	}

	private void addItemToRight(TitleItem view)
	{
		LinearLayout.LayoutParams params = SDViewUtil.getLayoutParamsLinearLayoutWM();
		view.showByType();
		view.setTag(view.getmTitleItemModel());
		view.setOnClickListener(this);
		mLlRight.addView(view, params);
	}

	private void addItemToMore(View view)
	{
		LinearLayout.LayoutParams params = SDViewUtil.getLayoutParamsLinearLayoutMM();
		view.setOnClickListener(this);
		mLlMoreItem.addView(view, params);
	}

	private void findMoreView()
	{
		mMaxItem = MAX_ITEM_COUNT;
		mIndexMore = mMaxItem - 1;
	}

	private void splitList()
	{
		mListTitleItemNormal.clear();
		mListTitleItemMore.clear();
		mMoreView = null;
		int size = mListTitleItem.size();
		if (size > 0)
		{
			if (size <= mMaxItem)
			{
				mListTitleItemNormal.addAll(mListTitleItem);
			} else
			{
				TitleItem item = null;
				for (int i = 0; i < size; i++)
				{
					item = mListTitleItem.get(i);
					if (i < mIndexMore)
					{
						mListTitleItemNormal.add(item);
					} else if (i == mIndexMore)
					{
						// TODO 添加一个更多的按钮
						TitleItemModel moreModel = addItemRight();
						moreModel.setIconLeftResId(R.drawable.ic_more).setShowType(EnumShowType.ICON);
						mMoreView = getItemRight(indexOfItemRight(moreModel));
						mListTitleItem.remove(mMoreView);
						mListTitleItemModel.remove(moreModel);

						// 更多里面要添加该项
						mListTitleItemMore.add(item);
					} else
					{
						mListTitleItemMore.add(item);
					}
				}
			}
		}
	}

	// ---------------------setCustomView
	public void setCustomViewLeft(View view, LinearLayout.LayoutParams params)
	{
		mLlLeft.removeAllViews();
		mLlLeft.addView(view, params);
	}

	public void setCustomViewMiddle(View view, LinearLayout.LayoutParams params)
	{
		mLlMiddle.removeAllViews();
		mLlMiddle.addView(view, params);
	}

	public void setCustomViewRight(View view, LinearLayout.LayoutParams params)
	{
		mLlRight.removeAllViews();
		mLlRight.addView(view, params);
	}

	public void setHeightTitle(int height)
	{
		this.removeAllViews();
		this.addView(mView, new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, height, 1));
	}

	private void setDefaultConfig()
	{
		setBackgroundColor(mConfig.getmTitleColor());
	}

	private Drawable getSelectorDrawable()
	{
		SDDrawable none = new SDDrawable();
		none.color(mConfig.getmTitleColor());

		SDDrawable pressed = new SDDrawable();
		pressed.color(mConfig.getmTitleColorPressed());

		return SDDrawable.getStateListDrawable(none, null, null, pressed);
	}

	private void registerClick()
	{
	}

	@Override
	public void onClick(View v)
	{
		if (v == mTlLeft)
		{
			if (mListener != null)
			{
				mListener.onLeftClick_SDTitleListener((TitleItem) v);
			}
		} else if (v == mTlMiddle)
		{
			if (mListener != null)
			{
				mListener.onMiddleClick_SDTitleListener((TitleItem) v);
			}
		} else if (v == mMoreView)
		{
			showPopMore();
		} else
		{
			if (mListener != null)
			{
				TitleItemModel model = (TitleItemModel) v.getTag();
				int index = indexOfItemRight(model);
				mListener.onRightClick_SDTitleListener(getItemRight(index), index);
			}
			disMissPop();
		}
	}

	public void showPopMore()
	{
		if (mMoreView != null)
		{
			mPop.showAsDropDown(mMoreView, 0, 0);
		}
	}

	public void disMissPop()
	{
		mPop.dismiss();
	}

	private SDTitleCreaterListener mDefaultCreater = new SDTitleCreaterListener()
	{
		private SDDrawableManager mDrawableManager = new SDDrawableManager();

		@Override
		public View getItemView(TitleItemModel item)
		{
			View view = LayoutInflater.from(getContext()).inflate(R.layout.item_title_more, null);
			ImageView ivLeft = (ImageView) view.findViewById(R.id.item_title_more_iv_left);
			TextView tvTitle = (TextView) view.findViewById(R.id.item_title_more_tv_title);

			int imageResId = item.getIconLeftResId();
			if (imageResId == 0)
			{
				ivLeft.setImageBitmap(null);
				ivLeft.setVisibility(View.GONE);
			} else
			{
				ivLeft.setImageResource(imageResId);
				ivLeft.setVisibility(View.VISIBLE);
			}
			SDViewBinder.setTextView(tvTitle, item.getTextBot());
			SDViewBinder.setBackgroundDrawable(view, mDrawableManager.getSelectorMainColor(false));
			return view;
		}
	};

	public SDTitle setMiddleTextTop(String text)
	{
		mTlMiddle.setTextTop(text);
		return this;
	}

	public SDTitle setMiddleTextBot(String text)
	{
		mTlMiddle.setTextBot(text);
		return this;
	}

	public SDTitle setMiddleImageLeft(int resId)
	{
		mTlMiddle.setImageLeft(resId);
		return this;
	}

	public SDTitle setMiddleImageRight(int resId)
	{
		mTlMiddle.setImageRight(resId);
		return this;
	}

	public SDTitle setMiddleBackgroundText(int resId)
	{
		mTlMiddle.setBackgroundText(resId);
		return this;
	}

	public SDTitle setLeftTextTop(String text)
	{
		mTlLeft.setTextTop(text);
		return this;
	}

	public SDTitle setLeftTextBot(String text)
	{
		mTlLeft.setTextBot(text);
		return this;
	}

	public SDTitle setLeftImageLeft(int resId)
	{
		mTlLeft.setImageLeft(resId);
		return this;
	}

	public SDTitle setLeftImageRight(int resId)
	{
		mTlLeft.setImageRight(resId);
		return this;
	}

	public SDTitle setLeftBackgroundText(int resId)
	{
		mTlLeft.setBackgroundText(resId);
		return this;
	}

	public interface SDTitleListener
	{
		public void onLeftClick_SDTitleListener(TitleItem item);

		public void onMiddleClick_SDTitleListener(TitleItem item);

		public void onRightClick_SDTitleListener(TitleItem item, int index);
	}

	public interface SDTitleCreaterListener
	{
		public View getItemView(TitleItemModel item);
	}

}

package com.wenzhoujie.library.title;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wenzhoujie.library.R;

public class TitleRight extends LinearLayout
{
	public View mView;
	public ImageView mIvRight = null;
	public TextView mTvRight = null;

	public TitleRight(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	public TitleRight(Context context)
	{
		this(context, null);
	}

	private void init()
	{
		mView = LayoutInflater.from(getContext()).inflate(R.layout.title_right, null);
		this.addView(mView, new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1));
		this.setBackgroundColor(getResources().getColor(android.R.color.transparent));

		mIvRight = (ImageView) findViewById(R.id.iv_image);
		mTvRight = (TextView) findViewById(R.id.tv_text);

		mIvRight.setVisibility(View.GONE);
		mTvRight.setVisibility(View.GONE);

	}

	// ------------------------------------right

	public TitleRight setText(String text)
	{
		if (TextUtils.isEmpty(text))
		{
			mTvRight.setVisibility(View.GONE);
		} else
		{
			mTvRight.setVisibility(View.VISIBLE);
			mTvRight.setText(text);
		}
		return this;
	}

	public TitleRight setTextBackground(int resId)
	{
		mTvRight.setBackgroundResource(resId);
		return this;
	}

	public TitleRight setImage(int resId)
	{
		if (resId == 0)
		{
			mIvRight.setVisibility(View.GONE);
		} else
		{
			mIvRight.setVisibility(View.VISIBLE);
			mIvRight.setImageResource(resId);
		}
		return this;
	}

}

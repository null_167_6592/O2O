package com.wenzhoujie.library.title;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.wenzhoujie.library.R;
import com.wenzhoujie.library.customview.SDTabItemCornerGroup;
import com.wenzhoujie.library.customview.SDViewNavigatorManager.SDViewNavigatorManagerListener;

public class SDTitleTabs extends LinearLayout implements OnClickListener
{
	public View mView;
	public TitleLeft mTlLeft;
	public TitleRight mTlRight;
	public SDTabItemCornerGroup mTlMiddle;

	private SDTitleTabsListener mListener = null;

	public SDTitleTabs(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	public SDTitleTabs(Context context)
	{
		this(context, null);
	}

	private void init()
	{
		mView = LayoutInflater.from(getContext()).inflate(R.layout.title_tabs, null);
		this.addView(mView, new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1));

		mTlLeft = (TitleLeft) mView.findViewById(R.id.tl_title_left);
		mTlRight = (TitleRight) mView.findViewById(R.id.tl_title_right);
		mTlMiddle = (SDTabItemCornerGroup) mView.findViewById(R.id.ticg_title_middle);

		mTlMiddle.setmListener(new SDViewNavigatorManagerListener()
		{

			@Override
			public void onItemClick(View v, int index)
			{
				if (mListener != null)
				{
					mListener.onItemClick_SDTitleTabs(v, index);
				}
			}
		});
		registerClick();
	}

	public void setHeightTitle(int height)
	{
		this.removeAllViews();
		this.addView(mView, new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, height));
	}

	public SDTitleTabsListener getmListener()
	{
		return mListener;
	}

	public void setmListener(SDTitleTabsListener mListener)
	{
		this.mListener = mListener;
	}

	private void registerClick()
	{
		mTlLeft.setOnClickListener(this);
		mTlRight.setOnClickListener(this);
	}

	public interface SDTitleTabsListener
	{
		public void onLeftButtonCLick_SDTitleTabs(View v);

		public void onRightButtonCLick_SDTitleTabs(View v);

		public void onItemClick_SDTitleTabs(View v, int index);
	}

	// ------------------------------left

	public SDTitleTabs setLeftLayoutBackground(int resId)
	{
		mTlLeft.setBackgroundResource(resId);
		return this;
	}

	public SDTitleTabs setLeftText(String text)
	{
		mTlLeft.setText(text);
		return this;
	}

	public SDTitleTabs setLeftTextBackground(int resId)
	{
		mTlLeft.setTextBackground(resId);
		return this;
	}

	public SDTitleTabs setLeftImage(int resId)
	{
		mTlLeft.setImage(resId);
		return this;
	}

	public SDTitleTabs setLeftLayoutVisibility(int visibility)
	{
		mTlLeft.setVisibility(visibility);
		return this;
	}

	public SDTitleTabs setLeftLayoutClick(OnClickListener listener)
	{
		mTlLeft.setOnClickListener(listener);
		return this;
	}

	// ------------------------------------right

	public SDTitleTabs setRightLayoutBackground(int resId)
	{
		mTlRight.setBackgroundResource(resId);
		return this;
	}

	public SDTitleTabs setRightText(String text)
	{
		mTlRight.setText(text);
		return this;
	}

	public SDTitleTabs setRightTextBackground(int resId)
	{
		mTlRight.setTextBackground(resId);
		return this;
	}

	public SDTitleTabs setRightImage(int resId)
	{
		mTlRight.setImage(resId);
		return this;
	}

	public SDTitleTabs setRightLayoutVisibility(int visibility)
	{
		mTlRight.setVisibility(visibility);
		return this;
	}

	public SDTitleTabs setRightLayoutClick(OnClickListener listener)
	{
		mTlRight.setOnClickListener(listener);
		return this;
	}

	@Override
	public void onClick(View v)
	{
		if (v == mTlLeft)
		{
			if (mListener != null)
			{
				mListener.onLeftButtonCLick_SDTitleTabs(v);
			}
		} else if (v == mTlRight)
		{
			if (mListener != null)
			{
				mListener.onRightButtonCLick_SDTitleTabs(v);
			}
		}
	}

}

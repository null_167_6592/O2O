package com.wenzhoujie.library.title;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.wenzhoujie.library.R;
import com.wenzhoujie.library.SDLibrary;
import com.wenzhoujie.library.config.SDLibraryConfig;
import com.wenzhoujie.library.drawable.SDDrawable;

public class SDTitleSimple extends LinearLayout implements OnClickListener
{
	public View mView;
	public TitleLeft mTlLeft;
	public TitleRight mTlRight;
	public TitleMiddleTwoText mTlMiddle;

	private SDTitleSimpleListener mListener = null;

	public SDTitleSimple(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	public SDTitleSimple(Context context)
	{
		this(context, null);
	}

	private void init()
	{
		mView = LayoutInflater.from(getContext()).inflate(R.layout.title_simple, null);
		this.addView(mView, new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

		mTlLeft = (TitleLeft) mView.findViewById(R.id.tl_title_left);
		mTlRight = (TitleRight) mView.findViewById(R.id.tl_title_right);
		mTlMiddle = (TitleMiddleTwoText) mView.findViewById(R.id.tl_title_middle);
		registerClick();
		setDefaultConfig();
	}

	private void setDefaultConfig()
	{
		SDLibraryConfig config = SDLibrary.getInstance().getmConfig();
		setBackgroundColor(config.getmTitleColor());
		setLeftImage(config.getmIconArrowLeftResId());
		setHeightTitle(config.getmTitleHeight());

		SDDrawable none = new SDDrawable();
		none.color(config.getmTitleColor());

		SDDrawable pressed = new SDDrawable();
		pressed.color(config.getmTitleColorPressed());

		mTlLeft.setBackgroundDrawable(SDDrawable.getStateListDrawable(none, null, null, pressed));
		mTlRight.setBackgroundDrawable(SDDrawable.getStateListDrawable(none, null, null, pressed));
	}

	public void setHeightTitle(int height)
	{
		this.removeAllViews();
		this.addView(mView, new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, height));
	}

	public SDTitleSimpleListener getmListener()
	{
		return mListener;
	}

	public void setmListener(SDTitleSimpleListener mListener)
	{
		this.mListener = mListener;
	}

	private void registerClick()
	{
		mTlLeft.setOnClickListener(this);
		mTlRight.setOnClickListener(this);
	}

	public interface SDTitleSimpleListener
	{
		public void onLeftButtonCLick_SDTitleSimple(View v);

		public void onRightButtonCLick_SDTitleSimple(View v);

	}

	// ------------------------------middle

	public SDTitleSimple setTitleTop(String text)
	{
		mTlMiddle.setTextTop(text);
		return this;
	}

	public SDTitleSimple setTitleBottom(String text)
	{
		mTlMiddle.setTextBottom(text);
		return this;
	}

	// ------------------------------left

	public SDTitleSimple setLeftLayoutBackground(int resId)
	{
		mTlLeft.setBackgroundResource(resId);
		return this;
	}

	public SDTitleSimple setLeftText(String text)
	{
		mTlLeft.setText(text);
		return this;
	}

	public SDTitleSimple setLeftTextBackground(int resId)
	{
		mTlLeft.setTextBackground(resId);
		return this;
	}

	public SDTitleSimple setLeftImage(int resId)
	{
		mTlLeft.setImage(resId);
		return this;
	}

	public SDTitleSimple setLeftLayoutVisibility(int visibility)
	{
		mTlLeft.setVisibility(visibility);
		return this;
	}

	public SDTitleSimple setLeftLayoutClick(OnClickListener listener)
	{
		mTlLeft.setOnClickListener(listener);
		return this;
	}

	// ------------------------------------right

	public SDTitleSimple setRightLayoutBackground(int resId)
	{
		mTlRight.setBackgroundResource(resId);
		return this;
	}

	public SDTitleSimple setRightText(String text)
	{
		mTlRight.setText(text);
		return this;
	}

	public SDTitleSimple setRightTextBackground(int resId)
	{
		mTlRight.setTextBackground(resId);
		return this;
	}

	public SDTitleSimple setRightImage(int resId)
	{
		mTlRight.setImage(resId);
		return this;
	}

	public SDTitleSimple setRightLayoutVisibility(int visibility)
	{
		mTlRight.setVisibility(visibility);
		return this;
	}

	public SDTitleSimple setRightLayoutClick(OnClickListener listener)
	{
		mTlRight.setOnClickListener(listener);
		return this;
	}

	@Override
	public void onClick(View v)
	{
		if (v == mTlLeft)
		{
			if (mListener != null)
			{
				mListener.onLeftButtonCLick_SDTitleSimple(v);
			}
		} else if (v == mTlRight)
		{
			if (mListener != null)
			{
				mListener.onRightButtonCLick_SDTitleSimple(v);
			}
		}
	}

}

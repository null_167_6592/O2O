package com.wenzhoujie.library.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wenzhoujie.library.R;
import com.wenzhoujie.library.drawable.SDDrawableManager;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.wenzhoujie.library.utils.ViewHolder;

public class SDSimpleTextAdapter extends BaseAdapter
{

	private Activity mActivity;
	private List<? extends Object> mListModel = new ArrayList<Object>();
	private SDDrawableManager mDrawableManager = new SDDrawableManager();

	public SDSimpleTextAdapter(List<? extends Object> listModel, Activity activity)
	{
		if (listModel != null)
		{
			this.mListModel = listModel;
		}
		this.mActivity = activity;
	}

	@Override
	public int getCount()
	{
		return mListModel.size();
	}

	@Override
	public Object getItem(int pos)
	{
		return mListModel.get(pos);
	}

	@Override
	public long getItemId(int pos)
	{
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = mActivity.getLayoutInflater().inflate(R.layout.item_simple_text, null);
		}
		SDViewBinder.setBackgroundDrawable(convertView, mDrawableManager.getSelectorWhiteGray(false));
		TextView tvName = ViewHolder.get(convertView, R.id.item_simple_text_tv_name);
		SDViewBinder.setTextView(tvName, getItem(pos).toString());
		return convertView;
	}

}

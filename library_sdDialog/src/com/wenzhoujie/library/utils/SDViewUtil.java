package com.wenzhoujie.library.utils;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.wenzhoujie.library.SDLibrary;
import com.wenzhoujie.library.adapter.SDBaseAdapter;

public class SDViewUtil
{

	@SuppressLint("NewApi")
	public static void scrollToViewY(final ScrollView sv, final View target, int delay)
	{
		if (Build.VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB)
		{
			SDHandlerUtil.runOnUiThreadDelayed(new Runnable()
			{

				@Override
				public void run()
				{
					sv.scrollTo(0, (int) target.getY());
				}
			}, delay);
		}
	}

	// -------------------------layoutParams
	public static LinearLayout.LayoutParams getLayoutParamsLinearLayoutWW()
	{
		return new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
	}

	public static LinearLayout.LayoutParams getLayoutParamsLinearLayoutMM()
	{
		return new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.MATCH_PARENT);
	}

	public static LinearLayout.LayoutParams getLayoutParamsLinearLayoutMW()
	{
		return new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
	}

	public static LinearLayout.LayoutParams getLayoutParamsLinearLayoutWM()
	{
		return new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.MATCH_PARENT);
	}

	public static RelativeLayout.LayoutParams getLayoutParamsRelativeLayoutWW()
	{
		return new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
	}

	public static RelativeLayout.LayoutParams getLayoutParamsRelativeLayoutMM()
	{
		return new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT);
	}

	public static RelativeLayout.LayoutParams getLayoutParamsRelativeLayoutMW()
	{
		return new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
	}

	public static RelativeLayout.LayoutParams getLayoutParamsRelativeLayoutWM()
	{
		return new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.MATCH_PARENT);
	}

	public static FrameLayout.LayoutParams getLayoutParamsFrameLayoutWW()
	{
		return new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,
				FrameLayout.LayoutParams.WRAP_CONTENT);
	}

	public static FrameLayout.LayoutParams getLayoutParamsFrameLayoutMM()
	{
		return new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
				FrameLayout.LayoutParams.MATCH_PARENT);
	}

	public static FrameLayout.LayoutParams getLayoutParamsFrameLayoutMW()
	{
		return new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
				FrameLayout.LayoutParams.WRAP_CONTENT);
	}

	public static FrameLayout.LayoutParams getLayoutParamsFrameLayoutWM()
	{
		return new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,
				FrameLayout.LayoutParams.MATCH_PARENT);
	}

	// ------------------------layoutInflater
	public static LayoutInflater getLayoutInflater()
	{
		return LayoutInflater.from(SDLibrary.getInstance().getApplication());
	}

	public static View inflate(int resource, ViewGroup root)
	{
		return getLayoutInflater().inflate(resource, root);
	}

	public static View inflate(int resource, ViewGroup root, boolean attachToRoot)
	{
		return getLayoutInflater().inflate(resource, root, attachToRoot);
	}

	public static DisplayMetrics getDisplayMetrics()
	{
		return SDLibrary.getInstance().getApplication().getResources().getDisplayMetrics();
	}

	public static int getScreenWidth()
	{
		DisplayMetrics metrics = getDisplayMetrics();
		return metrics.widthPixels;
	}

	public static int getScreenHeight()
	{
		DisplayMetrics metrics = getDisplayMetrics();
		return metrics.heightPixels;
	}

	public static float getDensity()
	{
		return SDLibrary.getInstance().getApplication().getResources().getDisplayMetrics().density;
	}

	public static float getScaledDensity()
	{
		return SDLibrary.getInstance().getApplication().getResources().getDisplayMetrics().scaledDensity;
	}

	public static int sp2px(float sp)
	{
		final float fontScale = getDisplayMetrics().scaledDensity;
		return (int) (sp * fontScale + 0.5f);
	}

	public static int dp2px(float dp)
	{
		final float scale = getDisplayMetrics().density;
		return (int) (dp * scale + 0.5f);
	}

	public static int px2dp(float px)
	{
		final float scale = getDisplayMetrics().density;
		return (int) (px / scale + 0.5f);
	}

	public static int getScaleHeight(int originalWidth, int originalHeight, int scaleWidth)
	{
		return originalHeight * scaleWidth / originalWidth;
	}

	public static int getScaleHeight(Bitmap bitmap, int scaleWidth)
	{
		return getScaleHeight(bitmap.getWidth(), bitmap.getHeight(), scaleWidth);
	}

	public static int getScaleWidth(int originalWidth, int originalHeight, int scaleHeight)
	{
		return originalWidth * scaleHeight / originalHeight;
	}

	public static int getScaleWidth(Bitmap bitmap, int scaleHeight)
	{
		return getScaleWidth(bitmap.getWidth(), bitmap.getHeight(), scaleHeight);
	}

	/**
	 * 判断当前线程是否是UI线程.
	 * 
	 * @return
	 */
	public static boolean isUIThread()
	{
		return Looper.getMainLooper().getThread().getId() == Thread.currentThread().getId();
	}

	/**
	 * 隐藏输入法
	 * 
	 * @param context
	 */
	public static void hideInputMethod()
	{
		InputMethodManager imm = (InputMethodManager) SDLibrary.getInstance().getApplication()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		if (imm.isActive())
		{
			imm.toggleSoftInput(InputMethodManager.RESULT_UNCHANGED_SHOWN, InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}

	/**
	 * 显示输入法
	 * 
	 * @param context
	 * @param view
	 * @param requestFocus
	 */
	public static void showInputMethod(View view, boolean requestFocus)
	{
		if (requestFocus)
		{
			view.requestFocus();
		}
		InputMethodManager imm = (InputMethodManager) SDLibrary.getInstance().getApplication()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
	}

	/**
	 * 重置listview高度，解决和scrollview嵌套问题
	 * 
	 * @param listView
	 */
	public static void resetListViewHeightBasedOnChildren(ListView listView)
	{
		int totalHeight = getListViewTotalHeight(listView);
		if (totalHeight > 0)
		{
			ViewGroup.LayoutParams params = listView.getLayoutParams();
			params.height = totalHeight;
			params.height += 5;
			listView.setLayoutParams(params);
		}
	}

	public static int getListViewTotalHeight(ListView listView)
	{
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null)
		{
			return 0;
		}

		int totalHeight = 0;
		for (int i = 0; i < listAdapter.getCount(); i++)
		{
			View listItem = listAdapter.getView(i, null, listView);
			if (listItem != null)
			{
				listItem.measure(0, 0);
				int height = listItem.getMeasuredHeight();
				int dividerHeight = listView.getDividerHeight() * (listAdapter.getCount() - 1);
				totalHeight += (height + dividerHeight);
			}
		}
		return totalHeight;
	}

	public static void measureView(View v)
	{
		if (v == null)
		{
			return;
		}
		int w = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
		int h = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
		v.measure(w, h);
	}

	public static int getViewHeight(View view)
	{
		if (view != null)
		{
			int height = view.getHeight();
			if (height <= 0)
			{
				measureView(view);
				height = view.getMeasuredHeight();
			}
			return height;
		}
		return 0;
	}

	public static int getViewWidth(View view)
	{
		if (view != null)
		{
			int width = view.getWidth();
			if (width <= 0)
			{
				measureView(view);
				width = view.getMeasuredWidth();
			}
			return width;
		}
		return 0;
	}

	public static void hide(View view)
	{
		if (view != null && view.getVisibility() != View.GONE)
		{
			view.setVisibility(View.GONE);
		}
	}

	public static void invisible(View view)
	{
		if (view.getVisibility() != View.INVISIBLE)
		{
			view.setVisibility(View.INVISIBLE);
		}
	}

	public static void show(View view)
	{
		if (view.getVisibility() != View.VISIBLE)
		{
			view.setVisibility(View.VISIBLE);
		}
	}

	public static void toggleEmptyMsgByList(List<? extends Object> list, View emptyView)
	{
		if (emptyView != null)
		{
			if (list != null && list.size() > 0)
			{
				if (emptyView.getVisibility() != View.GONE)
				{
					emptyView.setVisibility(View.GONE);
				}
			} else
			{
				if (emptyView.getVisibility() != View.VISIBLE)
				{
					emptyView.setVisibility(View.VISIBLE);
				}
			}
		}
	}

	public static void toggleViewByList(List<? extends Object> list, View view)
	{
		if (view != null)
		{
			if (list != null && list.size() > 0)
			{
				if (view.getVisibility() != View.VISIBLE)
				{
					view.setVisibility(View.VISIBLE);
				}
			} else
			{
				if (view.getVisibility() != View.GONE)
				{
					view.setVisibility(View.GONE);
				}
			}
		}
	}

	public static void setTextSizeSp(TextView view, float sizeSp)
	{
		view.setTextSize(TypedValue.COMPLEX_UNIT_SP, sizeSp);
	}

	public static <T> void updateAdapterByList(List<T> listOriginalData, List<T> listNewData,
			SDBaseAdapter<T> mAdapter, boolean isLoadMore)
	{
		if (mAdapter != null && listOriginalData != null)
		{
			if (listNewData != null && listNewData.size() > 0)
			{
				if (!isLoadMore)
				{
					listOriginalData.clear();
				}
				listOriginalData.addAll(listNewData);
			} else
			{
				listOriginalData.clear();
				if (listOriginalData.size() > 0)
				{
					SDToast.showToast("未找到数据");
				}
			}
			mAdapter.updateListViewData(listOriginalData);
		}
	}

	public static View wrapperTitle(int contentLayoutId, int titleLayoutId)
	{
		LayoutInflater inflater = LayoutInflater.from(SDLibrary.getInstance().getApplication());
		View contentView = inflater.inflate(contentLayoutId, null);
		View titleView = inflater.inflate(titleLayoutId, null);
		return wrapperTitle(contentView, titleView);
	}

	public static View wrapperTitle(View contentView, View titleView)
	{
		LinearLayout linAll = new LinearLayout(SDLibrary.getInstance().getApplication());
		linAll.setOrientation(LinearLayout.VERTICAL);
		LinearLayout.LayoutParams paramsTitle = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT);
		LinearLayout.LayoutParams paramsContent = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
		linAll.addView(titleView, paramsTitle);
		linAll.addView(contentView, paramsContent);
		return linAll;
	}

	public static View wrapperTitle(View contentView, int titleLayoutId)
	{
		LayoutInflater inflater = LayoutInflater.from(SDLibrary.getInstance().getApplication());
		View titleView = inflater.inflate(titleLayoutId, null);
		return wrapperTitle(contentView, titleView);
	}

	public static boolean setViewHeight(View view, int height)
	{
		if (height >= 0)
		{
			ViewGroup.LayoutParams params = view.getLayoutParams();
			if (params != null)
			{
				params.height = height;
				view.setLayoutParams(params);
				return true;
			}
		}
		return false;
	}

	public static boolean setViewWidth(View view, int width)
	{
		if (width >= 0)
		{
			ViewGroup.LayoutParams params = view.getLayoutParams();
			if (params != null)
			{
				params.width = width;
				view.setLayoutParams(params);
				return true;
			}
		}
		return false;
	}

}

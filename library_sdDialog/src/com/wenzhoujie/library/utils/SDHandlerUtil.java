package com.wenzhoujie.library.utils;

import android.os.Handler;
import android.os.Looper;

public class SDHandlerUtil
{

	private static Handler mHandler = new Handler(Looper.getMainLooper());

	public synchronized static void runOnUiThread(Runnable r)
	{
		if (Looper.myLooper() != Looper.getMainLooper())
		{
			mHandler.post(r);
		} else
		{
			r.run();
		}
	}

	public synchronized static void runOnUiThreadFrontOfQueue(Runnable r)
	{
		mHandler.postAtFrontOfQueue(r);
	}

	public synchronized static void runOnUiThreadAtTime(Runnable r, long uptimeMillis)
	{
		mHandler.postAtTime(r, uptimeMillis);
	}

	public synchronized static void runOnUiThreadAtTime(Runnable r, Object msgObj, long uptimeMillis)
	{
		mHandler.postAtTime(r, msgObj, uptimeMillis);
	}

	public synchronized static void runOnUiThreadDelayed(Runnable r, long delayMillis)
	{
		mHandler.postDelayed(r, delayMillis);
	}

}

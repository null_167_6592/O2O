package com.wenzhoujie.library.customview;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;

import com.wenzhoujie.library.popupwindow.SDPWindowBase;

public class SDLvCategoryViewHelper
{
	private static final int DEFAULT_INDEX = 0;

	public ListView mLvContent = null;
	private SDLvCategoryViewHelperAdapterInterface mAdapterHelper = null;
	public View mPopView = null;
	public PopupWindow mPopCategory = null;
	private int mCurrentIndex = DEFAULT_INDEX;
	private SDLvCategoryViewHelperListener mListener = null;
	private String mTitle;

	// -----------------get set

	public SDLvCategoryViewHelperListener getmListener()
	{
		return mListener;
	}

	public int getmCurrentIndex()
	{
		return mCurrentIndex;
	}

	public int getItemCount()
	{
		return mAdapterHelper.getAdapter().getCount();
	}

	public void setmCurrentIndex(int mCurrentIndex)
	{
		if (getItemCount() > 0 && mCurrentIndex >= 0 && mCurrentIndex < getItemCount())
		{
			this.mCurrentIndex = mCurrentIndex;
			mOnItemClickListener.onItemClick(mLvContent, null, mCurrentIndex + 1, mCurrentIndex);
		}
	}

	public String getmTitle()
	{
		return mTitle;
	}

	public void setmTitle(String mTitle)
	{
		this.mTitle = mTitle;
		if (mListener != null)
		{
			mListener.onTitleChange(mTitle);
		}
	}

	public void setmListener(SDLvCategoryViewHelperListener mListener)
	{
		this.mListener = mListener;
	}

	public SDLvCategoryViewHelper(View popView, ListView listView)
	{
		this.mPopView = popView;
		this.mLvContent = listView;
		init();
	}

	private void init()
	{
		initPopwindow();
	}

	private void initPopwindow()
	{
		mPopCategory = new SDPWindowBase(false);
		mPopCategory.setContentView(mPopView);
		mPopCategory.setOnDismissListener(new PopDismissListener());
	}

	public void setAdapter(SDLvCategoryViewHelperAdapterInterface adapter)
	{
		this.mAdapterHelper = adapter;
		initTitle();
		mLvContent.setOnItemClickListener(mOnItemClickListener);
		mLvContent.setAdapter(mAdapterHelper.getAdapter());
	}

	private void initTitle()
	{
		int titleIndex = mAdapterHelper.getTitleIndex();
		if (titleIndex >= 0)
		{
			mCurrentIndex = titleIndex;
		} else
		{
			mCurrentIndex = DEFAULT_INDEX;
		}
		mAdapterHelper.setPositionSelectState(mCurrentIndex, true, false);
		setmTitle(mAdapterHelper.getTitleNameFromPosition(mCurrentIndex));
	}

	private OnItemClickListener mOnItemClickListener = new OnItemClickListener()
	{

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id)
		{
			mAdapterHelper.setPositionSelectState(mCurrentIndex, false, false);
			mAdapterHelper.setPositionSelectState((int) id, true, true);
			mCurrentIndex = (int) id;

			setmTitle(mAdapterHelper.getTitleNameFromPosition(mCurrentIndex));
			dismissPopwindow();
			if (mListener != null)
			{
				mListener.onItemSelect(mCurrentIndex, mAdapterHelper.getSelectModelFromPosition(mCurrentIndex));
			}
		}
	};

	public void dismissPopwindow()
	{
		if (mPopCategory != null)
		{
			mPopCategory.dismiss();
		}
	}

	public void showPopwindow()
	{
		if (mAdapterHelper.getAdapter().getCount() > 0)
		{
			if (mListener != null)
			{
				mListener.onPopwindowShow(mPopCategory);
			}
		}
	}

	class PopDismissListener implements OnDismissListener
	{
		@Override
		public void onDismiss()
		{
			if (mListener != null)
			{
				mListener.onPopwindowDismiss(getmTitle());
			}
		}

	}

	public interface SDLvCategoryViewHelperListener
	{
		public void onItemSelect(int index, Object model);

		public void onPopwindowDismiss(String title);

		public void onTitleChange(String title);

		public void onPopwindowShow(PopupWindow popwindow);
	}

	public interface SDLvCategoryViewHelperAdapterInterface
	{
		public void setPositionSelectState(int position, boolean select, boolean notify);

		public String getTitleNameFromPosition(int position);

		public BaseAdapter getAdapter();

		public Object getSelectModelFromPosition(int position);

		public int getTitleIndex();
	}
}

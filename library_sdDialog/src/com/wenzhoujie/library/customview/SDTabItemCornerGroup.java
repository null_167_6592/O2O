package com.wenzhoujie.library.customview;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.LinearLayout;

import com.wenzhoujie.library.customview.SDViewBase.EnumTabPosition;
import com.wenzhoujie.library.customview.SDViewNavigatorManager.SDViewNavigatorManagerListener;

public class SDTabItemCornerGroup extends LinearLayout
{

	private SDViewAttr mAttr;
	private List<SDTabItemCorner> mListTabs;
	private SDViewNavigatorManager mViewManager = new SDViewNavigatorManager();
	private SDViewNavigatorManagerListener mListener;

	public List<SDTabItemCorner> getmListTabs()
	{
		return mListTabs;
	}

	public void setmListTabs(List<SDTabItemCorner> mListTabs)
	{
		this.mListTabs = mListTabs;
	}

	public SDViewNavigatorManager getmViewManager()
	{
		return mViewManager;
	}

	public void setmViewManager(SDViewNavigatorManager mViewManager)
	{
		this.mViewManager = mViewManager;
	}

	public SDViewNavigatorManagerListener getmListener()
	{
		return mListener;
	}

	public void setmListener(SDViewNavigatorManagerListener mListener)
	{
		this.mListener = mListener;
		mViewManager.setmListener(mListener);
	}

	public SDViewAttr getmAttr()
	{
		return mAttr;
	}

	public void setmAttr(SDViewAttr mAttr)
	{
		this.mAttr = mAttr;
	}

	public SDTabItemCornerGroup(Context context)
	{
		this(context, null);
	}

	public SDTabItemCornerGroup(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	private void init()
	{
		mAttr = new SDViewAttr();
		mListTabs = new ArrayList<SDTabItemCorner>();
		this.setBackgroundColor(getResources().getColor(android.R.color.transparent));
		this.setGravity(Gravity.CENTER);
		this.setOrientation(LinearLayout.HORIZONTAL);
	}

	public SDTabItemCornerGroup addItem(String name)
	{
		return addItem(name, 0, 1);
	}

	public SDTabItemCornerGroup addItem(String name, float textSizeSp, float weight)
	{
		if (name != null)
		{
			SDTabItemCorner tab = createTab(name, textSizeSp);
			tab.setmPosition(EnumTabPosition.MIDDLE);
			mListTabs.add(tab);
			this.addView(tab, getLayoutParamsWeight(weight));

			int tabCount = mListTabs.size();
			if (tabCount <= 0)
			{
				this.removeAllViews();
			} else if (tabCount == 1)
			{
				mListTabs.get(0).setmPosition(EnumTabPosition.SINGLE);
			} else
			{
				mListTabs.get(0).setmPosition(EnumTabPosition.FIRST);
				mListTabs.get(tabCount - 1).setmPosition(EnumTabPosition.LAST);
			}

			SDViewBase[] items = new SDTabItemCorner[mListTabs.size()];
			mListTabs.toArray(items);
			mViewManager.setItemsArr(items);
		}
		return this;
	}

	private LinearLayout.LayoutParams getLayoutParamsWeight(float weight)
	{
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, weight);
		return params;
	}

	private SDTabItemCorner createTab(String name, float textSizeSp)
	{
		SDTabItemCorner tab = new SDTabItemCorner(getContext());
		tab.setTabName(name);
		if (textSizeSp > 0)
		{
			tab.setTabTextSizeSp(textSizeSp);
		}
		tab.setmAttr(mAttr);
		return tab;
	}

	public interface SDTabItemCornerGroupListener
	{

	}

}

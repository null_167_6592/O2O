package com.wenzhoujie.library.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wenzhoujie.library.SDLibrary;
import com.wenzhoujie.library.config.SDLibraryConfig;
import com.wenzhoujie.library.drawable.SDDrawable;

public class SDTabItemCorner extends SDViewBase
{

	public TextView mTvTitle = null;

	private int mWidth = 0;
	private int mHeight = 0;

	private EnumTabPosition mPosition = EnumTabPosition.SINGLE;

	// ------------------get set
	public EnumTabPosition getmPosition()
	{
		return mPosition;
	}

	public void setmPosition(EnumTabPosition mPosition)
	{
		this.mPosition = mPosition;
		resetBackgroudDrawable();
	}

	public int getmWidth()
	{
		return mWidth;
	}

	public void setmWidth(int mWidth)
	{
		this.mWidth = mWidth;
		if (mWidth > 0)
		{
			ViewGroup.LayoutParams params = mTvTitle.getLayoutParams();
			params.width = mWidth;
			mTvTitle.setLayoutParams(params);
		}
	}

	public int getmHeight()
	{
		return mHeight;

	}

	public void setmHeight(int mHeight)
	{
		this.mHeight = mHeight;
		if (mHeight > 0)
		{
			ViewGroup.LayoutParams params = mTvTitle.getLayoutParams();
			params.height = mHeight;
			mTvTitle.setLayoutParams(params);
		}
	}

	public void setDefaultConfig()
	{
		SDLibraryConfig config = SDLibrary.getInstance().getmConfig();
		if (config != null)
		{
			mAttr.setmStrokeColor(config.getmMainColor());
			mAttr.setmStrokeWidth(config.getmStrokeWidth());
			mAttr.setmTextColorNormal(config.getmMainColor());
			mAttr.setmTextColorSelectedResId(android.R.color.white);
			mAttr.setmBackgroundColorNormalResId(android.R.color.white);
			mAttr.setmBackgroundColorSelected(config.getmMainColor());
			mAttr.setmCornerRadius(config.getmCornerRadius());
			resetBackgroudDrawable();
		}
	}

	public void reverseDefaultConfig()
	{
		SDLibraryConfig config = SDLibrary.getInstance().getmConfig();
		if (config != null)
		{
			mAttr.setmStrokeColorResId(android.R.color.white);
			mAttr.setmStrokeWidth(config.getmStrokeWidth());
			mAttr.setmTextColorNormalResId(android.R.color.white);
			mAttr.setmTextColorSelected(config.getmMainColor());
			mAttr.setmBackgroundColorNormal(config.getmMainColor());
			mAttr.setmBackgroundColorSelectedResId(android.R.color.white);
			mAttr.setmCornerRadius(config.getmCornerRadius());
			resetBackgroudDrawable();
		}
	}

	public SDTabItemCorner(Context context)
	{
		super(context);
		init();
	}

	public SDTabItemCorner(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	private void init()
	{
		mTvTitle = new TextView(getContext());
		mTvTitle.setGravity(Gravity.CENTER);

		this.addView(mTvTitle, getLayoutParamsWW());
		this.setGravity(Gravity.CENTER);

		setDefaultConfig();
		resetBackgroudDrawable();
		onNormal();
	}

	public void resetBackgroudDrawable()
	{
		SDDrawable drawableNormal = new SDDrawable();
		drawableNormal.color(mAttr.getmBackgroundColorNormal());
		drawableNormal.strokeColor(mAttr.getmStrokeColor());
		drawableNormal.strokeWidthAll(mAttr.getmStrokeWidth());
		drawableNormal.cornerAll(mAttr.getmCornerRadius());

		SDDrawable drawableSelected = new SDDrawable();
		drawableSelected.color(mAttr.getmBackgroundColorSelected());
		drawableSelected.strokeColor(mAttr.getmStrokeColor());
		drawableSelected.strokeWidthAll(mAttr.getmStrokeWidth());
		drawableSelected.cornerAll(mAttr.getmCornerRadius());

		switch (mPosition)
		{
		case FIRST:
			drawableNormal.strokeWidthRight(0);
			drawableNormal.cornerTopRight(0).cornerBottomRight(0);

			drawableSelected.strokeWidthRight(0);
			drawableSelected.cornerTopRight(0).cornerBottomRight(0);
			break;
		case MIDDLE:
			drawableNormal.strokeWidthRight(0);
			drawableNormal.cornerAll(0);

			drawableSelected.strokeWidthRight(0);
			drawableSelected.cornerAll(0);
			break;
		case LAST:
			drawableNormal.cornerTopLeft(0).cornerBottomLeft(0);

			drawableSelected.cornerTopLeft(0).cornerBottomLeft(0);
			break;
		case SINGLE:

			break;

		default:
			break;
		}

		mAttr.setmBackgroundDrawableNormal(drawableNormal);
		mAttr.setmBackgroundDrawableSelected(drawableSelected);
	}

	public void setTabName(String name)
	{
		if (name != null)
		{
			mTvTitle.setText(name);
		}
	}

	public void setTabTextSizeSp(float textSize)
	{
		setTextSizeSp(mTvTitle, textSize);
	}

	@Override
	public void onNormal()
	{
		onNormalTextColor(mTvTitle);
		onNormalViewBackground(this);
		super.onNormal();
	}

	@Override
	public void onSelected()
	{
		onSelectedTextColor(mTvTitle);
		onSelectedViewBackground(this);
		super.onSelected();
	}

}

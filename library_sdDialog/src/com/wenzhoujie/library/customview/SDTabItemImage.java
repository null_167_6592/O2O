package com.wenzhoujie.library.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

public class SDTabItemImage extends SDViewBase
{

	public ImageView mIvTitle = null;

	public SDTabItemImage(Context context)
	{
		this(context, null);
	}

	// -------------------get set

	public SDTabItemImage(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	private void init()
	{
		setGravity(Gravity.CENTER);
		mIvTitle = new ImageView(getContext());
		mIvTitle.setScaleType(ScaleType.CENTER_INSIDE);
		addView(mIvTitle, getLayoutParamsWW());
		onNormal();
	}

	@Override
	public void onSelected()
	{
		onSelectedImageView(mIvTitle);
		super.onSelected();
	}

	@Override
	public void onNormal()
	{
		onNormalImageView(mIvTitle);
		super.onNormal();
	}

}

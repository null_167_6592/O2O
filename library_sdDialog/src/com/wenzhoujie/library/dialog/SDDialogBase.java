package com.wenzhoujie.library.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.wenzhoujie.library.R;
import com.wenzhoujie.library.SDLibrary;
import com.wenzhoujie.library.common.SDActivityManager;
import com.wenzhoujie.library.config.SDLibraryConfig;
import com.wenzhoujie.library.drawable.SDDrawable;
import com.wenzhoujie.library.drawable.SDDrawableManager;
import com.wenzhoujie.library.utils.SDViewBinder;
import com.wenzhoujie.library.utils.SDViewUtil;

public class SDDialogBase extends Dialog implements View.OnClickListener, OnDismissListener
{

	public static final int DEFAULT_PADDING = SDViewUtil.dp2px(25);

	private View mView;
	public SDDrawableManager mDrawableManager;
	private LinearLayout mLlAll;
	protected SDLibraryConfig mConfig = SDLibrary.getInstance().getmConfig();
	protected boolean mDismissAfterClick = true;

	// ------------------getter setter-----------------

	public boolean ismDismissAfterClick()
	{
		return mDismissAfterClick;
	}

	public SDDialogBase setmDismissAfterClick(boolean mDismissAfterClick)
	{
		this.mDismissAfterClick = mDismissAfterClick;
		return this;
	}

	public SDDialogBase(Context context)
	{
		super(context, R.style.dialogBase);
		baseInit();
	}

	public SDDialogBase()
	{
		this(SDActivityManager.getInstance().getLastActivity());
	}

	private void initDrawable()
	{
		mDrawableManager = new SDDrawableManager();
	}

	private void baseInit()
	{
		mLlAll = new LinearLayout(getContext());
		mLlAll.setBackgroundColor(Color.parseColor("#00000000"));
		mLlAll.setGravity(Gravity.CENTER);
		this.setOnDismissListener(this);
		initDrawable();
		setCanceledOnTouchOutside(false);
	}

	@Override
	public void onClick(View v)
	{

	}

	// ---------------------show gravity

	public SDDialogBase setGrativity(int gravity)
	{
		getWindow().setGravity(gravity);
		return this;
	}

	public void showTop()
	{
		showTop(true);
	}

	public void showTop(boolean anim)
	{
		setGrativity(Gravity.TOP);
		if (anim)
		{
			getWindow().setWindowAnimations(R.style.anim_top_top);
		}
		show();
	}

	public void showBottom()
	{
		showBottom(true);
	}

	public void showBottom(boolean anim)
	{
		setGrativity(Gravity.BOTTOM);
		if (anim)
		{
			getWindow().setWindowAnimations(R.style.anim_bottom_bottom);
		}
		show();
	}

	public void showCenter()
	{
		setGrativity(Gravity.CENTER);
		show();
	}

	// -----------------------padding

	public SDDialogBase paddingTopBottom(int topBottom)
	{
		mLlAll.setPadding(mLlAll.getPaddingLeft(), topBottom, mLlAll.getPaddingRight(), topBottom);
		return this;
	}

	public SDDialogBase paddingLeftRight(int leftRight)
	{
		mLlAll.setPadding(leftRight, mLlAll.getPaddingTop(), leftRight, mLlAll.getPaddingBottom());
		return this;
	}

	public SDDialogBase paddings(int paddings)
	{
		padding(paddings, paddings, paddings, paddings);
		return this;
	}

	public SDDialogBase padding(int left, int top, int right, int bottom)
	{
		mLlAll.setPadding(left, top, right, bottom);
		return this;
	}

	// -----------------------------layoutParams

	public WindowManager.LayoutParams getLayoutParams()
	{
		return getWindow().getAttributes();
	}

	public SDDialogBase setLayoutParams(WindowManager.LayoutParams params)
	{
		getWindow().setAttributes(params);
		return this;
	}

	// ----------------------dialogView

	public SDDialogBase setDialogView(View view)
	{
		return setDialogView(view, null, true);
	}

	public SDDialogBase setDialogView(View view, boolean needDefaultBackground)
	{
		return setDialogView(view, null, needDefaultBackground);
	}

	public SDDialogBase setDialogView(View view, ViewGroup.LayoutParams params, boolean needDefaultBackground)
	{
		mView = view;
		wrapperView(mView);
		setDefaultBackgroundEnable(needDefaultBackground);
		if (params == null)
		{
			params = new ViewGroup.LayoutParams(SDViewUtil.getScreenWidth(), LayoutParams.WRAP_CONTENT);
		}
		paddings(DEFAULT_PADDING);
		super.setContentView(mLlAll, params);
		return this;
	}

	public SDDialogBase setDefaultBackgroundEnable(boolean enable)
	{
		if (enable)
		{
			SDViewBinder.setBackgroundDrawable(mView, new SDDrawable().cornerAll(mConfig.getmCornerRadius()));
		} else
		{
			SDViewBinder.setBackgroundDrawable(mView, null);
		}
		return this;
	}

	private void wrapperView(View view)
	{
		mLlAll.removeAllViews();
		mLlAll.addView(view, SDViewUtil.getLayoutParamsLinearLayoutMM());
	}

	public View getDialogView()
	{
		return mView;
	}

	// ------------------------setContentView

	@Override
	public void setContentView(int layoutResID)
	{
		this.setContentView(SDViewUtil.inflate(layoutResID, null));
	}

	@Override
	public void setContentView(View view)
	{
		this.setContentView(view, null);
	}

	@Override
	public void setContentView(View view, LayoutParams params)
	{
		setDialogView(view, params, false);
	}

	@Override
	public void onDismiss(DialogInterface dialog)
	{
		// TODO Auto-generated method stub

	}

	// ------------------------------background

	/**
	 * 边框：top，right 圆角：bottomLeft
	 * 
	 * @return
	 */
	public Drawable getBackgroundBottomLeft()
	{
		SDDrawable drawableCancel = new SDDrawable();
		drawableCancel.strokeColor(mConfig.getmStrokeColor()).strokeWidth(0, mConfig.getmStrokeWidth(), mConfig.getmStrokeWidth(), 0)
				.cornerBottomLeft(mConfig.getmCornerRadius());

		SDDrawable drawableCancelPressed = new SDDrawable();
		drawableCancelPressed.strokeColor(mConfig.getmStrokeColor()).color(mConfig.getmGrayPressColor())
				.strokeWidth(0, mConfig.getmStrokeWidth(), mConfig.getmStrokeWidth(), 0).cornerBottomLeft(mConfig.getmCornerRadius());

		return SDDrawable.getStateListDrawable(drawableCancel, null, null, drawableCancelPressed);
	}

	/**
	 * 边框：top 圆角：bottomRight
	 * 
	 * @return
	 */
	public Drawable getBackgroundBottomRight()
	{
		SDDrawable drawableConfirm = new SDDrawable();
		drawableConfirm.strokeColor(mConfig.getmStrokeColor()).strokeWidth(0, mConfig.getmStrokeWidth(), 0, 0)
				.cornerBottomRight(mConfig.getmCornerRadius());

		SDDrawable drawableConfirmPressed = new SDDrawable();
		drawableConfirmPressed.strokeColor(mConfig.getmStrokeColor()).color(mConfig.getmGrayPressColor())
				.strokeWidth(0, mConfig.getmStrokeWidth(), 0, 0).cornerBottomRight(mConfig.getmCornerRadius());

		return SDDrawable.getStateListDrawable(drawableConfirm, null, null, drawableConfirmPressed);
	}

	/**
	 * 边框：top 圆角：bottomLeft，bottomRight
	 * 
	 * @return
	 */
	public Drawable getBackgroundBottomSingle()
	{
		SDDrawable drawableConfirm = new SDDrawable();
		drawableConfirm.strokeColor(mConfig.getmStrokeColor()).strokeWidth(0, mConfig.getmStrokeWidth(), 0, 0)
				.corner(0, 0, mConfig.getmCornerRadius(), mConfig.getmCornerRadius());

		SDDrawable drawableConfirmPressed = new SDDrawable();
		drawableConfirmPressed.strokeColor(mConfig.getmStrokeColor()).color(mConfig.getmGrayPressColor())
				.strokeWidth(0, mConfig.getmStrokeWidth(), 0, 0).corner(0, 0, mConfig.getmCornerRadius(), mConfig.getmCornerRadius());
		return SDDrawable.getStateListDrawable(drawableConfirm, null, null, drawableConfirmPressed);
	}
}
